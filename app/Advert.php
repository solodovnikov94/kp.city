<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Relations\AdvertRelations;
use Illuminate\Database\Eloquent\SoftDeletes;

class Advert extends Model
{
	use AdvertRelations, SoftDeletes;

	protected $dates = ['created_at', 'updated_at', 'deleted_at'];

	public function getUrl()
	{
		//&& $this->category->parent
		if($this->id ) {
			return url('adverts/'.$this->id.'-'.$this->slug);
		}
		return '';
	}
}
