<?php

namespace App;

use App\Traits\Crops;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdvertCategory extends Model
{
	use SoftDeletes, Crops;

	protected $table = 'advert_categories';
	protected $crops_key = 'advert_categories';

	public function adverts()
	{
		return $this->hasMany(Advert::class, 'category_id', 'id');
	}

	public function scopeActive($query)
	{
		return $query->where('active', true);
	}

	public function getUrl()
	{
		if(!$this->id) {
			return null;
		}

		return url('adverts/' . $this->slug);
	}

}
