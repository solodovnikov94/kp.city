<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;

class Article extends ParentArticle
{
	protected $attributes = ['type' => 'article'];

	protected static function boot()
	{
		parent::boot();

		static::addGlobalScope('article', function (Builder $builder) {
			$builder->where('type', '=', 'article');
		});
	}

	public function category()
	{
		return $this->belongsTo(ArticleCategory::class, 'category_id', 'id');
	}

	public function getUrl()
	{
		if ($this->id) {
			return url('articles/' . $this->category->slug . '/' . $this->id . '-' . $this->slug);
		}

		return null;
	}
}
