<?php

namespace App;

use App\Traits\Crops;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ArticleCategory extends Model
{
	use SoftDeletes, Crops;

	protected $table = 'article_categories';
	protected $crops_key = 'article_categories';
	protected $attributes = ['type' => 'article'];

	protected static function boot()
	{
		parent::boot();

		static::addGlobalScope('article', function (Builder $builder) {
			$builder->where('type', '=', 'article');
		});
	}

	public function articles()
	{
		return $this->hasMany(Article::class, 'category_id', 'id');
	}

	public function getUrl()
	{
		return url('articles/' . $this->slug);
	}

	public function scopeActive($query)
	{
		return $query->where('active', true);
	}
}
