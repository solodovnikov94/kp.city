<?php

namespace App;

use App\Traits\Crops;
use Illuminate\Database\Eloquent\Model;

class ArticleImage extends Model
{
	use Crops;

	protected $table = 'article_images';
	protected $crops_key = 'article_images';
	public $timestamps = false;
}
