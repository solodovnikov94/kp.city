<?php

namespace App;

use App\Traits\Relations\CommentRelations;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model
{
	use CommentRelations, SoftDeletes;

	protected $fillable = ['user_id', 'parent_id', 'active', 'body'];
	protected $dates = ['created_at', 'updated_at', 'deleted_at'];

	public function save(array $options = [])
	{
		if (!$this->exists) {
			\App\Notifications\Telegram::comment($this);
		}
		
		parent::save($options);
	}

	public function scopeActive($query)
	{
		return $query->where('active', true);
	}

	public function getUrl()
	{
		$model = app($this->commentable_type)->where('id', $this->commentable_id)->first();
		return $model->getUrl() . '#comment' . $this->id;
	}

	public function getDate()
	{
		return \Carbon\Carbon::parse($this->created_at)->diffForHumans();
	}

	public function getComments($model)
	{
		//->with('likes')
		$model->comments_list = $model->comments()->where('parent_id', null)->active()->get();

		if(count($model->comments_list)) {
			$maxLikes = $model->comments()->max('likes');
			$commentWithMaxLikes = $model->comments()->where('likes', $maxLikes)->take(2)->get();

			if (count($commentWithMaxLikes) == 1) {
				$model->max_likes = $commentWithMaxLikes->first()->likes;
			}
		}
		
		return $model;
	}

	public function getRating($id, $model)
	{
		return $this->where([
			['commentable_type', '=', $model],
			['parent_id', '=', null],
			['commentable_id', '=', $id],
			['active', '=', 1]
		])->avg('rating');
	}

	public function ratingComments($id, $model)
	{
		return $this->where([
				['commentable_type', '=', $model],
				['parent_id', '=', null],
				['commentable_id', '=', $id],
				['active', '=', 1]
			])
			->with('user')
			->orderBy('id', 'desc')
			->get();
	}
}
