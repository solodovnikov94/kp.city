<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CheckContent extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:content';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		try {
			$curl_handle=curl_init();
			curl_setopt($curl_handle, CURLOPT_URL,'https://vdalo.info/wp-json/wp/v2/posts');
			curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
			curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Your application name');
			$json = curl_exec($curl_handle);
			curl_close($curl_handle);

			$collection = collect(json_decode($json, true));
			if (count($collection)) {
				$lastPost = $collection->first();
				$site = $lastPost['guid']['rendered'];
				$title = $lastPost['title']['rendered'];
				$link = $lastPost['link'];
				$row = \DB::table('news_competitors')->where([
					['site', '=', 'vdalo.info'],
					['link', '=', $link]
				])->first();

				if($row == null) {
					\DB::table('news_competitors')->where('id', 1)->update(['link' => $link, 'title' => $title]);
					\App\Notifications\Telegram::newCompetitorContent($site, $title, $link);
				}
			}
		} catch (Exception $e) {
			echo 'Выброшено исключение: ',  $e->getMessage(), "\n";
		}

		return false;
	}
}
