<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ExchangeRates extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'exchange:rates';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$curl_handle = curl_init();
		curl_setopt($curl_handle, CURLOPT_URL, 'https://api.privatbank.ua/p24api/pubinfo?exchange&json&coursid=11');
		curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
		curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Your application name');
		$json = json_decode(curl_exec($curl_handle));
		curl_close($curl_handle);
		$this->saveCurrency($json[0]);
		$this->saveCurrency($json[1]);
	}

	public function saveCurrency($json)
	{
		$currency = new \App\Currency;
		$currency->base_currency = $json->ccy;
		$currency->currency = $json->ccy;
		$currency->sale_rate_nb = $json->sale;
		$currency->purchase_rate_nb = $json->buy;
		$currency->sale_rate = $json->sale;
		$currency->purchase_rate = $json->buy;
		$currency->save();
	}
}
