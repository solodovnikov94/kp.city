<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class FacebookPost extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'facebook:post';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo 'record successfully posted';
        //https://developers.facebook.com/tools/explorer
			//https://developers.facebook.com/apps/249421485733779/app-review/current-request/
    }
}
