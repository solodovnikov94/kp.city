<?php

namespace App\Console\Commands;

use App\Article;
use App\ArticleCategory;
use App\News;
use App\NewsCategory;
use App\Organization;
use App\OrganizationCategory;
use App\Poll;
use Carbon\Carbon;
use Illuminate\Console\Command;

class SiteMap extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'sitemap:create';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{

		$articles = Article::where('published_at', '<', Carbon::now())
            ->where('active', 1)
			->orderBy('published_at', 'desc')->get();

		$news = News::where('published_at', '<', Carbon::now())
			->where('active', 1)
			->orderBy('published_at', 'desc')->get();

		$articleCategories = ArticleCategory::where('active', 1)->get();

		$newsCategories = NewsCategory::where('active', 1)->get();

		$organizationCategory = OrganizationCategory::where([
			['active', '=', 1],
			['parent_id', '=', 0]
		])->get();

		$organizations = Organization::where('active', 1)->get();

		$polls = Poll::separate()->active()->published()->get();

		$siteUrl = env('APP_URL');
		$baseStandardSiteMap = '<?xml version="1.0" encoding="UTF-8"?><sitemapindex xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/siteindex.xsd"></sitemapindex>';
		$base = '<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"></urlset> ';

		//articles
		$xmlBase = new \SimpleXMLElement($base);
		$xmlBaseStandardSiteMap = new \SimpleXMLElement($baseStandardSiteMap);

		$dom = new \DOMDocument('1.0');
		$dom->preserveWhiteSpace = false;
		$dom->formatOutput = true;

		$domStandardSiteMap = $dom;

		foreach ($articles as $article) {
			$row = $xmlBase->addChild("url");
			$row->addChild('loc', url($article->getUrl()));
			$row->addChild('lastmod', Carbon::parse($article->published_at)->toAtomString());
		}

		Header('Content-type: text/xml');

		$dom->loadXML($xmlBase->asXML());
		$dom->save(public_path()."/sitemap/sitemap_articles.xml");


		$data = implode("", file(public_path()."/sitemap/sitemap_articles.xml"));
		$gzData = gzencode($data, 9);
		$fp = fopen(public_path()."/sitemap/sitemap_articles.xml.gz", "w");
		fwrite($fp, $gzData);
		fclose($fp);

		$rowStandardSiteMap = $xmlBaseStandardSiteMap->addChild("sitemap");
		$rowStandardSiteMap->addChild('loc', $siteUrl.'/sitemap/sitemap_articles.xml.gz');
		$rowStandardSiteMap->addChild('lastmod', date('c'));
		//end articles

		//news
		$xmlBase = new \SimpleXMLElement($base);

		$dom = new \DOMDocument('1.0');
		$dom->preserveWhiteSpace = false;
		$dom->formatOutput = true;

		$domStandardSiteMap = $dom;

		foreach ($news as $item) {
			$row = $xmlBase->addChild("url");
			$row->addChild('loc', url($item->getUrl()));
			$row->addChild('lastmod', Carbon::parse($item->published_at)->toAtomString());
		}

		Header('Content-type: text/xml');

		$dom->loadXML($xmlBase->asXML());
		$dom->save(public_path()."/sitemap/sitemap_news.xml");


		$data = implode("", file(public_path()."/sitemap/sitemap_news.xml"));
		$gzData = gzencode($data, 9);
		$fp = fopen(public_path()."/sitemap/sitemap_news.xml.gz", "w");
		fwrite($fp, $gzData);
		fclose($fp);

		$rowStandardSiteMap = $xmlBaseStandardSiteMap->addChild("sitemap");
		$rowStandardSiteMap->addChild('loc', $siteUrl.'/sitemap/sitemap_news.xml.gz');
		$rowStandardSiteMap->addChild('lastmod', date('c'));
		//end news

		//articles category
		$xmlBase = new \SimpleXMLElement($base);

		$dom = new \DOMDocument('1.0');
		$dom->preserveWhiteSpace = false;
		$dom->formatOutput = true;

		foreach ($articleCategories as $category) {
			$row = $xmlBase->addChild("url");
			$row->addChild('loc', url('/articles/'.$category->slug));
		}

		Header('Content-type: text/xml');

		$dom->loadXML($xmlBase->asXML());
		$dom->save(public_path()."/sitemap/sitemap_article_category.xml");

		$data = implode("", file(public_path()."/sitemap/sitemap_article_category.xml"));
		$gzData = gzencode($data, 9);
		$fp = fopen(public_path()."/sitemap/sitemap_article_category.xml.gz", "w");
		fwrite($fp, $gzData);
		fclose($fp);

		$rowStandardSiteMap = $xmlBaseStandardSiteMap->addChild("sitemap");
		$rowStandardSiteMap->addChild('loc', $siteUrl.'/sitemap/sitemap_article_category.xml.gz');
		$rowStandardSiteMap->addChild('lastmod', date('c'));
		//end articles category

		//news category
		$xmlBase = new \SimpleXMLElement($base);

		$dom = new \DOMDocument('1.0');
		$dom->preserveWhiteSpace = false;
		$dom->formatOutput = true;

		foreach ($newsCategories as $category) {
			$row = $xmlBase->addChild("url");
			$row->addChild('loc', url('/news/'.$category->slug));
		}

		Header('Content-type: text/xml');

		$dom->loadXML($xmlBase->asXML());
		$dom->save(public_path()."/sitemap/sitemap_news_category.xml");

		$data = implode("", file(public_path()."/sitemap/sitemap_news_category.xml"));
		$gzData = gzencode($data, 9);
		$fp = fopen(public_path()."/sitemap/sitemap_news_category.xml.gz", "w");
		fwrite($fp, $gzData);
		fclose($fp);

		$rowStandardSiteMap = $xmlBaseStandardSiteMap->addChild("sitemap");
		$rowStandardSiteMap->addChild('loc', $siteUrl.'/sitemap/sitemap_news_category.xml.gz');
		$rowStandardSiteMap->addChild('lastmod', date('c'));
		//news articles category

		//organization category
		$xmlBase = new \SimpleXMLElement($base);

		$dom = new \DOMDocument('1.0');
		$dom->preserveWhiteSpace = false;
		$dom->formatOutput = true;

		foreach ($organizationCategory as $category) {
			$row = $xmlBase->addChild("url");
			$row->addChild('loc', url('organizations/' . $category->slug));
			if($category->children) {
				foreach ($category->children()->where('active', 1)->get() as $subcategory) {
					$row = $xmlBase->addChild("url");
					$row->addChild('loc', url('organizations/' . $category->slug . '/' . $subcategory->slug));
				}
			}
		}

		Header('Content-type: text/xml');

		$dom->loadXML($xmlBase->asXML());
		$dom->save(public_path()."/sitemap/sitemap_organization_category.xml");

		$data = implode("", file(public_path()."/sitemap/sitemap_organization_category.xml"));
		$gzData = gzencode($data, 9);
		$fp = fopen(public_path()."/sitemap/sitemap_organization_category.xml.gz", "w");
		fwrite($fp, $gzData);
		fclose($fp);

		$rowStandardSiteMap = $xmlBaseStandardSiteMap->addChild("sitemap");
		$rowStandardSiteMap->addChild('loc', $siteUrl.'/sitemap/sitemap_organization_category.xml.gz');
		$rowStandardSiteMap->addChild('lastmod', date('c'));
		//end organization category

		//organization
		$xmlBase = new \SimpleXMLElement($base);

		$dom = new \DOMDocument('1.0');
		$dom->preserveWhiteSpace = false;
		$dom->formatOutput = true;

		$domStandardSiteMap = $dom;

		foreach ($organizations as $organization) {
			$row = $xmlBase->addChild("url");
			$row->addChild('loc', $organization->getUrl());
		}

		Header('Content-type: text/xml');

		$dom->loadXML($xmlBase->asXML());
		$dom->save(public_path()."/sitemap/sitemap_organizations.xml");


		$data = implode("", file(public_path()."/sitemap/sitemap_organizations.xml"));
		$gzData = gzencode($data, 9);
		$fp = fopen(public_path()."/sitemap/sitemap_organizations.xml.gz", "w");
		fwrite($fp, $gzData);
		fclose($fp);

		$rowStandardSiteMap = $xmlBaseStandardSiteMap->addChild("sitemap");
		$rowStandardSiteMap->addChild('loc', $siteUrl.'/sitemap/sitemap_organizations.xml.gz');
		$rowStandardSiteMap->addChild('lastmod', date('c'));
		//end organization

		//polls
		$xmlBase = new \SimpleXMLElement($base);

		$dom = new \DOMDocument('1.0');
		$dom->preserveWhiteSpace = false;
		$dom->formatOutput = true;

		$domStandardSiteMap = $dom;

		foreach ($polls as $poll) {
			$row = $xmlBase->addChild("url");
			$row->addChild('loc', $poll->getUrl());
		}

		Header('Content-type: text/xml');

		$dom->loadXML($xmlBase->asXML());
		$dom->save(public_path()."/sitemap/sitemap_polls.xml");


		$data = implode("", file(public_path()."/sitemap/sitemap_polls.xml"));
		$gzData = gzencode($data, 9);
		$fp = fopen(public_path()."/sitemap/sitemap_polls.xml.gz", "w");
		fwrite($fp, $gzData);
		fclose($fp);

		$rowStandardSiteMap = $xmlBaseStandardSiteMap->addChild("sitemap");
		$rowStandardSiteMap->addChild('loc', $siteUrl.'/sitemap/sitemap_polls.xml.gz');
		$rowStandardSiteMap->addChild('lastmod', date('c'));
		//end polls


		$domStandardSiteMap->loadXML($xmlBaseStandardSiteMap->asXML());
		$domStandardSiteMap->save(public_path()."/sitemap.xml");
	}
}
