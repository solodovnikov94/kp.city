<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class TelegramNews extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'telegram:message';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'mailing news to telegram';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
			$lastShortNews = \App\ShortNews::published()->active()
				->orderBy('published_at', 'desc')->take(10)->whereDate('published_at', \Carbon\Carbon::today())
				->get();

			$lastNews = \App\News::newest()->published()
				->active()->take(10)
				->whereDate('published_at', \Carbon\Carbon::today())
				->get();

			$merged = $lastNews->merge($lastShortNews);

			if(count($merged) >= 1) {
				$todayNews = $merged->sortBy('published_at')->take(15);
				$string = '';

				foreach ($todayNews as $news) {
					$string .= '<strong>' . $news->published_at->format('H:i') . '</strong> — ';
					if (get_class($news) == 'App\ShortNews') {
						$text = str_ireplace('<p>','', $news->body);
						$text = str_ireplace('</p>','', $text);
						$string .= $text . PHP_EOL . PHP_EOL;
					} else {
						$string .= '<a href="' . $news->getUrl(). '">' . $news->title . '</a>' . PHP_EOL . PHP_EOL;
					}
				}

				$telegram = new \Telegram\Bot\Api(env('TELEGRAM_BOT_TOKEN'));
				$telegram->setAsyncRequest(true)
					->sendMessage([
						'chat_id' => env('TELEGRAM_CONTENT_CHAT_ID'),
						'text' => '<strong>Останні новини на порталі KP.CITY за '.now()->format('d.m.Y').':</strong>' . PHP_EOL . PHP_EOL . $string,
						'parse_mode' => 'html'
					]);
			}
    }
}
