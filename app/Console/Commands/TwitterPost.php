<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class TwitterPost extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'twitter:post';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Twitter Post';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
			$news = \App\News::where('published_at', '>=', now()->subHour())->newest()->active()->published()->first();
			if(!is_null($news)) {
				$news->notify(new \App\Notifications\TwitterNews());
			}

			$news = \App\ShortNews::where('published_at', '>=', now()->subHour())->orderBy('published_at', 'desc')->active()->published()->first();
			if(!is_null($news)) {
				$news->notify(new \App\Notifications\TwitterShortNews());
			}
    }
}
