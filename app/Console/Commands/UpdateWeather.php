<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class UpdateWeather extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'weather:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
			$curl_handle = curl_init();
			curl_setopt($curl_handle, CURLOPT_URL, 'https://api.apixu.com/v1/current.json?key=ce638b08165b42998a094944190305&q=kamenets%20podolski');
			curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 120);
			curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($curl_handle, CURLOPT_USERAGENT, 'kp.city');
			$json = json_decode(curl_exec($curl_handle));
			curl_close($curl_handle);
			$weather = new \App\Weather;
			if(isset($json->current) && $json->current) {
				$weather->temp_c = $json->current->temp_c;
				$weather->last_updated = $json->current->last_updated.':00';
				$weather->save();
			}
    }
}
