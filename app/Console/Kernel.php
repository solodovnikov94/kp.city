<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
			Commands\SiteMap::class,
			Commands\CheckContent::class,
			Commands\TelegramNews::class,
			Commands\TwitterPost::class,
//			Commands\FacebookPost::class,
			Commands\ExchangeRates::class,
			Commands\UpdateWeather::class,
		];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
			$schedule->command('sitemap:create')->dailyAt('04:00');
			$schedule->command('check:content')->everyMinute();
			$schedule->command('telegram:message')->dailyAt('18:20');
			$schedule->command('twitter:post')->hourly();
//			$schedule->command('facebook:post')->hourly();
			$schedule->command('exchange:rates')->hourly();
			$schedule->command('weather:update')->hourly();
		}

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
