<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
	public function save(array $options = [])
	{
		if (!$this->exists) {
			\App\Notifications\Telegram::contact($this);
		}

		parent::save($options);
	}
}
