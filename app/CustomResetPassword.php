<?php

namespace App;

use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Notifications\Messages\MailMessage;

class CustomResetPassword extends ResetPassword
{
	public function toMail($notifiable)
	{

		$resetLink = url(config('app.url') . route('password.reset', $this->token, false));

		return (new MailMessage)
			->subject('Відновлення паролю')
			->markdown('emails.reset-password', compact('resetLink'));
	}
}
