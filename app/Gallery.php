<?php

namespace App;

use App\Traits\Crops;
use App\Traits\Relations\GalleryRelations;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Gallery extends Model
{
	use GalleryRelations, SoftDeletes, Crops;

	protected $table = 'galleries';
	protected $crops_key = 'galleries';
	protected $dates = ['created_at', 'updated_at', 'deleted_at'];
	
}
