<?php

namespace App;

use App\Traits\Crops;
use App\Traits\Relations\GalleryPhotoRelations;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GalleryPhoto extends Model
{
	use GalleryPhotoRelations, SoftDeletes, Crops;

	protected $table = 'gallery_photos';
	protected $crops_key = 'galleries';
	protected $dates = ['created_at', 'updated_at', 'deleted_at'];
}
