<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;
use DB;

class ApiController extends Controller
{
	public function counter(Request $request)
	{
		$model = $request->model;
		$column = $request->column;
		$id = $request->id;

		$this->validate($request, [
			'model' => 'required|string',
			'column' => 'required|string',
			'id' => 'required|numeric',
		]);

		$table = app('App\\'.$model)->getTable();

		if (Schema::hasColumn($table, $column)) {
			DB::table($table)->where('id', $id)->increment($column);
		}
	}
}
