<?php

namespace App\Http\Controllers;

use App\Article;
use App\ArticleCategory;
use App\News;
use App\NewsCategory;
use App\Traits\ShortCode;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ArticlesController extends Controller
{
	use ShortCode;

	public function showNewsCategoryPage(Request $request, $categorySlug = null)
	{
		$news = News::query();

		if (isset($categorySlug)) {
			$news = $news->whereHas('category', function ($query) use ($categorySlug) {
				$query->where('slug', '=', $categorySlug);
			});

			$category = NewsCategory::where('slug', '=', $categorySlug)->active()->firstOrFail();
		}

		$newsModel = new News;
		$bestWeek = $newsModel->bestThanWeek();
		$bestMonth = $newsModel->bestThanMonth();

		$onPage = 8;
		$news = $news->with('category');
		$news = $news->active()->published()->newest()->paginate($onPage);
		$onPage = 'Завантажити ще ' . on_next_page($onPage, $news);
		$showMore = $news->lastPage() !== 1;

		if ($request->ajax()) {
			$returnHTML = view('articles.articles-list')->with([
				'articles' => $news,
				'type'     => 'news',
				'socialBannerPosition' => 5
			])->render();

			return response()->json([
				'html'       => $returnHTML,
				'lastPage'   => $news->lastPage(),
				'onNextPage' => $onPage,
			]);
		}

		$data = [
			'categories' => NewsCategory::active()->get(),
			'articles'   => $news,
			'type'       => 'news',
			'showMore'   => $showMore,
			'onNextPage' => $onPage,
			'best_week'  => $bestWeek,
			'best_month' => $bestMonth,
		];

		if (isset($category)) {
			$data['category'] = $category;
		}

		return view('articles.articles-categories')->with($data);
	}

	public function showArticleCategoryPage(Request $request, $categorySlug = null)
	{
		$articles = Article::query();

		if (isset($categorySlug)) {
			$articles = $articles->whereHas('category', function ($query) use ($categorySlug) {
				$query->where('slug', '=', $categorySlug);
			});

			$category = ArticleCategory::where('slug', '=', $categorySlug)->active()->firstOrFail();
		}

		$article = new Article;
		$bestWeek = $article->bestThanWeek();
		$bestMonth = $article->bestThanMonth();

		$onPage = 8;
		$articles = $articles->with('category')->active()->published()->newest()->paginate($onPage);
		$onPage = 'Завантажити ще ' . on_next_page($onPage, $articles);
		$showMore = $articles->lastPage() !== 1;

		if ($request->ajax()) {
			$returnHTML = view('articles.articles-list')->with([
				'articles' => $articles,
				'type'     => 'articles',
				'socialBannerPosition' => 5
			])->render();

			return response()->json([
				'html'       => $returnHTML,
				'lastPage'   => $articles->lastPage(),
				'onNextPage' => $onPage,
			]);
		}

		$data = [
			'categories' => ArticleCategory::active()->get(),
			'articles'   => $articles,
			'type'       => 'articles',
			'showMore'   => $showMore,
			'onNextPage' => $onPage,
			'best_week'  => $bestWeek,
			'best_month' => $bestMonth,
		];

		if (isset($category)) {
			$data['category'] = $category;
		}

		return view('articles.articles-categories')->with($data);
	}

	public function showNewsPage($categorySlug, $articleId, $articleSlug)
	{
		$news = News::whereHas('category', function ($query) use ($categorySlug) {
				$query->where('slug', '=', $categorySlug);
			})
		->where([
			['id', '=', $articleId],
			['slug', '=', $articleSlug]
		])
		->with('comments')->withCount(['comments' => function($query) {
			$query->where('active', true);
		}])
		->firstOrFail();

		$comments = new \App\Comment;
		$news = $comments->getComments($news);

		$recent = News::where('id', '!=', $news->id)
			->with('category')
			->active()
			->published()
			->orderBy('published_at', 'desc')
			->take(6)
			->get();

		$news->body = advertising_in_content($news->body, 'includes.banners.content-advert');

		$news->body = $this->renderShortCodes($news->body);

		return view('articles.articles-single')->with([
			'article' => $news,
			'type'    => 'news',
			'recent'  => $recent,
			'model'   => 'News'
		]);
	}

	public function showArticlePage($categorySlug, $articleId, $articleSlug)
	{
		$article = Article::whereHas('category', function ($query) use ($categorySlug) {
				$query->where('slug', '=', $categorySlug);
			})
		->where([
			['id', '=', $articleId],
			['slug', '=', $articleSlug]
		])->with('comments')->withCount(['comments' => function($query) {
			$query->where('active', true);
		}])->firstOrFail();

		$comments = new \App\Comment;
		$article = $comments->getComments($article);

		$recent = Article::where('id', '!=', $article->id)
			->with('category')
			->active()
			->published()
			->orderBy('published_at', 'desc')
			->take(6)
			->get();

		$article->body = advertising_in_content($article->body, 'includes.banners.content-advert');
		$article->body = $this->renderShortCodes($article->body);

		return view('articles.articles-single')->with([
			'article' => $article,
			'type'    => 'articles',
			'recent'  => $recent,
			'model'   => 'Article'
		]);
	}
}
