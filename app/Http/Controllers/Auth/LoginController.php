<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use App\UserSocial;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Hash;
use Mockery\CountValidator\Exception;

class LoginController extends Controller
{
	/*
	|--------------------------------------------------------------------------
	| Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles authenticating users for the application and
	| redirecting them to your home screen. The controller uses a trait
	| to conveniently provide its functionality to your applications.
	|
	*/
	use AuthenticatesUsers;
	/**
	 * Where to redirect users after login.
	 *
	 * @var string
	 */
	protected $redirectTo = '/';

	protected $socialDrivers = [
		'facebook',
		'google',
	];

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest')->except('logout');
	}

	public function login(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'email' => 'required|email|string',
			'password' => 'required|string',
		]);

		if ($validator->fails()) {
			return response()->json([
				'errors' => $validator->errors()
			]);
		}

		$data = [
			'email' => $request->email,
			'password' => $request->password,
			'active' => 1
		];

		if (Auth::attempt($data, ($request->remember_me == 'on') ? true : false)) {
			return response()->json([
				'redirect_url' => url()->previous() ?? env('APP_URL') . '/'
			]);
		}

		return response()->json([
			'errors' => ['email' => ['Ці облікові дані не збігаються з нашими записами.']]
		]);
	}

	public function redirectToProvider($driver)
	{
		if (!in_array($driver, $this->socialDrivers)) {
			return abort(404)->withErrors([
				'errors' => "Реєстрація через {$driver} на даний момент не підтримується"
			]);
		}

		try {
			return Socialite::driver($driver)->redirect();
		} catch (Exception $e) {
			return abort(404)->withErrors([
				'errors' => $e->getMessage() ?: 'Сталася помилка, спробуйте пізніше'
			]);
		}

	}

	public function handleProviderCallback($driver)
	{
		if (strripos(url()->previous(), env('APP_URL')) !== false) {
			session()->put('redirect-after-auth', url()->previous());
		}

		try {
			$providerUser = Socialite::driver($driver)->user();
		} catch (Exception $e) {
			return abort(404)->withErrors([
				'errors' => $e->getMessage() ?: 'Сталася помилка, спробуйте пізніше'
			]);
		}

		$user = $this->firstOrCreateUser($providerUser, $driver);

		if (is_bool($user) && $user === false) {
			return redirect('account-confirm');
		}

		$this->guard()->login($user, false);

		return redirect(session()->get('redirect-after-auth') ?: '/')->with('success-auth', 'Ви успішно авторизувалися');
	}

	private function firstOrCreateUser($providerUser, $driver)
	{
		if (is_null($providerUser->getEmail())) {
			$providerUser->email = $driver . $providerUser->getId() . '@social.com';
		}

		$user = User::where('email', $providerUser->getEmail())->first();
		$folder = 'users/';

		if (is_null($user)) {
			$avatar = $providerUser->getAvatar();
			$fullPath = $folder . $providerUser->id . '_min.jpg';
			Image::make($avatar)->save(Storage::disk('public')->path($fullPath));

			$user = new User;
			$user->email = $providerUser->getEmail();
			$user->name = $providerUser->getName();
			$user->password = Hash::make(str_random(8));
			$user->active = 1;
			$user->image = $providerUser->id;
			$user->save();

			$socialUser = new UserSocial;
			$socialUser->user_id = $user->id;
			$socialUser->driver_user_id = $providerUser->getId();
			$socialUser->driver = $driver;
			$socialUser->save();
		} else {
			$socialUser = UserSocial::where([
				['driver', '=', $driver],
				['user_id', '=', $user->id]
			])->first();

			if (is_null($socialUser)) {
				session()->put('account-confirm', [
					'user_id' => $user->id,
					'driver_user_id' => $providerUser->getId(),
					'driver' => $driver,
					'url' => url()->previous()
				]);
				return false;
			}
		}
		return $user;
	}

	public function accountConfirm()
	{
		$data = session()->get('account-confirm');
		$user = User::where('id', $data['user_id'])->firstOrFail();

		return view('auth.account-confirm', compact('user'));
	}

	public function accountConfirmSuccess()
	{
		$data = session()->get('account-confirm');
		session()->put('account-confirm', false);

		$user = User::where('id', $data['user_id'])->firstOrFail();
		$socialUser = UserSocial::where([
			['user_id', '=', $user->id],
			['driver', '=', $data['driver']]
		])->first();

		if (is_null($socialUser)) {
			$socialUser = new UserSocial;
			$socialUser->user_id = $user->id;
			$socialUser->driver_user_id = $data['driver_user_id'];
			$socialUser->driver = $data['driver'];
			$socialUser->save();
		}

		$this->guard()->login($user, false);

		if(isset($data['url'])) {
			return redirect($data['url'])->with('success-auth', 'Ви успішно підтвердили ваш аккаунт та авторизувалися');
		}

		return redirect('/')->with('success-auth', 'Ви успішно підтвердили ваш аккаунт та авторизувалися');
	}

	public function accountConfirmDanger()
	{
		session()->put('account-confirm', false);

		$data = [
			'confirmDanger' => 'Вибачте, але дана email адреса вже зайнята. Спробуйте, будь-ласка, іншу email адресу'
		];
		
		return view('auth.account-confirm')->with($data);
	}

	public function loggedOut(Request $request)
	{
		return redirect()->intended(url()->previous())->with('success-logout', 'Ви успішно вилогувалися');
	}
}
