<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
	/*
	|--------------------------------------------------------------------------
	| Register Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users as well as their
	| validation and creation. By default this controller uses a trait to
	| provide this functionality without requiring any additional code.
	|
	*/

	use RegistersUsers;

	/**
	 * Where to redirect users after registration.
	 *
	 * @var string
	 */
	protected $redirectTo = '/';

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	protected function validator(array $data)
	{
		return Validator::make($data, [
			'name' => 'required|string|max:255',
			'email' => 'required|string|email|max:255|unique:users',
			'password' => 'required|string|min:6|confirmed',
		]);
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array $data
	 * @return \App\User
	 */
	protected function create(array $data)
	{
		$user = User::create([
			'name' => $data['name'],
			'email' => $data['email'],
			'password' => Hash::make($data['password']),
			'verify_token' => Str::random(40)
		]);

		$this->sendVerifyToken($user);

		return $user;
	}

	/**
	 * Handle a registration request for the application.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function register(Request $request)
	{
		session()->put('redirect-after-register', url()->previous());

		$validator = $this->validator($request->all());

		if ($validator->fails()) {
			return response()->json([
				'errors' => $validator->errors()
			]);
		}

		event(new Registered($user = $this->create($request->all())));

		return response()->json([
			'alert' => view('includes.alert')->with([
				'type' => 'success',
				'class' => '',
				'messages' => ['Ваш аккаунт створенний! На вашу пошту надіслано інструкцію з активації аккаунта']
			])->render()
		]);
	}

	public function sendVerifyToken($user)
	{
		$emailJob = (new \App\Jobs\SendEmailJob($user))->delay(1);
		dispatch($emailJob);
	}

	public function verifyEmail($email, $verify_token)
	{
		$user = User::where([
			['email', '=', $email],
			['verify_token', '=', $verify_token]
		])->firstOrFail();

		$user->active = 1;
		$user->verify_token = null;
		$user->save();

		$this->guard()->login($user);

		if(session()->get('redirect-after-register')) {
			return redirect(session()->get('redirect-after-register'))->with('success-auth', 'Ви успішно зареєстувалися і авторизувалися');

		}
		return redirect('/')->with('success-auth', 'Ви успішно зареєстувалися і авторизувалися');
	}
}
