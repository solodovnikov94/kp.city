<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BannersController extends Controller
{
    public function closeBanner(Request $request)
	{
		$bannerId = $request->banner_id;
		$hiddenBanners = session()->get('hidden-banners');
		if(is_array($hiddenBanners)) {
			array_push($hiddenBanners, $bannerId);
		} else {
			$hiddenBanners = [$bannerId];
		}

		session()->put('hidden-banners', $hiddenBanners);
	}

	public function clickCounter(Request $request)
	{
		return 'click to banner'. $request->banner_id;
	}
}
