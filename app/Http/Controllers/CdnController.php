<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CdnController extends Controller
{
  private $payment = true;

  public function returnStyle($project, $key)
  {
    if (isset($project) && in_array($project, ['like', 'celebrity', 'auto', 'evakuator', 'hadu', 'ib', 'imoto', 'kneu', 'rest']) && $this->payment) {
      $project_key = '';
      $random = rand(15, 100);

      $key_date = substr($key, 0, 8) - $random;
      $key_string = substr($key, 8);

      $date = date('zHtL') - $random;

      switch ($project) {
        case 'like':
          $project_key = '5d99fc0fec5c9';//ok
          break;
        case 'celebrity':
          $project_key = '5d99fcd68f699';//ok
          break;
        case 'auto':
          $project_key = '5d99fcd68f669';//ok
          break;
        case 'evakuator':
          $project_key = '5d9a03bf98ae4';//ok
          break;
        case 'hadu':
          $project_key = '5d9a03c572ed8';//ok
          break;
        case 'ib':
          $project_key = '5d9a03c9ca250';//ok
          break;
        case 'imoto':
          $project_key = '5d9a03ce488aa';//ok
          break;
        case 'kneu':
          $project_key = '5d9a03d2d3825';//ok
          break;
        case 'rest':
          $project_key = '5d9a03d76dc6d';//ok
          break;
      }

      if ($key_string.$key_date === $project_key.$date) {
        return response()->file(public_path('cdn/' . $project . '.css'), [
          'Content-Type' => 'text/css',
        ]);
      }
    }

    return response()->file(public_path('cdn/license'));
  }
}
