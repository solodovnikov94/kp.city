<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CommentsController extends Controller
{
	public function addComment(Request $request)
	{
		$rules = [
			'model' => 'required|string',
			'id' => 'required|numeric',
			'message' => 'required',
		];

		if (!is_null($request->rating)) {
			$rules['rating'] = 'required|numeric';
		}

		$messages = [
			'rating.numeric' => 'Поле рейтинг є обов\'язковим для заповнення.'
		];

		$validator = Validator::make($request->all(), $rules, $messages);

		if ($validator->fails()) {
			return response()->json([
				'errors' => $validator->errors()
			]);
		}

		if (!Auth::check()) {
			return response()->json([
				'message' => 'Для того щоб залишити коментар, потрібно авторизуватися!'
			]);
		}

		$query = app('App\\' . $request->model)
			->select('id', 'title', 'deleted_at')
			->where('id', $request->id);

		$model = $query->first();

		$isAnonymous = $request->anonymous == 'on' ? 1 : 0;
		$active = $isAnonymous ? 0 : 1;

		if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('developer')) {
			$active = 1;
		}

		if (!is_null($model)) {
			$comment = new Comment;
			$comment->user_id = Auth::id();
			$comment->parent_id = is_null($request->comment_id) ? null : $request->comment_id;
			$comment->active = $active;
			$comment->anonymous = $isAnonymous;
			$comment->body = $request->message;
			$comment->rating = is_null($request->rating) ? null : $request->rating;
			$comment->save();

			$model->comments()->save($comment);

			$model = $query->with('comments')->withCount(['comments' => function ($query) {
				$query->where('active', true);
			}])->first();

			$model = $comment->getComments($model);

			$comments = Comment::where('id', $comment->id)->get();
			$commentBlockHtml = view('comments.comment')->with([
				'comments' => $comments,
				'children' => is_null($request->comment_id) ? false : true
			])->render();

			$ratingBlockHtml = '';
			if (!is_null($request->rating)) {
				$ratingBlockHtml = view('organizations.organization-rating')->with([
					'rating' => $comment->getRating($request->id, 'App\\' . $request->model),
					'ratingComments' => $comment->ratingComments($request->id, 'App\\' . $request->model)
				])->render();
			}

			$resultMessage = $isAnonymous ? 'Анонімний коментар успішно добавлений і буде опублікований після модерації' : 'Коментар успішно добавлений';

			return response()->json([
				'alert' => view('includes.float-alert')->with([
					'type' => 'success',
					'class' => '',
					'message' => $resultMessage
				])->render(),
				'comment_block_html' => $commentBlockHtml,
				'rating_block_html' => $ratingBlockHtml,
				'total_comments' => $model->comments_count,
				'parent' => $comment->parent_id
			]);
		}

		return response()->json([
			'message' => 'На сервері відбулася помилка, спробуйте пізніше'
		]);
	}

	public function removeComment(Request $request)
	{
		Comment::where('id', $request->comment_id)
			->orWhere('parent_id', '=', $request->comment_id)->delete();

		return 'die';
	}

	public function likeComment(Request $request)
	{
		$commentId = $request->comment_id;
		$comment = Comment::where('id', $commentId)->first();
		$like = new \App\Like;
		$comment = $like->putLike($comment);
		$trophyComment = $this->trophyComment($comment->commentable);

		return response()->json([
			'comment_id' => $trophyComment ?? false,
			'active' => $comment->user_like,
			'count_likes' => $comment->likes,
			'trophy' => view('includes.trophy')->render()
		]);
	}

	public function trophyComment($model)
	{
		$maxLikes = $model->comments()->max('likes');
		$commentWithMaxLikes = $model->comments()->where('likes', $maxLikes)->take(2)->get();

		if ($commentWithMaxLikes->first()->likes > 0 && count($commentWithMaxLikes) == 1) {
			return $commentWithMaxLikes->first()->id;
		}

		return false;
	}
}
