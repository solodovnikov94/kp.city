<?php

namespace App\Http\Controllers;

use App\OrganizationCategory;
use Illuminate\Http\Request;

class OrganizationsController extends Controller
{
	public function showOrganizationCategoriesPage()
	{
		$categories = OrganizationCategory::main()
			->active()
			->with(['children' => function ($query){
				$query->active()->orderBy('order_', 'asc');
			}])
			->withCount('children')
			->get();

		$data = [
			'categories' => $categories,
		];

		return view('organizations.organizations-categories')->with($data);
	}

	public function showOrganizationCategoryPage(Request $request, $categorySlug)
	{
		$category = OrganizationCategory::where('slug', $categorySlug)
			->active()
			->firstOrFail();

		$subcategories = OrganizationCategory::where('parent_id', '=', $category->id)
			->orderBy('order_', 'asc')
			->active()->withCount(['organizations' => function ($query) {
				$query->where('active', 1);
			}])
			->with(['organizations' => function ($query) {
				$query->where('active', 1);
			}])
			->get();

		return view('organizations.organizations-category', compact('category', 'subcategories'));
	}

	public function showOrganizationSubcategoryPage(Request $request, $categorySlug, $subcategorySlug)
	{
		$category = OrganizationCategory::where('slug', $categorySlug)
			->active()
			->firstOrFail();

		$subcategory = $category->children()
			->where('slug', '=', $subcategorySlug)
			->active()
			->firstOrFail();

		$onPage = 6;
		$organizations = $subcategory->organizations()->active()->paginate($onPage);
		$onNextPage = 'Завантажити ще ' . on_next_page($onPage, $organizations);
		$showMore = $organizations->lastPage() !== 1;

		if ($request->ajax()) {
			$returnHTML = view('organizations.organizations-list')->with([
				'category' => $category,
				'subcategory' => $subcategory,
				'organizations' => $organizations,
			])->render();

			return response()->json([
				'html'       => $returnHTML,
				'lastPage'   => $organizations->lastPage(),
				'onNextPage' => $onNextPage,
			]);
		}
		
		return view('organizations.organizations-subcategory', compact('category', 'subcategory', 'organizations', 'showMore', 'onNextPage'));
	}

	public function showOrganizationPage(Request $request, $categorySlug, $subcategorySlug, $organizationId, $organizationSlug)
	{
		$category = OrganizationCategory::where('slug', $categorySlug)
			->active()
			->firstOrFail();

		$subcategory = $category->children()
			->where('slug', '=', $subcategorySlug)
			->active()
			->firstOrFail();

		$organization = $subcategory->organizations()
			->where([
				['slug', '=', $organizationSlug],
				['id', '=', $organizationId]
			])
			->with('comments')->withCount(['comments' => function($query) {
				$query->where('active', 1);
			}])
			->firstOrFail();
		$comments = new \App\Comment;
		$organization = $comments->getComments($organization);

		$rating = $comments->getRating($organization->id, 'App\Organization');
		$ratingComments = $comments->ratingComments($organization->id, 'App\Organization');

		$data = [
			'category' => $category,
			'subcategory' => $subcategory,
			'organization' => $organization,
			'ratingComments' => $ratingComments,
			'rating' => $rating,
			'todayParam' => $organization->getToday(),
			'days' => $organization->getDays(),
			'isOpen' => $organization->isOpen(),
		];

		if($organization->gallery) {
			$onPage = 6;
			$gallery = $organization->gallery->photos()->paginate($onPage);
			$data['gallery'] = $gallery;
			$data['galleryShowMore'] = $gallery->lastPage() !== 1;
			$nextPage = 'Завантажити ще ' . on_next_page($onPage, $gallery);
			$data['galleryOnNextPage'] = $nextPage;

			if ($request->ajax() && $request->param == 'gallery') {
				$html = view('organizations.organization-images-list')->with([
					'gallery' => $gallery,
				])->render();


				return response()->json([
					'html'       => $html,
					'lastPage'   => $gallery->lastPage(),
					'onNextPage' => $nextPage,
				]);
			}
		}

		if($organization->news) {
			$news = $organization->news()->active()->published()->paginate(6);
			$data['news'] = $news;
		}

		if($organization->group) {
			$filiations = \App\Organization::where([
				['id', '!=', $organization->id],
				['group_id', '=', $organization->group->id]
			])->active()->paginate(6);
			$data['filiations'] = $filiations;
		}

		return view('organizations.organizations-single')->with($data);
	}

	public function likeOrganization(Request $request)
	{
		$organization = \App\Organization::where('id', $request->organization_id)->first();

		$like = new \App\Like;
		$organization = $like->putLike($organization);

		return response()->json([
			'active' => $organization->user_like,
			'count_likes' => $organization->count_likes,
		]);
	}

	public function addChanges(Request $request)
	{
		$validator = \Illuminate\Support\Facades\Validator::make($request->all(), [
			'organization_id' => 'required|numeric',
			'message' => 'required',
			'g-recaptcha-response' => 'required',
		]);

		if ($validator->fails()) {
			return response()->json([
				'errors' => $validator->errors()
			]);
		}

		$organizationChange = new \App\OrganizationChange;
		$organizationChange->user_id = auth()->check() ? auth()->user()->id : null;
		$organizationChange->organization_id = $request->organization_id;
		$organizationChange->message = $request->message;
		$organizationChange->active = 0;
		$organizationChange->save();


		return response()->json([
			'alert' => view('includes.float-alert')->with([
				'type' => 'success',
				'class' => '',
				'message' => 'Ваші зміни прийняті і будуть розглянуті'
			])->render()
		]);
	}
}
