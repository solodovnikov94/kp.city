<?php

namespace App\Http\Controllers;

use App\Article;
use App\News;
use App\ShortNews;
use App\Organization;
use App\Poll;
use Illuminate\Http\Request;

class PagesController extends Controller
{
	public function index()
	{
		$news = News::newest()->with('category')->published()->active()->take(6)->get();

		$lastShortNews = ShortNews::published()
			->active()->orderBy('published_at', 'desc')->take(9)
			->get();

		$lastNews = News::newest()->published()
			->active()->take(9)
			->get();

		$merged = $lastNews->merge($lastShortNews);
		$lastNewsByDays = $merged->sortByDesc('published_at')->take(9)->groupBy(function($date) {
			return \Carbon\Carbon::parse($date->published_at)->format('d');
		});

		$articles = Article::newest()->active()->published()->take(8)->get();

		$organizations = Organization::with('category', 'category.parent')
			->active()
			->inRandomOrder()
			->take(8)
			->get();

		$tags = \Cache::remember('tags_cache', 60, function () {
			return \App\Tag::join('taggables', 'tags.id', '=', 'taggables.tag_id')
				->select('*')->orderBy('taggables.created_at', 'desc')
				->whereNotNull('taggables.created_at')->active()->get()
				->unique('tag_id')->take(6);
		});

		$poll = Poll::active()->separate()->published()
			->where('home_page', 1)->notDeactivation()->inRandomOrder()->first();

		$data = [
			'lastNewsByDays' => $lastNewsByDays,
			'news'		    	 => $news,
			'type'           => 'news',
			'articles'			 => $articles,
			'organizations'  => $organizations,
			'tags'					 => $tags,
			'poll'					 => $poll
		];

		return view('homepage')->with($data);
	}

	public function about()
	{
		return view('pages.about');
	}

	public function contacts()
	{
		return view('pages.contacts');
	}

	public function saveContacts(Request $request)
	{
		$validator = \Illuminate\Support\Facades\Validator::make($request->all(), [
			'email' => 'required|email|string',
			'name' => 'required',
			'text' => 'required',
			'g-recaptcha-response' => 'required',
		]);

		if ($validator->fails()) {
			return response()->json([
				'errors' => $validator->errors()
			]);
		}

		$contacts = new \App\Contact;
		$contacts->name = $request->name;
		$contacts->email = $request->email;
		$contacts->text = $request->text;
		$contacts->active = 0;
		$contacts->save();

		return response()->json([
			'alert' => view('includes.float-alert')->with([
				'type' => 'success',
				'class' => '',
				'message' => 'Ваше повідомлення відправлено! Ми з вами зв\'яжемся'
			])->render()
		]);
	}

	public function advertising()
	{
		return view('pages.advertising');
	}

	public function documentation()
	{
		return view('pages.documentation');
	}

	public function privacy()
	{
		return view('pages.privacy');
	}

	public function rules()
	{
		return view('pages.rules');
	}
}
