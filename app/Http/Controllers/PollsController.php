<?php

namespace App\Http\Controllers;

use App\Poll;
use App\PollAnswerScore;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PollsController extends Controller
{
	public function showPollsPage(Request $request)
	{
		$onPage = 6;
		$polls = Poll::active()->separate()->published()
			->withCount('answerScores')->orderBy('published_at', 'desc')->paginate($onPage);
		$onPage = 'Завантажити ще ' . on_next_page($onPage, $polls);
		$showMore = $polls->lastPage() !== 1;

		if ($request->ajax()) {
			$html = view('polls.polls-list')->with([
				'polls' => $polls,
			])->render();

			return response()->json([
				'html'       => $html,
				'lastPage'   => $polls->lastPage(),
				'onNextPage' => $onPage,
			]);
		}

		$popularPolls = Poll::active()->published()->separate()->notDeactivation()
			->withCount('answerScores')
			->orderBy('answer_scores_count', 'desc')
			->take(4)
			->get();

		$news = \App\News::newest()->active()->published()->take(8)->get();

		$organizations = \App\Organization::with('category', 'category.parent')
			->active()
			->inRandomOrder()
			->take(8)
			->get();

		$data = [
			'polls' => $polls,
			'showMore' => $showMore,
			'onNextPage' => $onPage,
			'popularPolls' => $popularPolls,
			'news' => $news,
			'organizations' => $organizations,
		];

		return view('polls.polls-all')->with($data);
	}

	public function showPollPage($id, $slug)
	{
		$poll = \App\Poll::where([
			['id', '=', $id],
			['slug', '=', $slug]
		])->with('comments')->withCount(['comments' => function($query) {
				$query->where('active', 1);
		}])->first();

		$comments = new \App\Comment;
		$poll = $comments->getComments($poll);

		$model = 'Poll';

		$otherPolls =  Poll::active()->published()->separate()->notDeactivation()
			->where('id', '!=', $id)->inRandomOrder()->take(4)->get();

		return view('polls.polls-single', compact('poll', 'model', 'otherPolls'));
	}

	public function pollVote(Request $request, $slug)
	{
		$validator = Validator::make($request->all(),
			array(
				'poll' => 'required|numeric|exists:poll_answers,id',
			)
		);

		if ($validator->fails()) {
			return response()->json([
				'message' => 'Виберіть варіант віповіді',
			]);
		} else {
			$poll = Poll::where('slug', '=', $slug)->first();
			if (!$poll) {
				return response()->json([
					'message' => 'Опитування не знайдено',
				]);
			}
			if (!$poll->isVoted()) {
				return response()->json([
					'message' => 'Голос зараховано!',
				]);
			}

			$pollAnswerScore = new PollAnswerScore;
			$pollAnswerScore->poll_answer_id = (int)$request->poll;
			$pollAnswerScore->poll_id = $poll->id;
			if (\Auth::user()) {
				$pollAnswerScore->user_id = \Auth::id();
			}
			$pollAnswerScore->user_ip = $request->ip();
			$pollAnswerScore->user_agent = $request->header('user-agent');
			$pollAnswerScore->save();

			$returnHTML = view('includes.poll.poll-result')->with([
				'poll' => $poll
			])->render();

			return response()->json([
				'html' => $returnHTML
			])->cookie('vote', $slug);
		}
	}
}
