<?php

namespace App\Http\Controllers;

use App\OrganizationChange;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
//	protected $user;
//
//	public function __construct(User $user)
//	{
////		$this->middleware('auth');
//		$this->user = $user;
//	}

	public function profile()
	{
		return view('profile.profile');
	}

	public function notifications()
	{

	}

	public function profileEdit()
	{

	}

	public function profileUpdate(Request $request)
	{

	}

	public function changePasswordEdit()
	{

	}

	public function changePasswordUpdate(Request $request)
	{
		$messages = [
			'current_password.required' => __('Введіть ваш пароль'),
			'password.required' => __('Пароль є обов\'яковим для заповнення')
		];

		$validator = Validator::make($request->all(), [
			'current_password' => 'required',
			'password' => 'required|same:password',
			'password_confirmation' => 'required|same:password',
		], $messages);

		if ($validator->fails()) {
			return redirect()->back()->withInput()->withErrors($validator->errors())->with('danger', 'Помилки у формі');
		} else {
			$currentPassword = $this->user->password;
			if (Hash::check($request->current_password, $currentPassword)) {
				$user = $this->user;
				$user->password = Hash::make($request->password);;
				$user->save();
				return redirect('/profile')->with('success', __('Ваш пароль успішно оновлений'));
			} else {
				return redirect()->back()->withInput()->withErrors($validator->errors())->with('danger-password', __('Старий пароль введений не вірно'));
			}
		}
	}

	public function comments()
	{

	}

	public function adverts()
	{

	}

	public function createAdvert(Request $request)
	{

	}

	public function editAdvert()
	{

	}

	public function updateAdvert(Request $request)
	{

	}

	public function bookmarks()
	{

	}

	//додати щось (кнопка)
	public function addNewRecords()
	{

	}

	public function organizationChangesIndex()
	{
		return OrganizationChange::all();
	}

	public function organizationChangesShow($id)
	{
		return OrganizationChange::findOrFail($id);
	}

	public function organizationChangesUpdate(Request $request, $id)
	{
		$company = OrganizationChange::findOrFail($id);
		$company->message = $request->message;
 		$company->save();
		return '';
	}

	public function organizationChangesStore(Request $request)
	{
		$validator = \Illuminate\Support\Facades\Validator::make($request->all(), [
			'message' => 'required',
		]);

		if ($validator->fails()) {
			return response()->json([
				'errors' => $validator->errors()
			]);
		}

		$company = new OrganizationChange;
		$company->message = $request->message;
		$company->save();
		return $company;
	}

	public function organizationChangesDestroy($id)
	{
		$company = OrganizationChange::findOrFail($id);
		$company->delete();
		return '';
	}

	public function organizationList()
	{
		$organizations = \App\Organization::paginate(6);
//		$organizations = \App\Organization::take(6)->get();
		foreach ($organizations as $organization) {
			$organization->setAttribute('low_image', $organization->getCroppedImageOrTransparent('low'));
			$organization->setAttribute('card_image', $organization->getCroppedImageOrTransparent('card'));
			$organization->setAttribute('url', $organization->getUrl());
		}
//		dd($organizations);
		return $organizations;
	}
}
