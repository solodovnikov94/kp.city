<?php

namespace App\Http\Controllers;

use App\Search;
use Laravel\Nova\Http\Requests\NovaRequest;
use Illuminate\Support\Facades\Validator;

class SearchController extends Controller
{
	public function index(NovaRequest $request)
	{
		$validator = Validator::make($request->all(), [
			'q' => 'required|min:3',
		]);

		if ($validator->fails()) {
			return view('search.search-results')->withErrors($validator->errors());
		}

		$array = [
			'article' => 'App\Nova\Article',
			'news' => 'App\Nova\News',
			'organizations' => 'App\Nova\Organization',
		];

		if($request->param) {
			$param = $request->param;
			foreach ($array as $key => $item) {
				if($key != $request->param) {
					unset($array[$key]);
				}
			}
		}

		$collection = collect($array);
		$result = (new Search($request, $collection))->get();
		$searchParam = $request->q;

		if ($request->ajax()) {
			$returnHTML = '';
			switch ($param) {
				case 'news':
					$returnHTML = view('articles.articles-list')->with([
						'articles' => $result['news'],
					])->render();
					break;
				case 'article':
					$returnHTML = view('articles.articles-list')->with([
						'articles' => $result['article'],
					])->render();
					break;
				case 'organizations':
					$returnHTML = view('organizations.organizations-list')->with([
						'organizations' => $result['organizations'],
					])->render();
					break;
			}

			return response()->json([
				'html'       => $returnHTML,
				'lastPage'   => $result[$param.'_last_page'],
				'onNextPage' => $result[$param.'_on_next_page'],
			]);
		}

		return view('search.search-results', compact('result', 'searchParam'));
	}
}
