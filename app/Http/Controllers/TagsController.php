<?php

namespace App\Http\Controllers;

use App\Tag;
use Illuminate\Http\Request;

class TagsController extends Controller
{
	public function tag(Request $request, $tag_slug)
	{
		$onPage = 6;
		$tag = Tag::active()->where('slug', $tag_slug)->firstOrFail();
		$articles = $tag->articles()->active()->published()->newest()->paginate($onPage);
		$news = $tag->news()->active()->published()->newest()->paginate($onPage);
		$organizations = $tag->organizations()->active()->paginate($onPage);
		$onNextPageArticles = $this->onNextPage($articles, $onPage);
		$onNextPageNews = $this->onNextPage($news, $onPage);
		$onNextPageOrganizations = $this->onNextPage($organizations, $onPage);


		if ($request->ajax()) {
			$html = '';
			$lastPage = 0;
			$onNextPage = 0;
			switch ($request->param) {
				case 'news':
					$html = view('articles.articles-list')->with([
						'articles' => $news,
					])->render();
					$onNextPage = $onNextPageArticles;
					$lastPage = $news->lastPage();
					break;
				case 'article':
					$html = view('articles.articles-list')->with([
						'articles' => $articles,
					])->render();
					$onNextPage = $onNextPageNews;
					$lastPage = $articles->lastPage();
					break;
				case 'organizations':
					$html = view('organizations.organizations-list')->with([
						'organizations' => $organizations,
					])->render();
					$onNextPage = $onNextPageOrganizations;
					$lastPage = $organizations->lastPage();
					break;
			}

			return response()->json([
				'html'       => $html,
				'lastPage'   => $lastPage,
				'onNextPage' => $onNextPage,
			]);
		}

		$tag->total = $articles->total() + $news->total() + $organizations->total();

		$data = [
			'tag' => $tag,
			'articles' => $articles,
			'news' => $news,
			'organizations' => $organizations,
			'onNextPageArticles' => $onNextPageArticles,
			'onNextPageNews' => $onNextPageNews,
			'onNextPageOrganizations' => $onNextPageOrganizations,
			'showMoreArticles' => $articles->lastPage() !== 1,
			'showMoreNews' => $news->lastPage() !== 1,
			'showMoreOrganizations' => $organizations->lastPage() !== 1
		];

		return view('tags.tags-single')->with($data);
	}

	public function tags(Request $request)
	{
		$onPage = 18;

		$tags = Tag::active()->paginate($onPage);

		$onNextPage = $this->onNextPage($tags, $onPage);
		$showMore = $tags->lastPage() !== 1;

		if ($request->ajax()) {
			$html = view('tags.tags-list')->with([
				'tags' => $tags,
			])->render();

			return response()->json([
				'html'       => $html,
				'lastPage'   => $tags->lastPage(),
				'onNextPage' => $onPage,
			]);
		}

		return view('tags.tags-hot', compact('tags', 'onNextPage', 'showMore'));
	}

	public function onNextPage($model, $onNextPage)
	{
		$nextPage = $model->currentPage() + 1;
		if ($nextPage === $model->lastPage()) {
			$onNextPage = $model->total() - ($model->perPage() * $model->currentPage());
		}

		return 'Завантажити ще ' . $onNextPage;
	}
}
