<?php

function first_element($array, $index)
{
	if (isset($array[0][$index])) {
		return $array[0][$index];
	}
	return null;
}

function strengthened($nowRate, $yesterdayRate)
{
	if ($nowRate > $yesterdayRate) {
		return 'up';
	} else if ($nowRate < $yesterdayRate) {
		return 'down';
	} else {
		return '';
	}
}

function not_null($array)
{
	if (!is_null($array) && count($array)) {
		return $array;
	}
	return false;
}

function unset_first_element($array)
{
	unset($array[0]);
	return $array;
}

function on_next_page($onPage, $model)
{
	$nextPage = $model->currentPage() + 1;
	if ($nextPage === $model->lastPage()) {
		return $model->total() - ($model->perPage() * $model->currentPage());
	}
	return $onPage;
}

function mask_email($email)
{
	$char_shown = 3;

	$mail_parts = explode("@", $email);
	$username = $mail_parts[0];
	$len = strlen($username);

	if ($len <= $char_shown) {
		return implode("@", $mail_parts);
	}

	$mail_parts[0] = substr($username, 0, $char_shown)
		. str_repeat("*", $len - $char_shown - 1)
		. substr($username, $len - $char_shown + 2, 1);

	return implode("@", $mail_parts);
}

function format_link($value)
{
	if (!preg_match('#^http(s)?://#', $value)) {
		$value = 'http://' . $value;
	}

	$urlParts = parse_url($value);

	$domain = preg_replace('/^www\./', '', $urlParts['host']);

	return $domain;
}

function format_array($value)
{
	return array_flip(array_keys($value));
}

function show_banner($id)
{
	$hiddenBanners = session()->get('hidden-banners');

	if (is_array($hiddenBanners)) {
		if (in_array($id, $hiddenBanners)) {
			return false;
		}
	}

	return true;
}

function instagram_media()
{
	if (\Cache::has('cache_instagram')) {
		$result = \Cache::get('cache_instagram');
	} else {
		$instagramToken = env('INSTAGRAM_TOKEN');
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://api.instagram.com/v1/users/self/media/recent/?access_token={$instagramToken}&count=9");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 20);
		$result = curl_exec($ch);
		curl_close($ch);

		\Cache::put('cache_instagram', $result, 1440);
	}

	return json_decode($result);
}

function num2word($n, $titles)
{
	$cases = [2, 0, 1, 1, 1, 2];

	return $titles[($n % 100 > 4 && $n % 100 < 20) ? 2 : $cases[min($n % 10, 5)]];
}

function advertising_in_content($content, $includePath)
{
	$content = explode("</p>", $content);
	$paragraphs = count($content) ?: 1;
	$paragraphs = (int)round($paragraphs / 2);
	$paragraphAfter = $paragraphs; //номер абзаца, после которого вставляем.
	$new_content = '';
	for ($i = 0; $i < count($content); $i++) {
		if ($i == $paragraphAfter) {
			$new_content .= view($includePath)->render();
		}
		$new_content .= $content[$i] . "</p>";
	}
	return $new_content;
}