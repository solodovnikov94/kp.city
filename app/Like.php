<?php

namespace App;

use App\Traits\Relations\LikeRelations;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Schema;

class Like extends Model
{
    use LikeRelations, SoftDeletes;
    
    public function putLike($model)
    {
        $like = $model->likes()->where('user_id', \Auth::id())->withTrashed()->first();
        $user_like = false;
        $modelHasField = Schema::hasColumn($model->getTable(), 'likes');

        if(is_null($like)) {
            $like = new Like;
            $like->user_id = \Auth::id();
            $like->save();
            $model->likes()->save($like);
            if($modelHasField) {
                $model->likes = $model->likes + 1;
            }
            $user_like = true;
        } else {
            if($like->trashed()) {
                $like->restore();
                if($modelHasField) {
                    $model->likes = $model->likes + 1;
                }
                $user_like = true;
            } else {
                $like->delete();
                if($modelHasField) {
                    $model->likes = $model->likes - 1;
                }
            }
        }

        $model->save();

        $model->user_like = $user_like;

        return $model;
    }
}
