<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class VerifyEmail extends Mailable
{
	use Queueable, SerializesModels;

	public $user;

	public function __construct(User $user)
	{
		$this->user = $user;
	}

	public function build()
	{
		$data = [
			'actionUrl' => route('verify.email', ['email' => $this->user->email, 'verify_token' => $this->user->verify_token])
		];
		
		return $this
			->subject('Активація аккаунта')
			->markdown('emails.verify-email')
			->with($data);
	}
}
