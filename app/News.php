<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Builder;

class News extends ParentArticle
{
	use Notifiable;

	protected $attributes = ['type' => 'news'];

	protected static function boot()
	{
		parent::boot();

		static::addGlobalScope('news', function (Builder $builder) {
			$builder->where('type', '=', 'news');
		});
	}

	public function category()
	{
		return $this->belongsTo(NewsCategory::class, 'category_id', 'id');
	}

	public function getUrl()
	{
		if($this->id) {
			return url('news/'.$this->category->slug.'/'.$this->id.'-'.$this->slug);
		}
		return null;
	}
}
