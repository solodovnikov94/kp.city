<?php

namespace App;

use App\Traits\Crops;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NewsCategory extends Model
{
	use SoftDeletes, Crops;

	protected $table = 'article_categories';
	protected $crops_key = 'article_categories';
	protected $attributes = ['type' => 'news'];

	protected static function boot()
	{
		parent::boot();

		static::addGlobalScope('article', function (Builder $builder) {
			$builder->where('type', '=', 'news');
		});
	}

	public function news()
	{
		return $this->hasMany(News::class, 'category_id', 'id');
	}

	public function getUrl()
	{
		return url('news/' . $this->slug);
	}
	
	public function scopeActive($query)
	{
		return $query->where('active', true);
	}
}
