<?php

namespace App\Notifications;

class Telegram
{
	private static function checkAllKnownErrors($url)
	{
		$message = 'Немає повідомлення';
		$disable_notification = false;
		$error = false;

		//Перевірка на ультрахакерів
		if (strrpos($url, '/wp-')) {
			$message = 'Хакери WP зробили жалюгідну спробу нас ломанути...';
			$disable_notification = true;
			$error = true;
		}

		//Приклад: https://kp.city/public/public/wordpress
		if (request()->ip() == '96.30.196.250') {
			$message = 'Хацкер з Канади перевіряє наш сайт на міцність...';
			$disable_notification = true;
			$error = true;
		}

		//Перевірка на відомих ботів
		if (request()->userAgent() == 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)'
			|| request()->userAgent() == 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/534+ (KHTML, like Gecko) BingPreview/1.0b'
			|| request()->userAgent() == 'Mozilla/5.0 (compatible; SemrushBot/3~bl; +http://www.semrush.com/bot.html)'
			|| request()->ip() == '66.249.64.91' || request()->ip() == '66.249.64.95' || request()->ip() == '66.249.64.93'
		) {
			$message = 'До нас навідався бот...';
			$disable_notification = true;
			$error = true;
		}

		//Перевірка на інші помилки
		if (strrpos($url, 'adframe.js') || strrpos($url, 'ads.txt')) {
			$message = 'adframe.js або ads.txt помилка...';
			$disable_notification = true;
			$error = true;
		}

		//Повідомлення, які не будуть приходити в канал
		if (
			//Тест на сайті pr-cy.ru викликає цю помилку
			strrpos(request()->userAgent(), 'https://a.pr-cy.ru')
			//Помилки від хостінгера (хз чого). Наприклад: https://kp.city/public/public/.well-known/hostinger-challenge
			|| request()->ip() == '153.92.6.85'
			//Помилки ультракласної адмінки
			|| strrpos($url, '/nova-api')
		) {
//			TODO замінити на return false після тестів...
			$message = 'Виникла помилка, про яку можна і не писати в канал. Тестуємо...';
			$disable_notification = true;
			$error = true;
		}

		if ($error) {
			return [
				'message'              => $message,
				'disable_notification' => $disable_notification,
			];
		}

		return false;
	}

	public static function kpCityBot()
	{
		return env('TELEGRAM_CHAT_ID');
	}

	public static function telegramObject()
	{
		return new \Telegram\Bot\Api(env('TELEGRAM_BOT_TOKEN'));
	}

	public static function article($exists, $object)
	{
		if ($object->type == 'article') {
			$type_row_name = 'статтю';
		} else {
			$type_row_name = 'новину';
		}

		$text = '<strong>' . ($exists ? 'Створено' : 'Відредаговано') . '</strong> ' . $type_row_name . ': ' . PHP_EOL
			. '<a href="' . $object->getUrl() . '">' . $object->title . '</a>' . PHP_EOL
			. ($exists ? 'Автор' : 'Редактор') . ': ' . auth()->user()->name . ' ' . auth()->user()->surname;

		self::telegramObject()->setAsyncRequest(true)
			->sendMessage([
				'chat_id'                  => self::kpCityBot(),
				'text'                     => $text,
				'parse_mode'               => 'html',
				'disable_notification'     => $exists ? false : true,
				'disable_web_page_preview' => true,
			]);
	}

	public static function organization($exists, $object)
	{
		$type_work = $exists ? "Створено" : "Відредаговано";

		self::telegramObject()->setAsyncRequest(true)
			->sendMessage([
				'chat_id'    => self::kpCityBot(),
				'text'       => '<strong>' . $type_work . '</strong> організацію: <a href="' . $object->getUrl() . '">' . $object->title . '</a>' . PHP_EOL . 'Редактор: ' . auth()->user()->name . ' ' . auth()->user()->surname,
				'parse_mode' => 'html',
			]);
	}

	public static function comment($object)
	{
		$text = $object->anonymous ? 'Коментар анонімний' : 'Коментар не анонімний';
		self::telegramObject()->setAsyncRequest(true)
			->sendMessage([
				'chat_id'    => self::kpCityBot(),
				'text'       => '<strong>Залишено коментар</strong>' . PHP_EOL . 'Користувач: ' . auth()->user()->name . ' ' . auth()->user()->surname . PHP_EOL . '<strong>' . $text . '</strong>',
				'parse_mode' => 'html',
			]);
	}

	public static function contact($object)
	{
		self::telegramObject()->setAsyncRequest(true)
			->sendMessage([
				'chat_id'    => self::kpCityBot(),
				'text'       => '<strong>Відправлена форма контактів</strong>' . PHP_EOL . '<strong>Текст</strong>: ' . $object->text,
				'parse_mode' => 'html',
			]);
	}

	public static function organizationChanges()
	{
		self::telegramObject()->setAsyncRequest(true)
			->sendMessage([
				'chat_id'    => self::kpCityBot(),
				'text'       => '<strong>Запропоновані зміни до організації</strong>',
				'parse_mode' => 'html',
			]);
	}

	public static function siteError($file, $line, $message, $url)
	{
		if (env('APP_ENV') == 'production' && request()->userAgent() !== 'TelegramBot (like TwitterBot)') {
			$knownErrors = self::checkAllKnownErrors($url);

			if ($knownErrors) {
				$message = $knownErrors['message'];
				$disable_notification = $knownErrors['disable_notification'];
			} else {
				$message = '<strong>На сайті сталася помилка.</strong>'
					. PHP_EOL . 'Файл: <code>' . $file . '</code>'
					. PHP_EOL . 'Рядок: <code>' . $line . '</code>'
					. PHP_EOL . 'Повідомлення: <code>' . (empty($message) ? 'Без повідомлення' : $message) . '</code>'
					. PHP_EOL . 'Посилання: ' . $url;

				if (auth()->user()) {
					$message .= PHP_EOL . 'Користувач: <code>' . auth()->user()->name . ' ' . auth()->user()->surname . '</code>';
				} else {
					$message .= PHP_EOL . 'Користувач не авторизований. IP: <code>' . request()->ip() . '</code>. UserAgent: <code>' . request()->userAgent() . '</code>';
				}

				$message .= PHP_EOL . 'Попередня сторінка: ' . url()->previous();
			}

			self::telegramObject()->setAsyncRequest(true)
				->sendMessage([
					'chat_id'                  => env('TELEGRAM_ERRORS_CHAT_ID'),
					'text'                     => $message,
					'parse_mode'               => 'html',
					'disable_notification'     => $disable_notification ?? false,
					'disable_web_page_preview' => true,
				]);
		}
	}

	public static function newCompetitorContent($site, $title, $url)
	{
		$message = '<strong>На сайті ' . $site . '</strong> з\'явилася новина '
			. PHP_EOL . '<a href="' . $url . '">' . $title . '</a>'
			. PHP_EOL . '<a href="https://efect-moda.com/nevdalo.html">Дати вдалого ляща</a>';

		self::telegramObject()->setAsyncRequest(true)
			->sendMessage([
				'chat_id'                  => self::kpCityBot(),
				'text'                     => $message,
				'parse_mode'               => 'html',
				'disable_web_page_preview' => true,
			]);
	}
}
