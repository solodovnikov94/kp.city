<?php

namespace App\Notifications;

use Illuminate\Notifications\Notification;
use NotificationChannels\Twitter\TwitterChannel;
use NotificationChannels\Twitter\TwitterStatusUpdate;

class TwitterNews extends Notification
{
	public function via($notifiable)
	{
		return [TwitterChannel::class];
	}

	public function toTwitter($news) {
		return new TwitterStatusUpdate("#камянець #україна #кп #kamianets #kpcity #камянецьподільський\n\n" . $news->getUrl());
	}
}
