<?php

namespace App\Nova;

use Ajhaupt7\ImageUploadPreview\ImageUploadPreview;
use App\Nova\Customization\RemoveCroppedImages;
use App\Nova\Customization\StoreCroppedImages;
use Benjaminhirsch\NovaSlugField\Slug;
use Benjaminhirsch\NovaSlugField\TextWithSlug;
use ElevateDigital\CharcountedFields\CharcountedText;
use ElevateDigital\CharcountedFields\CharcountedTextarea;
use Laravel\Nova\Fields\Avatar;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Panel;
use Illuminate\Http\Request;
use Waynestate\Nova\CKEditor;

class Advert extends Resource
{
	public static $model = \App\Advert::class;
	public static $title = 'title';
	public static $search = ['title', 'meta_title', 'meta_description'];
	public static $group = '6 - Оголошення';
	public static $rowCounterParams = [
		[
			'field' => 'active',
			'operator' => '!=',
			'value' => 1,
			'class' => 'info'
		]
	];

	public static function label()
	{
		return 'Оголошення';
	}

	public static function singularLabel()
	{
		return 'Оголошення';
	}

	public function fields(Request $request)
	{
		return [
			ID::make('№', 'id')
				->exceptOnForms()
				->sortable()
			,

//			Avatar::make('Зображення', 'image')
//				->thumbnail(function () {
//					return $this->getCroppedImage('brief', 'jpg', 'image');
//				})
//				->onlyOnIndex()
//			,

			BelongsTo::make('Автор', 'creator', User::class)
				->exceptOnForms()
			,

			BelongsTo::make('Редактор', 'editor', User::class)
				->help('Користувач, який останній редагував статтю')
				->onlyOnDetail()
			,

			BelongsTo::make('Категорія', 'category', AdvertCategory::class)
				->hideFromIndex()
			,

//			ImageUploadPreview::make('Зображення', 'image')
//				->store(new StoreCroppedImages('organizations', 'main', 'image'))
//				->delete(new RemoveCroppedImages('organizations', 'main', 'image'))
//				->thumbnail(function () {
//					return $this->getCroppedImage('card', 'jpg', 'image');
//				})
//				->hideFromIndex()
//				->rules('max:8192')
//			,

			TextWithSlug::make('Назва оголошення', 'title')
				->help('Рекомендовано 60-80 символів')
				->slug('Слаг')
				->rules('required', 'max:100')
			,

			Slug::make('Слаг', 'slug')
				->help('Назва організації в адресній стрічці')
				->creationRules(['required', 'alpha_dash'])
				->hideWhenUpdating()
				->hideFromIndex()
			,

			CKEditor::make('Контент', 'body')
				->options(config('nova.ckeditor-field'))
				->hideFromIndex()
				->rules(['required'])
			,


			Text::make('Оголошення', function () {
				if (isset($this->slug))
					return "<a href='" . $this->getUrl() . "' target='_blank'>Перейти</a>";

				return 'Посилання немає(';
			})
				->asHtml()
				->exceptOnForms()
			,

			new Panel('Інформація', $this->infoFields()),
			new Panel('SEO', $this->metaFields()),
		];
	}

	protected function metaFields()
	{
		return [
			CharcountedText::make('Meta: Назва', 'meta_title')
				->hideFromIndex()
				->rules(['required'])
				->maxChars(80)
				->warningAt(60)
				->help('Назва сторінки. Оптимально 50-65, максимум 80 символів')
			,
			CharcountedTextarea::make('Meta: Опис', 'meta_description')
				->hideFromIndex()
				->rules('required', 'max:300')
				->maxChars(300)
				->warningAt(250)
				->help('Опис для пошукових систем. Оптимально 170-290, максимум 300 символів')
			,
			CharcountedTextarea::make('Meta: Ключові слова', 'meta_keywords')
				->hideFromIndex()
				->maxChars(255)
				->warningAt(250)
			,
		];
	}

	protected function infoFields()
	{
		return [
			Boolean::make('Відмодеровано', 'active')
				->help('Переглянено адміністрацією')
			,
			Text::make('Перегляди', 'views')
				->hideWhenCreating()
				->hideWhenUpdating()
				->hideFromIndex()
			,
			DateTime::make('Створено', 'created_at')
				->format('DD.MM.YY HH:mm')
				->hideWhenCreating()
				->hideWhenUpdating()
			,
			DateTime::make('Редаговано', 'updated_at')
				->format('DD.MM.YY HH:mm')
				->onlyOnDetail()
			,
			BelongsTo::make('Останній редагував', 'editor', User::class)
				->onlyOnDetail()
			,
		];
	}
}
