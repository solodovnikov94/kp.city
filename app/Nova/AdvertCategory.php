<?php

namespace App\Nova;

use Ajhaupt7\ImageUploadPreview\ImageUploadPreview;
use App\Nova\Customization\RemoveCroppedImages;
use App\Nova\Customization\StoreCroppedImages;
use Benjaminhirsch\NovaSlugField\Slug;
use Benjaminhirsch\NovaSlugField\TextWithSlug;
use ElevateDigital\CharcountedFields\CharcountedText;
use ElevateDigital\CharcountedFields\CharcountedTextarea;
use Laravel\Nova\Fields\Avatar;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Panel;
use Illuminate\Http\Request;

class AdvertCategory extends Resource
{
	public static $model = \App\AdvertCategory::class;
	public static $title = 'title';
	public static $search = ['title'];
	public static $group = '6 - Оголошення';

	public static function label()
	{
		return 'Категорії оголошень';
	}

	public static function singularLabel()
	{
		return 'Категорія оголошення';
	}

	public function fields(Request $request)
	{
		return [
			ID::make('№', 'id'),

//			Avatar::make('Зображення', 'og_image')
//				->thumbnail(function () {
//					return $this->getCroppedImage('avatar', 'jpg', 'og_image');
//				})
//				->onlyOnIndex()
//			,

//			ImageUploadPreview::make('Зображення', 'icon')
//				->disk('public')
//				->path('organization_categories')
//				->storeAs(function (Request $request) {
//					return ($this->id).'_icon.'.$request->icon->getClientOriginalExtension();
//				})
//				->updateRules('max:12')
//				->hideWhenCreating()
//				->hideFromIndex()
//			,

			TextWithSlug::make('Назва', 'title')
				->slug('Слаг')
			,

			Slug::make('Слаг', 'slug')
				->help('Назва категорії в адресній стрічці')
				->creationRules(['required', 'alpha_dash', 'unique:advert_categories,slug'])
				->updateRules(['required', 'alpha_dash', 'unique:advert_categories,slug,{{resourceId}}'])
			,
			new Panel('Інформація', $this->infoFields()),
			new Panel('SEO', $this->metaFields()),
		];
	}

	protected function metaFields()
	{
		return [
			CharcountedText::make('Meta: Назва', 'meta_title')
				->hideFromIndex()
				->rules(['required'])
				->maxChars(80)
				->warningAt(60)
				->help('Назва сторінки. Оптимально 50-65, максимум 80 символів')
			,
			CharcountedText::make('Meta: Ключові слова', 'meta_keywords')
				->hideFromIndex()
				->rules(['required'])
				->maxChars(80)
				->warningAt(60)
				->help('Ключові слова. Оптимально не більше 5 ключових слів')
			,
			CharcountedTextarea::make('Meta: Опис', 'meta_description')
				->hideFromIndex()
				->rules(['required'])
				->maxChars(300)
				->warningAt(250)
				->help('Опис для пошукових систем. Оптимально 170-290, максимум 300 символів')
			,
			CharcountedText::make('OG: Назва', 'og_title')
				->hideFromIndex()
				->maxChars(65)
				->warningAt(50)
				->help('Назва для соціальних мереж. Оптимально до 50, максимум 65 символів')
			,
			CharcountedTextarea::make('OG: Опис', 'og_description')
				->hideFromIndex()
				->maxChars(300)
				->warningAt(250)
				->help('Опис для соціальних мереж. Оптимально 2-4 речення. Максимум 300 символів')
			,
//			ImageUploadPreview::make('OG: Зображення', 'og_image')
//				->store(new StoreCroppedImages('organization_categories', 'og', 'og_image'))
//				->delete(new RemoveCroppedImages('organization_categories', 'og', 'og_image'))
//				->thumbnail(function () {
//					return $this->getCroppedImage('og', 'jpg', 'og_image');
//				})
//				->updateRules('max:8192')
//				->hideFromIndex()
//				->help('Якісне фото для соціальних мереж. Рекомендовано не менше 1200х630')
//			,
		];
	}

	protected function infoFields()
	{
		return [
			Boolean::make('Активовано', 'active'),
			DateTime::make('Створено', 'created_at')
				->format('DD.MM.YY HH:mm:ss')
				->exceptOnForms()
			,
			DateTime::make('Редаговано', 'updated_at')
				->format('DD.MM.YY HH:mm:ss')
				->onlyOnDetail()
			,
		];
	}
}
