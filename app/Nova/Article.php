<?php

namespace App\Nova;

use App\Nova\Customization\RemoveCroppedImages;
use App\Nova\Customization\StoreCroppedImages;
use Benjaminhirsch\NovaSlugField\Slug;
use Benjaminhirsch\NovaSlugField\TextWithSlug;
use ElevateDigital\CharcountedFields\CharcountedText;
use ElevateDigital\CharcountedFields\CharcountedTextarea;
use Laravel\Nova\Fields\Avatar;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\MorphToMany;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Image;
use Illuminate\Http\Request;
use Laravel\Nova\Panel;
use Waynestate\Nova\CKEditor;

class Article extends Resource
{
	public static $model = \App\Article::class;
	public static $title = 'title';
	public static $search = ['title'];
	public static $with = ['tags'];
	public static $group = '2 - Статті';
	public static $rowCounterParams = [
		[
			'field' => 'active',
			'operator' => '!=',
			'value' => 1,
			'class' => 'danger'
		]
	];

	public static function label()
	{
		return 'Статті';
	}

	public static function singularLabel()
	{
		return 'Статтю';
	}

	public function fields(Request $request)
	{
		return [
			ID::make('№', 'id')
				->exceptOnForms()
				->sortable()
			,

			Avatar::make('Зображення', 'image')
				->thumbnail(function () {
					return $this->getCroppedImage('avatar');
				})
				->onlyOnIndex()
			,

			BelongsTo::make('Автор', 'author', User::class)
				->exceptOnForms()
			,

			Select::make('Автор', 'author_id')
				->options(\App\User::role('author')
					->get(['id', 'name'])
					->mapWithKeys(function ($item) {
						return [$item['id'] => $item['name']];
					})
					->toArray())
				->displayUsingLabels()
				->onlyOnForms()
				->help('Користувач, який створив статтю')
				->rules('required')
			,

			BelongsTo::make('Категорія', 'category', ArticleCategory::class)
			,

			Image::make('Зображення', 'image')
				->store(new StoreCroppedImages('articles', 'main', 'image'))
				->delete(new RemoveCroppedImages('articles', 'main', 'image'))
				->thumbnail(function () {
					return $this->getCroppedImage('card');
				})
				->preview(function () {
					return $this->getCroppedImage('card');
				})
				->rules(['max:8192'])
				->hideFromIndex()
			,

			Text::make('Зображення: назва', 'image_title')
				->help('Назва дається відповідно до того, що на зображенні')
				->hideFromIndex()
				->rules('required', 'max:100')
			,

			TextWithSlug::make('Назва', 'title')
				->help('Рекомендовано 60-80 символів')
				->slug('slug')
				->rules('required', 'max:100')
			,

			Slug::make('Слаг', 'slug')
				->help('Назва статті в адресній стрічці')
				->creationRules(['required', 'alpha_dash', 'unique:articles,slug'])
				->updateRules(['required', 'alpha_dash', 'unique:articles,slug,{{resourceId}}'])
				->hideFromIndex()
				->hideWhenUpdating()
				->rules('required', 'max:120')
			,

			CharcountedTextarea::make('Короткий опис', 'excerpt')
				->maxChars(499)
				->warningAt(450)
				->help('Виводиться на початку сттаті перед головним фото')
				->rules('required', 'max:499')
				->hideFromIndex()
			,

			CKEditor::make('Контент', 'body')
				->options(config('nova.ckeditor-field'))
				->help('Приклад вставки відео [youtube id="ANjX2JY77GA"]. Іноді потрібно добавляти через View HTML')
				->hideFromIndex()
				->rules('required')
			,

			BelongsTo::make('Опитування', 'poll', Poll::class)
				->nullable()
				->hideFromIndex()
			,

			Text::make('Стаття', function () {
				if (isset($this->category) && isset($this->slug))
					return "<a href='" . $this->getUrl() . "' target='_blank'>Перейти</a>";

				return '';
			})
				->asHtml()
				->exceptOnForms()
			,
			new Panel('Інформація', $this->infoFields()),
			new Panel('Джерело', $this->sourceFields()),
			new Panel('SEO', $this->metaFields()),
			MorphToMany::make('Теги', 'tags', Tag::class),
			HasMany::make('Зображення', 'images', ArticleImage::class),
		];
	}

	protected function metaFields()
	{
		return [
			CharcountedText::make('Meta: Назва', 'meta_title')
				->hideFromIndex()
				->rules(['required'])
				->maxChars(80)
				->warningAt(60)
				->help('Назва сторінки. Оптимально 50-65, максимум 80 символів')
			,
			CharcountedTextarea::make('Meta: Опис', 'meta_description')
				->hideFromIndex()
				->rules('required', 'max:300')
				->maxChars(300)
				->warningAt(250)
				->help('Опис для пошукових систем. Оптимально 170-290, максимум 300 символів')
			,
			CharcountedTextarea::make('Meta: Ключові слова', 'meta_keywords')
				->hideFromIndex()
				->maxChars(255)
				->warningAt(250)
			,
			CharcountedText::make('OG: Назва', 'og_title')
				->hideFromIndex()
				->maxChars(65)
				->warningAt(50)
				->help('Назва для соціальних мереж. Оптимально до 50, максимум 65 символів')
			,
			CharcountedTextarea::make('OG: Опис', 'og_description')
				->hideFromIndex()
				->maxChars(300)
				->warningAt(250)
				->help('Опис для соціальних мереж. Оптимально 2-4 речення. Максимум 300 символів')
			,
			Image::make('OG: Зображення', 'og_image')
				->store(new StoreCroppedImages('articles', 'og', 'og_image'))
				->delete(new RemoveCroppedImages('articles', 'og', 'og_image'))
				->thumbnail(function () {
					return $this->getCroppedImage('og');
				})
				->preview(function () {
					return $this->getCroppedImage('card');
				})
				->updateRules('max:8192')
				->hideFromIndex()
				->help('Якісне фото для соціальних мереж. Рекомендовано не менше 1200х630')
			,
		];
	}

	protected function infoFields()
	{
		return [
			Boolean::make('Активовано', 'active')
				->help('Показувати статтю на сайті')
			,
			Boolean::make('Ексклюзив', 'exclusive')
				->hideFromIndex()
				->help('Ексклюзивна стаття')
			,
			Boolean::make('Реклама', 'advertising')
				->hideFromIndex()
				->help('Рекламна стаття з поміткою')
			,
			Text::make('Перегляди', 'views')
				->hideFromIndex()
				->hideWhenCreating()
				->hideWhenUpdating()
			,
			Text::make('Рейтинг', function () {
				return $this->interesting - $this->not_interesting;
			})
				->hideFromIndex()
				->help('Рейтинг на основі відповідей цікаво/не цікаво')
			,
			DateTime::make('Створено', 'created_at')
				->format('DD.MM.YY HH:mm')
				->onlyOnDetail()
			,
			DateTime::make('Редаговано', 'updated_at')
				->format('DD.MM.YY HH:mm')
				->onlyOnDetail()
			,
			DateTime::make('Опубліковано', 'published_at')
				->format('DD.MM.YY HH:mm')
				->help('Коли стаття стане видимою на сторінках')
				->creationRules(['required'])
				->updateRules(['required'])
			,
			BelongsTo::make('Останній редагував', 'editor', User::class)
				->onlyOnDetail()
			,
		];
	}

	protected function sourceFields()
	{
		return [
			Text::make('Джерело: назва', 'source_title')
				->hideFromIndex()
				->help('Заповнювати лише при необхідності')
			,
			Text::make('Джерело: посилання', 'source_link')
				->hideFromIndex()
				->help('Заповнювати лише при необхідності')
			,
		];
	}
}
