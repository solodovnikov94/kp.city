<?php

namespace App\Nova;

use Ajhaupt7\ImageUploadPreview\ImageUploadPreview;
use App\Nova\Customization\RemoveCroppedImages;
use App\Nova\Customization\StoreCroppedImages;
use Laravel\Nova\Fields\Avatar;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Illuminate\Http\Request;
//use Sixlive\TextCopy\TextCopy;
use Laravel\Nova\Panel;

class ArticleImage extends Resource
{
	public static $model = \App\ArticleImage::class;
	public static $displayInNavigation = false;

	public static function label()
	{
		return 'Картинки статей';
	}

	public static function singularLabel()
	{
		return 'Картинка статті';
	}

	public function fields(Request $request)
	{
		return [
			ID::make('№', 'id')
				->sortable(),

			Avatar::make('Зображення', 'url')
				->thumbnail(function () {
					return $this->getCroppedImageOrError('mobile', 'jpg', 'url');
				})
				->onlyOnIndex()
			,

//			TextCopy::make('Шорт код', function () {
//				return '[image id="' . $this->id . '"]';
//			})
//			,

			ImageUploadPreview::make('Зображення', 'url')
				->store(new StoreCroppedImages('article_images', 'main', 'url'))
				->delete(new RemoveCroppedImages('article_images', 'main', 'url'))
				->thumbnail(function () {
					return $this->getCroppedImage('mobile', 'jpg', 'url');
				})
				->updateRules('max:8192')
//				->hideWhenCreating()
				->hideFromIndex()
			,

			Boolean::make('Не обрізати', 'save_aspect_ratio')
				->help('Зберегти оригінальне співвідношення сторін. При зміні параметру потрібно перезавантажити зображення. Використовувати для "квадратних" і вертикальних зображень з важливою інформацією')
				->hideWhenCreating()
				->hideFromIndex()
			,

			Text::make('Зображення: назва', 'title')
				->creationRules(['required'])
				->updateRules(['required'])
			,
		];
	}
}
