<?php

namespace App\Nova;

use Ajhaupt7\ImageUploadPreview\ImageUploadPreview;
use App\Nova\Customization\RemoveCroppedImages;
use App\Nova\Customization\StoreCroppedImages;
use Benjaminhirsch\NovaSlugField\Slug;
use Benjaminhirsch\NovaSlugField\TextWithSlug;
use ElevateDigital\CharcountedFields\CharcountedText;
use ElevateDigital\CharcountedFields\CharcountedTextarea;
use Laravel\Nova\Fields\Avatar;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Panel;

class Comment extends Resource
{
	public static $model = \App\Comment::class;
	public static $title = 'title';
	public static $group = '5 - Загальне';

	public static function label()
	{
		return 'Коментарії';
	}

	public static function singularLabel()
	{
		return 'Коментар';
	}

	public function fields(Request $request)
	{
		return [
			ID::make('№', 'id')
				->sortable(),

			BelongsTo::make('Автор', 'user', User::class)
				->exceptOnForms()
			,

			Text::make('Рейтинг', 'rating')
			,

			Textarea::make('Коментар', 'body')
			,

			Text::make('Коментар', function () {
				if (isset($this->commentable_type))
					return "<a href='" . $this->getUrl() . "' target='_blank'>Перейти</a>";
				return '';
			})
				->asHtml()
				->exceptOnForms()
			,

			Boolean::make('Активовано', 'active')
			,

			DateTime::make('Створено', 'created_at')
				->format('DD.MM.YY HH:mm:ss')
				->exceptOnForms()
			,
			DateTime::make('Редаговано', 'updated_at')
				->format('DD.MM.YY HH:mm:ss')
				->onlyOnDetail()
			,

		];
	}
}
