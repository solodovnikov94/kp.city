<?php

namespace App\Nova;

use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Textarea;

class Contact extends Resource
{
	public static $model = \App\Contact::class;
	public static $title = 'title';
	public static $group = '4 - Користувачі';
	public static $rowCounterParams = [
		[
			'field' => 'active',
			'operator' => '!=',
			'value' => 1,
			'class' => 'warning'
		]
	];

	public static function label()
	{
		return 'Контакти';
	}

	public static function singularLabel()
	{
		return 'Контакти';
	}

	public function fields(Request $request)
	{
		return [
			ID::make('№', 'id')
				->sortable(),

			Text::make('І\'мя', 'name'),
			Text::make('Емел', 'email'),
			Textarea::make('Текс', 'text'),
			Boolean::make('Переглянуто', 'active'),
			DateTime::make('Створено', 'created_at')
				->format('DD.MM.YY HH:mm'),
		];
	}
}
