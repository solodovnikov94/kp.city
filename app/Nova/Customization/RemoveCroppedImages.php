<?php

namespace App\Nova\Customization;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class RemoveCroppedImages
{
	private $key;
	private $type;
	private $field;

	public function __construct($key, $type, $field)
	{
		$this->key = $key;
		$this->type = $type;
		$this->field = $field;
	}

	public function __invoke(Request $request, $model)
	{
		foreach (config('crops.' . $this->key . '.' . $this->type . '.types') as $name => $settings) {
			Storage::disk('public')
				->delete($this->key . '/' . $model->image . '_' . $name . '.' . ($settings['e'] ?? config('crops.extension')));
		}

		return [
			$this->field => null,
		];
	}
}