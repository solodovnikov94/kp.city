<?php

namespace App\Nova\Customization;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class StoreCroppedImages
{
	private $key;
	private $type;
	private $field;

	/**
	 * StoreArticleImages constructor.
	 * @param $key : from config/crops.php. Eg: 'articles'
	 * @param $type : from config/crops.php. Eg: 'main'
	 * @param $field : field name from database
	 */
	public function __construct($key, $type, $field)
	{
		$this->key = $key;
		$this->type = $type;
		$this->field = $field;
	}

	public function __invoke(Request $request, $model)
	{
		$key = $model['image'] ?? uniqid();

		$image = Image::make($request[$this->field]);
		$image->backup();

		foreach (config('crops.' . $this->key . '.' . $this->type . '.types') as $name => $settings) {
			$image->reset();

			$ratio = ($image->width() * $image->height()) / ($settings['w'] * $settings['h']);
			$quality = $settings['q'];

			if ($quality < 95) {
				if ($ratio < 0.125) {
					$quality = $settings['q'] + 20 <= 95 ? $settings['q'] + 20 : 95;
				} elseif ($ratio >= 0.125 && $ratio < 0.25) {
					$quality = $settings['q'] + 15 <= 95 ? $settings['q'] + 15 : 95;
				} elseif ($ratio >= 0.25 && $ratio < 0.5) {
					$quality = $settings['q'] + 10 <= 95 ? $settings['q'] + 10 : 95;
				} elseif ($ratio >= 0.5 && $ratio < 0.75) {
					$quality = $settings['q'] + 5 <= 95 ? $settings['q'] + 5 : 95;
				} elseif ($ratio >= 0.75 && $ratio < 0.9) {
					$quality = $settings['q'] + 2 <= 95 ? $settings['q'] + 2 : 95;
				}
			}

			$this->saveImage(
				$image,
				$key,
				$name,
				$settings['w'],
				$settings['h'],
				$quality,
				$settings['e'] ?? config('crops.extension'),
				$settings['b'] ?? false,
				$request['save_aspect_ratio'] ?? false
			);
		}

		$image->destroy();

		return [
			$this->field => $key,
		];
	}

	private function saveImage($image, $key, $name, $width, $height, $quality, $extension, $blur = false, $original_aspect_ratio = false)
	{
		$file_path = $this->key . '/' . $key . '_' . $name . '.' . $extension;

		if ($original_aspect_ratio) {
			$image->resize($width, $height, function ($constraint) {
				$constraint->aspectRatio();
				$constraint->upsize();
			});
		} else {
			$image->fit($width, $height);
		}

		if ($blur) {
			$image->blur($blur);
		}

		$image->encode($extension, $quality);

		return Storage::disk('public')
			->put($file_path, (string)$image);
	}
}