<?php

namespace App\Nova;

use Ajhaupt7\ImageUploadPreview\ImageUploadPreview;
use App\Nova\Customization\RemoveCroppedImages;
use App\Nova\Customization\StoreCroppedImages;
use Benjaminhirsch\NovaSlugField\Slug;
use Benjaminhirsch\NovaSlugField\TextWithSlug;
use ElevateDigital\CharcountedFields\CharcountedText;
use ElevateDigital\CharcountedFields\CharcountedTextarea;
use Laravel\Nova\Fields\Avatar;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Panel;
//use Sixlive\TextCopy\TextCopy;

class Gallery extends Resource
{
	public static $model = \App\Gallery::class;
	public static $title = 'title';
	public static $search = ['title'];
	public static $group = '5 - Загальне';

	public static function label()
	{
		return 'Галереї';
	}

	public static function singularLabel()
	{
		return 'Галерея';
	}

	public function fields(Request $request)
	{
		return [
			ID::make('№', 'id')
				->sortable(),

			Avatar::make('Зображення', 'image')
				->thumbnail(function () {
					return isset($this->photos) && count($this->photos) > 0
						? $this->photos->first()->getCroppedImage('avatar')
						: null;
				})
				->onlyOnIndex()
			,

			TextWithSlug::make('Назва', 'title')
				->slug('Слаг')
				->rules('required', 'max:100')
			,

			Slug::make('Слаг', 'slug')
				->help('Назва галереї в адресній стрічці')
				->creationRules(['required', 'alpha_dash', 'unique:galleries,slug'])
				->updateRules(['required', 'alpha_dash', 'unique:galleries,slug,{{resourceId}}'])
				->hideFromIndex()
			,

//			TextCopy::make('Шорт код', function (){
//				return '[gallery id="'.$this->id.'"]';
//			})
//				->hideWhenCreating()
//				->hideWhenUpdating()
//			,

			Textarea::make('Опис', 'body')
				->creationRules(['required'])
				->updateRules(['required'])
			,
			
			new Panel('Інформація', $this->infoFields()),
			new Panel('SEO', $this->metaFields()),

			HasMany::make('Світлини', 'photos', GalleryPhoto::class),
		];
	}

	protected function metaFields()
	{
		return [
			CharcountedText::make('Meta: Назва', 'meta_title')
				->hideFromIndex()
				->maxChars(80)
				->warningAt(60)
				->help('Назва сторінки. Оптимально 50-65, максимум 80 символів')
			,
			CharcountedTextarea::make('Meta: Опис', 'meta_description')
				->hideFromIndex()
				->maxChars(300)
				->warningAt(250)
				->help('Опис для пошукових систем. Оптимально 170-290, максимум 300 символів')
			,
			CharcountedTextarea::make('Meta: Ключові слова', 'meta_keywords')
				->hideFromIndex()
				->maxChars(255)
				->warningAt(250)
			,
			CharcountedText::make('OG: Назва', 'og_title')
				->hideFromIndex()
				->maxChars(65)
				->warningAt(50)
				->help('Назва для соціальних мереж. Оптимально до 50, максимум 65 символів')
			,
			CharcountedTextarea::make('OG: Опис', 'og_description')
				->hideFromIndex()
				->maxChars(300)
				->warningAt(250)
				->help('Опис для соціальних мереж. Оптимально 2-4 речення. Максимум 300 символів')
			,
			ImageUploadPreview::make('OG: Зображення', 'og_image')
				->store(new StoreCroppedImages('galleries', 'og', 'og_image'))
				->delete(new RemoveCroppedImages('galleries', 'og', 'og_image'))
				->thumbnail(function () {
					return $this->getCroppedImage('og', 'jpg', 'og_image');
				})
				->updateRules('max:8192')
				->hideFromIndex()
//				->hideWhenCreating()
				->help('Якісне фото для соціальних мереж. Рекомендовано не менше 1200х630')
			,
		];
	}

	protected function infoFields()
	{
		return [
			Boolean::make('Активовано', 'active'),

			DateTime::make('Створено', 'created_at')
				->format('DD.MM.YY HH:mm:ss')
				->exceptOnForms()
			,
			DateTime::make('Редаговано', 'updated_at')
				->format('DD.MM.YY HH:mm:ss')
				->onlyOnDetail()
			,
		];
	}
}
