<?php

namespace App\Nova;

use Ajhaupt7\ImageUploadPreview\ImageUploadPreview;
use App\Nova\Customization\RemoveCroppedImages;
use App\Nova\Customization\StoreCroppedImages;
use Laravel\Nova\Fields\Avatar;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Number;
use Illuminate\Http\Request;
use Laravel\Nova\Panel;

class GalleryPhoto extends Resource
{
	public static $model = \App\GalleryPhoto::class;
	public static $title = 'image_title';
	public static $search = ['image_title'];
	public static $displayInNavigation = false;

	public static function label()
	{
		return 'Фотографії галерей';
	}

	public static function singularLabel()
	{
		return 'Фотографія';
	}

	public function fields(Request $request)
	{
		return [
			ID::make('№', 'id')
				->sortable(),

			Avatar::make('Зображення', 'image')
				->thumbnail(function () {
					return $this->getCroppedImage('avatar', 'jpg', 'image');
				})
				->onlyOnIndex()
			,

			Number::make('Порядок', 'order_')
				->min(1)->max(1000)->step(1)
				->sortable()
				->rules('required', 'max:100')
			,

			BelongsTo::make('Галерея', 'gallery', Gallery::class),

			ImageUploadPreview::make('Зображення', 'image')
				->store(new StoreCroppedImages('galleries', 'main', 'image'))
				->delete(new RemoveCroppedImages('galleries', 'main', 'image'))
				->thumbnail(function () {
					return $this->getCroppedImage('mobile', 'jpg', 'image');
				})
				->rules('max:8192')
				->hideFromIndex()
			,

			Text::make('Зображення: назва', 'image_title')
				->rules('required', 'max:100')
			,

			new Panel('Інформація', $this->infoFields()),

		];
	}

	protected function infoFields()
	{
		return [
			DateTime::make('Створено', 'created_at')
				->format('DD.MM.YY HH:mm:ss')
				->exceptOnForms()
			,
			DateTime::make('Редаговано', 'updated_at')
				->format('DD.MM.YY HH:mm:ss')
				->onlyOnDetail()
			,
		];
	}
}
