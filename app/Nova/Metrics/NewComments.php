<?php

namespace App\Nova\Metrics;

use App\Comment;
use Illuminate\Http\Request;
use Laravel\Nova\Metrics\Value;

class NewComments extends Value
{
    /**
     * Calculate the value of the metric.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    public function calculate(Request $request)
    {
        return $this->count($request, Comment::class);
    }

    /**
     * Get the ranges available for the metric.
     *
     * @return array
     */
    public function ranges()
    {
        return [
            30 => '30 Днів',
            60 => '60 Днів',
            365 => '365 Днів',
            'MTD' => 'З початку місяця до сьогодні',
            'QTD' => 'З початку кварталу до сьогодні',
            'YTD' => 'З початку року до сьогодні',
        ];
    }

    /**
     * Determine for how many minutes the metric should be cached.
     *
     * @return  \DateTimeInterface|\DateInterval|float|int
     */
    public function cacheFor()
    {
        // return now()->addMinutes(5);
    }

    /**
     * Get the URI key for the metric.
     *
     * @return string
     */
    public function uriKey()
    {
        return 'new-comments';
    }
}
