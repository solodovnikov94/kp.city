<?php

namespace App\Nova;

use App\Nova\Customization\RemoveCroppedImages;
use App\Nova\Customization\StoreCroppedImages;
use Benjaminhirsch\NovaSlugField\Slug;
use Benjaminhirsch\NovaSlugField\TextWithSlug;
use EmilianoTisato\GoogleAutocomplete\GoogleAutocomplete;
use ElevateDigital\CharcountedFields\CharcountedText;
use ElevateDigital\CharcountedFields\CharcountedTextarea;
use Fourstacks\NovaRepeatableFields\Repeater;
use Laravel\Nova\Fields\Avatar;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\File;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\MorphToMany;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Panel;
use Media24si\NovaYoutubeField\Youtube;
use Illuminate\Http\Request;
use Waynestate\Nova\CKEditor;

class Organization extends Resource
{
	public static $model = \App\Organization::class;
	public static $title = 'title';
	public static $search = ['title', 'meta_title', 'meta_description', 'search_text'];
	public static $with = ['tags'];
	public static $group = '3 - Організації';
	public static $rowCounterParams = [
		[
			'field' => 'active',
			'operator' => '!=',
			'value' => 1,
			'class' => 'danger'
		]
	];

	public static function label()
	{
		return 'Організації';
	}

	public static function singularLabel()
	{
		return 'Організація';
	}

	public function calculate()
	{
		return $this->result(Organization::count());
	}

	public function fields(Request $request)
	{
		return [
			ID::make('№', 'id')
				->exceptOnForms()
				->sortable()
			,

			Avatar::make('Зображення', 'image')
				->thumbnail(function () {
					return $this->getCroppedImage('brief', 'jpg', 'image');
				})
				->onlyOnIndex()
			,

			Text::make('Шорт код', function () {
				return '[organization id="' . $this->id . '"]';
			})
			,

			BelongsTo::make('Автор', 'creator', User::class)
				->exceptOnForms()
			,

			BelongsTo::make('Редактор', 'editor', User::class)
				->help('Користувач, який останній редагував статтю')
				->onlyOnDetail()
			,

			Select::make('Категорія', 'category_id')
				->options(\App\OrganizationCategory::where('parent_id', '=', 0)
					->get(['id', 'title'])
					->mapWithKeys(function ($item) {
						$tmp = [$item->id => $item->title];
						if ($item->children) {
							foreach ($item->children as $children) {
								$tmp[$children->id] = '--' . $children->title;
							}
						}
						return $tmp;
					})
					->toArray())
				->displayUsingLabels()
				->onlyOnForms()
				->rules('required')
				->help('Основна категорія')
			,

			Image::make('Зображення', 'image')
				->store(new StoreCroppedImages('organizations', 'main', 'image'))
				->delete(new RemoveCroppedImages('organizations', 'main', 'image'))
				->thumbnail(function () {
					return $this->getCroppedImage('card', 'jpg', 'image');
				})
				->hideFromIndex()
				->rules('max:8192')
			,

			Image::make('Логотип', 'logotype')
				->disk('public')
				->path('organizations')
				->storeAs(function (Request $request) {
					$key = uniqid();

					if (isset($this->logotype)) {
						$key = substr($this->logotype, 14, 13);
					}

					return $key . '_logo.' . $request->logotype->getClientOriginalExtension();
				})
				->rules('max:128')
				->hideFromIndex()
			,

			File::make('Файл', 'document')
				->disk('public')
				->path('organizations')
				->storeAs(function (Request $request) {
					$extension = $request->document->getClientOriginalExtension();

					return $this->id . '_document.' . $extension;
				})
			,

			Text::make('Оригінальна назва закладу', 'name')
				->rules('required', 'max:255')
				->hideFromIndex()
			,

			TextWithSlug::make('Назва (відображається на сайті)', 'title')
				->help('Рекомендовано 60-80 символів')
				->slug('Слаг')
				->rules('required', 'max:100')
			,

			Slug::make('Слаг', 'slug')
				->help('Назва організації в адресній стрічці')
				->creationRules(['required', 'alpha_dash'])
				->hideWhenUpdating()
				->hideFromIndex()
			,

			CharcountedTextarea::make('Короткий опис', 'excerpt')
				->maxChars(499)
				->warningAt(450)
				->help('Виводиться на початку сттаті перед головним фото')
				->rules('required', 'max:499')
				->hideFromIndex()
			,

			CKEditor::make('Контент', 'body')
				->options(config('nova.ckeditor-field'))
				->hideFromIndex()
				->rules(['required'])
			,

			Repeater::make('Графік роботи', 'schedule')
				->addField([
					'label' => 'День',
					'name' => 'day',
					'type' => 'select',
					'options' => [
						'1' => 'Понеділок',
						'2' => 'Вівторок',
						'3' => 'Середа',
						'4' => 'Четвер',
						'5' => 'П\'ятниця',
						'6' => 'Субота',
						'7' => 'Неділя',
					],
				])
				->addField([
					'label' => 'Працює від',
					'name' => 'work_from',
					'type' => 'text',
				])
				->addField([
					'label' => 'Працює до',
					'name' => 'work_to',
					'type' => 'text',
				])
				->addButtonText('Додати день')
				->hideFromIndex()
				->maximumRows(7)
			,

			Repeater::make('Перерва', 'break_time')
				->addField([
					'label' => 'Перерва від',
					'name' => 'break_from',
					'type' => 'text',
				])
				->addField([
					'label' => 'Перерва до',
					'name' => 'break_to',
					'type' => 'text',
				])
				->addButtonText('Додати перерву')
				->hideFromIndex()
				->maximumRows(1)
			,

			Repeater::make('Адреси', 'addresses')
				->addField([
					'label' => 'Адреса',
					'name' => 'address',
					'type' => 'text',
				])
				->addField([
					'label' => 'Примітка',
					'name' => 'note',
					'type' => 'text',
				])
				->addButtonText('Додати адресу')
				->maximumRows(1)
				->hideFromIndex()
			,

			Repeater::make('Емейли', 'emails')
				->addField([
					'label' => 'Email',
					'name' => 'email',
					'type' => 'text',
				])
				->addField([
					'label' => 'Примітка',
					'name' => 'note',
					'type' => 'text',
				])
				->addButtonText('Додати адресу')
				->maximumRows(1)
				->hideFromIndex()
			,

			Repeater::make('Особливості закладу', 'specifics')
				->addField([
					'label' => 'Особливість',
					'name' => 'specific',
					'type' => 'text',
				])
				->addButtonText('Додати особливість')
				->hideFromIndex()
			,

			GoogleAutocomplete::make('Адрес на карті', 'google_map_id')
				->hideFromIndex()
			,

			Repeater::make('Телефони', 'phones')
				->addField([
					'label' => 'Телефон',
					'name' => 'phone',
					'type' => 'text',
					'placeholder' => '+380 (хх) хх хх ххх, +380 (3849) х хх хх'
				])
				->addField([
					'label' => 'Примітка',
					'name' => 'note',
					'type' => 'text',
				])
				->addButtonText('Додати телефон')
				->hideFromIndex()
				->help('Приклад оформлення телефону: +380 (хх) хх хх ххх, +380 (3849) х хх хх')
			,

			Repeater::make('Сайти', 'sites')
				->addField([
					'label' => 'Адреса (повна)',
					'name' => 'url',
					'type' => 'text',
				])
				->addField([
					'label' => 'Примітка',
					'name' => 'note',
					'type' => 'text',
				])
				->addButtonText('Додати сайт')
				->hideFromIndex()
			,

			Repeater::make('Соцмережі', 'social_networks')
				->addField([
					'label' => 'Тип',
					'name' => 'type',
					'type' => 'select',
					'options' => [
						'facebook' => 'Facebook',
						'instagram' => 'Instagram',
						'vkontakte' => 'Vkontakte',
						'twitter' => 'Twitter',
						'telegram' => 'Telegram',
						'gplus' => 'Google+',
						'skype' => 'Skype',
					]
				])
				->addField([
					'label' => 'Адреса (повна)',
					'name' => 'url',
					'type' => 'text',
				])
				->addButtonText('Додати соцмережу')
				->hideFromIndex()
			,

			Youtube::make('Відео', 'video')
				->help('Відео з youtube')
				->hideFromIndex()
			,

			BelongsTo::make('Групи', 'group', OrganizationGroup::class)
				->nullable()
				->hideFromIndex()
			,

			BelongsTo::make('Опитування', 'poll', Poll::class)
				->nullable()
				->hideFromIndex()
			,

			BelongsTo::make('Галерея', 'gallery', Gallery::class)
				->nullable()
				->hideFromIndex()
			,

			Text::make('Організація', function () {
				if (isset($this->category) && isset($this->slug))
					return "<a href='" . $this->getUrl() . "' target='_blank'>Перейти</a>";

				return 'Посилання немає(';
			})
				->asHtml()
				->exceptOnForms()
			,
			new Panel('Інформація', $this->infoFields()),
			new Panel('SEO', $this->metaFields()),
			MorphToMany::make('Теги', 'tags', Tag::class),

			CharcountedTextarea::make('Пошукові запити', 'search_text')
				->hideFromIndex()
				->maxChars(600)
				->warningAt(500)
				->help('Пошук на сайті')
			,
		];
	}

	protected function metaFields()
	{
		return [
			CharcountedText::make('Meta: Назва', 'meta_title')
				->hideFromIndex()
				->rules(['required'])
				->maxChars(80)
				->warningAt(60)
				->help('Назва сторінки. Оптимально 50-65, максимум 80 символів')
			,
			CharcountedTextarea::make('Meta: Опис', 'meta_description')
				->hideFromIndex()
				->rules('required', 'max:300')
				->maxChars(300)
				->warningAt(250)
				->help('Опис для пошукових систем. Оптимально 170-290, максимум 300 символів')
			,
			CharcountedTextarea::make('Meta: Ключові слова', 'meta_keywords')
				->hideFromIndex()
				->maxChars(255)
				->warningAt(250)
			,
			CharcountedText::make('OG: Назва', 'og_title')
				->hideFromIndex()
				->maxChars(65)
				->warningAt(50)
				->help('Назва для соціальних мереж. Оптимально до 50, максимум 65 символів')
			,
			CharcountedTextarea::make('OG: Опис', 'og_description')
				->hideFromIndex()
				->maxChars(300)
				->warningAt(250)
				->help('Опис для соціальних мереж. Оптимально 2-4 речення. Максимум 300 символів')
			,
			Image::make('OG: Зображення', 'og_image')
				->store(new StoreCroppedImages('organizations', 'og', 'og_image'))
				->delete(new RemoveCroppedImages('organizations', 'og', 'og_image'))
				->preview(function () {
					return $this->getCroppedImage('og', 'jpg', 'og_image');
				})
				->updateRules('max:8192')
				->hideFromIndex()
				->help('Якісне фото для соціальних мереж. Рекомендовано не менше 1200х630')
			,
		];
	}

	protected function infoFields()
	{
		return [
			Boolean::make('Активовано', 'active')
				->canSee(function () {
					return \Auth::user()->hasRole('developer');
				})
				->help('Показувати організацію на сайті')
			,
			Text::make('Перегляди', 'views')
				->hideWhenCreating()
				->hideWhenUpdating()
				->hideFromIndex()
			,
			DateTime::make('Створено', 'created_at')
				->format('DD.MM.YY HH:mm')
				->onlyOnDetail()
			,
			DateTime::make('Редаговано', 'updated_at')
				->format('DD.MM.YY HH:mm')
				->onlyOnDetail()
			,
			BelongsTo::make('Останній редагував', 'editor', User::class)
				->onlyOnDetail()
			,
		];
	}
}
