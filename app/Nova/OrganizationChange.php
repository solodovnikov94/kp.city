<?php

namespace App\Nova;

use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Textarea;


class OrganizationChange extends Resource
{
	public static $model = \App\OrganizationChange::class;
	public static $group = '3 - Організації';
	public static $rowCounterParams = [
		[
			'field' => 'active',
			'operator' => '!=',
			'value' => 1,
			'class' => 'success'
		]
	];
	public static function label()
	{
		return 'Запроп. зміни';
	}

	public static function singularLabel()
	{
		return 'Запропоновані зміни';
	}

	public function fields(Request $request)
	{
		return [
			ID::make('№', 'id')
				->sortable(),
			BelongsTo::make('Користувач', 'user', User::class)
				->exceptOnForms()
			,
			BelongsTo::make('Організація', 'organization', Organization::class)
				->exceptOnForms()
			,
			Boolean::make('Переглянуто', 'active'),
			Textarea::make('Зміни', 'message'),
		];
	}
}
