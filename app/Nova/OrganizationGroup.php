<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Illuminate\Http\Request;
use Laravel\Nova\Panel;

class OrganizationGroup extends Resource
{
	public static $model = \App\OrganizationGroup::class;
	public static $title = 'title';
	public static $search = ['title'];
	public static $group = '3 - Організації';

	public static function label()
	{
		return 'Групи організацій';
	}

	public static function singularLabel()
	{
		return 'Група орагнізації';
	}

	public function fields(Request $request)
	{
		return [
			ID::make('№', 'id')
				->exceptOnForms()
				->sortable()
			,

			Text::make('Назва групи', 'title')
			,
		];
	}
}
