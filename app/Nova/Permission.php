<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\MorphToMany;

class Permission extends Resource
{
	public static $model = \App\Permission::class;
	public static $title = 'name';
	public static $search = ['name'];
	public static $group = '4 - Користувачі';

	public static function label()
	{
		return 'Права доступу';
	}

	public static function singularLabel()
	{
		return 'Право доступу';
	}

	/**
	 * Get the fields displayed by the resource.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return array
	 */
	public function fields(Request $request)
	{
		return [
			ID::make()->sortable()
			,

			Text::make('Назва права', 'name')
				->sortable()
				->rules('required', 'max:255')
			,

			Text::make('Назва для відображення', 'display_name')
				->sortable()
				->rules('required', 'max:255')
			,

			MorphToMany::make('Прикриплена до ролів', 'roles', Role::class)
		];
	}

	/**
	 * Get the cards available for the request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return array
	 */
	public function cards(Request $request)
	{
		return [];
	}

	/**
	 * Get the filters available for the resource.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return array
	 */
	public function filters(Request $request)
	{
		return [];
	}

	/**
	 * Get the lenses available for the resource.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return array
	 */
	public function lenses(Request $request)
	{
		return [];
	}

	/**
	 * Get the actions available for the resource.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return array
	 */
	public function actions(Request $request)
	{
		return [];
	}
}
