<?php

namespace App\Nova;

use App\Nova\Customization\RemoveCroppedImages;
use App\Nova\Customization\StoreCroppedImages;
use Benjaminhirsch\NovaSlugField\Slug;
use Benjaminhirsch\NovaSlugField\TextWithSlug;
use Laravel\Nova\Fields\Avatar;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\MorphToMany;
use Laravel\Nova\Fields\Text;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Panel;

class Poll extends Resource
{
	public static $model = \App\Poll::class;
	public static $title = 'title';
	public static $search = ['title'];
	public static $group = '5 - Загальне';

	public static function label()
	{
		return 'Опитування';
	}

	public static function singularLabel()
	{
		return 'Опитування';
	}

	public function fields(Request $request)
	{
		return [
			ID::make('№', 'id')
				->sortable(),

//			Avatar::make('Зображення', 'image')
//				->thumbnail(function () {
//					return $this->getCroppedImage('avatar');
//				})
//				->onlyOnIndex()
//			,
//
//			ImageUploadPreview::make('Зображення', 'image')
//				->store(new StoreCroppedImages('polls', 'main', 'image'))
//				->delete(new RemoveCroppedImages('polls', 'main', 'image'))
//				->thumbnail(function () {
//					return $this->getCroppedImage('mobile', 'jpg', 'image');
//				})
//				->updateRules('max:8192')
//				->hideFromIndex()
//			,

			TextWithSlug::make('Назва', 'title')
				->slug('slug')
				->rules('required', 'max:100')
			,

			Slug::make('Слаг', 'slug')
				->help('Назва питування в адресній стрічці')
				->creationRules(['required', 'alpha_dash', 'unique:polls,slug'])
				->updateRules(['required', 'alpha_dash', 'unique:polls,slug,{{resourceId}}'])
				->hideFromIndex()
				->hideWhenUpdating()
			,

			Text::make('Опитування', function () {
				return "<a href='" . $this->getUrl() . "' target='_blank'>Перейти</a>";
			})
				->asHtml()
				->exceptOnForms()
			,

			Textarea::make('Опис', 'description'),
			new Panel('Інформація', $this->infoFields()),
			new Panel('SEO', $this->metaFields()),

			HasMany::make('Відповіді', 'answers', PollAnswer::class),
//			MorphToMany::make('Теги', 'tags', Tag::class),
		];
	}

	protected function metaFields()
	{
		return [
			Text::make('Meta: Назва', 'meta_title')->hideFromIndex(),
			Text::make('Meta: Ключові слова', 'meta_keywords')->hideFromIndex(),
			Textarea::make('Meta: Опис', 'meta_description')->hideFromIndex(),
			Text::make('OG: Назва', 'og_title')->hideFromIndex(),
			Textarea::make('OG: Опис', 'og_description')->hideFromIndex(),
			Image::make('OG: Зображення', 'og_image')
				->store(new StoreCroppedImages('polls', 'og', 'og_image'))
				->delete(new RemoveCroppedImages('polls', 'og', 'og_image'))
				->thumbnail(function () {
					return $this->getCroppedImage('og', 'jpg', 'og_image');
				})
				->preview(function () {
					return $this->getCroppedImage('og', 'jpg', 'og_image');
				})
				->updateRules('max:8192')
				->hideFromIndex()
			,
		];
	}

	protected function infoFields()
	{
		return [
			Boolean::make('Активовано', 'active'),
			Boolean::make('На головну', 'home_page'),
			Boolean::make('Сторінка опитувань', 'separate'),
			Text::make('Перегляди', 'views')
				->exceptOnForms()
				->onlyOnDetail()
			,
//			Text::make('Відповіді', function () {
//				return $this->answers()
//			})
//				->exceptOnForms()
//			,
			DateTime::make('Опубліковано', 'published_at')
				->format('DD.MM.YY HH:mm')
				->help('Коли опитування стане видимим на сторінках')
				->creationRules(['required'])
				->updateRules(['required'])
			,

			DateTime::make('Деактивовано', 'deactivation_at')
				->format('DD.MM.YY HH:mm')
				->help('Коли опитування стане не активним на сторінках')
				->hideFromIndex()
			,

			DateTime::make('Створено', 'created_at')
				->format('DD.MM.YY HH:mm:ss')
				->exceptOnForms()
				->onlyOnDetail()
			,
			DateTime::make('Редаговано', 'updated_at')
				->format('DD.MM.YY HH:mm:ss')
				->onlyOnDetail()
			,
		];
	}
}
