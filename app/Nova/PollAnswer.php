<?php

namespace App\Nova;

use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Number;
use Illuminate\Http\Request;
use Laravel\Nova\Panel;

class PollAnswer extends Resource
{
	public static $model = \App\PollAnswer::class;
	public static $displayInNavigation = false;

	public static function label()
	{
		return 'Відповіді Опитувань';
	}

	public static function singularLabel()
	{
		return 'Відповідь Опитування';
	}

	public function fields(Request $request)
	{
		return [
			ID::make('№', 'id')
				->sortable(),

			Number::make('Порядок', 'order_')
				->min(1)->max(1000)->step(1)
				->sortable()
				->rules('required', 'max:100')
			,

			Text::make('Відповідь', 'title')
				->sortable()
				->rules('required', 'max:255')
			,

			Text::make('Сума голосів', function () {
				return $this->scores->count();
			})
				->sortable()
			,

			HasMany::make('Статистика', 'scores', PollAnswerScore::class),
		];
	}
}
