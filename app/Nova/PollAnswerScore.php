<?php

namespace App\Nova;

use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Illuminate\Http\Request;
use Laravel\Nova\Panel;

class PollAnswerScore extends Resource
{
	public static $model = \App\PollAnswerScore::class;
	public static $displayInNavigation = false;

	public static function label()
	{
		return 'Статистика відповідей опитувань';
	}

	public static function singularLabel()
	{
		return 'Статистика відповіді опитування';
	}

	public function fields(Request $request)
	{
		return [
			ID::make('№', 'id')
				->sortable()
			,

			BelongsTo::make('Відповідь', 'answer', PollAnswer::class),
			BelongsTo::make('Користувач', 'user', User::class),

			Text::make('IP', 'user_ip'),
			Text::make('User Agent', 'user_agent'),
			DateTime::make('Створено', 'created_at')
				->format('D.MM.YY HH:mm:ss')
			,
		];
	}
}
