<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\MorphToMany;

class Role extends Resource
{
	public static $model = \App\Role::class;
	public static $title = 'name';
	public static $search = ['name'];
	public static $group = '4 - Користувачі';

	public static function label()
	{
		return 'Ролі';
	}

	public static function singularLabel()
	{
		return 'Роль';
	}

	/**
	 * Get the fields displayed by the resource.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return array
	 */
	public function fields(Request $request)
	{
		return [
			ID::make()->sortable()
			,

			Text::make('Назва ролі', 'name')
				->sortable()
				->rules('required', 'max:255')
			,

			Text::make('Назва для відображення', 'display_name')
				->sortable()
				->rules('required', 'max:255')
			,

			MorphToMany::make('Права доступу', 'permissions', Permission::class)
			,

		];
	}

	/**
	 * Get the cards available for the request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return array
	 */
	public function cards(Request $request)
	{
		return [];
	}

	/**
	 * Get the filters available for the resource.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return array
	 */
	public function filters(Request $request)
	{
		return [];
	}

	/**
	 * Get the lenses available for the resource.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return array
	 */
	public function lenses(Request $request)
	{
		return [];
	}

	/**
	 * Get the actions available for the resource.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return array
	 */
	public function actions(Request $request)
	{
		return [];
	}
}
