<?php

namespace App\Nova;

use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Panel;
use ElevateDigital\CharcountedFields\CharcountedTextarea;

class ShortNews extends Resource
{
	public static $model = \App\ShortNews::class;
	public static $title = 'title';
	public static $group = '1 - Новини';

	public static function label()
	{
		return 'Короткі новини';
	}

	public static function singularLabel()
	{
		return 'Коротка новина';
	}

	public function fields(Request $request)
	{
		return [
			ID::make('№', 'id')
				->sortable()
			,

			BelongsTo::make('Редактор', 'editor', User::class)
				->help('Користувач, який останній редагував статтю')
				->onlyOnDetail()
			,

			CharcountedTextarea::make('Контент', 'body')
				->maxChars(499)
				->warningAt(450)
				->help('Текст короткої новини')
				->rules('required', 'max:499')
			,

			new Panel('Інформація', $this->infoFields()),

		];
	}

	protected function infoFields()
	{
		return [
			Boolean::make('Активовано', 'active')
				->help('Показувати новину на сайті')
			,

//			Boolean::make('Ексклюзив', 'exclusive')
//				->hideFromIndex()
//				->help('Ексклюзивна новина')
//			,
			DateTime::make('Створено', 'created_at')
				->format('DD.MM.YY HH:mm')
				->onlyOnDetail()
			,
			DateTime::make('Редаговано', 'updated_at')
				->format('DD.MM.YY HH:mm')
				->onlyOnDetail()
			,
			DateTime::make('Опубліковано', 'published_at')
				->format('DD.MM.YY HH:mm')
				->help('Коли стаття стане видимою на сторінках')
				->creationRules(['required'])
				->updateRules(['required'])
			,
			BelongsTo::make('Останній редагував', 'editor', User::class)
				->onlyOnDetail()
			,
		];
	}
}
