<?php

namespace App\Nova;

use Ajhaupt7\ImageUploadPreview\ImageUploadPreview;
use App\Nova\Customization\RemoveCroppedImages;
use App\Nova\Customization\StoreCroppedImages;
use Benjaminhirsch\NovaSlugField\Slug;
use Benjaminhirsch\NovaSlugField\TextWithSlug;
use ElevateDigital\CharcountedFields\CharcountedText;
use ElevateDigital\CharcountedFields\CharcountedTextarea;
use Laravel\Nova\Fields\Avatar;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\MorphedByMany;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Trix;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Panel;
use Waynestate\Nova\CKEditor;


class Tag extends Resource
{
	public static $model = \App\Tag::class;
	public static $title = 'title';
	public static $search = ['title'];
	public static $group = '5 - Загальне';

	public static function label()
	{
		return 'Теги';
	}

	public static function singularLabel()
	{
		return 'Тег';
	}

	public function fields(Request $request)
	{
		return [
			ID::make('№', 'id')
				->exceptOnForms()
				->sortable()
			,

			Avatar::make('Зображення', 'og_image')
				->thumbnail(function () {
					return $this->getCroppedImage('avatar', 'jpg', 'og_image');
				})
				->onlyOnIndex()
			,

			TextWithSlug::make('Назва', 'title')
				->help('Рекомендовано 60-80 символів')
				->slug('Слаг')
				->rules('required', 'max:100')
			,

			Slug::make('Слаг', 'slug')
				->help('Назва тега в адресній стрічці')
				->creationRules(['required', 'alpha_dash', 'unique:articles,slug'])
				->updateRules(['required', 'alpha_dash', 'unique:articles,slug,{{resourceId}}'])
				->hideFromIndex()
			,

			Text::make('Тег', function () {
				if (isset($this->slug)) {
					return "<a href='". $this->getUrl() ."' target='_blank'>Перейти</a>";
				}
				return '';
			})
				->asHtml()
				->exceptOnForms()
			,

			CKEditor::make('Контент', 'body')
				->options(config('nova.ckeditor-field'))
				->hideFromIndex()
				->rules(['required'])
			,

			new Panel('Інформація', $this->infoFields()),
			new Panel('SEO', $this->metaFields()),
			MorphedByMany::make('Статті', 'articles', Article::class),
			MorphedByMany::make('Новини', 'news', News::class),
			MorphedByMany::make('Організації', 'organizations', Organization::class),
//			MorphedByMany::make('Опитування', 'polls', Poll::class),
		];
	}

	protected function metaFields()
	{
		return [
			CharcountedText::make('Meta: Назва', 'meta_title')
				->hideFromIndex()
				->rules(['required'])
				->maxChars(80)
				->warningAt(60)
				->help('Назва сторінки. Оптимально 50-65, максимум 80 символів')
			,
			CharcountedTextarea::make('Meta: Опис', 'meta_description')
				->hideFromIndex()
				->rules('required', 'max:300')
				->maxChars(300)
				->warningAt(250)
				->help('Опис для пошукових систем. Оптимально 170-290, максимум 300 символів')
			,
			CharcountedTextarea::make('Meta: Ключові слова', 'meta_keywords')
				->hideFromIndex()
				->maxChars(255)
				->warningAt(250)
			,
			CharcountedText::make('OG: Назва', 'og_title')
				->hideFromIndex()
				->maxChars(65)
				->warningAt(50)
				->help('Назва для соціальних мереж. Оптимально до 50, максимум 65 символів')
			,
			CharcountedTextarea::make('OG: Опис', 'og_description')
				->hideFromIndex()
				->maxChars(300)
				->warningAt(250)
				->help('Опис для соціальних мереж. Оптимально 2-4 речення. Максимум 300 символів')
			,
			ImageUploadPreview::make('OG: Зображення', 'og_image')
				->store(new StoreCroppedImages('tags', 'og', 'og_image'))
				->delete(new RemoveCroppedImages('tags', 'og', 'og_image'))
				->thumbnail(function () {
					return $this->getCroppedImage('og', 'jpg', 'og_image');
				})
				->updateRules('max:8192')
				->hideFromIndex()
				->hideWhenCreating()
				->help('Якісне фото для соціальних мереж. Рекомендовано не менше 1200х630')
			,
		];
	}

	protected function infoFields()
	{
		return [
			Boolean::make('Активовано', 'active')
				->help('Показувати тег на сайті')
			,
			DateTime::make('Створено', 'created_at')
				->format('DD.MM.YY HH:mm:ss')
				->onlyOnDetail()
			,
			DateTime::make('Редаговано', 'updated_at')
				->format('DD.MM.YY HH:mm:ss')
				->onlyOnDetail()
			,
		];
	}
}
