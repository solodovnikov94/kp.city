<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Traits\Crops;
use App\Traits\Relations\OrganizationRelations;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class Organization extends Model
{
	use OrganizationRelations, SoftDeletes, Crops;

	protected $casts = [
		'schedule' => 'array',
		'break_time' => 'array',
		'addresses' => 'array',
		'emails' => 'array',
		'phones' => 'array',
		'sites' => 'array',
		'social_networks' => 'array',
		'specifics' => 'array'
	];
	protected $table = 'organizations';
	protected $crops_key = 'organizations';
	protected $dates = ['created_at', 'updated_at', 'deleted_at'];
	protected $days = [
		1 => 'Пн',
		2 => 'Вт',
		3 => 'Ср',
		4 => 'Чт',
		5 => 'Пт',
		6 => 'Сб',
		7 => 'Нд',
	];

	public function scopeActive($query)
	{
		return $query->where('active', true);
	}

	public function save(array $options = [])
	{
		$authId = \Auth::id();
		$exists = false;
		
		if (!$this->exists) {
			$exists = true;
			$this->creator_id = $authId;
			$this->meta_title = $this->title." у місті Кам'янець-Подільський - Портал міста KP.CITY";
			$this->meta_description = "Інформація про ".$this->title." міста Кам'янця-Подільського. Адреса та контакти, рейтинги та відгуки користувачів на порталі Кам'янця-Подільського KP.CITY";
			$this->meta_keywords = $this->title.", адреса, рейтинги, відгуки, kp.city";
			$category = OrganizationCategory::where('id', $this->category_id)->first();
			$this->search_text = $category->title . ' ' . $category->parent->title;
		}

		$this->editor_id = $authId;

		parent::save($options);

		\App\Notifications\Telegram::organization($exists, $this);
	}

	public function getUrl()
	{
		if($this->id && $this->category->parent) {
			return url('organizations/'.$this->category->parent->slug .'/'.$this->category->slug.'/'.$this->id.'-'.$this->slug);
		}
		return '';
	}

	public function getDays()
	{
		return $this->days;
	}

	public function scheduleCollect()
	{
		$collect = collect($this->schedule);
		$firstDay = $collect->first();

		if(is_array($firstDay)) {
			$workFrom = $firstDay['work_from'];
			$workTo = $firstDay['work_to'];

			foreach ($collect as $workDay) {
				if($workFrom != $workDay['work_from'] || $workTo != $workDay['work_to']) {
					$workFrom = false;
					break;
				}
			}

			if($workFrom) {
				$collect->first_day = $firstDay['day'];
				$collect->last_day = $collect->last()['day'];
			}
		}

		$array = array_diff_key(format_array($this->getDays()), format_array($collect->groupBy('day')->toArray()));
		$weekends = '';
		$count = 0;
		foreach ($array as $key => $value) {
			$weekends .= $this->getDays()[$key] . ', ';
			$count++;
		}
		$collect->weekends = substr($weekends, 0, -2);
		$collect->count_day = $count;

		return $collect;
	}

	public function getToday()
	{
		if(count($this->scheduleCollect()) > 1) {
			$today = $this->scheduleCollect()->firstWhere('day', $this->numberDay);
			if($today['work_from'] && $today['work_to']) {
				return $today;
			}
			return false;
		}
		return null;
	}

	public function getNumberDayAttribute()
	{
		return Carbon::now()->format('N');
	}

	public function convertToDate($time)
	{
		$todayDate =  Carbon::now()->format('Y-m-d');

		return Carbon::parse($todayDate . ' ' . $time);
	}

	public function isOpen()
	{
		$todayParam = $this->getToday();
		$workFrom = $this->convertToDate($todayParam['work_from']);
		$workTo = $this->convertToDate($todayParam['work_to']);

		if ($workFrom > $workTo) {
			if ($workFrom < now()) {
				$workTo->addDay();
			} else {
				$workFrom->subDay();
			}
		}

		if($todayParam === null) {
			return null;
		}

		$todayDate = Carbon::now();

		if($todayParam && ($todayDate > $workFrom && $todayDate < $workTo)) {
			$breakTime = collect($this->break_time)->first();

			if(is_array($breakTime) && $todayDate > $this->convertToDate($breakTime['break_from'])
				&& $todayDate < $this->convertToDate($breakTime['break_to'])) {
				return 'break';
			}
			return true;

		} else {
			return false;
		}
	}

	public function getOrganizationRating()
	{
		return $this->comments()->where([
			['parent_id', '=', null],
			['active', '=', 1]
		])->avg('rating');
	}
}
