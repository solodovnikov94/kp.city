<?php

namespace App;

use App\Traits\Crops;
use App\Traits\Relations\OrganizationCategoryRelations;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class OrganizationCategory extends Model
{
	use OrganizationCategoryRelations, SoftDeletes, Crops;

	protected $table = 'organization_categories';
	protected $crops_key = 'organization_categories';

	public function scopeActive($query)
	{
		return $query->where('active', true);
	}

	public function scopeMain($query)
	{
		return $query->where('parent_id', '=', 0)->orWhere('parent_id', '=', null);
	}

	public function getOrganizationsCount()
	{
		if ($this->parent_id) {
			return $this->organizations()->active()->count();
		}

		return DB::select("SELECT count(*) as `organizations_count` FROM `organizations`
			WHERE `active` = 1 AND `category_id` in ( SELECT id FROM `organization_categories` 
			WHERE `parent_id` = ( SELECT `id` FROM `organization_categories` WHERE `id` = ".$this->id." ) )")[0]->organizations_count;
	}

	public function getUrl()
	{
		if(!$this->id) {
			return null;
		}

		if ($this->parent){
			return url('organizations/' . $this->parent->slug . '/' . $this->slug);
		}
		return url('organizations/' . $this->slug);
	}

}
