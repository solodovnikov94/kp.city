<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrganizationChange extends Model
{
	use SoftDeletes;

	public function save(array $options = [])
	{
		parent::save($options);
		\App\Notifications\Telegram::organizationChanges();
	}

	public function user()
	{
		return $this->belongsTo(User::class, 'user_id');
	}

	public function organization()
	{
		return $this->belongsTo(Organization::class, 'organization_id');
	}
}
