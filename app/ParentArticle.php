<?php

namespace App;

use App\Traits\Relations\ParentArticleRelations;
use App\Traits\Scopes\ParentArticleScopes;
use App\Traits\Crops;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ParentArticle extends Model
{
	use ParentArticleScopes, ParentArticleRelations, SoftDeletes, Crops;

	protected $table = 'articles';
	protected $crops_key = 'articles';
	protected $dates = ['created_at', 'updated_at', 'deleted_at', 'published_at'];

	public function save(array $options = [])
	{
		$authId = \Auth::id();
		$exists = false;

		if (!$this->exists) {
			$this->creator_id = $authId;
			$exists = true;
		}

		$this->editor_id = $authId;

		parent::save($options);

		\App\Notifications\Telegram::article($exists, $this);
	}

	public function isPublished()
	{
		return boolval($this->active && ($this->published_at <= Carbon::now()));
	}

	public function bestThanWeek()
	{
		return $this->where('published_at', '>', Carbon::now()->subDays(7))
			->orderBy('views', 'desc')
			->active()->published()
			->take(6)
			->get();
	}

	public function bestThanMonth()
	{
		return $this->where('published_at', '>', Carbon::now()->subMonth(1))
			->orderBy('views', 'desc')
			->active()->published()
			->take(6)
			->get();
	}
}
