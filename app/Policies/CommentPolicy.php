<?php

namespace App\Policies;

use Auth;
use Illuminate\Auth\Access\HandlesAuthorization;

class CommentPolicy
{
	use HandlesAuthorization;

	public function viewAny()
	{
		if(Auth::user()->hasRole('developer')){
			return true;
		}
		return false;
	}

	public function view()
	{
		if(Auth::user()->hasRole('developer')){
			return true;
		}
		return false;
	}

	public function create()
	{
		if(Auth::user()->hasRole('developer')){
			return true;
		}
		return false;
	}

	public function update()
	{
		if(Auth::user()->hasRole('developer')){
			return true;
		}
		return false;
	}

	public function delete()
	{
		if(Auth::user()->hasRole('developer')){
			return true;
		}
		return false;
	}

	public function restore()
	{
		if(Auth::user()->hasRole('developer')){
			return true;
		}
		return false;
	}

	public function forceDelete()
	{
		if(Auth::user()->hasRole('developer')){
			return true;
		}
		return false;
	}
}
