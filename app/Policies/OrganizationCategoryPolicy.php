<?php

namespace App\Policies;

use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Access\HandlesAuthorization;

class OrganizationCategoryPolicy
{
    use HandlesAuthorization;

    public function viewAny()
    {
        return true;
    }

    public function view()
    {
        if (Auth::user()->hasRole('developer')) {
            return true;
        }
        return false;
    }

    public function create()
    {
        if (Auth::user()->hasRole('developer')) {
            return true;
        }
        return false;
    }

    public function update()
    {
        if (Auth::user()->hasRole('developer')) {
            return true;
        }
        return false;
    }

    public function delete()
    {
        if (Auth::user()->hasRole('developer')) {
            return true;
        }
        return false;
    }

    public function restore()
    {
        if (Auth::user()->hasRole('developer')) {
            return true;
        }
        return false;
    }

    public function forceDelete()
    {
        if (Auth::user()->hasRole('developer')) {
            return true;
        }
        return false;
    }
}
