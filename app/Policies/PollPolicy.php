<?php

namespace App\Policies;

use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Access\HandlesAuthorization;

class PollPolicy
{
    use HandlesAuthorization;

    public function viewAny()
    {
        return true;
    }

    public function view()
    {
        return true;
    }

    public function create()
    {
        return true;
    }

    public function update()
    {
        return true;
    }

    public function delete()
    {
        if (Auth::user()->hasRole('developer')) {
            return true;
        }
        return false;
    }

    public function restore()
    {
        return true;
    }

    public function forceDelete()
    {
        if (Auth::user()->hasRole('developer')) {
            return true;
        }
        return false;
    }
}
