<?php

namespace App;

use App\Traits\Crops;
use App\Traits\Relations\PollRelations;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Poll extends Model
{
	use PollRelations, SoftDeletes, Crops;

	protected $table = 'polls';
	protected $crops_key = 'polls';
	protected $dates = ['created_at', 'updated_at', 'published_at', 'deleted_at', 'deactivation_at'];

	public function save(array $options = [])
	{
		if (!$this->exists) {
			$this->meta_title = $this->title . " - Опитування на порталі Кам'янця-Подільського";
			$this->meta_description = "Нам цікава ваша думка стосовно питання \"" . $this->title ."\". Головний портал Кам'янця-Подільського - KP.CITY";
			$this->meta_keywords = $this->title . ", опитування, kp.city";
		}

		$this->deactivation_at = $this->deactivation_at ?: null;

		parent::save($options);
	}

	public function scopeActive($query)
	{
		return $query->where('active', true);
	}

	public function scopeSeparate($query)
	{
		return $query->where('separate', true);
	}

	public function scopePublished($query)
	{
		return $query->where('published_at', '<=', now());
	}

	public function scopeNotDeactivation($query)
	{
		return $query->where('deactivation_at', '>=', now())->oRwhere('deactivation_at', '=', null);
	}

	// poll active to vote?
	public function isActiveToVote()
	{
		if ($this->deactivation_at == null) {
			return true;
		}
		return $this->deactivation_at >= now() ? true : false;
	}

	// published to index?
	public function isPublished()
	{
		return boolval($this->active && ($this->published_at <= now()));
	}

	public function getUrl()
	{
		if ($this->id) {
			return url('polls/' . $this->id . '-' . $this->slug);
		}

		return null;
	}

	public function pollAnswers()
	{
		return $this->answers->sortBy('order_');
	}

	public function pollResultAnswers()
	{
		return $this->answers()->withCount('scores')->orderBy('scores_count', 'desc')->get();
	}

	public function pollAnswersIds()
	{
		return $this->pollAnswers()->pluck('id')->toArray();
	}

	public function totalVotes()
	{
		return PollAnswerScore::where('poll_id', $this->id)->count();
	}

	// user can voted?
	public function isVoted()
	{
		if (!$this->isActiveToVote()) {
			return false;
		}

		if (request()->cookie('vote') == $this->slug) {
			return false;
		}

		$query = PollAnswerScore::whereIn('poll_answer_id', $this->pollAnswersIds());
		$countAnswer = 0;

		if (auth()->check()) {
			$countAnswer = $query->where('user_id', auth()->user()->id)->count();
		}

		if ($countAnswer) {
			return false;
		}

		$countAnswer = $query->where([
			['user_ip', '=', request()->ip()],
			['user_agent', '=', request()->header('user-agent')]
		])->count();

		if ($countAnswer) {
			return false;
		}

		return true;
	}
}
