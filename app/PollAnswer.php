<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PollAnswer extends Model
{
	use SoftDeletes;

	protected $table = 'poll_answers';
	protected $dates = ['created_at', 'updated_at', 'deleted_at'];

	public function poll()
	{
		return $this->belongsTo(Poll::class, 'poll_id', 'id');
	}

	public function scores()
	{
		return $this->hasMany(PollAnswerScore::class, 'poll_answer_id', 'id');
	}
}
