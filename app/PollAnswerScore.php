<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PollAnswerScore extends Model
{
	protected $table = 'poll_answer_scores';
}
