<?php

namespace App\Providers;

use Carbon\CarbonInterval;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		\URL::forceScheme(env('URL_SCHEMA', 'http'));
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register()
	{
		CarbonInterval::setLocale('uk');
	}
}
