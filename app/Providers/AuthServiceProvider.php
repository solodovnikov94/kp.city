<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
	/**
	 * The policy mappings for the application.
	 *
	 * @var array
	 */
	protected $policies = [
		\App\Article::class => \App\Policies\ArticlePolicy::class,
		\App\ArticleCategory::class => \App\Policies\ArticleCategoryPolicy::class,
		\App\ArticleImage::class => \App\Policies\ArticleImagePolicy::class,
		\App\Comment::class => \App\Policies\CommentPolicy::class,
		\App\Gallery::class => \App\Policies\GalleryPolicy::class,
		\App\GalleryPhoto::class => \App\Policies\GalleryPhotoPolicy::class,
		\App\News::class => \App\Policies\NewsPolicy::class,
		\App\NewsCategory::class => \App\Policies\NewsCategoryPolicy::class,
		\App\Organization::class => \App\Policies\OrganizationPolicy::class,
		\App\OrganizationCategory::class => \App\Policies\OrganizationCategoryPolicy::class,
		\App\OrganizationGroup::class => \App\Policies\OrganizationGroupPolicy::class,
		\App\Permission::class => \App\Policies\PermissionPolicy::class,
		\App\Poll::class => \App\Policies\PollPolicy::class,
		\App\PollAnswer::class => \App\Policies\PollAnswerPolicy::class,
		\App\Role::class => \App\Policies\RolePolicy::class,
		\App\Tag::class => \App\Policies\TagPolicy::class,
		\App\User::class => \App\Policies\UserPolicy::class,
	];

	/**
	 * Register any authentication / authorization services.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->registerPolicies();

		//
	}
}
