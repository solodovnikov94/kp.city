<?php

namespace App\Providers;

use App\Article;
use App\News;
use App\Organization;
use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider
{
	private function getContent()
	{
		return [
			'news' => $this->getNews(),
			'articles' => $this->getArticles(),
			'organizations' => $this->getOrganizations(),
			'instagram_media' => instagram_media(),
		];
	}

	private function getCurrencies()
	{
		return \Cache::remember('currency_cache', 180, function () {
			$usd = $this->getCurrency('USD');
			$eur = $this->getCurrency('EUR');
			return [$usd, $eur];
		});
	}

	public function getCurrency($currencyCode)
	{
		$property = 'sale_rate';
		$currencies = \App\Currency::orderBy('id', 'desc')->where('currency', $currencyCode)->take(2)->get();
		$currency = $currencies->first();
		$currency->strengthened = strengthened($currency->$property, $currencies->last()->$property);
		$currency->value = number_format($currency->$property, 2, '.', '');
		return $currency;
	}

	private function getWeather()
	{
		return \Cache::remember('weather_cache', 60, function () {
			return \App\Weather::orderBy('id', 'desc')->first();
		});
	}

	private function getNews()
	{
		return \Cache::remember('news_cache', config('cache.query_cache'), function () {
			return News::active()->with('category')->published()->newest()->take(6)->get();
		});
	}

	private function getArticles()
	{
		return \Cache::remember('articles_cache', config('cache.query_cache'), function () {
			return Article::active()->with('category')->published()->newest()->take(6)->get();
		});
	}


	private function getOrganizations()
	{
		return \Cache::remember('organizations_cache', config('cache.query_cache'), function () {
			return Organization::active()->take(6)->get();
		});
	}

	public function getPopularOrganization()
	{
		return \Cache::remember('popular_organizations_cache', config('cache.query_cache'), function () {
			return Organization::with('category')->active()->take(7)->orderBy('views', 'desc')->get();
		});
	}

	public function getPopularOrganizationCategory()
	{
		return \Cache::remember('popular_category_organizations_cache', config('cache.query_cache'), function () {
			return \App\OrganizationCategory::with('parent')->withCount('organizations')
				->where('parent_id', '!=', 0)->active()->take(10)->orderBy('organizations_count', 'desc')->get();
		});
	}

	public function boot()
	{
		view()->composer(['layouts.layouts-main', 'errors::404'], function ($view) {
			$view->with($this->getContent());
		});

		view()->composer(['layouts.layouts-main', 'errors::403'], function ($view) {
			$view->with($this->getContent());
		});

		view()->composer(['layouts.layouts-main', 'errors::500'], function ($view) {
			$view->with($this->getContent());
		});

		view()->share('popularOrganizations', $this->getPopularOrganization());
		view()->share('popularCategoryOrganizations', $this->getPopularOrganizationCategory());
		view()->share('currencies', $this->getCurrencies());
		view()->share('weather', $this->getWeather());
	}

	public function register()
	{

	}
}
