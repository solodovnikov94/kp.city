<?php

namespace App;

use Laravel\Nova\GlobalSearch;

class Search extends GlobalSearch
{
	public function get()
	{
		$modelsFormatted = [];
		$modelsFormatted['total_number'] = 0;
		$key = '';

		foreach ($this->getSearchResults() as $resource => $models) {
			$formatted = [];
			foreach ($models as $model) {
				$key = $model->type ?? $model->getTable();
				$formatted[] = $model;
			}
			$modelsFormatted[$key] = $formatted;
			$modelsFormatted[$key.'_last_page'] = $models->lastPage();
			$modelsFormatted[$key.'_on_next_page'] = 'Завантажити ще ' . on_next_page(6, $models);
			$modelsFormatted[$key.'_total'] = $models->total();
			$modelsFormatted['total_number'] += $models->total();
		}

		return $modelsFormatted;
	}

	protected function getSearchResults()
	{
		$results = [];

		foreach ($this->resources as $resource) {
			$query = $resource::buildIndexQuery(
				$this->request, $resource::newModel()->newQuery(),
				$this->request->q
			);

			if (method_exists($query->getModel(), 'scopePublished')) {
				$query = $query->published();
			}

			if (count($models = $query->active()->paginate(6)) > 0) {
				$results[$resource] = $models;
			}
		}

		return collect($results)->sortKeys()->all();
	}
}
