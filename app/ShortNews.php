<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShortNews extends Model
{
	use SoftDeletes;
	use Notifiable;

	protected $dates = ['created_at', 'updated_at', 'deleted_at', 'published_at'];

	public function save(array $options = [])
	{
		$authId = \Auth::id();

		if (!$this->exists) {
			$this->creator_id = $authId;
		}

		$this->editor_id = $authId;

		parent::save($options);

//		work_with_short_news($this);
	}

	public function creator()
	{
		return $this->belongsTo(User::class, 'creator_id', 'id');
	}

	public function editor()
	{
		return $this->belongsTo(User::class, 'editor_id', 'id');
	}

	public function scopeActive($query)
	{
		return $query->where('active', true);
	}

	public function scopePublished($query)
	{
		return $query->where('published_at', '<=', now());
	}

}
