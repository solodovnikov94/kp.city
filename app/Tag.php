<?php

namespace App;

use App\Traits\Crops;
use App\Traits\Relations\TagRelations;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tag extends Model
{
	use TagRelations, SoftDeletes, Crops;

	protected $table = 'tags';
	protected $crops_key = 'tags';
	protected $dates = ['created_at', 'updated_at', 'deleted_at'];

	public function save(array $options = [])
	{
		if (!$this->exists) {
			$this->meta_title = $this->title.". Новини та статті на тему - ".$this->title." на сайті KP.CITY";
			$this->meta_description = "Усі новини та статті по темі - ".$this->title." на головному порталі Кам'янця-Подільського KP.CITY";
			$this->meta_keywords = $this->title.", новини, статті, kp.city";
		}
		return parent::save($options);
	}

	public function scopeActive($query)
	{
		return $query->where('active', true);
	}

	public function getUrl()
	{
		if (isset($this->id) || isset($this->slug)) {
			return url('tags/' . $this->slug);
		}
		return null;
	}
}
