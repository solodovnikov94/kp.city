<?php

namespace App\Traits;

use Illuminate\Support\Facades\Storage;

trait Crops
{
	public function getCroppedImage($type, $extension = 'jpg', $field = 'image')
	{
		if ($this[$field] && in_array($type, config('crops.' . $this->crops_key . '.all_types'))) {

			if (file_exists(
				public_path() . '/storage/' . $this->crops_key . '/' . $this[$field] . '_'
				. ($extension != 'jpg' ? $type . '_' . $extension : $type)
				. '.' . $extension))

				return Storage::disk('public')->url($this->crops_key . '/' . $this[$field] . '_'
					. ($extension != 'jpg' ? $type . '_' . $extension : $type)
					. '.' . $extension);
		}

		return null;
	}

	public function getCroppedImageOrTransparent($type, $extension = 'jpg', $field = 'image')
	{
		$image = $this->getCroppedImage($type, $extension, $field);

		return $image ?? asset('images/transparent-pixel.png');
	}

	public function getCroppedImageOrError($type, $extension = 'jpg', $field = 'image')
	{
		$image = $this->getCroppedImage($type, $extension, $field);

		return $image ?? asset('images/error.svg');
	}
}