<?php

namespace App\Traits\Relations;

use App\OrganizationCategory;
use App\User;

trait AdvertRelations
{
	public function creator()
	{
		return $this->belongsTo(User::class, 'creator_id', 'id');
	}

	public function editor()
	{
		return $this->belongsTo(User::class, 'editor_id', 'id');
	}

	public function category()
	{
		return $this->belongsTo(OrganizationCategory::class, 'category_id', 'id');
	}

//	public function comments()
//	{
//		return $this->morphMany(Comment::class, 'commentable');
//	}

}