<?php

namespace App\Traits\Relations;

use App\Comment;
use App\Like;
use App\User;

trait CommentRelations
{
	public function commentable()
	{
		return $this->morphTo();
	}

	public function user()
	{
		return $this->belongsTo(User::class, 'user_id', 'id');
	}

	public function children()
	{
		return $this->hasMany(Comment::class, 'parent_id', 'id');
	}

	public function likes()
	{
		return $this->morphMany(Like::class, 'likeable');
	}

	public function likesRelation()
	{
		return $this->likes();
	}
}