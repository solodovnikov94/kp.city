<?php

namespace App\Traits\Relations;

use App\Gallery;

trait GalleryPhotoRelations
{
	public function gallery()
	{
		return $this->belongsTo(Gallery::class, 'gallery_id', 'id');
	}
}