<?php

namespace App\Traits\Relations;

use App\GalleryPhoto;

trait GalleryRelations
{
	public function photos()
	{
		return $this->hasMany(GalleryPhoto::class, 'gallery_id', 'id');
	}
}