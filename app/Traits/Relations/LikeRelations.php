<?php

namespace App\Traits\Relations;

use App\User;

trait LikeRelations
{
	public function likeable()
	{
		return $this->morphTo();
	}

	public function user()
	{
		return $this->belongsTo(User::class, 'user_id', 'id');
	}
}