<?php

namespace App\Traits\Relations;

use App\OrganizationCategory;
use App\Organization;

trait OrganizationCategoryRelations
{
	public function organizations()
	{
		return $this->hasMany(Organization::class, 'category_id', 'id');
	}

	public function parent()
	{
		return $this->belongsTo(OrganizationCategory::class, 'parent_id', 'id');
	}

	public function children()
	{
		return $this->hasMany(OrganizationCategory::class, 'parent_id', 'id');
	}
}