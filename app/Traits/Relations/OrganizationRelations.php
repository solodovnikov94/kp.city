<?php

namespace App\Traits\Relations;

use App\News;
use App\Comment;
use App\Gallery;
use App\OrganizationGroup;
use App\OrganizationCategory;
use App\Poll;
use App\Tag;
use App\User;

trait OrganizationRelations
{
	public function creator()
	{
		return $this->belongsTo(User::class, 'creator_id', 'id');
	}

	public function editor()
	{
		return $this->belongsTo(User::class, 'editor_id', 'id');
	}

	public function poll()
	{
		return $this->belongsTo(Poll::class, 'poll_id', 'id');
	}

	public function tags()
	{
		return $this->morphToMany(Tag::class, 'taggable')->withTimestamps();
	}

	public function category()
	{
		return $this->belongsTo(OrganizationCategory::class, 'category_id', 'id');
	}

	public function gallery()
	{
		return $this->belongsTo(Gallery::class, 'gallery_id', 'id');
	}

	public function group()
	{
		return $this->belongsTo(OrganizationGroup::class, 'group_id', 'id');
	}

	public function comments()
	{
		return $this->morphMany(Comment::class, 'commentable');
	}

	public function news()
	{
		return $this->hasMany(News::class, 'organization_id', 'id');
	}
}