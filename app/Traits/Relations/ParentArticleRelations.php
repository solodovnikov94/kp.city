<?php

namespace App\Traits\Relations;

use App\ArticleImage;
use App\Comment;
use App\Poll;
use App\Tag;
use App\User;

trait ParentArticleRelations
{
	public function creator()
	{
		return $this->belongsTo(User::class, 'creator_id', 'id');
	}

	public function editor()
	{
		return $this->belongsTo(User::class, 'editor_id', 'id');
	}

	public function author()
	{
		return $this->belongsTo(User::class, 'author_id', 'id');
	}

	public function poll()
	{
		return $this->belongsTo(Poll::class, 'poll_id', 'id');
	}

	public function tags()
	{
		return $this->morphToMany(Tag::class, 'taggable')->withTimestamps();
	}

	public function images()
	{
		return $this->hasMany(ArticleImage::class, 'article_id', 'id');
	}

	public function comments()
	{
		return $this->morphMany(Comment::class, 'commentable');
	}
}