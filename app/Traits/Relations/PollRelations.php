<?php

namespace App\Traits\Relations;

use App\Article;
use App\Comment;
use App\News;
use App\PollAnswer;
use App\PollAnswerScore;
use App\Tag;

trait PollRelations
{
	public function news()
	{
		return $this->hasMany(News::class, 'poll_id', 'id');
	}

	public function articles()
	{
		return $this->hasMany(Article::class, 'poll_id', 'id');
	}

	public function answers()
	{
		return $this->hasMany(PollAnswer::class, 'poll_id', 'id');
	}

	public function tags()
	{
		return $this->morphToMany(Tag::class, 'taggable')->withTimestamps();
	}

	public function comments()
	{
		return $this->morphMany(Comment::class, 'commentable');
	}

	public function answerScores()
	{
		return $this->hasMany(PollAnswerScore::class, 'poll_id', 'id');
	}
}