<?php

namespace App\Traits\Relations;

use App\Article;
use App\News;
use App\Organization;
use App\Poll;

trait TagRelations
{
	public function articles()
	{
		return $this->morphedByMany(Article::class, 'taggable')->withTimestamps();
	}

	public function news()
	{
		return $this->morphedByMany(News::class, 'taggable')->withTimestamps();
	}

	public function polls()
	{
		return $this->morphedByMany(Poll::class, 'taggable')->withTimestamps();
	}

	public function organizations()
	{
		return $this->morphedByMany(Organization::class, 'taggable')->withTimestamps();
	}
}