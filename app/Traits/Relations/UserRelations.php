<?php

namespace App\Traits\Relations;

use App\Article;
use App\UserSocial;

trait UserRelations
{
	public function articles()
	{
		return $this->hasMany(Article::class, 'creator_id', 'id');
	}

	public function socials()
	{
		return $this->hasMany(UserSocial::class, 'creator_id', 'id');
	}

	public function comments()
	{
		return $this->morphMany(Comment::class, 'commentable');
	}
}