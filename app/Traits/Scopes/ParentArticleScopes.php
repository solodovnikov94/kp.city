<?php

namespace App\Traits\Scopes;

use Carbon\Carbon;

trait ParentArticleScopes
{
	public function scopeActive($query)
	{
		return $query->where('active', true);
	}

	public function scopePublished($query)
	{
		return $query->where('published_at', '<=', Carbon::now());
	}

	public function scopeNewest($query)
	{
		return $query->orderBy('published_at', 'desc');
	}

	public function scopeOldest($query)
	{
		return $query->orderBy('published_at');
	}

	public function scopeToday($query)
	{
		return $query->whereDate('published_at', '=', Carbon::today());
	}

	public function scopeYesterday($query)
	{
		return $query->whereDate('published_at', '=', Carbon::yesterday());
	}

	public function scopeExclusive($query)
	{
		return $query->where('exclusive', true);
	}

	public function scopeAdvertising($query)
	{
		return $query->where('advertising', true);
	}
}