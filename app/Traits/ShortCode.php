<?php

namespace App\Traits;

use App\ArticleImage;
use App\Gallery;
use App\Organization;

trait ShortCode
{
	public function renderShortCodes($content)
	{
		$shortCodes = $this->searchShortCodes($content);

		$fully = [];
		$html = [];
		foreach ($shortCodes as $shortCode) {
			$data = $this->getDataByShortCode($shortCode);
			if (!$data) {
				$fully[] = $shortCode['fully'];
				continue;
			}
			$fully[] = $shortCode['fully'];
			$html[] = (string)view('short-code.' . $shortCode['type'], ['data' => $data]);
		}

		$output = str_replace($fully, $html, $content);

		return $output;
	}

	public function searchShortCodes($content)
	{
		$pattern = '/\[(gallery|image|youtube|organization|googlemap) id="(\d+|[\.\,a-zA-Z0-9_-]{5,100})"\]/';

		$shortCodes = [];

		preg_match_all($pattern, $content, $matches);
		foreach ($matches[1] as $key => $matche) {
			$shortCodes[$key] = [
				'type'  => $matche,
				'id'    => explode(",", $matches[2][$key]),
				'fully' => $matches[0][$key],
			];
		}
		
		return $shortCodes;
	}

	public function getDataByShortCode($shortCode)
	{
		switch ($shortCode['type']) {
			case 'gallery' :
				$data = $this->getGallery($shortCode['id'][0]);
				break;
			case 'image':
				$data = $this->getImage($shortCode['id'][0]);
				break;
			case 'youtube':
				$data = $shortCode['id'][0];
				break;
			case 'googlemap':
				$data = $shortCode['id'];
				break;
			case 'organization':
				$data = $this->getOrganization($shortCode['id'][0]);
				break;
			default:
				$data = null;
		}

		return $data;
	}

	public function getGallery($id)
	{
		return Gallery::where([
			['active', '=', 1],
			['id', '=', $id],
		])->first();
	}

	public function getImage($id)
	{
		return ArticleImage::find($id);
	}

	public function getOrganization($id)
	{
		return Organization::where([
			['active', '=', 1],
			['id', '=', $id]
		])->first();
	}
}
