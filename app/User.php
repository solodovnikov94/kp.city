<?php

namespace App;

use App\Traits\Crops;
use App\Traits\Relations\UserRelations;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
	use Notifiable, HasRoles, Crops, UserRelations;

	protected $guard_name = 'web';
	protected $fillable = ['name', 'surname', 'email', 'password', 'verify_token'];
	protected $hidden = ['password', 'remember_token'];
	protected $crops_key = 'users';

	public function getAvatar($showAvatar = true)
	{
		if ($showAvatar) {
			if (file_exists(public_path() . '/storage/users/' . $this->image . '_min.jpg') && $showAvatar) {
				return $this->getCroppedImage('min', 'jpg', 'image');
			}

			return 'https://www.gravatar.com/avatar/' . md5($this->email) . '?s=60&d=wavatar';
		}

		return asset('images/error.svg');
	}

	public function sendPasswordResetNotification($token)
	{
		$this->notify(new CustomResetPassword($token));
	}

	public function getCountComments()
	{
		return Comment::where('user_id', $this->id)->count();
	}

	public function getCountAdverts()
	{
		return Advert::where('creator_id', $this->id)->count();
	}
}
