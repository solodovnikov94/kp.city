<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Weather extends Model
{
	public $dates = ['created_at', 'updated_at', 'last_updated'];
}
