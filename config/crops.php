<?php

return [

	'extension'    => 'jpg',
	'default_size' => [
		'og'  => ['w' => 1200, 'h' => 630],
		'max' => ['w' => 870, 'h' => 490,],
	],

	'articles' => [
		'all_types' => ['max', 'card', 'avatar', 'og', 'low', 'max_webp', 'mobile', 'mobile_webp'],

		'main' => [
			'default' => 'max',
			'types'   => [
				'max'         => ['w' => 870, 'h' => 490, 'q' => 90],
				'max_webp'    => ['w' => 870, 'h' => 490, 'q' => 90, 'e' => 'webp'],
				'mobile'      => ['w' => 530, 'h' => 298, 'q' => 88],
				'mobile_webp' => ['w' => 530, 'h' => 298, 'q' => 88, 'e' => 'webp'],
				'card'        => ['w' => 420, 'h' => 260, 'q' => 80],
				'avatar'      => ['w' => 32, 'h' => 32, 'q' => 95],
				'low'         => ['w' => 64, 'h' => 36, 'q' => 95, 'b' => 50],
			],
		],

		'og' => [
			'default' => 'og',
			'types'   => [
				'og' => ['w' => 1200, 'h' => 630, 'q' => 90],
			],
		],
	],

	'article_images' => [
		'all_types' => ['max', 'max_webp', 'mobile', 'mobile_webp', 'avatar', 'low'],

		'main' => [
			'default' => 'max',
			'types'   => [
				'max'         => ['w' => 870, 'h' => 490, 'q' => 90],
				'max_webp'    => ['w' => 870, 'h' => 490, 'q' => 90, 'e' => 'webp'],
				'mobile'      => ['w' => 530, 'h' => 298, 'q' => 88],
				'mobile_webp' => ['w' => 530, 'h' => 298, 'q' => 88, 'e' => 'webp'],
				'avatar'      => ['w' => 32, 'h' => 32, 'q' => 95],
				'low'         => ['w' => 64, 'h' => 36, 'q' => 95, 'b' => 50],
			],
		],
	],

	'article_categories' => [
		'all_types' => ['og', 'avatar'],

		'og' => [
			'default' => 'og',
			'types'   => [
				'og'     => ['w' => 1200, 'h' => 630, 'q' => 90],
				'avatar' => ['w' => 32, 'h' => 32, 'q' => 95],
			],
		],
	],

	'polls' => [
		'all_types' => ['max', 'og', 'avatar', 'low', 'max_webp', 'mobile', 'mobile_webp'],

		'main' => [
			'default' => 'max',
			'types'   => [
				'max'         => ['w' => 870, 'h' => 490, 'q' => 90],
				'max_webp'    => ['w' => 870, 'h' => 490, 'q' => 90, 'e' => 'webp'],
				'mobile'      => ['w' => 530, 'h' => 298, 'q' => 88],
				'mobile_webp' => ['w' => 530, 'h' => 298, 'q' => 88, 'e' => 'webp'],
				'avatar'      => ['w' => 32, 'h' => 32, 'q' => 95],
				'low'         => ['w' => 64, 'h' => 36, 'q' => 95, 'b' => 50],
			],
		],

		'og' => [
			'default' => 'og',
			'types'   => [
				'og' => ['w' => 1200, 'h' => 630, 'q' => 90],
			],
		],
	],

	'galleries' => [
		'all_types' => ['max', 'mobile', 'og', 'avatar', 'low', 'max_webp', 'mobile_webp', 'card'],

		'main' => [
			'default' => 'max',
			'types'   => [
				'max'         => ['w' => 870, 'h' => 490, 'q' => 90],
				'max_webp'    => ['w' => 870, 'h' => 490, 'q' => 90, 'e' => 'webp'],
				'mobile'      => ['w' => 530, 'h' => 298, 'q' => 88],
				'mobile_webp' => ['w' => 530, 'h' => 298, 'q' => 88, 'e' => 'webp'],
				'card'        => ['w' => 256, 'h' => 256, 'q' => 80],
				'avatar'      => ['w' => 32, 'h' => 32, 'q' => 95],
				'low'         => ['w' => 64, 'h' => 36, 'q' => 95, 'b' => 50],
			],
		],

		'og' => [
			'default' => 'og',
			'types'   => [
				'og' => ['w' => 1200, 'h' => 630, 'q' => 90],
			],
		],
	],

	'tags' => [
		'all_types' => ['og', 'card'],

		'og' => [
			'default' => 'card',
			'types'   => [
				'og'   => ['w' => 1200, 'h' => 630, 'q' => 90],
				'card' => ['w' => 160, 'h' => 50, 'q' => 80],
			],
		],
	],

	'organization_categories' => [
		'all_types' => ['og', 'avatar'],

		'og' => [
			'default' => 'og',
			'types'   => [
				'og'     => ['w' => 1200, 'h' => 630, 'q' => 90],
				'avatar' => ['w' => 32, 'h' => 32, 'q' => 95],
			],
		],
	],

	'organizations' => [
		'all_types' => ['max', 'card', 'og', 'low', 'max_webp', 'mobile', 'mobile_webp', 'brief'],

		'main' => [
			'default' => 'max',
			'types'   => [
				'max'         => ['w' => 870, 'h' => 490, 'q' => 90],
				'max_webp'    => ['w' => 870, 'h' => 490, 'q' => 90, 'e' => 'webp'],
				'mobile'      => ['w' => 530, 'h' => 298, 'q' => 88],
				'mobile_webp' => ['w' => 530, 'h' => 298, 'q' => 88, 'e' => 'webp'],
				'card'        => ['w' => 420, 'h' => 180, 'q' => 80],
				'brief'       => ['w' => 66, 'h' => 66, 'q' => 95],
				'low'         => ['w' => 64, 'h' => 36, 'q' => 95, 'b' => 50],
			],
		],

		'og' => [
			'default' => 'og',
			'types'   => [
				'og' => ['w' => 1200, 'h' => 630, 'q' => 90],
			],
		],

		'logo' => [
			'default' => 'logo',
			'types'   => [
				'logo' => ['w' => 66, 'h' => 66, 'q' => 90],
			],
		],
	],

	'users' => [
		'all_types' => ['min'],

		'main' => [
			'default' => 'min',
			'types'   => [
				'min' => ['w' => 57, 'h' => 57, 'q' => 95],
			],
		],
	],
];