<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
	public function up()
	{
		Schema::create('articles', function (Blueprint $table) {
			$table->increments('id');
			$table->string('type')->comment('news or article');
			$table->integer('category_id');
			$table->integer('creator_id');
			$table->integer('editor_id')->nullable();
			$table->integer('author_id')->nullable();
			$table->integer('organization_id')->nullable();
			$table->integer('poll_id')->nullable();

			$table->string('title', 330);
			$table->string('slug', 200);
			$table->string('excerpt', 500)->nullable();
			$table->mediumText('body');

			$table->string('source_title', 100)->nullable();
			$table->string('source_link', 200)->nullable();
			$table->string('image', 255)->nullable();
			$table->string('image_title', 100)->nullable();

			$table->integer('views')->default(0);
			$table->integer('interesting')->default(0);
			$table->integer('not_interesting')->default(0);

			$table->string('meta_title', 255)->nullable();
			$table->string('meta_description', 300)->nullable();
			$table->string('meta_keywords', 255)->nullable();
			$table->string('og_title', 200)->nullable();
			$table->string('og_description', 300)->nullable();
			$table->string('og_image', 255)->nullable();

			$table->boolean('active')->default(false);
			$table->boolean('exclusive')->default(false);
			$table->boolean('advertising')->default(false);

			$table->timestamp('published_at')->nullable()->useCurrent()->comment('when to publish an article');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::dropIfExists('articles');
	}
}
