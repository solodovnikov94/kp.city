<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticleCategoriesTable extends Migration
{
	public function up()
	{
		Schema::create('article_categories', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('order_')->default(1);
			$table->string('type', 100)->comment('news or article');

			$table->string('title', 255);
			$table->string('slug', 255);
			$table->string('class', 100)->nullable()->comment('css class name');

			$table->string('meta_title', 255);
			$table->string('meta_keywords', 300);
			$table->string('meta_description', 300);
			$table->string('og_title', 200)->nullable();
			$table->string('og_description', 300)->nullable();
			$table->string('og_image', 255);

			$table->boolean('active')->default(true);
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::dropIfExists('article_categories');
	}
}
