<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePollsTable extends Migration
{
	public function up()
	{
		Schema::create('polls', function (Blueprint $table) {
			$table->increments('id');

			$table->string('title', 255);
			$table->string('slug', 255);
			$table->text('description')->nullable();

			$table->string('image', 255)->nullable();

			$table->string('meta_title', 255)->nullable();
			$table->string('meta_description', 300)->nullable();
			$table->string('meta_keywords', 300)->nullable();
			$table->string('og_title', 200)->nullable();
			$table->string('og_description', 300)->nullable();
			$table->string('og_image', 255)->nullable();

			$table->integer('views')->default(0);
			$table->boolean('active')->default(false);
			$table->boolean('home_page')->default(false);
			$table->boolean('separate')->default(false)->comment('show on polls page');

			$table->timestamp('published_at')->nullable()->useCurrent()->comment('when to publish');
			$table->timestamp('deactivation_at')->nullable()->comment('when to deactivation');

			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::dropIfExists('polls');
	}
}
