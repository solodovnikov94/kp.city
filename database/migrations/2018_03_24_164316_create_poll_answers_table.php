<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePollAnswersTable extends Migration
{
	public function up()
	{
		Schema::create('poll_answers', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('poll_id');
			$table->integer('order_')->default(0);
			$table->string('title');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::dropIfExists('poll_answers');
	}
}
