<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePollAnswerScoresTable extends Migration
{
	public function up()
	{
		Schema::create('poll_answer_scores', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('poll_id');
			$table->integer('poll_answer_id');
			$table->integer('user_id')->nullable();
			$table->ipAddress('user_ip')->nullable();
			$table->string('user_agent')->nullable();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::dropIfExists('poll_answer_scores');
	}
}
