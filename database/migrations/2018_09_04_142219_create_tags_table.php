<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTagsTable extends Migration
{
	public function up()
	{
		Schema::create('tags', function (Blueprint $table) {
			$table->increments('id');

			$table->string('title', 330);
			$table->string('slug', 200);
			$table->text('body');
			$table->string('image', 255)->nullable();

			$table->string('meta_title', 255)->nullable();
			$table->string('meta_description', 300)->nullable();
			$table->string('meta_keywords', 255)->nullable();
			$table->string('og_title', 200)->nullable();
			$table->string('og_description', 300)->nullable();
			$table->string('og_image', 255)->nullable();

			$table->boolean('active')->default(false);

			$table->timestamps();
			$table->softDeletes();
		});

		Schema::create('taggables', function (Blueprint $table) {
			$table->integer('tag_id');
			$table->integer('taggable_id');
			$table->string('taggable_type');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::dropIfExists('tags');
		Schema::dropIfExists('taggables');
	}
}
