<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrateGalleriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
			Schema::create('galleries', function (Blueprint $table) {
				$table->increments('id');

				$table->string('title', 330);
				$table->string('slug', 200);
				$table->text('body');

				$table->string('meta_title', 255)->nullable();
				$table->string('meta_description', 300)->nullable();
				$table->string('meta_keywords', 255)->nullable();
				$table->string('og_title', 200)->nullable();
				$table->string('og_description', 300)->nullable();
				$table->string('og_image', 255)->nullable();

				$table->boolean('active')->default(false);

				$table->timestamps();
				$table->softDeletes();
			});

			Schema::create('gallery_photos', function (Blueprint $table) {
				$table->increments('id');

				$table->integer('gallery_id');
				$table->string('image', 255)->nullable();
				$table->string('image_title', 255)->nullable();
				$table->integer('order_')->default(3);

				$table->timestamps();
				$table->softDeletes();
			});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
			Schema::dropIfExists('taggables');
			Schema::dropIfExists('tags');
    }
}
