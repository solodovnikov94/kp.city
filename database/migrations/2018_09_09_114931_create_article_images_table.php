<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticleImagesTable extends Migration
{
	public function up()
	{
		Schema::create('article_images', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('article_id');
			$table->string('url', 255)->nullable();
			$table->string('title', 100)->nullable();
			$table->boolean('save_aspect_ratio')->default(false)->comment('Save image original aspect ratio');
		});
	}

	public function down()
	{
		Schema::dropIfExists('article_images');
	}
}
