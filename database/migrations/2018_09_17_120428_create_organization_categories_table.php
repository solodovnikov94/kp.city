<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrganizationCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organization_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_')->default(1);
            $table->integer('parent_id')->nullable();
						$table->string('schema_type', 255);
						$table->string('title', 255);
            $table->string('slug', 255);
            $table->string('icon')->nullable();
            $table->boolean('active')->default(true);
            $table->string('meta_title', 255);
            $table->string('meta_keywords', 300);
            $table->string('meta_description', 300);

            $table->string('og_title', 200)->nullable();
            $table->string('og_description', 300)->nullable();
            $table->string('og_image', 255)->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organization_categories');
    }
}
