<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrganizationsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('organizations', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('creator_id');
			$table->integer('editor_id')->nullable();
			$table->integer('curator_id')->nullable();
			$table->integer('group_id')->nullable();
			$table->integer('category_id');
			$table->integer('poll_id')->nullable();
			$table->integer('gallery_id')->nullable();
			$table->string('schema_type', 255);
			$table->string('name', 255);
			$table->string('title', 330);
			$table->string('slug', 200);
			$table->string('excerpt', 500)->nullable();
			$table->text('body');
			$table->text('search_text');
			$table->text('schedule')->nullable();
			$table->text('break_time')->nullable();
			$table->text('addresses')->nullable();
			$table->text('emails')->nullable();
			$table->text('phones')->nullable();
			$table->text('sites')->nullable();
			$table->text('social_networks')->nullable();
			$table->text('specifics')->nullable();
			$table->string('background_image', 255)->nullable();
			$table->string('image', 255)->nullable();
			$table->string('logotype', 255)->nullable();
			$table->string('panorama', 255)->nullable();
			$table->string('video', 255)->nullable();
			$table->string('document', 255)->nullable();
			$table->string('google_map_id', 333)->nullable();
			$table->integer('views')->default(0);

			$table->string('meta_title', 255)->nullable();
			$table->string('meta_description', 300)->nullable();
			$table->string('meta_keywords', 255)->nullable();

			$table->string('og_title', 200)->nullable();
			$table->string('og_description', 300)->nullable();
			$table->string('og_image', 255)->nullable();

			$table->boolean('active')->default(false);

			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('organizations');
	}
}
