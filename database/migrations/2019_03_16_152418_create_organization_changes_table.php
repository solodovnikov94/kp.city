<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrganizationChangesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organization_changes', function (Blueprint $table) {
					$table->increments('id');
					$table->integer('user_id')->nullable();
					$table->integer('organization_id');
					$table->text('message')->nullable();
					$table->boolean('active')->default(false);
					$table->softDeletes();
					$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organization_changes');
    }
}
