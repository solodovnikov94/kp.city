<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShortNewsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('short_news', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('creator_id');
			$table->integer('editor_id')->nullable();
			$table->string('body', 500)->nullable();
			$table->boolean('active')->default(false);
			$table->timestamp('published_at')->nullable()->useCurrent()->comment('when to publish an article');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('short_news');
	}
}
