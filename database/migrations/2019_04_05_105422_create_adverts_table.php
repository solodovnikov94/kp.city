<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvertsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('adverts', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('category_id');
			$table->integer('creator_id');
			$table->integer('editor_id')->nullable();
			$table->string('title', 330);
			$table->string('slug', 200);
			$table->string('image', 255)->nullable();
			$table->text('body')->nullable();
			$table->string('phone', 50);
			$table->string('email', 100);
			$table->boolean('active')->default(false);

			$table->integer('views')->default(0);
			$table->string('meta_title', 255)->nullable();
			$table->string('meta_description', 300)->nullable();
			$table->string('meta_keywords', 255)->nullable();

			$table->timestamp('published_at')->nullable()->useCurrent()->comment('when to publish an article');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('adverts');
	}
}
