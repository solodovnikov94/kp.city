<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvertCategoriesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('advert_categories', function (Blueprint $table) {
			$table->increments('id');
			$table->string('title', 255);
			$table->string('slug', 255);
			$table->boolean('active')->default(true);
			$table->string('meta_title', 255);
			$table->string('meta_keywords', 300);
			$table->string('meta_description', 300);
			$table->string('og_title', 200)->nullable();
			$table->string('og_description', 300)->nullable();
			$table->string('og_image', 255)->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('advert_categories');
	}
}
