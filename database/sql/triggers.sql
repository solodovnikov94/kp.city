CREATE TRIGGER like_before_insert
AFTER INSERT
   ON likes FOR EACH ROW
BEGIN
   -- variable declarations
  UPDATE comments SET likes = votes + 1 WHERE id = NEW.comment_id;
END;

CREATE TRIGGER LIKE_BEFORE_DELETE
AFTER UPDATE
   ON table_name FOR EACH ROW

BEGIN
   -- variable declarations
    UPDATE nominees SET votes = (select count(*) from `votes` where nominee_id = OLD.nominee_id) WHERE id = OLD.nominee_id;
END;

// show triggers
select trigger_schema, trigger_name, action_statement from information_schema.triggers