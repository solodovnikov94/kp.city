function commonSlider(id) {
	var slider = $("#" + id).lightSlider({
		item: 3,
		slideMargin: 25,
		easing: "ease",
		speed: 300,
		pager: !1,
		controls: !1,
		pauseOnHover: !0,
		pause: 5e3,
		enableTouch: 0,
		responsive: [
			// {breakpoint: 1600, settings: {item: 3, slideMargin: 25}},
			{breakpoint: 1200, settings: {item: 3, slideMargin: 15}},
			{breakpoint: 992, settings: {item: 2, slideMargin: 15}},
			{breakpoint: 480, settings: {item: 1, slideMargin: 15}}
		]
	});

	$("#" + id + "-prev").on("click", function () {
		slider.goToPrevSlide();
	});

	$("#" + id + "-next").on("click", function () {
		slider.goToNextSlide();
	});
}