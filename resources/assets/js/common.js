$(document).ready(function () {
	let c, currentScrollTop = 0,
		header = $('.header'),
		headerBanner = $('.header__banner-wrapper');
	let headerContent = $(".header__content");

	$(window).scroll(function () {
		let a = $(window).scrollTop();
		let b = headerContent.height();

		currentScrollTop = a;

		if ($(window).width() >= 992) {
			if (currentScrollTop > (b + headerBanner.height() - 5)) {
				headerContent.css("transform", "translateY(-" + (b - 5) + "px)");
				setTimeout(function () {
					header.addClass("fixed");
				}, 1);
			} else {
				header.removeClass("fixed");
				headerContent.css("transform", "translateY(0)");
			}
		} else {
			if (currentScrollTop > b && c < currentScrollTop) {
				headerContent.css("transform", "translateY(-" + b + "px)");
				setTimeout(function () {
					header.addClass("fixed");
				}, 1);
			} else  {
				headerContent.css("transform", "translateY(0)");
			}
		}
		c = currentScrollTop;
	});

	header.mouseover(function () {
		if (header.hasClass('fixed')) {
			headerContent.css("transform", "translateY(0)");
			$('.header__second').addClass('active');
		}
	});

	$(document).mouseover(function (e) {
		if (header.has(e.target).length === 0 && header.hasClass('fixed')) {
			if ($(window).width() >= 992) {
				headerContent.css("transform", "translateY(-" + (headerContent.height() - 5) + "px)");
				$('.header__second').removeClass('active');
			}
			// else
			// 	headerContent.css("transform", "translateY(0)");
		}
	});

	var lazyLoad = new LazyLoad({
		elements_selector: ".b-lazy",
		callback_error: function (e) {
			e.src = '../images/error.svg';

			setTimeout(function () {
				e.classList.add('error-loaded');
			}, 100);
		}
	});

	// Show/hide mobile menu
	$("#mobile-menu-hamburger").on("click", function () {
		$("#mobile-menu").toggleClass('active');
		$(".wrapper").toggleClass('menu-active');
		$(".header").toggleClass('menu-active');
		$("body").toggleClass('no-scroll');
		$(this).toggleClass('active');
	});

	$(".toggle-header-search").on("click", function () {
		var searchForm = $(".header__main-search-form");

		$(document).mouseup(function (e) {
			if (searchForm.has(e.target).length === 0) {
				searchForm.removeClass('active');
			}
		});

		searchForm.toggleClass('active');
		$("#header-form-search-input").focus();
	});

	$(".advert-single__buttons-item").on("click", function () {
		let item = $(this);

		if (!item.hasClass("disabled")) {
			item.find("span").addClass('disabled');

			setTimeout(function () {
				item.find("a").fadeIn(150);
			}, 150)
		}
	});

	$("body").on("click", ".float-alert__close", function () {
		$(this).parent().parent().slideUp(150);
	});

	// $(".header__banner-close").on("click", function () {
	// 	$(this).parent().parent().parent().slideUp(150);
	// });

	$(".rating__item-radio").on("click", function () {
		var rating = $(this).val();

		$(this).parent().parent().find(".rating__item").removeClass('active');
		for (var i = 1; i <= rating; i++) {
			$("#rating-item-" + i).addClass('active');
		}
	});

	$(".info-dropdown__item--active").on("click", function () {
		var list = $(this).next();

		$(document).mouseup(function (e) {
			if (list.parent().has(e.target).length === 0) {
				list.removeClass('active').slideUp(150);
				list.parent().parent().removeClass('active');
			}
		});

		list.toggleClass('active').slideToggle(150);
		list.parent().parent().toggleClass('active');
	});

	//Плавная прокрутка к якорям
	$("body").on('click', '[href*="#"]', function (e) {
		$('html,body').stop().animate({scrollTop: $(this.hash).offset().top - 20}, 500);
		e.preventDefault();
	});

	// $(".organization__share-button").on("click", function () {
	// 	var shareList = $(this).prev();
	//
	// 	$(document).mouseup(function (e) {
	// 		if (shareList.has(e.target).length === 0){
	// 			shareList.removeClass('active');
	// 		}
	// 	});
	//
	// 	if (shareList.hasClass('active')) {
	// 		shareList.removeClass('active');
	// 	} else {
	// 		$('.organization__share-list').removeClass('active');
	// 		shareList.addClass('active');
	// 	}
	// });

	$(".oc__btn").on("click", function () {
		var oc = $(this).parent();

		$(document).mouseup(function (e) {
			if (oc.has(e.target).length === 0) {
				oc.removeClass('active');
			}
		});

		if (oc.hasClass('active')) {
			oc.removeClass('active')
		} else {
			$(".oc").removeClass('active');
			oc.addClass('active');
		}
	});

	var token = $('meta[name="csrf-token"]').attr('content');

	//counter
	var route = $("#api-counter").val();
	if (route != undefined) {
		var model = $("#api-model").val();
		var column = $("#api-column").val();
		var id = $("#api-id").val();
		$.post(route, {
			model: model,
			column: column,
			id: id
		});
	}
	//end counter

	//ajax load
	$('.show-more').on('click', function () {
		var app = this;
		var currentPage = $(app).attr('data-current-page');
		var path = $(app).attr('data-url');
		var urlParam = $(app).attr('data-url-param');
		currentPage = parseInt(currentPage);
		var url = path + '?page=' + currentPage;
		if (urlParam !== undefined) {
			url += '&' + urlParam;
		}

		$.ajax({
			type: "GET",
			url: url,
			success: function (response) {
				var html = response.html;
				$(app).prev().append($(html).css("display", "none").fadeIn(300));
				$(app).text(response.onNextPage);
				lazyLoad.update();
				if (currentPage > response.lastPage) {
					$(app).remove();
				}
			},
			error: function (response) {
				cloneDangerAlert();
			}
		});
		currentPage = currentPage + 1;
		$(app).attr('data-current-page', currentPage);
	});
	//end ajax load

	//poll
	$('.send-poll').on('click', function (e) {
		e.preventDefault();
		var thisForm = $(".poll__form");
		var titlePoll = $(this).closest('div.poll').find('h3').prop('outerHTML');
		$.ajax({
			url: thisForm.attr('action'),
			type: "POST",
			dataType: "html",
			data: thisForm.serialize(),
			success: function (response) {
				var response = JSON.parse(response);
				if (response.message) {
					alert(response.message);
				} else {
					$('.poll').html(titlePoll + response.html).css("display", "none").fadeIn(300);
				}
			},
			error: function (response) {
				cloneDangerAlert();
			}
		});
	});
	//end poll

	//auth user
	$('.auth').on('click', function (e) {
		e.preventDefault();
		var thisForm = $(this).parent();
		thisForm.closest('div.popup').addClass('loader');
		thisForm.find('label.error').removeClass('error');
		thisForm.find('span.popup__form-error').html('');

		$.ajax({
			url: thisForm.attr('action'),
			type: thisForm.attr('method'),
			dataType: "html",
			data: thisForm.serialize(),
			success: function (response) {
				response = $.parseJSON(response);
				if (response.redirect_url) {
					// window.location.href = response.redirect_url;
					location.reload();
				}

				if (response.alert) {
					thisForm.html(response.alert);
				}

				if (response.errors) {
					var key, label, errors = response.errors;

					for (key in errors) {
						label = thisForm.find('input[name=' + key + ']').parent();
						label.addClass('error');
						label.find('span').html(errors[key]);
					}
					console.log(errors);
				}

				setTimeout(function () {
					thisForm.closest('div.popup').removeClass('loader');
				}, 500);
			},
			error: function (response) {
				cloneDangerAlert();
				thisForm.closest('div.popup').removeClass('loader');
			}
		});
	});

	// if (window.location.href.indexOf('#_=_') > 0) {
	// 	window.location = window.location.href.replace(/#.*/, '');
	// }
	//end auth user

	//comments
	var commentId = '', appendComment = '';

	function resetForm() {
		$('.comment-form').trigger('reset');
		$('.leave-comment__footer').find('label.rating__item').removeClass('active');
		$('.leave-comment-button').hide();
	}

	function leaveComment() {
		commentId = '';
		$('#anchor-leave-comment').find('a:first-child').html('');
		$('.leave-comment__rating').show();
		$('.leave-comment-button').hide();
	}

	function copyTextToClipboard(text) {
		var textCode = document.createElement("textarea");
		textCode.value = text;
		document.body.appendChild(textCode);
		textCode.select();
		document.execCommand('copy');
		textCode.remove();
	}

	$('.add-comment').on('click', function (e) {
		e.preventDefault();
		var model = $("#api-model").val();
		var id = $("#api-id").val();
		var message = $("#comment-textarea").val();
		var action = $(this).closest('form').attr('action');
		var rating = $('.rating__item-radio:checked').val();
		var anonymous = $("#leave-anonymous-comment:checked").val();
		if ($(".leave-comment__rating").is(":visible") && !rating) {
			rating = 'string';
		}

		$.post(action, {
			_token: token,
			model: model,
			id: id,
			comment_id: commentId,
			message: message,
			rating: rating,
			anonymous: anonymous
		}).done(function (data) {
			if (!data.parent && data.comment_block_html) {
				if ($('.comments-list').children().hasClass('alert')) {
					$('.comments-list').children().remove();
				}
				$('.comments-list').append(data.comment_block_html);
			} else {
				$(appendComment).after(data.comment_block_html);
			}

			if (data.comment_block_html) {
				$('.comment-form').find('.popup__form-error').text('');
				$('.rating-block').html(data.rating_block_html);
				$('.total-comment').text('(' + data.total_comments + ')');
				$('.counters').find('span:last-child').html('<i class="fal fa-comment"></i>' + data.total_comments);
				lazyLoad.update();
				resetForm();
				var id = '#' + $(data.comment_block_html).attr('id');
				$('html,body').stop().animate({scrollTop: $(id).offset().top}, 500);
				leaveComment();
				swimmingAlert($(data.alert), true);
			}

			if (data.errors) {
				var text = '';
				for (var error in data.errors) {
					text += data.errors[error] + ' ';
				}
				$('.comment-form').find('.popup__form-error').text(text);
			}
		}).fail(function (data) {
			cloneDangerAlert();
		});
	});

	$('body').on('click', '.comment-action_reply', function (e) {
		e.preventDefault();
		resetForm();
		var replyBlock = $(this).closest('div.comment');
		var id = '#' + replyBlock.attr('id');
		commentId = replyBlock.attr('data-id');
		appendComment = $('body').find('.comment[data-id="' + commentId + '"]');
		appendComment = appendComment[appendComment.length - 1];
		replyBlock = replyBlock.find('div.comment-author__name');
		var authorName = replyBlock.text();
		$('#anchor-leave-comment').find('a:first-child').attr('href', id).html('у відповідь ' + authorName);
		$('#anchor-leave-comment').find('.section-title__second-text').fadeIn(150);
		$('.leave-comment__rating').hide();
		$('.leave-comment-button').show();
	});

	$('body').on('click', '.leave-comment-button', function (e) {
		$('#anchor-leave-comment').find('.section-title__second-text').fadeOut(150);
		setTimeout(function () {
			e.preventDefault();
			leaveComment();
		}, 150);
	});

	$('body').on('click', '.copy-url', function (e) {
		copyTextToClipboard($(this).data("url"));
		$(this).closest('a').find('div').html('Скопійовано');
	});

	$('body').on('mouseover', '.copy-url', function (e) {
		$(this).closest('a').find('div').html('Скопіювати посилання на коментар');
	});

	$('body').on('click', '.delete-comment', function (e) {
		e.preventDefault();
		var commentId = $(this).closest('div.comment').attr('data-comment-id');
		$.post('/comment/remove', {
			_token: token,
			comment_id: commentId,
		}).done(function (data) {
			location.reload();
		}).fail(function (data) {
			cloneDangerAlert();
		});
	});

	$('body').on('click', '.comment-action_helpful', function (e) {
		e.preventDefault();
		var app = $(this);
		var block = $(this).closest('div.comment');
		block.addClass('loader loader-without-bg');
		var commentId = block.attr('data-comment-id');

		$.post('/comment/like', {
			_token: token,
			comment_id: commentId
		}).done(function (data) {
			if (data.active) {
				app.addClass('active');
			} else {
				app.removeClass('active');
			}

			trophyComment(data.comment_id, data.trophy);
			app.find('span').html(data.count_likes);

			setTimeout(function () {
				block.removeClass('loader loader-without-bg');
			}, 500);
		}).fail(function (data) {
			block.removeClass('loader loader-without-bg');
			cloneDangerAlert();
		});
	});

	function trophyComment(commentId, html) {
		$('body').find('.comment-buttons__useful.tooltip-parent').html('');
		console.log(commentId);
		if (commentId !== false) {
			$('body').find('#comment' + commentId).find('.comment-buttons__useful.tooltip-parent').html(html);
		}
	}

	// end comments

	// alerts
	function cloneDangerAlert() {
		var html = $('#clone-float-alert').clone();
		swimmingAlert(html.removeClass('float-alert__container'), true);
	}

	function swimmingAlert(html, fadeIn) {
		var alert = html;
		var alertContainer = $('#float-alerts-container');
		if (alertContainer.children().length >= 3) {
			alertContainer.find('div:first').eq(0).slideUp(150, function () {
				$(this).remove();
			});
		}

		if (fadeIn) {
			alertContainer.append(alert.css("display", "none").fadeIn(150));
		}

		setTimeout(function () {
			alert.slideUp(150, function () {
				$(this).remove();
			});
		}, 5000);
	}

	swimmingAlert($('#float-alerts-container').children().find('div:first'), false);
	// end alert

	// banners
	$(".close-banner-timer").on("click", function () {
		var bannerBlock = $(this).closest('.banner-block');
		bannerBlock.slideUp(150);
		var bannerId = bannerBlock.attr('data-id');
		$.post('/banners/close-banner', {
			banner_id: bannerId,
			_token: token
		});
	});

	$(".banner-click-counter").on('click', function () {
		var bannerId = $(this).attr('data-id');
		$.post('/banners/click-counter', {
			banner_id: bannerId,
			_token: token
		});
	});
	// end banners


	// contacts form
	$('.send-contacts').on('click', function (e) {
		e.preventDefault();
		var thisForm = $(this).parent();
		thisForm.addClass('loader loader-without-bg');
		thisForm.find('label.error').removeClass('error');
		thisForm.find('span.popup__form-error').html('');

		$.ajax({
			url: thisForm.attr('action'),
			type: thisForm.attr('method'),
			dataType: "html",
			data: thisForm.serialize(),
			success: function (response) {
				response = $.parseJSON(response);

				if (response.errors) {
					var key, label, errors = response.errors;

					for (key in errors) {
						label = thisForm.find('#contacts-' + key).parent();
						label.addClass('error');
						label.find('span.popup__form-error').html(errors[key]);
					}
				} else {
					thisForm[0].reset();
				}
				grecaptcha.reset();

				if (response.alert) {
					swimmingAlert($(response.alert), true);
				}

				setTimeout(function () {
					thisForm.removeClass('loader');
				}, 500);
			},
			error: function (response) {
				cloneDangerAlert();
				thisForm.removeClass('loader');
			}
		});
	});
	//end contacts form


	//organization
	$('.add-changes').on('click', function (e) {
		e.preventDefault();
		var thisForm = $(this).parent();
		thisForm.closest('div.popup').addClass('loader');
		thisForm.find('label.error').removeClass('error');
		thisForm.find('span.popup__form-error').html('');

		$.ajax({
			url: thisForm.attr('action'),
			type: thisForm.attr('method'),
			dataType: "html",
			data: thisForm.serialize() + '&organization_id=' + $('#api-id').val(),
			success: function (response) {
				response = $.parseJSON(response);

				if (response.alert) {
					swimmingAlert($(response.alert), true);
				}

				if (response.errors) {
					var key, label, errors = response.errors;
					for (key in errors) {
						label = thisForm.find('#changes-' + key).parent();
						label.addClass('error');
						label.find('span.popup__form-error').html(errors[key]);
					}
				} else {
					thisForm[0].reset();
				}
				grecaptcha.reset();

				setTimeout(function () {
					thisForm.closest('div.popup').removeClass('loader');
				}, 500);
			},
			error: function (response) {
				cloneDangerAlert();
				thisForm.closest('div.popup').removeClass('loader');
			}
		});
	});
	// end origanization
});

// require('./bootstrap');
// window.Vue = require('vue');
// window.axios = require('axios');
// // window.VueRouter = require('vue-router');
// import VueRouter from 'vue-router';
//
// window.Vue.use(VueRouter);
//
// // Vue.component('organization-index', require('./components/organization/OrganizationIndex.vue'));
//
// // const app = new Vue({
// // 	el: '#app'
// // });
//
// import OrganizationIndex from './components/organization/OrganizationIndex.vue';
// import OrganizationCreate from './components/organization/OrganizationCreate.vue';
// import OrganizationEdit from './components/organization/OrganizationEdit.vue';
//
// const routes = [
// 	{
// 		path: '/profile/',
// 		components: {
// 			organizationIndex: OrganizationIndex
// 		}
// 	},
// 	{path: '/profile/companies/create', component: OrganizationCreate, name: 'organizationCreate'},
// 	{path: '/profile/companies/edit/:id', component: OrganizationEdit, name: 'organizationEdit'},
// ];
//
// const router = new VueRouter({
// 	routes,
// 	mode: 'history'
// });
// Vue.component('organization-list', require('./components/organization/OrganizationList.vue'));
//
// const app = new Vue({ router }).$mount('#app');
