@extends('layouts.layouts-main')

@section('meta')
	@include('includes.meta', [
		'title' => config('metatags.advert_categories.title'),
		'meta_description' => config('metatags.advert_categories.description'),
		'meta_keywords' => config('metatags.advert_categories.keywords'),
		'og_title' => config('metatags.advert_categories.title'),
		'og_description' => config('metatags.advert_categories.description'),
		'og_image' => asset('images/pages/homepage.jpg'),
		'og_image_type' => 'og',
		'active' => false
	])
@stop

@section('styles')
	<link rel="stylesheet" href="{{ mix('css/common.css') }}">
	<link rel="stylesheet" href="{{ mix('css/adwerts.css') }}">
	<link rel="stylesheet" href="{{ asset('libs/lightslider/lightslider.css') }}">
@endsection

@section('content')
	<div class="breadcrumbs">
		@include('includes.breadcrumbs', ['current' => 'Оголошення'])
	</div>

	@include('search.search-block', ['class' => 'm-full'])

	<hr>

	<ul class="categories-list m-full">
		<li class="categories-list__item">
			<a href="#" class="btn btn-md btn-transparent btn-bordered btn-news-emergency">Авто</a>
			<a href="#" class="btn btn-md btn-transparent btn-bordered btn-news-organizations">Знайдено/Загублено</a>
		</li>
	</ul>

	<hr class="m-lr">

	<div class="m-t m--b">
		<div class="adverts-list">

			<div class="advert">

				<div class="advert__header">
					<div class="advert__buttons">
						<a href="#category" class="btn btn-sm btn-bordered btn-news-emergency active">Знайдено/Загублено</a>
					</div>
				</div>

				<div class="advert__main">

					<div class="advert__content">

						<a href="#adv" class="advert__image">
							<img class="b-lazy"
									 data-src="https://images.glavred.info/2018_06/1530000140-50658487.jpg"
									 alt="">
						</a>

						<a href="#adv" class="advert__info">
							<div class="advert__title">Знайдено гаманець та паспорт по вул. Драгоманова</div>
							<div class="advert__price">$ 23 000</div>
						</a>

						<a href="#author" class="advert__author">
							<div class="advert__author-avatar">
								<img class="advert__author-avatar--image b-lazy"
										 data-src="https://images.glavred.info/2018_06/1530000140-50658487.jpg"
										 alt="">
							</div>
							<div class="advert__author-info">
								<div class="advert__author-name">Олександр Солодовніков</div>
								<div class="advert__author-role">Автор оголошення</div>
							</div>
						</a>

					</div>

					<a href="#adv" class="advert__footer">
						<div class="advert__date">Сьогодні, 11:30</div>
						<div class="advert__views"><i class="advert__views-icon fal fa-eye"></i> <span>15</span></div>
					</a>

				</div>

			</div>
			<div class="advert">

				<div class="advert__header">
					<div class="advert__buttons">
						<a href="#category" class="btn btn-sm btn-bordered btn-news-crime active">Авто</a>
					</div>
				</div>

				<div class="advert__main">

					<div class="advert__content">

						<a href="#adv" class="advert__image">
							<img class="b-lazy"
									 data-src="https://cdn.fishki.net/upload/post/201310/09/1209726/gallery/auto-01.jpg"
									 alt="">
						</a>

						<a href="#adv" class="advert__info">
							<div class="advert__title">Продам друга, з яким я пережив все: і любов, і ненависть, і всіх президентів України</div>
							<div class="advert__price">$ 23 000</div>
						</a>

						<a href="#author" class="advert__author">
							<div class="advert__author-avatar">
								<img class="advert__author-avatar--image b-lazy"
										 data-src="https://images.glavred.info/2018_06/1530000140-50658487.jpg"
										 alt="">
							</div>
							<div class="advert__author-info">
								<div class="advert__author-name">Олександр Солодовніков</div>
								<div class="advert__author-role">Автор оголошення</div>
							</div>
						</a>

					</div>

					<a href="#adv" class="advert__footer">
						<div class="advert__date">Сьогодні, 11:30</div>
						<div class="advert__views"><i class="advert__views-icon fal fa-eye"></i> <span>15</span></div>
					</a>

				</div>

			</div>
			<div class="advert">

				<div class="advert__header">
					<div class="advert__buttons">
						<a href="#category" class="btn btn-sm btn-bordered btn-news-crime active">Авто</a>
					</div>
				</div>

				<div class="advert__main">

					<div class="advert__content">

						<a href="#adv" class="advert__image">
							<img class="b-lazy"
									 data-src="https://delo.ua/files/images/471/39/pervaja-semejnaja-komnata-v_47139_p0.jpg"
									 alt="">
						</a>

						<a href="#adv" class="advert__info">
							<div class="advert__title">Шукаю кицю</div>
						</a>

						<a href="#author" class="advert__author">
							<div class="advert__author-avatar">
								<img class="advert__author-avatar--image b-lazy"
										 data-src="https://images.glavred.info/2018_06/1530000140-50658487.jpg"
										 alt="">
							</div>
							<div class="advert__author-info">
								<div class="advert__author-name">Олександр Солодовніков</div>
								<div class="advert__author-role">Автор оголошення</div>
							</div>
						</a>

					</div>

					<a href="#adv" class="advert__footer">
						<div class="advert__date">Сьогодні, 11:30</div>
						<div class="advert__views"><i class="advert__views-icon fal fa-eye"></i> <span>15</span></div>
					</a>

				</div>

			</div>
			<div class="advert">

				<div class="advert__header">
					<div class="advert__buttons">
						<a href="#category" class="btn btn-sm btn-bordered btn-news-crime active">Авто</a>
					</div>
				</div>

				<div class="advert__main">

					<div class="advert__content">

						<a href="#adv" class="advert__image">
							<img class="b-lazy"
									 data-src="https://delo.ua/files/images/471/39/pervaja-semejnaja-komnata-v_47139_p0.jpg"
									 alt="">
						</a>

						<a href="#adv" class="advert__info">
							<div class="advert__title">Шукаю кицю</div>
						</a>

						<a href="#author" class="advert__author">
							<div class="advert__author-avatar">
								<img class="advert__author-avatar--image b-lazy"
										 data-src="https://images.glavred.info/2018_06/1530000140-50658487.jpg"
										 alt="">
							</div>
							<div class="advert__author-info">
								<div class="advert__author-name">Олександр Солодовніков</div>
								<div class="advert__author-role">Автор оголошення</div>
							</div>
						</a>

					</div>

					<a href="#adv" class="advert__footer">
						<div class="advert__date">Сьогодні, 11:30</div>
						<div class="advert__views"><i class="advert__views-icon fal fa-eye"></i> <span>15</span></div>
					</a>

				</div>

			</div>
			<div class="advert">

				<div class="advert__header">
					<div class="advert__buttons">
						<a href="#category" class="btn btn-sm btn-bordered btn-news-emergency active">Знайдено/Загублено</a>
					</div>
				</div>

				<div class="advert__main">

					<div class="advert__content">

						<a href="#adv" class="advert__image">
							<img class="b-lazy"
									 data-src="https://images.glavred.info/2018_06/1530000140-50658487.jpg"
									 alt="">
						</a>

						<a href="#adv" class="advert__info">
							<div class="advert__title">Знайдено гаманець та паспорт по вул. Драгоманова</div>
							<div class="advert__price">$ 23 000</div>
						</a>

						<a href="#author" class="advert__author">
							<div class="advert__author-avatar">
								<img class="advert__author-avatar--image b-lazy"
										 data-src="https://images.glavred.info/2018_06/1530000140-50658487.jpg"
										 alt="">
							</div>
							<div class="advert__author-info">
								<div class="advert__author-name">Олександр Солодовніков</div>
								<div class="advert__author-role">Автор оголошення</div>
							</div>
						</a>

					</div>

					<a href="#adv" class="advert__footer">
						<div class="advert__date">Сьогодні, 11:30</div>
						<div class="advert__views"><i class="advert__views-icon fal fa-eye"></i> <span>15</span></div>
					</a>

				</div>

			</div>
			<div class="advert">

				<div class="advert__header">
					<div class="advert__buttons">
						<a href="#category" class="btn btn-sm btn-bordered btn-news-crime active">Авто</a>
					</div>
				</div>

				<div class="advert__main">

					<div class="advert__content">

						<a href="#adv" class="advert__image">
							<img class="b-lazy"
									 data-src="https://cdn.fishki.net/upload/post/201310/09/1209726/gallery/auto-01.jpg"
									 alt="">
						</a>

						<a href="#adv" class="advert__info">
							<div class="advert__title">Продам друга, з яким я пережив все: і любов, і ненависть, і всіх президентів України</div>
							<div class="advert__price">$ 23 000</div>
						</a>

						<a href="#author" class="advert__author">
							<div class="advert__author-avatar">
								<img class="advert__author-avatar--image b-lazy"
										 data-src="https://images.glavred.info/2018_06/1530000140-50658487.jpg"
										 alt="">
							</div>
							<div class="advert__author-info">
								<div class="advert__author-name">Олександр Солодовніков</div>
								<div class="advert__author-role">Автор оголошення</div>
							</div>
						</a>

					</div>

					<a href="#adv" class="advert__footer">
						<div class="advert__date">Сьогодні, 11:30</div>
						<div class="advert__views"><i class="advert__views-icon fal fa-eye"></i> <span>15</span></div>
					</a>

				</div>

			</div>

		</div>
	</div>

	{{--TODO here news and organizations sliders--}}

@stop

@section('scripts')
	<script src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@12.0.0/dist/lazyload.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/jquery@3.4.1/dist/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sticky-sidebar@3.3.1/dist/sticky-sidebar.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/resize-sensor@0.0.6/ResizeSensor.min.js"></script>

	{{--<script src="{{ asset('js/common-slider.js') }}"></script>--}}
	{{--<script src="{{ asset('libs/lightslider/lightslider-blur-fix.min.js') }}"></script>--}}
	{{--<script>--}}
		{{--commonSlider('best-week-news');--}}
		{{--commonSlider('best-month-news');--}}
	{{--</script>--}}
@stop