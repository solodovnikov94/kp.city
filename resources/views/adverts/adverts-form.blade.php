@extends('layouts.layouts-main')

{{--@section('meta')--}}
{{--@include('includes.meta', [--}}
{{--'title' => config('metatags.advert_categories.title'),--}}
{{--'meta_description' => config('metatags.advert_categories.description'),--}}
{{--'meta_keywords' => config('metatags.advert_categories.keywords'),--}}
{{--'og_title' => config('metatags.advert_categories.title'),--}}
{{--'og_description' => config('metatags.advert_categories.description'),--}}
{{--'og_image' => asset('images/pages/homepage.jpg'),--}}
{{--'og_image_type' => 'og',--}}
{{--'active' => false--}}
{{--])--}}
{{--@stop--}}

@section('styles')
	<link rel="stylesheet" href="{{ mix('css/common.css') }}">
	<link rel="stylesheet" href="{{ mix('css/adwerts.css') }}">
	<link rel="stylesheet" href="{{ mix('css/articles.css') }}">
	<link rel="stylesheet" href="{{ asset('libs/lightslider/lightslider.css') }}">
	<link rel="stylesheet" href="{{ asset('libs/magnific-popup/magnific-popup.css') }}">
@endsection

@section('content')

	<div id="app">
		<div class="breadcrumbs">
			@include('includes.breadcrumbs', ['links' => [
				['link' => "#", 'title' => 'Панель адміністратора'],
				['link' => '#', 'title' => 'Новини']
			], 'current' => 'В Дунаївцях кам\'янчанин скоїв наїзд на дитину'])
		</div>

		<div class="form p-t p-lr">

			<div class="form__left">

				@include('includes.form.image', [
					'required' => true,
					'name' => 'Зображення',
					'hint' => 'Короткий опис відображеного на зображенні',
					'field_name' => 'mainImage'
				])

				@include('includes.form.input', [
					'type' => 'text',
					'required' => true,
					'name' => 'Назва зображення',
					'hint' => 'Короткий опис відображеного на зображенні. Максимально 50, попередження 45, рекомендовано 20-40 символів. Лишнє обріжеться',
					'counter_max' => '50',
					'counter_slice' => true,
					'counter_warning' => '45',
					'counter_recommended_min' => '20',
					'counter_recommended_max' => '40'
				])

				<div class="form__item">
					<div class="form__item-header">
						<span class="form__item-title">Автор<strong>*</strong></span>
					</div>
					<label class="form__item-field">
						<select class="form__item-select">
							<option value="">Редакція</option>
							<option value="">Микола Богачов</option>
							<option value="">Олександр Солодовніков</option>
							<option value="">Viktoria Kriva</option>
						</select>
					</label>
				</div>

				<div class="form__item">
					<div class="form__item-header">
						<span class="form__item-title">Категорія<strong>*</strong></span>
					</div>
					<label class="form__item-field">
						<select class="form__item-select">
							<option value="">Редакція</option>
							<option value="">Микола Богачов</option>
							<option value="">Олександр Солодовніков</option>
							<option value="">Viktoria Kriva</option>
						</select>
					</label>
				</div>

				<div class="form__item">
					<div class="form__item-header">
						<span class="form__item-title">Опитування</span>
						<span class="form__item-hint tooltip-parent">
						<i class="fa fa-info-circle"></i>
						<span class="tooltip tooltip-left tooltip-blue">Опис для соціальних мереж. Оптимально 2-4 речення. Максимум 300 символів</span>
					</span>
					</div>
					<label class="form__item-field">
						<select class="form__item-select">
							<option value="" disabled selected>Обрати опитування</option>
							<option value="">Де ви плануєте провести відпустку у цьому році?</option>
						</select>
					</label>
				</div>

				@include('includes.form.image', [
					'required' => true,
					'name' => 'OG зображення',
					'hint' => 'Зображення для соціальних мереж. Буде використано у форматі 1200x630',
					'field_name' => 'OGImage'
				])

			</div>

			<div class="form__right">

				@include('includes.form.input', [
					'type' => 'text',
					'required' => true,
					'name' => 'Заголовок новини',
					'hint' => 'Унікальна назва новини, така щоб прям ахірєнна. Максимально 70, рекомендовано 25-60 символів. Лишнє обріжеться',
					'counter_max' => '70',
					'counter_slice' => true,
					'counter_recommended_min' => '25',
					'counter_recommended_max' => '60'
				])

				<div class="form__item error">
					<div class="form__item-header">
						<span class="form__item-title">Короткий опис<strong>*</strong></span>
						<span class="form__item-hint tooltip-parent">
						<i class="fa fa-info-circle"></i>
						<span class="tooltip tooltip-left tooltip-blue">Заповнювати лише при необхідності</span>
					</span>
					</div>
					<label class="form__item-field">
						<textarea name="" id="" class="form__item-textarea" placeholder="Короткий опис"></textarea>
					</label>
					<div class="form__item-alert">Поле "Короткий опис" є обов'язковим для заповнення!</div>
				</div>

				<div class="form__item">
					<div class="form__item-header">
						<span class="form__item-title">Контент<strong>*</strong></span>
						<span class="form__item-hint tooltip-parent">
						<i class="fa fa-info-circle"></i>
						<span class="tooltip tooltip-left tooltip-blue">Хєрачим сюди все що треба</span>
					</span>
					</div>
					<label class="form__item-field">
						<textarea id="ckeditor">This is some sample content.</textarea>
					</label>
					<div class="form__item-alert">Поле "Короткий опис" є обов'язковим для заповнення!</div>
				</div>

				@include('includes.form.input', [
					'type' => 'text',
					'name' => 'Назва джерела',
					'hint' => 'Назва джерела, звідки спі***но матеріали. Максимально 70. Лишнє не обрізається',
					'counter_max' => '50'
				])

				@include('includes.form.input', [
					'type' => 'text',
					'name' => 'Посилання на джерело',
					'placeholder' => '',
					'hint' => 'Повна url адреса. Максимально 255, лишнє обріжемо',
					'prefix' => 'https://',
					'counter_max' => '255',
					'counter_slice' => true,
				])

				@include('includes.form.input', [
					'type' => 'text',
					'required' => true,
					'name' => 'Телефон',
					'placeholder' => '',
					'hint' => 'Телефон Вашої секретутки. Максимум 9, лишнє обрізаєм',
					'counter_max' => '9',
					'counter_slice' => true,
					'prefix' => '+380'
				])

				@include('includes.form.input', [
					'type' => 'datetime-local',
					'required' => true,
					'name' => 'Дата',
					'placeholder' => '',
					'hint' => 'Дата того дня, коли ти появилося',
					'value' => now()->toDateTimeLocalString()
				])

				<div class="form__item width-auto">
					<label class="form__item-checkbox">
						<input type="checkbox">
						<span class="form__item-checkbox--marker">
							<i class="fa fa-check"></i>
						</span>
						<span>Активувати</span>
					</label>
				</div>

				<div class="form__item width-auto">
					<label class="form__item-checkbox">
						<input type="checkbox">
						<span class="form__item-checkbox--marker">
							<i class="fa fa-check"></i>
						</span>
						<span>Ексклюзив</span>
					</label>
				</div>

				<div class="form__item width-auto">
					<label class="form__item-checkbox">
						<input type="checkbox">
						<span class="form__item-checkbox--marker">
							<i class="fa fa-check"></i>
						</span>
						<span>Реклама</span>
					</label>
				</div>

			</div>

		</div>
	</div>

@stop

@section('scripts')
	<script src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@12.0.0/dist/lazyload.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/jquery@3.4.1/dist/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sticky-sidebar@3.3.1/dist/sticky-sidebar.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/resize-sensor@0.0.6/ResizeSensor.min.js"></script>

	<script src="https://cdn.ckeditor.com/ckeditor5/12.2.0/classic/ckeditor.js"></script>

	<script src="{{ asset('js/common-slider.js') }}"></script>
	<script src="{{ asset('libs/lightslider/lightslider-blur-fix.min.js') }}"></script>
	<script src="https://cdn.jsdelivr.net/npm/magnific-popup@1.1.0/dist/jquery.magnific-popup.min.js"></script>
	<script>
		commonSlider('author-adverts');

		$('.gallery').magnificPopup({
			delegate: 'a',
			type: 'image',
			gallery: {
				enabled: true
			},
			image: {
				titleSrc: 'title'
				// this tells the script which attribute has your caption
			}
		});

		ClassicEditor
			.create(document.querySelector('#ckeditor'))
			.then(editor => {
				console.log(editor);
			})
			.catch(error => {
				console.error(error);
			});

		let loadFile = function (event, id) {
			let reader = new FileReader();
			reader.onload = function () {
				let output = document.getElementById(id);
				output.src = reader.result;
			};
			reader.readAsDataURL(event.target.files[0]);
		};

		$(".form__item-input--counted").on("keyup", function () {
			let field = $(this),
				counter = field.next(),
				max = counter.data('max'),
				warning = counter.data('warning'),
				recommendedMin = counter.data('recommendedLeft'),
				recommendedMax = counter.data('recommendedRight');

			if (warning - field.val().length <= 0 && max - field.val().length > 0) {
				counter.addClass('warning');
			} else counter.removeClass('warning');

			if (field.val().length >= recommendedMin && field.val().length <= recommendedMax) {
				counter.addClass('recommended');
			} else counter.removeClass('recommended');

			if (max - field.val().length <= 0) {
				counter.addClass('danger');

				if (counter.hasClass('slice')) {
					field.val(function (index, value) {
						return value.substr(0, max);
					});
				}

			} else counter.removeClass('danger');

			console.log(max, warning, recommendedMin, recommendedMax);

			counter.text(max - field.val().length);
		})
	</script>
@stop