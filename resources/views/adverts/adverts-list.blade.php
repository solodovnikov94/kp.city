<div class="advert">

	<div class="advert__header">
		<div class="advert__buttons">
			<a href="#category" class="btn btn-sm btn-bordered btn-news-emergency active">Знайдено/Загублено</a>
		</div>
	</div>

	<div class="advert__main">

		<div class="advert__content">

			<a href="#adv" class="advert__image">
				<img class="b-lazy"
						 data-src="https://images.glavred.info/2018_06/1530000140-50658487.jpg"
						 alt="">
			</a>

			<a href="#adv" class="advert__info">
				<div class="advert__title">Знайдено гаманець та паспорт по вул. Драгоманова</div>
				<div class="advert__price">$ 23 000</div>
			</a>

			<a href="#author" class="advert__author">
				<div class="advert__author-avatar">
					<img class="advert__author-avatar--image b-lazy"
							 data-src="https://images.glavred.info/2018_06/1530000140-50658487.jpg"
							 alt="">
				</div>
				<div class="advert__author-info">
					<div class="advert__author-name">Олександр Солодовніков</div>
					<div class="advert__author-role">Автор оголошення</div>
				</div>
			</a>

		</div>

		<a href="#adv" class="advert__footer">
			<div class="advert__date">Сьогодні, 11:30</div>
			<div class="advert__views"><i class="advert__views-icon fal fa-eye"></i> <span>15</span></div>
		</a>

	</div>

</div>
<div class="advert">

	<div class="advert__header">
		<div class="advert__buttons">
			<a href="#category" class="btn btn-sm btn-bordered btn-news-crime active">Авто</a>
		</div>
	</div>

	<div class="advert__main">

		<div class="advert__content">

			<a href="#adv" class="advert__image">
				<img class="b-lazy"
						 data-src="https://cdn.fishki.net/upload/post/201310/09/1209726/gallery/auto-01.jpg"
						 alt="">
			</a>

			<a href="#adv" class="advert__info">
				<div class="advert__title">Продам друга, з яким я пережив все: і любов, і ненависть, і всіх президентів України</div>
				<div class="advert__price">$ 23 000</div>
			</a>

			<a href="#author" class="advert__author">
				<div class="advert__author-avatar">
					<img class="advert__author-avatar--image b-lazy"
							 data-src="https://images.glavred.info/2018_06/1530000140-50658487.jpg"
							 alt="">
				</div>
				<div class="advert__author-info">
					<div class="advert__author-name">Олександр Солодовніков</div>
					<div class="advert__author-role">Автор оголошення</div>
				</div>
			</a>

		</div>

		<a href="#adv" class="advert__footer">
			<div class="advert__date">Сьогодні, 11:30</div>
			<div class="advert__views"><i class="advert__views-icon fal fa-eye"></i> <span>15</span></div>
		</a>

	</div>

</div>
<div class="advert">

	<div class="advert__header">
		<div class="advert__buttons">
			<a href="#category" class="btn btn-sm btn-bordered btn-news-crime active">Авто</a>
		</div>
	</div>

	<div class="advert__main">

		<div class="advert__content">

			<a href="#adv" class="advert__image">
				<img class="b-lazy"
						 data-src="https://delo.ua/files/images/471/39/pervaja-semejnaja-komnata-v_47139_p0.jpg"
						 alt="">
			</a>

			<a href="#adv" class="advert__info">
				<div class="advert__title">Шукаю кицю</div>
			</a>

			<a href="#author" class="advert__author">
				<div class="advert__author-avatar">
					<img class="advert__author-avatar--image b-lazy"
							 data-src="https://images.glavred.info/2018_06/1530000140-50658487.jpg"
							 alt="">
				</div>
				<div class="advert__author-info">
					<div class="advert__author-name">Олександр Солодовніков</div>
					<div class="advert__author-role">Автор оголошення</div>
				</div>
			</a>

		</div>

		<a href="#adv" class="advert__footer">
			<div class="advert__date">Сьогодні, 11:30</div>
			<div class="advert__views"><i class="advert__views-icon fal fa-eye"></i> <span>15</span></div>
		</a>

	</div>

</div>
<div class="advert">

	<div class="advert__header">
		<div class="advert__buttons">
			<a href="#category" class="btn btn-sm btn-bordered btn-news-crime active">Авто</a>
		</div>
	</div>

	<div class="advert__main">

		<div class="advert__content">

			<a href="#adv" class="advert__image">
				<img class="b-lazy"
						 data-src="https://delo.ua/files/images/471/39/pervaja-semejnaja-komnata-v_47139_p0.jpg"
						 alt="">
			</a>

			<a href="#adv" class="advert__info">
				<div class="advert__title">Шукаю кицю</div>
			</a>

			<a href="#author" class="advert__author">
				<div class="advert__author-avatar">
					<img class="advert__author-avatar--image b-lazy"
							 data-src="https://images.glavred.info/2018_06/1530000140-50658487.jpg"
							 alt="">
				</div>
				<div class="advert__author-info">
					<div class="advert__author-name">Олександр Солодовніков</div>
					<div class="advert__author-role">Автор оголошення</div>
				</div>
			</a>

		</div>

		<a href="#adv" class="advert__footer">
			<div class="advert__date">Сьогодні, 11:30</div>
			<div class="advert__views"><i class="advert__views-icon fal fa-eye"></i> <span>15</span></div>
		</a>

	</div>

</div>
<div class="advert">

	<div class="advert__header">
		<div class="advert__buttons">
			<a href="#category" class="btn btn-sm btn-bordered btn-news-emergency active">Знайдено/Загублено</a>
		</div>
	</div>

	<div class="advert__main">

		<div class="advert__content">

			<a href="#adv" class="advert__image">
				<img class="b-lazy"
						 data-src="https://images.glavred.info/2018_06/1530000140-50658487.jpg"
						 alt="">
			</a>

			<a href="#adv" class="advert__info">
				<div class="advert__title">Знайдено гаманець та паспорт по вул. Драгоманова</div>
				<div class="advert__price">$ 23 000</div>
			</a>

			<a href="#author" class="advert__author">
				<div class="advert__author-avatar">
					<img class="advert__author-avatar--image b-lazy"
							 data-src="https://images.glavred.info/2018_06/1530000140-50658487.jpg"
							 alt="">
				</div>
				<div class="advert__author-info">
					<div class="advert__author-name">Олександр Солодовніков</div>
					<div class="advert__author-role">Автор оголошення</div>
				</div>
			</a>

		</div>

		<a href="#adv" class="advert__footer">
			<div class="advert__date">Сьогодні, 11:30</div>
			<div class="advert__views"><i class="advert__views-icon fal fa-eye"></i> <span>15</span></div>
		</a>

	</div>

</div>
<div class="advert">

	<div class="advert__header">
		<div class="advert__buttons">
			<a href="#category" class="btn btn-sm btn-bordered btn-news-crime active">Авто</a>
		</div>
	</div>

	<div class="advert__main">

		<div class="advert__content">

			<a href="#adv" class="advert__image">
				<img class="b-lazy"
						 data-src="https://cdn.fishki.net/upload/post/201310/09/1209726/gallery/auto-01.jpg"
						 alt="">
			</a>

			<a href="#adv" class="advert__info">
				<div class="advert__title">Продам друга, з яким я пережив все: і любов, і ненависть, і всіх президентів України</div>
				<div class="advert__price">$ 23 000</div>
			</a>

			<a href="#author" class="advert__author">
				<div class="advert__author-avatar">
					<img class="advert__author-avatar--image b-lazy"
							 data-src="https://images.glavred.info/2018_06/1530000140-50658487.jpg"
							 alt="">
				</div>
				<div class="advert__author-info">
					<div class="advert__author-name">Олександр Солодовніков</div>
					<div class="advert__author-role">Автор оголошення</div>
				</div>
			</a>

		</div>

		<a href="#adv" class="advert__footer">
			<div class="advert__date">Сьогодні, 11:30</div>
			<div class="advert__views"><i class="advert__views-icon fal fa-eye"></i> <span>15</span></div>
		</a>

	</div>

</div>