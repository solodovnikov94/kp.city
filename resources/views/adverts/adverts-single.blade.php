@extends('layouts.layouts-main')

{{--@section('meta')--}}
	{{--@include('includes.meta', [--}}
		{{--'title' => config('metatags.advert_categories.title'),--}}
		{{--'meta_description' => config('metatags.advert_categories.description'),--}}
		{{--'meta_keywords' => config('metatags.advert_categories.keywords'),--}}
		{{--'og_title' => config('metatags.advert_categories.title'),--}}
		{{--'og_description' => config('metatags.advert_categories.description'),--}}
		{{--'og_image' => asset('images/pages/homepage.jpg'),--}}
		{{--'og_image_type' => 'og',--}}
		{{--'active' => false--}}
	{{--])--}}
{{--@stop--}}

@section('styles')
	<link rel="stylesheet" href="{{ mix('css/common.css') }}">
	<link rel="stylesheet" href="{{ mix('css/adwerts.css') }}">
	<link rel="stylesheet" href="{{ mix('css/articles.css') }}">
	<link rel="stylesheet" href="{{ asset('libs/lightslider/lightslider.css') }}">
	<link rel="stylesheet" href="{{ asset('libs/magnific-popup/magnific-popup.css') }}">
@endsection

@section('content')

	<div class="breadcrumbs">
		@include('includes.breadcrumbs', ['links' => [
			['link' => url('adverts'), 'title' => 'Оголошення'],
			['link' => '#', 'title' => 'Знайдено/Загублено']
		], 'current' => 'Знайдено гаманець та паспорт по вул. Драгоманова'])
	</div>

		<div class="advert-single p-t p-lr">
			<div class="advert-single__left">
				<div class="advert-single__images gallery">
					<div class="advert-single__images-item">
						<a href="https://hronika.info/uploads/posts/2017-10/1508582698_screenshot_21-1.png">
							<img class="b-lazy"
									 data-src="https://hronika.info/uploads/posts/2017-10/1508582698_screenshot_21-1.png"
									 alt="Продам однокімнатну квартиру, мікр-н Жовтневий (фото 1)">
						</a>
					</div>
					<div class="advert-single__images-item">
						<a href="https://i.pinimg.com/736x/2a/a2/5d/2aa25d48af14dbd551111b98f6b80569.jpg">
							<img class="b-lazy"
									 data-src="https://i.pinimg.com/736x/2a/a2/5d/2aa25d48af14dbd551111b98f6b80569.jpg"
									 alt="Продам однокімнатну квартиру, мікр-н Жовтневий (фото 2)">
						</a>
					</div>
					<div class="advert-single__images-item">
						<a href="https://hronika.info/uploads/posts/2017-10/1508582698_screenshot_21-1.png">
							<img class="b-lazy"
									 data-src="https://hronika.info/uploads/posts/2017-10/1508582698_screenshot_21-1.png"
									 alt="Продам однокімнатну квартиру, мікр-н Жовтневий (фото 3)">
						</a>
					</div>
					<div class="advert-single__images-item">
						<a href="https://i.pinimg.com/736x/2a/a2/5d/2aa25d48af14dbd551111b98f6b80569.jpg">
							<img class="b-lazy"
									 data-src="https://i.pinimg.com/736x/2a/a2/5d/2aa25d48af14dbd551111b98f6b80569.jpg"
									 alt="Продам однокімнатну квартиру, мікр-н Жовтневий (фото 4)">
						</a>
					</div>
				</div>
				<div class="advert-single__buttons m-t">
					<div class="advert-single__buttons-item email">
						<span>Показати E-mail адресу автора</span>
						{{--<a href="mailto:email@email.com" class="advert-single__buttons">email@email.com</a>--}}
					</div>
					<div class="advert-single__buttons-item phone">
						<span>Показати номер телефону автора</span>
						{{--<a href="tel:+380979797977">+380979797977</a>--}}
					</div>
				</div>
			</div>
			<div class="advert-single__right">
				<h1 class="advert-single__title">Продам однокімнатну квартиру, мікр-н Жовтневий</h1>
				<div class="advert-single__price">$ 23 000</div>
				<div class="advert-single__author">
					#author here
				</div>
				<div class="advert-single__description">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi debitis illo neque numquam quae tempora. At beatae fugit natus quo.</p>
					<p>&nbsp;</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium consequatur, dignissimos est eum hic inventore ipsum maxime nisi odio officiis quam, quibusdam recusandae repellat suscipit temporibus ut veniam? Accusamus aliquam, consequatur cupiditate distinctio ea earum fugit id ipsa minus quis quo tempora veritatis! Dignissimos, fugit maiores perferendis quae quas saepe.</p>
					<p>&nbsp;</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis, quis rem! Pariatur quae quis rerum.</p>
				</div>
				<div class="advert-single__social-buttons">
					#social buttons here
				</div>
				<div class="advert-single__footer article-single__footer">
					<div class="counters">
						<span>2 години тому</span>
						<span><i class="fal fa-eye"></i>11</span>
					</div>

					@include('includes.social-buttons', ['url' => url(Request::path()), 'class' => 'share'])
				</div>
			</div>
		</div>

	<div class="slider-section m-lr m-t">
		<div class="slider-section__header">
			<h3 class="slider-section__title">Інші оголошення автора</h3>
			<div class="slider-section__controls">
				<div class="slider-section__controls--prev" id="author-adverts-prev">
					<i class="fal fa-angle-left"></i>
				</div>
				<div class="slider-section__controls--next" id="author-adverts-next">
					<i class="fal fa-angle-right"></i>
				</div>
			</div>
		</div>
		<div class="slider-section__content" id="author-adverts">
			@include('adverts.adverts-list')
		</div>
	</div>

	<hr class="m-t">

	{{--TODO here top month adverts slider--}}

@stop

@section('scripts')
	<script src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@12.0.0/dist/lazyload.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/jquery@3.4.1/dist/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sticky-sidebar@3.3.1/dist/sticky-sidebar.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/resize-sensor@0.0.6/ResizeSensor.min.js"></script>

	<script src="{{ asset('js/common-slider.js') }}"></script>
	<script src="{{ asset('libs/lightslider/lightslider-blur-fix.min.js') }}"></script>
	<script src="https://cdn.jsdelivr.net/npm/magnific-popup@1.1.0/dist/jquery.magnific-popup.min.js"></script>
	<script>
	commonSlider('author-adverts');

	$('.gallery').magnificPopup({
		delegate: 'a',
		type: 'image',
		gallery: {
			enabled: true
		},
		image: {
			titleSrc: 'title'
			// this tells the script which attribute has your caption
		}
	});
	</script>
@stop