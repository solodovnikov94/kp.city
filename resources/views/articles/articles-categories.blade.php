@extends('layouts.layouts-main')

@section('meta')
	@if(isset($category))
		@include('includes.meta', [
			'title' => $category->meta_title,
			'meta_description' => $category->meta_description,
			'meta_keywords' => $category->meta_keywords,
			'og_title' => $category->meta_title,
			'og_description' => $category->meta_description,
			'og_image' => $category->getCroppedImage('og', 'jpg', 'og_image') ?? ($type == 'news' ? asset('images/pages/news.jpg') : asset('images/pages/articles.jpg')),
			'og_image_type' => 'og',
			'active' => $category->active
		])
	@else
		@include('includes.meta', [
			'title' => $type == 'news' ? config('metatags.news.title') : config('metatags.articles.title'),
			'meta_description' => $type == 'news' ? config('metatags.news.description') : config('metatags.articles.description'),
			'meta_keywords' => $type == 'news' ? config('metatags.news.keywords') : config('metatags.articles.keywords'),
			'og_title' => $type == 'news' ? config('metatags.news.title') : config('metatags.articles.title'),
			'og_description' => $type == 'news' ? config('metatags.news.description') : config('metatags.articles.description'),
			'og_image' => $type == 'news' ? asset('images/pages/news.jpg') : asset('images/pages/articles.jpg'),
			'og_image_type' => 'og',
			'active' => true
		])
	@endif
@stop

@section('styles')
	<link rel="stylesheet" href="{{ mix('css/common.css') }}">
	<link rel="stylesheet" href="{{ mix('css/articles.css') }}">
	<link rel="stylesheet" href="{{ asset('libs/lightslider/lightslider.css') }}">
@endsection

@section('content')
	<div class="all-news">

		<div class="breadcrumbs">
			@if(isset($category))
				@include('includes.breadcrumbs', ['links' => [
					['link' => route($type), 'title' => ($type == 'articles' ? 'Статті' : 'Новини')]
				], 'current' => $category->title])
			@else
				@include('includes.breadcrumbs', ['current' => $type == 'articles' ? 'Статті' : 'Новини'])
			@endif
		</div>

		@isset($categories)
			<ul class="categories-list m-lr m-b">
				@foreach($categories->sortBy('order_') as $item)
					<li class="categories-list__item">
						<a href="{{ $item->getUrl() }}"
							 class="btn btn-md btn-transparent btn-bordered btn-{{ $item->class }}
							 {{ isset($category) && $category->id == $item->id ? 'active' : '' }}">
							{{ $item->title }}
						</a>
					</li>
				@endforeach
			</ul>
		@endisset

		@if(isset($articles) && $articles->count() > 0)

			@include('includes.banners.top-banner')

			<div class="articles-list m-t">
				@include('articles.articles-list', [
					'articles' => $articles,
					'socialBannerPosition' => 5
				])
			</div>
			@if(isset($showMore) && $showMore)
				<a class="load-more show-more m-lr m-b"
					 data-current-page="2"
					 data-url="{{ url(Request::path()) }}">{{ $onNextPage }}</a>
			@endif
		@else
			@include('includes.alert', [
				'type' => 'info',
				'class' => 'm-lr m-b',
				'messages' => ['Упс... В даній категорії ще немає матеріалів']
			])
		@endif

		<hr class="m-lr m-b">

		@if(isset($best_week) && count($best_week) > 0)
			<div class="slider-section m-full">
				<div class="slider-section__header">
					<h3 class="slider-section__title">Найцікавіше за тиждень</h3>
					<div class="slider-section__controls">
						<div class="slider-section__controls--prev" id="best-week-news-prev">
							<i class="fal fa-angle-left"></i>
						</div>
						<div class="slider-section__controls--next" id="best-week-news-next">
							<i class="fal fa-angle-right"></i>
						</div>
					</div>
				</div>
				<div class="slider-section__content" id="best-week-news">
					@include('articles.articles-list', ['articles' => $best_week])
				</div>
			</div>

			<hr class="m-full">
		@endif

		@include('includes.banners.horizontal-banner1')

		@if(isset($best_month) && count($best_month) > 0)
			<div class="slider-section m-lr m-t">
				<div class="slider-section__header">
					<h3 class="slider-section__title">Найцікавіше за місяць</h3>
					<div class="slider-section__controls">
						<div class="slider-section__controls--prev" id="best-month-news-prev">
							<i class="fal fa-angle-left"></i>
						</div>
						<div class="slider-section__controls--next" id="best-month-news-next">
							<i class="fal fa-angle-right"></i>
						</div>
					</div>
				</div>
				<div class="slider-section__content" id="best-month-news">
					@include('articles.articles-list', ['articles' => $best_month])
				</div>
			</div>
		@endif

	</div>
@stop

@section('scripts')
	<script src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@12.0.0/dist/lazyload.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/jquery@3.4.1/dist/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sticky-sidebar@3.3.1/dist/sticky-sidebar.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/resize-sensor@0.0.6/ResizeSensor.min.js"></script>

	<script src="{{ asset('js/common-slider.js') }}"></script>
	<script src="{{ asset('libs/lightslider/lightslider-blur-fix.min.js') }}"></script>
	<script>
		commonSlider('best-week-news');
		commonSlider('best-month-news');
	</script>
@stop