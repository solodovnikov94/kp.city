@if(isset($articles) && count($articles) > 0)
	@foreach($articles as $article)
		@if(isset($socialBannerPosition) && $socialBannerPosition == $loop->iteration)
			@include('includes.banners.banners-social-card', ['type' => 'article'])
		@endif
		<div class="article">
			<div class="loaded-loader"></div>
			<div class="article__image">
				<img src="{{ $article->getCroppedImageOrTransparent('low') }}"
						 class="b-lazy-default"
						 alt="{{ $article->image_title ?? $article->title }}">
				<img class="b-lazy"
						 data-src="{{ $article->getCroppedImageOrError('card') }}"
						 alt="{{ $article->image_title ?? $article->title }}">
			</div>

			<div class="article__buttons">
				<a href="{{ $article->category->getUrl() }}" class="btn btn-sm btn-bordered btn-{{ $article->category->class }} active">{{ $article->category->title }}</a>
			</div>

			<a href="{{ $article->getUrl() }}"
				 class="article__content">
				<div class="article__footer">
					<div class="article__title">{{ $article->title }}</div>
					<div class="article__information">
						<time class="article__datetime"
									datetime="{{ $article->published_at->toAtomString() }}">
							{{ $article->published_at->addDays(7) < now() ? $article->published_at->format('d.m.Y — H:i') : $article->published_at->diffForHumans() }}
						</time>
						<div class="article__views-counter">
              <svg aria-hidden="true" focusable="false" data-prefix="fal" data-icon="eye" class="svg-inline--fa fa-eye fa-w-18" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M288 288a64 64 0 0 0 0-128c-1 0-1.88.24-2.85.29a47.5 47.5 0 0 1-60.86 60.86c0 1-.29 1.88-.29 2.85a64 64 0 0 0 64 64zm284.52-46.6C518.29 135.59 410.93 64 288 64S57.68 135.64 3.48 241.41a32.35 32.35 0 0 0 0 29.19C57.71 376.41 165.07 448 288 448s230.32-71.64 284.52-177.41a32.35 32.35 0 0 0 0-29.19zM288 96a128 128 0 1 1-128 128A128.14 128.14 0 0 1 288 96zm0 320c-107.36 0-205.46-61.31-256-160a294.78 294.78 0 0 1 129.78-129.33C140.91 153.69 128 187.17 128 224a160 160 0 0 0 320 0c0-36.83-12.91-70.31-33.78-97.33A294.78 294.78 0 0 1 544 256c-50.53 98.69-148.64 160-256 160z"></path></svg>
							<span>{{ $article->views }}</span>
						</div>
					</div>
				</div>
			</a>
		</div>
	@endforeach
@else
	<div class="article">
		<div class="article__image">
			<img src="https://kp.city/storage/articles/60_low.jpg"
					 class="b-lazy-default"
					 alt="Жовтневі фестивалі в Кам'янці-Подільському">
			<img class="b-lazy"
					 data-src="https://kp.city/storage/articles/60_card.jpg"
					 alt="Жовтневі фестивалі в Кам'янці-Подільському">
		</div>

		<div class="article__buttons">
			<a href="#" class="btn btn-sm btn-bordered btn-news-culture active">Культура і мистецтво</a>
		</div>

		<a href="#"
			 class="article__content">
			<div class="article__footer">
				<div class="article__title">Жовтневі фестивалі в Кам'янці-Подільському</div>
				<div class="article__information">
					<div class="article__datetime">23.09.18 — 13:11</div>
					<div class="article__views-counter">
						<i class="fal fa-eye"></i>
						<span>10</span>
					</div>
				</div>
			</div>
		</a>
	</div>
	<div class="article">
		<div class="article__image">
			<img src="https://kp.city/storage/articles/60_low.jpg"
					 class="b-lazy-default"
					 alt="Жовтневі фестивалі в Кам'янці-Подільському">
			<img class="b-lazy"
					 data-src="https://kp.city/storage/articles/60_card.jpg"
					 alt="Жовтневі фестивалі в Кам'янці-Подільському">
		</div>

		<div class="article__buttons">
			<a href="#" class="btn btn-sm btn-bordered btn-news-culture active">Культура і мистецтво</a>
		</div>

		<a href="#"
			 class="article__content">
			<div class="article__footer">
				<div class="article__title">Жовтневі фестивалі в Кам'янці-Подільському</div>
				<div class="article__information">
					<div class="article__datetime">23.09.18 — 13:11</div>
					<div class="article__views-counter">
						<i class="fal fa-eye"></i>
						<span>10</span>
					</div>
				</div>
			</div>
		</a>
	</div>
	<div class="article">
		<div class="article__image">
			<img src="https://kp.city/storage/articles/60_low.jpg"
					 class="b-lazy-default"
					 alt="Жовтневі фестивалі в Кам'янці-Подільському">
			<img class="b-lazy"
					 data-src="https://kp.city/storage/articles/60_card.jpg"
					 alt="Жовтневі фестивалі в Кам'янці-Подільському">
		</div>

		<div class="article__buttons">
			<a href="#" class="btn btn-sm btn-bordered btn-news-culture active">Культура і мистецтво</a>
		</div>

		<a href="#"
			 class="article__content">
			<div class="article__footer">
				<div class="article__title">Жовтневі фестивалі в Кам'янці-Подільському</div>
				<div class="article__information">
					<div class="article__datetime">23.09.18 — 13:11</div>
					<div class="article__views-counter">
						<i class="fal fa-eye"></i>
						<span>10</span>
					</div>
				</div>
			</div>
		</a>
	</div>
	<div class="article">
		<div class="article__image">
			<img src="https://kp.city/storage/articles/60_low.jpg"
					 class="b-lazy-default"
					 alt="Жовтневі фестивалі в Кам'янці-Подільському">
			<img class="b-lazy"
					 data-src="https://kp.city/storage/articles/60_card.jpg"
					 alt="Жовтневі фестивалі в Кам'янці-Подільському">
		</div>

		<div class="article__buttons">
			<a href="#" class="btn btn-sm btn-bordered btn-news-culture active">Культура і мистецтво</a>
		</div>

		<a href="#"
			 class="article__content">
			<div class="article__footer">
				<div class="article__title">Жовтневі фестивалі в Кам'янці-Подільському</div>
				<div class="article__information">
					<div class="article__datetime">23.09.18 — 13:11</div>
					<div class="article__views-counter">
						<i class="fal fa-eye"></i>
						<span>10</span>
					</div>
				</div>
			</div>
		</a>
	</div>
	<div class="article">
		<div class="article__image">
			<img src="https://kp.city/storage/articles/60_low.jpg"
					 class="b-lazy-default"
					 alt="Жовтневі фестивалі в Кам'янці-Подільському">
			<img class="b-lazy"
					 data-src="https://kp.city/storage/articles/60_card.jpg"
					 alt="Жовтневі фестивалі в Кам'янці-Подільському">
		</div>

		<div class="article__buttons">
			<a href="#" class="btn btn-sm btn-bordered btn-news-culture active">Культура і мистецтво</a>
		</div>

		<a href="#"
			 class="article__content">
			<div class="article__footer">
				<div class="article__title">Жовтневі фестивалі в Кам'янці-Подільському</div>
				<div class="article__information">
					<div class="article__datetime">23.09.18 — 13:11</div>
					<div class="article__views-counter">
						<i class="fal fa-eye"></i>
						<span>10</span>
					</div>
				</div>
			</div>
		</a>
	</div>
	<div class="article">
		<div class="article__image">
			<img src="https://kp.city/storage/articles/60_low.jpg"
					 class="b-lazy-default"
					 alt="Жовтневі фестивалі в Кам'янці-Подільському">
			<img class="b-lazy"
					 data-src="https://kp.city/storage/articles/60_card.jpg"
					 alt="Жовтневі фестивалі в Кам'янці-Подільському">
		</div>

		<div class="article__buttons">
			<a href="#" class="btn btn-sm btn-bordered btn-news-culture active">Культура і мистецтво</a>
		</div>

		<a href="#"
			 class="article__content">
			<div class="article__footer">
				<div class="article__title">Жовтневі фестивалі в Кам'янці-Подільському</div>
				<div class="article__information">
					<div class="article__datetime">23.09.18 — 13:11</div>
					<div class="article__views-counter">
						<i class="fal fa-eye"></i>
						<span>10</span>
					</div>
				</div>
			</div>
		</a>
	</div>
@endif
