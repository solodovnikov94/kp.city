@extends('layouts.layouts-main')

@section('schema')
	@include('includes.schema.article-schema')
@endsection

@section('meta')
	@include('includes.meta', [
		'title' => $article->meta_title ?? $article->title,
		'meta_description' => $article->meta_description,
		'meta_keywords' => $article->meta_keywords,
		'og_title' => $article->og_title ?? $article->meta_title,
		'og_description' => $article->og_description ?? $article->meta_description,
		'og_image' => $article->getCroppedImage('og', 'jpg', 'og_image') ?? $article->getCroppedImage('max') ?? ($type == 'news' ? asset('images/pages/news.jpg') : asset('images/pages/articles.jpg')),
		'og_image_type' => $article->getCroppedImage('og', 'jpg', 'og_image') ? 'og' : ($article->getCroppedImage('max') ? 'max' : 'og'),
		'active' => $article->isPublished()
	])
@stop

@section('styles')
	<link rel="stylesheet" href="{{ mix('css/common.css') }}">
	<link rel="stylesheet" href="{{ mix('css/articles.css') }}">
	@if($article->poll)
		<link rel="stylesheet" href="{{ mix('css/polls.css') }}">
	@endif
	<link rel="stylesheet" href="{{ mix('css/comments.css') }}">
	<link rel="stylesheet" href="{{ asset('libs/lightslider/lightslider.css') }}">
@endsection

@section('content')
	<div class="breadcrumbs">
		@include('includes.breadcrumbs', ['links' => [
			['link' => route($type), 'title' => $type == 'articles' ? 'Статті' : 'Новини'],
			['link' => url($article->category->getUrl()), 'title' => $article->category->title]
		], 'current' => $article->title])
		@can('Access to Nova')
			<div class="breadcrumbs-controls tooltip-parent">
				<i class="fal fa-ellipsis-h"></i>
				<div class="tooltip tooltip-left tooltip-controls tooltip-blue">
					<a target="_blank" href="{{ url('/adm/resources/'.$type.'/'.$article->id) }}">Переглянути</a>
					<a target="_blank" href="{{ url('/adm/resources/'.$type.'/'.$article->id.'/edit') }}">Редагувати</a>
				</div>
			</div>
		@endcan
	</div>

	<div class="article-single">

		<div class="article-single__header">
			<div class="article-single__author">
				<div class="article-single__author-avatar">
					<img class="b-lazy"
							 data-src="{{ $article->author->getAvatar() }}"
							 alt="{{ $article->author->name }}">
				</div>
				<div class="article-single__author-information">
					<div class="article-single__author-name">{{ $article->author->name }} {{ $article->author->surname }}</div>
					<div class="article-single__author-role">Автор матеріалу</div>
				</div>
			</div>
			<div class="article-single__information">
				<time class="article-single__information-datetime" datetime="{{ $article->published_at->toAtomString() }}">
					<span>{{ $article->published_at->format('H:i') }}</span>
					<span>{{ $article->published_at->format('d.m.y') }}</span>
				</time>
				<div class="article-single__information-labels btn-container">
					<a class="article-single__information-category btn btn-xl btn-bordered btn-{{ $article->category->class }} active"
						 href="{{ $article->category->getUrl() }}">{{ $article->category->title }}</a>

					@if($article->advertising)
						<div class="btn btn-xl btn-blue circle tooltip-parent">
							<i class="fal fa-dollar-sign"></i>
							<div class="tooltip tooltip-left tooltip-blue">Ця стаття <a href="#commercial-article">комерційна</a>. Адміністрація
								сайту публікує статті з такою поміткою на правах
								реклами.
							</div>
						</div>
					@endif
				</div>
			</div>
		</div>

		<div class="article-single__content media-content">

			<h1>{{ $article->title }}</h1>

			@if(isset($article->source_title) && isset($article->source_link))
				<div class="article-single__content-source">Джерело: <a href="{{ $article->source_link }}" target="_blank" rel="nofollow">{{ $article->source_title }}</a></div>
			@endif

			@if($article->excerpt)
				<p>{{ $article->excerpt }}</p>
			@endif

			<figure class="media-content__image m-tb m--lr">
				<div class="media-content__image-container">
					<img src="{{ $article->getCroppedImageOrTransparent('low') }}" alt="{{ $article->image_title ?? $article->title }}">
					<picture>
						<source media="(min-width: 992px)"
										data-srcset="{{ $article->getCroppedImageOrError('max', 'webp') }}"
										type="image/webp">
						<source media="(min-width: 992px)"
										data-srcset="{{ $article->getCroppedImageOrError('max', 'jpg') }}">

						<source media="(min-width: 768px) and (max-width: 991px)"
										data-srcset="{{ $article->getCroppedImageOrError('mobile', 'webp') }}"
										type="image/webp">
						<source media="(min-width: 768px) and (max-width: 991px)"
										data-srcset="{{ $article->getCroppedImageOrError('mobile', 'jpg') }}">

						<source media="(min-width: 560px) and (max-width: 767px)"
										data-srcset="{{ $article->getCroppedImageOrError('max', 'webp') }}"
										type="image/webp">
						<source media="(min-width: 560px) and (max-width: 767px)"
										data-srcset="{{ $article->getCroppedImageOrError('max', 'jpg') }}">

						<source media="(min-width: 450px) and (max-width: 559px)"
										data-srcset="{{ $article->getCroppedImageOrError('mobile', 'webp') }}"
										type="image/webp">
						<source media="(min-width: 450px) and (max-width: 559px)"
										data-srcset="{{ $article->getCroppedImageOrError('mobile', 'jpg') }}">

						<img class="b-lazy"
								 data-src="{{ $article->getCroppedImageOrError('card') }}"
								 alt="{{ $article->image_title ?? $article->title }}">
					</picture>
				</div>
				<figcaption>{{ $article->image_title ?? $article->title }}</figcaption>
			</figure>

			@if($type == 'articles')
				@include('includes.banners.horizontal-banner1')
			@endif
			{!! $article->body !!}

			@if($article->poll)
				@include('includes.poll.poll-list', ['poll' => $article->poll])
			@endif

		</div>

		<div class="article-single__footer m-lr m-b">

			<div class="counters">
				<span><i class="fal fa-eye"></i>{{ $article->views }}</span>
				<span><i class="fal fa-comment"></i>{{ $article->comments_count }}</span>
			</div>

			<div id="fb-root"></div>
			<script async defer src="https://connect.facebook.net/uk_UA/sdk.js#xfbml=1&version=v3.2&appId=249421485733779&autoLogAppEvents=1"></script>

			{{-- button subscribe button --}}
			{{--<div class="fb-like" data-href="https://www.facebook.com/kp.city.official"--}}
				 {{--data-layout="button_count"--}}
				 {{--data-action="like"--}}
				 {{--data-size="small"--}}
				 {{--data-show-faces="true"--}}
				 {{--data-share="false"--}}
				 {{--data-colorscheme="light"--}}
			{{--></div>--}}

			{{-- twitter subscribe button --}}
			{{--<a href="https://twitter.com/kp__city?ref_src=twsrc%5Etfw"--}}
			   {{--class="twitter-follow-button"--}}
			   {{--data-show-count="false">Follow @kp__city</a>--}}
			{{--<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>--}}

			@include('includes.social-buttons', ['class' => 'share', 'url' => url(Request::path())])

		</div>

	</div>

	@if(isset($recent) && count($recent) > 0)
		<div class="slider-section m-lr m-t">
			<div class="slider-section__header">
				<h3 class="slider-section__title">Читайте також</h3>
				<div class="slider-section__controls">
					<div class="slider-section__controls--prev" id="slider-recent-prev">
						<i class="fal fa-angle-left"></i>
					</div>
					<div class="slider-section__controls--next" id="slider-recent-next">
						<i class="fal fa-angle-right"></i>
					</div>
				</div>
			</div>
			<div class="slider-section__content" id="slider-recent">
				@include('articles.articles-list', ['articles' => $recent])
			</div>
		</div>

		<hr class="m-t">
	@endif

	@include('comments.comments-list', [
		'comments' => $article->comments_list,
		'totalComments' => $article->comments_count,
		'maxLikes' => $article->max_likes
	])

@endsection

@section('scripts')
	<script src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@12.0.0/dist/lazyload.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/jquery@3.4.1/dist/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sticky-sidebar@3.3.1/dist/sticky-sidebar.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/resize-sensor@0.0.6/ResizeSensor.min.js"></script>

	<script src="{{ asset('js/common-slider.js') }}"></script>
	<script src="{{ asset('libs/lightslider/lightslider-blur-fix.min.js') }}"></script>
	<script>
		$("#article-slider").lightSlider({
			item: 1,
			slideMargin: 0,
			easing: 'ease',
			speed: 300,
			loop: false,
			enableTouch: 0,
			prevHtml: '<i class="fal fa-angle-left"></i>',
			nextHtml: '<i class="fal fa-angle-right"></i>'
		});

		commonSlider('slider-recent');
	</script>
@stop

@section('counter')
	@include('includes.counter', [
		'model' => $model,
		'column' => 'views',
		'id' => $article->id
	])
@endsection
