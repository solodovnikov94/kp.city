@extends('layouts.layouts-main')

@section('meta')
	<title>Перевірка аккаунта</title>
@stop

@section('styles')
	<link rel="stylesheet" href="{{ mix('css/common.css') }}">
	<link rel="stylesheet" href="{{ mix('css/articles.css') }}">
	<link rel="stylesheet" href="{{ mix('css/organizations.css') }}">
	<link rel="stylesheet" href="{{ asset('libs/lightslider/lightslider.css') }}">
@endsection

@section('content')
	<div class="breadcrumbs">
		@include('includes.breadcrumbs', ['current' => 'Перевірка аккаунта'])
	</div>
	<div class="m-full">
		@if (isset($confirmDanger))
			@include('includes.alert', ['type' => 'danger', 'messages' => [$confirmDanger]])
		@else
			<div class="check-user">
				<div class="check-user__title">Ви вже реєструвались на KP.CITY?</div>
				<div class="check-user__description">Ми виявили, що такий користувач вже зареєстрований у нас на сайті. Можливо, це Ви?</div>
				<div class="check-user__profile">
					<div class="check-user__profile-avatar">
						<img src="{{ $user->getAvatar() }}" alt="{{ $user->name }} {{ $user->surname }}">
					</div>
					<div class="check-user__profile-info">
						<div class="check-user__profile-info--name">{{ $user->name }} {{ $user->surname }}</div>
						<div class="check-user__profile-info--email">{{ mask_email($user->email) }}</div>
					</div>
				</div>
				<div class="btn-container m-t">
					<a href="{{ route('account.confirm.success') }}" class="btn btn-xl btn-red btn-hover"
					   onclick="event.preventDefault(); document.getElementById('user-confirm').submit();">
						Так, це я
					</a>
					<a href="{{ route('account.confirm.danger') }}" class="btn btn-xl btn-transparent btn-bordered"
					   onclick="event.preventDefault(); document.getElementById('user-do-not-confirm').submit();">Ні, це не я</a>
				</div>
			</div>
			<form id="user-confirm"
				  action="{{ route('account.confirm.success') }}"
				  method="POST"
				  style="display: none;">
				{{ csrf_field() }}
			</form>

			<form id="user-do-not-confirm"
				  action="{{ route('account.confirm.danger') }}"
				  method="POST"
				  style="display: none;">
				{{ csrf_field() }}
			</form>
		@endif
	</div>
@stop

@section('scripts')
	<script src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@12.0.0/dist/lazyload.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/jquery@3.4.1/dist/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sticky-sidebar@3.3.1/dist/sticky-sidebar.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/resize-sensor@0.0.6/ResizeSensor.min.js"></script>

	<script src="{{ asset('js/common-slider.js') }}"></script>
	<script src="{{ asset('libs/lightslider/lightslider-blur-fix.min.js') }}"></script>
	<script>
		commonSlider('slider-news');
		commonSlider('slider-articles');
		commonSlider('slider-organizations');
	</script>
@stop