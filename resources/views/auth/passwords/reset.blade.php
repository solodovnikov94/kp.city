@extends('layouts.layouts-main')

@section('meta')
	<title>Скидання паролю</title>
@stop

@section('styles')
	<link rel="stylesheet" href="{{ mix('css/common.css') }}">
@endsection

@section('content')
	<div class="breadcrumbs">
		@include('includes.breadcrumbs', ['current' => 'Відновлення паролю до аккаунта'])
	</div>
	<div class="m-full">
		<div class="check-user">
			<div class="check-user__title">Хочете відновити пароль на KP.CITY?</div>
			<div class="check-user__description">Для відновлення паролю необхідно ввести Вашу email адресу та новий пароль.</div>
			<form method="POST" class="popup__form m-t" action="{{ route('password.request') }}">

				@csrf

				<input type="hidden" name="token" value="{{ $token }}">

				<label for="reset-login-email" class="popup__form-item {{ $errors->has('email') ? ' error' : '' }}">
					<input type="email" name="email" inputmode="email" id="reset-login-email" placeholder="E-mail" value="{{ $email ?? old('email') }}" required>
					@if ($errors->has('email'))
						<span class="popup__form-error">{{ $errors->first('email') }}</span>
					@endif
				</label>
				<label for="login-password" class="popup__form-item {{ $errors->has('password') ? ' error' : '' }}">
					<input type="password" name="password" id="login-password" placeholder="Новий пароль">
					@if ($errors->has('password'))
						<span class="popup__form-error">{{ $errors->first('password') }}</span>
					@endif
				</label>
				<label for="login-password-confirmation" class="popup__form-item">
					<input type="password" name="password_confirmation" id="login-password-confirmation" placeholder="Підтвердження нового паролю">
				</label>
				<button type="submit">Змінити пароль</button>
			</form>
		</div>
	</div>
@stop

@section('scripts')
	<script src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@12.0.0/dist/lazyload.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/jquery@3.4.1/dist/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sticky-sidebar@3.3.1/dist/sticky-sidebar.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/resize-sensor@0.0.6/ResizeSensor.min.js"></script>
@stop

