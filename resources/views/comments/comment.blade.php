@foreach($comments as $comment)
	<div class="comment @if(isset($children) && $children) answer @endif"
			 data-id="{{ $comment_id ?? $comment->id }}"
			 data-comment-id="{{ $comment->id }}"
			 id="comment{{ $comment->id }}">

		<div class="comment-header">

			<div class="comment-information">
				<div class="comment-author__avatar">
					<img class="b-lazy"
							 data-src="{{ $comment->user->getAvatar(!$comment->anonymous) }}"
							 alt="@if(!$comment->anonymous) {{ $comment->user->name }} {{ $comment->user->surname }} @elseАнонімно@endif">
				</div>

				<div class="comment-author-date-information">
					<div class="comment-author__name @if(auth()->user() && auth()->user()->id == $comment->user_id) selected  @endif">
						@if(!$comment->anonymous) {{ $comment->user->name }} {{ $comment->user->surname }} @else Анонімний користувач @endif
					</div>
					<time class="comment-datetime"
								datetime="{{ $comment->created_at->toAtomString() }}">{{ $comment->created_at->diffForHumans() }}</time>

					@if($comment->rating)
						<div class="rating">
							@for($i=0; $i<5; $i++)
								<div class="rating__item @if($i < $comment->rating) active @endif">
									<i class="fa fa-star"></i>
								</div>
							@endfor
						</div>
					@endif
				</div>

			</div>

			<div class="comment-buttons">
				<div class="comment-buttons__useful tooltip-parent">
					@if(isset($maxLikes) && $maxLikes > 0 && $maxLikes == $comment->likes)
						@include('includes.trophy')
					@endif
				</div>
				<a class="comment-buttons__copy comment-link-button copy-link tooltip-parent">
					<i class="fal fa-link copy-url" data-url="{{ url(Request::url()) }}#comment{{ $comment->id }}"></i>
					<div class="tooltip tooltip-left tooltip-blue">Скопіювати посилання на коментар</div>
				</a>

				@if(auth()->user() && auth()->user()->hasAnyRole(['developer', 'admin']))
					<div class="comment-controls tooltip-parent">
						<i class="fal fa-ellipsis-h"></i>
						<div class="tooltip tooltip-left tooltip-controls tooltip-blue">
							<a class="delete-comment">Видалити</a>
							<a href="#">Забанити автора</a>
						</div>
					</div>
				@endif

			</div>

		</div>
		<div class="comment-body convert-emoji">
			{{ $comment->body }}
		</div>
		@auth
			<div class="comment-footer">
				<ul class="comment-actions">
					<li class="comment-action">
						<a class="comment-action_reply" href="#anchor-leave-comment">Відповісти</a>
					</li>
					@php
						$yourLike = $comment->likesRelation->firstWhere('user_id', '=', auth()->user()->id);
					@endphp
					<li class="comment-action">
						<a class="comment-action_helpful @if($yourLike) active @endif" href="">Корисно (<span>{{ count($comment->likesRelation) ?? 0 }}</span>)</a>
					</li>
					{{--<li class="comment-action">--}}
					{{--<a class="comment-action_complain" href="">Поскаржитись</a>--}}
					{{--</li>--}}
				</ul>
			</div>
		@endauth

	</div>
	@if(count($comment->children->where('active', 1)))
		@include('comments.comment', ['comments' => $comment->children->where('active', 1), 'children' => true, 'comment_id' => $comment->id])
	@endif
@endforeach
