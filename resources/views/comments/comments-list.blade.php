<div class="section-block comments p-full">

	<h3 class="section-title">
		<span>Відгуки до матеріалу</span>
		<span class="section-title__second-text total-comment">@if($totalComments)({{ $totalComments }})@endif</span>
	</h3>
	@if(!is_null($comments) && $comments->isNotEmpty())
		<div class="comments-list">
			@include('comments.comment', ['comments' => $comments])
		</div>
	@else
		<div class="comments-list">
			@include('includes.alert', [
				'type' => 'info',
				'class' => '',
				'messages' => ['Нажаль, ще ніхто не залишив свій відгук...']
			])
		</div>
	@endif

</div>

<hr>

<h3 class="section-title leave-comment__title m-t m-lr" id="anchor-leave-comment">
	<span>Залишити відгук</span>
	<span class="section-title__second-text" style="display: none"><a href="#"></a><a><i class="far fa-times leave-comment-button" style="display: none"></i></a></span>
</h3>

@auth
	<div class="section-block leave-comment m-lr">
		<form action="{{ route('comment.add') }}" class="comment-form">
			<div class="author-avatar">
				<img class="b-lazy"
						 data-src="{{ auth()->user()->getAvatar() }}"
						 alt="{{ auth()->user()->name }}">
			</div>
			<textarea id="comment-textarea"
								placeholder="Залишити свій відгук до матеріалу..."
								data-autoresize
								rows="1"
								aria-label="Залишити свій відгук"></textarea>
			<span class="popup__form-error"></span>

			<div class="leave-comment__footer m-t @if(isset($commentRating)) with-rating @endif">
				<div class="leave-comment__footer-interactions">
					@if(isset($commentRating))
						<div class="leave-comment__rating">
							<span class="leave-comment__rating-title">Оцінка закладу: </span>
							<div class="rating rating--form">
								<label class="rating__item" id="rating-item-5">
									<i class="fa fa-star"></i>
									<input class="rating__item-radio" type="radio" name="rating" value="5">
								</label>
								<label class="rating__item" id="rating-item-4">
									<i class="fa fa-star"></i>
									<input class="rating__item-radio" type="radio" name="rating" value="4">
								</label>
								<label class="rating__item" id="rating-item-3">
									<i class="fa fa-star"></i>
									<input class="rating__item-radio" type="radio" name="rating" value="3">
								</label>
								<label class="rating__item" id="rating-item-2">
									<i class="fa fa-star"></i>
									<input class="rating__item-radio" type="radio" name="rating" value="2">
								</label>
								<label class="rating__item" id="rating-item-1">
									<i class="fa fa-star"></i>
									<input class="rating__item-radio" type="radio" name="rating" value="1">
								</label>
							</div>
						</div>
					@endif

					<label class="leave-comment__anonymous">
						<input type="checkbox" id="leave-anonymous-comment" name="anonymous">
						<span class="leave-comment__anonymous-marker"><i class="fa fa-check"></i></span>
						<span>Анонімний відгук</span>
					</label>
				</div>

				<button class="btn btn-xl btn-red btn-hover add-comment">Відправити</button>
			</div>

		</form>
	</div>
@endauth

@guest
	@include('includes.alert', [
		'type' => 'info',
		'class' => 'm-lr',
		'messages' => ['Коментувати записи можуть лише <a class="show-popup" data-popup-id="popup-login">авторизовані</a> користувачі']
	])
@endguest