@extends('layouts.layouts-main')

@section('meta')
	@include('includes.meta', [
		'title' => config('metatags.403.title'),
		'meta_description' => config('metatags.403.description'),
		'meta_keywords' => config('metatags.403.keywords'),
		'og_title' => config('metatags.403.title'),
		'og_description' => config('metatags.403.description'),
		'og_image' => asset('images/pages/static.jpg'),
		'active' => true
	])
@stop

@section('styles')
	<link rel="stylesheet" href="{{ mix('css/common.css') }}">
	<link rel="stylesheet" href="{{ mix('css/articles.css') }}">
	<link rel="stylesheet" href="{{ mix('css/organizations.css') }}">
	<link rel="stylesheet" href="{{ asset('libs/lightslider/lightslider.css') }}">
@endsection

@section('content')
	<div class="breadcrumbs">
		@include('includes.breadcrumbs', ['current' => '404'])
	</div>
	<div class="error-code m-lr">404</div>

	@include('includes.alert', ['type' => 'danger', 'class' => 'm-lr m-b', 'messages' => ['Вибачте, але сторінка за такою адресою не існує. Будь ласка, перевірте адресу сторінки.']])

	<hr>

	@include('search.search-block', ['class' => 'm-full'])

	<hr>

	@if(isset($news) && count($news) > 0)
		<div class="slider-section m-full">
			<div class="slider-section__header">
				<h3 class="slider-section__title">Свіжі новини</h3>
				<div class="slider-section__controls">
					<div class="slider-section__controls--prev" id="slider-news-prev">
						<i class="fal fa-angle-left"></i>
					</div>
					<div class="slider-section__controls--next" id="slider-news-next">
						<i class="fal fa-angle-right"></i>
					</div>
				</div>
			</div>
			<div class="slider-section__content" id="slider-news">
				@include('articles.articles-list', ['articles' => $news])
			</div>
		</div>
	@endif

	<hr>

	@if(isset($articles) && count($articles) > 0)
		<div class="slider-section m-full">
			<div class="slider-section__header">
				<h3 class="slider-section__title">Цікаві статті</h3>
				<div class="slider-section__controls">
					<div class="slider-section__controls--prev" id="slider-articles-prev">
						<i class="fal fa-angle-left"></i>
					</div>
					<div class="slider-section__controls--next" id="slider-articles-next">
						<i class="fal fa-angle-right"></i>
					</div>
				</div>
			</div>
			<div class="slider-section__content" id="slider-articles">
				@include('articles.articles-list', ['articles' => $articles])
			</div>
		</div>
	@endif

	<hr>

	@if(isset($organizations) && count($organizations) > 0)
		<div class="slider-section m-lr m-t">
			<div class="slider-section__header">
				<h3 class="slider-section__title">Популярні заклади міста</h3>
				<div class="slider-section__controls">
					<div class="slider-section__controls--prev" id="slider-organizations-prev">
						<i class="fal fa-angle-left"></i>
					</div>
					<div class="slider-section__controls--next" id="slider-organizations-next">
						<i class="fal fa-angle-right"></i>
					</div>
				</div>
			</div>
			<div class="slider-section__content" id="slider-organizations">
				@include('organizations.organizations-list', ['organizations' => $organizations])
			</div>
		</div>
	@endif

@endsection

@section('scripts')
	<script src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@12.0.0/dist/lazyload.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/jquery@3.4.1/dist/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sticky-sidebar@3.3.1/dist/sticky-sidebar.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/resize-sensor@0.0.6/ResizeSensor.min.js"></script>

	<script src="{{ asset('js/common-slider.js') }}"></script>
	<script src="{{ asset('libs/lightslider/lightslider-blur-fix.min.js') }}"></script>
	<script>
		commonSlider('slider-news');
		commonSlider('slider-articles');
		commonSlider('slider-organizations');
	</script>
@stop