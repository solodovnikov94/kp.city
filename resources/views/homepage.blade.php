@extends('layouts.layouts-main')

@section('meta')
	@include('includes.meta', [
		'title' => config('metatags.homepage.title'),
		'meta_description' => config('metatags.homepage.description'),
		'meta_keywords' => config('metatags.homepage.keywords'),
		'og_title' => config('metatags.homepage.title'),
		'og_description' => config('metatags.homepage.description'),
		'og_image' => asset('images/pages/homepage.jpg'),
		'active' => true
	])
@stop

@section('schema')
	@include('includes.schema.homepage-schema')
@stop

@section('styles')
	<link rel="stylesheet" href="{{ mix('css/common.css') }}">
	<link rel="stylesheet" href="{{ mix('css/articles.css') }}">
	<link rel="stylesheet" href="{{ mix('css/polls.css') }}">
	<link rel="stylesheet" href="{{ mix('css/organizations.css') }}">
@endsection

@section('content')

	@include('includes.banners.top-banner')
	<div class="recent-news p-lr p-t">
		<div class="recent-news__list news-list">
			@include('articles.articles-list', ['articles' => $news])
		</div>
		<div class="recent-news__feed news-feed">
			<h3 class="news-feed__title">Останні новини</h3>
			@if($lastNewsByDays->isNotEmpty())
				@foreach($lastNewsByDays as $lastNews)
					<div class="news-feed__subtitle">
						@php $blockDate = \Carbon\Carbon::parse($lastNews->first()->published_at); @endphp

						@switch($blockDate->format('Y-m-d'))
							@case(now()->format('Y-m-d'))
								Сьогодні
							@break
							@case(now()->subDays(1)->format('Y-m-d'))
								Вчора
							@break
							@default
								{{ (int) $blockDate->format('d') }} {{ __($blockDate->format('F')) }}
						@endswitch
					</div>
					<ul class="news-feed__list">
						@foreach($lastNews->sortByDesc('published_at') as $item)
							@if(get_class($item) == 'App\ShortNews')
								<li class="news-feed__item">
									<div class="news-feed__item-left">
										{{--TODO Юзай "news-feed__item-time hover" якщо є посилання --}}
										<div class="news-feed__item-time">{{ $item->published_at->format('H:i') }}</div>
									</div>
									<div class="news-feed__item-right">
										<div>
											{{--<span class="news-feed__item-label lbl lbl-md lbl-orange">Партнер</span>--}}
											<span class="news-feed__item-label lbl lbl-md lbl-grey">Коротко</span>
										</div>
										<div class="news-feed__item-title">{!! $item->body !!}</div>
									</div>
								</li>
							@else
								<li class="news-feed__item @if($item->exclusive) news-feed__item_selected @endif">
									<div class="news-feed__item-left">
										<div class="news-feed__item-time hover">{{ $item->published_at->format('H:i') }}</div>
									</div>
									<div class="news-feed__item-right">
										<div>
											<span class="news-feed__item-label lbl lbl-md lbl-green">Повна новина</span>
										</div>
										<div class="news-feed__item-title">
											<a href="{{ $item->getUrl() }}">{{ $item->title }}</a>
										</div>
									</div>
								</li>
							@endif
						@endforeach
					</ul>
				@endforeach
			@endif

			<div class="btn-container margin-top">
				<a href="{{ route('news') }}" class="btn btn-xl btn-red btn-hover">Всі новини</a>
			</div>
		</div>
	</div>

	<hr class="m-t">

	@isset($poll)
		<div class="m-lr">
			@include('includes.poll.poll-list', ['poll' => $poll])
		</div>
		<a href="{{ url('/polls') }}" class="load-more m-full">Більше опитувань...</a>
		<hr>
	@endisset

	@include('includes.banners.horizontal-banner1')

	@if(isset($articles) && count($articles) > 0)
		<h3 class="section-title m-t m-lr">Цікаві статті</h3>
		<div class="articles-list m-t">
			@include('articles.articles-list', [
				'articles' => $articles,
				'socialBannerPosition' => 5
			])
		</div>
		<a href="{{ route('articles') }}" class="load-more m-lr m-b">Більше статей...</a>
	@endif

	@if(isset($organizations) && count($organizations) > 0)
		<hr class="m-t">
		<h3 class="section-title m-full">Заклади міста</h3>
		<div class="organizations-list">
			@include('organizations.organizations-list', [
				'organizations' => $organizations,
				'socialBannerPosition' => 5
			])
		</div>
		<a href="{{ route('organizations.categories') }}" class="load-more m-lr m-b">Більше закладів міста...</a>
	@endif

{{--	@include('includes.banners.banners-horizontal', [--}}
{{--		'class' => 'm-t',--}}
{{--		'size' => 'sm',--}}
{{--		'color' => '#1E5BEA',--}}
{{--		'link' => '/advertising',--}}
{{--		'image' => asset('images/banners/test158.png'),--}}
{{--		'image_title' => 'Стоматологічна клініка "Medical Center"'])--}}

	<hr>

	@if(isset($tags) && count($tags) > 0)
		<h3 class="section-title m-full">Підбірка матеріалів за темою</h3>
		<div class="m-lr m--b">
			<div class="tags-list">
				@include('tags.tags-list', ['tags' => $tags])
			</div>
		</div>
	@endif

@stop

@section('scripts')
	<script src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@12.0.0/dist/lazyload.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/jquery@3.4.1/dist/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sticky-sidebar@3.3.1/dist/sticky-sidebar.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/resize-sensor@0.0.6/ResizeSensor.min.js"></script>
@stop
