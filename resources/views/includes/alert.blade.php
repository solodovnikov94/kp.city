<div class="alert alert-{{ $type }} {{ $class ?? '' }}">
	<div class="alert__icon"><i class="far {{ $icon ?? 'fa-exclamation-circle' }}"></i></div>
	<div class="alert__content">
		@foreach($messages as $message)
			<p>{!! $message !!}</p>
		@endforeach
	</div>
</div>