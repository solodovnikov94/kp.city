@php $authBanner = true @endphp
{{--@if(show_banner(2))--}}
	{{--@php $authBanner = false @endphp--}}
	{{--<a href="/advertising"--}}
		 {{--aria-label="Новорічні свята в Болгарії всього за 400$!"--}}
		 {{--class="header__banner banner-block"--}}
		 {{--data-id="2"--}}
		 {{--style="background-color: #79cdfe;">--}}
		{{--<div class="container">--}}
			{{--<div class="header__banner-container">--}}
				{{--<div class="header__banner-image">--}}
					{{--<img class="b-lazy"--}}
							 {{--data-src="{{ asset('images/banners/header_banner.png') }}"--}}
							 {{--alt="Новорічні свята в Болгарії всього за 400$!">--}}
				{{--</div>--}}
				{{--<span class="header__banner-close close-banner-timer" onclick="return false;">--}}
					{{--<svg aria-hidden="true" data-prefix="far" data-icon="times"--}}
							 {{--class="svg-inline--fa fa-times fa-w-10" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">--}}
						{{--<path d="M207.6 256l107.72-107.72c6.23-6.23 6.23-16.34 0-22.58l-25.03-25.03c-6.23-6.23-16.34-6.23-22.58 0L160 208.4 52.28 100.68c-6.23-6.23-16.34-6.23-22.58 0L4.68 125.7c-6.23 6.23-6.23 16.34 0 22.58L112.4 256 4.68 363.72c-6.23 6.23-6.23 16.34 0 22.58l25.03 25.03c6.23 6.23 16.34 6.23 22.58 0L160 303.6l107.72 107.72c6.23 6.23 16.34 6.23 22.58 0l25.03-25.03c6.23-6.23 6.23-16.34 0-22.58L207.6 256z"></path>--}}
					{{--</svg>--}}
				{{--</span>--}}
			{{--</div>--}}
		{{--</div>--}}
	{{--</a>--}}
{{--@endif--}}
@if(!auth()->user() && show_banner(1) && $authBanner)
	<div class="header__banner banner-block" data-id="1" style="background-color: #15172D;">
		<div class="container">
			<div class="header__banner-container">
				<div class="header__banner-background">
					<img class="b-lazy"
							 data-src="{{ asset('images/icons/header-banner.png') }}"
							 alt="Зареєструйся та отримай додаткові функції для користування веб-сайтом!">
				</div>
				<div class="header__banner-content">
					<a class="show-popup" data-popup-id="popup-register">Зареєструйся</a> та отримай додаткові функції для користування веб-сайтом!
				</div>
				<span class="header__banner-close close-banner-timer">
					<svg aria-hidden="true" data-prefix="far" data-icon="times" class="svg-inline--fa fa-times fa-w-10" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">
							<path d="M207.6 256l107.72-107.72c6.23-6.23 6.23-16.34 0-22.58l-25.03-25.03c-6.23-6.23-16.34-6.23-22.58 0L160 208.4 52.28 100.68c-6.23-6.23-16.34-6.23-22.58 0L4.68 125.7c-6.23 6.23-6.23 16.34 0 22.58L112.4 256 4.68 363.72c-6.23 6.23-6.23 16.34 0 22.58l25.03 25.03c6.23 6.23 16.34 6.23 22.58 0L160 303.6l107.72 107.72c6.23 6.23 16.34 6.23 22.58 0l25.03-25.03c6.23-6.23 6.23-16.34 0-22.58L207.6 256z"></path>
					</svg>
				</span>
			</div>
		</div>
	</div>
@endif