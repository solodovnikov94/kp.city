<div class="{{ $class ?? '' }}">
	<a href="{{ $link }}" aria-label="{{ $image_title }}" class="banner-horizontal size-{{ $size }}" @isset($color) style="background-color: {{ $color }};" @endisset>
		<div class="banner-horizontal__image">
			<img class="b-lazy"
					 data-src="{{ $image }}"
					 alt="{{ $image_title }}">
		</div>
	</a>
</div>