@php
	switch (rand(1, 5)) {
		case 1: $social = [
			'type' => 'twitter',
			'image' => 'images/banners/social/' . $type . '-twitter.png',
			'image_alt' => config('app.kp_socials.twitter.alt_text'),
			'link' => config('app.kp_socials.twitter.link'),
			'title' => config('app.kp_socials.twitter.title'),
			'subtitle' => config('app.kp_socials.twitter.subtitle'),
			'button' => 'Підписатися'
		]; break;
		case 2: $social = [
			'type' => 'facebook',
			'image' => 'images/banners/social/' . $type . '-facebook.png',
			'image_alt' => config('app.kp_socials.facebook.alt_text'),
			'link' => config('app.kp_socials.facebook.link'),
			'title' => config('app.kp_socials.facebook.title'),
			'subtitle' => config('app.kp_socials.facebook.subtitle'),
			'button' => 'Підписатися'
		]; break;
		case 3: $social = [
			'type' => 'telegram',
			'image' => 'images/banners/social/' . $type . '-telegram.png',
			'image_alt' => config('app.kp_socials.telegram.alt_text'),
			'link' => config('app.kp_socials.telegram.link'),
			'title' => config('app.kp_socials.telegram.title'),
			'subtitle' => config('app.kp_socials.telegram.subtitle'),
			'button' => 'Підписатися'
		]; break;
		case 4: $social = [
			'type' => 'instagram',
			'image' => 'images/banners/social/' . $type . '-instagram.png',
			'image_alt' => config('app.kp_socials.instagram.alt_text'),
			'link' => config('app.kp_socials.instagram.link'),
			'title' => config('app.kp_socials.instagram.title'),
			'subtitle' => config('app.kp_socials.instagram.subtitle'),
			'button' => 'Підписатися'
		]; break;
		default: $social = [
			'type' => 'vk',
			'image' => 'images/banners/social/' . $type . '-vk.png',
			'image_alt' => config('app.kp_socials.vkontakte.alt_text'),
			'link' => config('app.kp_socials.vkontakte.link'),
			'title' => config('app.kp_socials.vkontakte.title'),
			'subtitle' => config('app.kp_socials.vkontakte.subtitle'),
			'button' => 'Приєднатися'
		];
	}
@endphp

<div class="{{ $type }} social-banner {{ $type }}-{{ $social['type'] }}">
	<div class="social-banner__image">
		<img class="b-lazy"
				 data-src="{{ asset($social['image']) }}"
				 alt="{{ $social['image_alt'] }}">
	</div>
	<a href="{{ $social['link'] }}" target="_blank" rel="noopener" class="social-banner__content">
		<div class="social-banner__title">{{ $social['title'] }}</div>
		<div class="social-banner__subtitle">{{ $social['subtitle'] }}</div>
		<span class="social-banner__button">{{ $social['button'] }}</span>
	</a>
</div>