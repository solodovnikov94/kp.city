@php
	switch (rand(1, 4)) {
		case 1: $social = [
			'type' => 'telegram',
			'title' => config('app.kp_socials.telegram.title'),
			'subtitle' => config('app.kp_socials.telegram.subtitle'),
			'link' => config('app.kp_socials.telegram.link'),
		]; break;
		case 2: $social = [
			'type' => 'twitter',
			'title' => config('app.kp_socials.twitter.title'),
			'subtitle' => config('app.kp_socials.twitter.subtitle'),
			'link' => config('app.kp_socials.twitter.link'),
		]; break;
		case 3: $social = [
			'type' => 'vk',
			'title' => config('app.kp_socials.vkontakte.title'),
			'subtitle' => config('app.kp_socials.vkontakte.subtitle'),
			'link' => config('app.kp_socials.vkontakte.link'),
		]; break;
		default: $social = [
			'type' => 'facebook',
			'title' => config('app.kp_socials.facebook.title'),
			'subtitle' => config('app.kp_socials.facebook.subtitle'),
			'link' => config('app.kp_socials.facebook.link'),
		];
	}
@endphp

<div class="banner__wrapper m-full">
	<div class="banner banner-social banner-social--{{ $social['type'] }} banner-h150 banner-rounded">
		<a class="banner__link" href="{{ $social['link'] }}" target="_blank" rel="noopener">
			<div class="banner__content">
				<div class="banner__icon"></div>
				<div class="banner__title">{{ $social['title'] }}</div>
				<div class="banner__subtitle">{{ $social['subtitle'] }}</div>
			</div>
		</a>
		<div class="banner__button-container">
			<a href="{{ $social['link'] }}" target="_blank" rel="noopener">Відвідати</a>
		</div>
	</div>
</div>