<ol class="breadcrumbs-list">
	<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb" @if(isset($links) && count($links) > 0) itemref="breadcrumb-1" @endif
	@if(!(isset($links) && count($links) > 0)) class="breadcrumbs-list__back-link" @endif>
		<a href="{{ route('home') }}" itemprop="url"><span itemprop="title">KP.CITY</span></a>
	</li>
	@if(isset($links) && count($links) > 0)
		@foreach($links as $item)
			<li itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" id="breadcrumb-{{ $loop->iteration }}"
					@if(!$loop->last) itemref="breadcrumb-{{ $loop->iteration + 1 }}" @else class="breadcrumbs-list__back-link" @endif>
				<a href="{{ $item['link'] }}" itemprop="url"><span itemprop="title">{{ $item['title'] }}</span></a>
			</li>
		@endforeach
	@endif
	<li>
		<span>{{ $current }}</span>
	</li>
</ol>