{{--float-alert__container display none--}}
<div class="">
	<div class="float-alert float-alert--{{ $type }}">
		<div class="float-alert__content">{!! $message !!}</div>
		<a class="float-alert__close"><i class="far fa-times"></i></a>
	</div>
</div>