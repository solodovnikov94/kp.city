<div class="form__item @isset($error) error @endisset">
	<div class="form__item-header">
		<span class="form__item-title">{{ $name }}@isset($required)<strong>*</strong>@endisset</span>
		@isset($hint)
			<span class="form__item-hint tooltip-parent">
				<i class="fa fa-info-circle"></i>
				<span class="tooltip tooltip-left tooltip-blue">{{ $hint }}</span>
			</span>
		@endisset
	</div>
	<label class="form__item-field form__item-image--container">
		<span class="form__item-image">
			<input type="file" accept="image/*"
						 onchange="loadFile(event, '{{ $field_name }}-preview')"
						 name="{{ $field_name }}">
			<img class="b-lazy"
					 id="{{ $field_name }}-preview"
					 data-src=""
					 alt="">
		</span>
	</label>
	@isset($error)
		<div class="form__item-alert">{{ $error }}</div>
	@endisset
</div>