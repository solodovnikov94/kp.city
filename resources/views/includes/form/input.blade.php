<div class="form__item @isset($error) error @endisset">
	<div class="form__item-header">
		<span class="form__item-title">{{ $name }}@isset($required)<strong>*</strong>@endisset</span>
		@isset($hint)
			<span class="form__item-hint tooltip-parent">
				<i class="fa fa-info-circle"></i>
				<span class="tooltip tooltip-left tooltip-blue">{{ $hint }}</span>
			</span>
		@endisset
	</div>
	<label class="form__item-field">
		@isset($prefix)
			<span class="form__item-prefix">{{ $prefix }}</span>
		@endisset
		<input class="form__item-input @isset($counter_max) form__item-input--counted @endisset"
					 type="{{ $type ?? 'text' }}"
					 placeholder="{{ $placeholder ?? $name }}"
					 value="{{ $value ?? '' }}"
					 @isset($required) required @endisset>
		@isset($counter_max)
			<span class="form__item-counter @isset($counter_slice) slice @endisset"
						data-max="{{ $counter_max }}"
						@isset($counter_warning) data-warning="{{ $counter_warning }}" @endisset
						@isset($counter_recommended_min) data-recommended-left="{{ $counter_recommended_min }}" @endisset
						@isset($counter_recommended_max) data-recommended-right="{{ $counter_recommended_max }}" @endisset
			>{{ $counter_max }}</span>
		@endisset
	</label>
	@isset($error)
		<div class="form__item-alert">{{ $error }}</div>
	@endisset
</div>