<title>{{ $title }}</title>

<meta name="description" content="{{ $meta_description }}">

@isset($meta_keywords)
	<meta name="keywords" content="{{ $meta_keywords }}">
@endisset

<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="{{ config('app.kp_socials.twitter.title') }}">
<meta name="twitter:title" content="{{ $og_title }}">
<meta name="twitter:description" content="{{ $og_description }}">
<meta name="twitter:creator" content="{{ config('app.kp_socials.twitter.title') }}">
<meta name="twitter:image:src" content="{{ $og_image }}">
<meta name="twitter:domain" content="{{ url('/') }}">

<meta itemprop="name" content="{{ $og_title }}">
<meta itemprop="description" content="{{ $og_description }}">
<meta itemprop="image" content="{{ $og_image }}">

<meta property="og:title" content="{{ $og_title }}">
<meta property="og:description" content="{{ $og_description }}">
<meta property="og:type" content="website">
<meta property="og:url" content="{{ url(Request::path()) }}">
<meta property="og:image" content="{{ $og_image }}">
<meta property="og:locale" content="uk_UA">

@if(isset($og_image_type))
	@if($og_image_type == 'og')
		<meta property="og:image:width" content="{{ config('crops.default_size.og.w') }}">
		<meta property="og:image:height" content="{{ config('crops.default_size.og.h') }}">
	@elseif($og_image_type == 'max')
		<meta property="og:image:width" content="{{ config('crops.default_size.max.w') }}">
		<meta property="og:image:height" content="{{ config('crops.default_size.max.h') }}">
	@endif
@else
	@if(isset($og_image_width) && isset($og_image_height))
		<meta property="og:image:width" content="{{ $og_image_width }}">
		<meta property="og:image:height" content="{{ $og_image_height }}">
	@endif
@endif

@if(!$active)
	<meta name="robots" content="noindex, nofollow">
@endif