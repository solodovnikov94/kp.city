<div class="poll m-t">
	<h3 class="poll__title">{{ $poll->title }}</h3>
	@if($poll->isVoted())
		<form class="poll__form" action="{{ route('poll', $poll->slug) }}" method="post">
			@csrf
			@foreach($poll->pollAnswers() as $answer)
				<div class="poll__form-answer">
					<label for="answer-{{ $answer->id }}">
						<input type="radio" name="poll" @if ($loop->first) checked="checked" @endif id="answer-{{ $answer->id }}" value="{{ $answer->id }}">
						<span class="marker"></span>
						<span>{{ $answer->title }}</span>
					</label>
				</div>
			@endforeach
			<div class="buttons-container">
				<button type="submit" class="btn btn-xl btn-hover btn-red send-poll">Проголосувати</button>
				<span>Вже проголосувало: {{ $poll->totalVotes() }}</span>
			</div>
		</form>
	@else
		@include('includes.poll.poll-result')
	@endif
</div>