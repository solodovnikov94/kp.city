<div class="poll__results">
	@foreach ($poll->pollResultAnswers() as $key => $pollAnswer)
		<div class="poll__results-answer">
			<div class="poll__results-title">{{ $pollAnswer->title }}</div>
			<div class="poll__results-stripe">
				@if($poll->totalVotes())
					<div style="width: {{ round(100 / $poll->totalVotes() * $pollAnswer->scores->count()) }}%"></div>
				@endif
				<span>{{ $pollAnswer->scores->count() }} гол.</span>
			</div>
		</div>
	@endforeach
</div>
<div class="buttons-container">
	@if($poll->separate && url(Request::path()) != $poll->getUrl())
		<a href="{{ $poll->getUrl() }}" class="btn btn-xl btn-hover btn-red">Детальніше</a>
	@endif
	<span>Всього голосів: {{ $poll->totalVotes() }}</span>
</div>