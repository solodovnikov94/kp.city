<div class="popup__wrapper" id="popup-login">
	<div class="popup">
		<div class="popup__controls">
			<a class="popup__close"><i class="far fa-times"></i></a>
		</div>
		<div class="popup__header">
			<div class="popup__title">Вхід на KP.CITY</div>
			<div class="popup__description">Увійдіть та отримайте додаткові функції для повного використання веб-сайту.</div>
		</div>
		<div class="popup__content">
			<div class="popup__socials">
				<a href="{{ url('login/facebook') }}" class="popup__socials-item facebook">
					<span class="popup__socials-item--icon">
						<i class="fab fa-facebook-f"></i>
					</span>
					<span>Facebook</span>
				</a>
				<a href="{{ url('login/google') }}" class="popup__socials-item gmail">
					<span class="popup__socials-item--icon">
						<i class="far fa-envelope"></i>
					</span>
					<span>Gmail</span>
				</a>
			</div>

			<div class="popup__divider m-tb">
				<span>АБО</span>
			</div>

			<form method="POST" action="{{ route('login') }}" class="popup__form">
				@csrf
				<label for="login-email" class="popup__form-item">
					<input type="email" name="email" inputmode="email" id="login-email" placeholder="E-mail">
					<span class="popup__form-error"></span>
				</label>
				<label for="login-password" class="popup__form-item">
					<input type="password" name="password" id="login-password" placeholder="Пароль">
				</label>
				<div class="popup__form-line">
					<label class="popup__form-line--checkbox" for="login-remember-me">
						<input type="checkbox" checked name="remember_me" id="login-remember-me">
						<span class="popup__form-line--checkbox-marker"><i class="fa fa-check"></i></span>
						<span>Запам'ятати мене</span>
					</label>
					<a class="show-popup" data-popup-id="popup-remember-email">Забули пароль?</a>
				</div>
				<button class="auth">Увійти на сайт</button>
			</form>

		</div>
		<div class="popup__footer">
			<div class="popup__footer-line">Ще не зареєстровані?</div>
			<div class="popup__footer-line"><a class="show-popup" data-popup-id="popup-register">Створити профіль</a></div>
		</div>
	</div>
</div>