<div class="popup__wrapper" id="popup-propose-changes">
	<div class="popup">
		<div class="popup__controls">
			<a class="popup__close"><i class="far fa-times"></i></a>
		</div>
		<div class="popup__header">
			<div class="popup__title">Запропонувати зміни</div>
			<div class="popup__description">Представлені на сайті дані про заклад є неактуальними? Будь ласка, повідомте нас про це і ми найближчим часом оновимо їх.</div>
		</div>
		<div class="popup__content">

			<form method="POST" action="{{ url('/organization-changes') }}" class="popup__form">
				<label for="remember-email" class="popup__form-item">
					<textarea placeholder="Які дані потребують оновлення?" name="message" id="changes-message"></textarea>
					<span class="popup__form-error"></span>
				</label>
				<label class="popup__form-item">
					<span class="g-recaptcha" data-sitekey="6LfqoIQUAAAAAK2WDkXbwN4so4la7h6sS-_Iooqr"></span>
					<input type="hidden" id="changes-g-recaptcha-response" aria-label="Captcha">
					<span class="popup__form-error"></span>
				</label>
				<button class="add-changes">Відправити</button>
				@csrf
			</form>

		</div>
	</div>
</div>