<div class="popup__wrapper" id="popup-register">
	<div class="popup "> {{--loader--}}
		<div class="popup__controls justify-between">
			<a class="popup__back show-popup" data-popup-id="popup-login"><i class="far fa-long-arrow-left"></i>Вхід</a>
			<a class="popup__close"><i class="far fa-times"></i></a>
		</div>
		<div class="popup__header">
			<div class="popup__title">Реєстрація на KP.CITY</div>
			<div class="popup__description">Зареєструйтесь та отримайте додаткові функції для повного використання веб-сайту.</div>
		</div>
		<div class="popup__content">
			<div class="popup__socials">
				<a href="{{ url('login/facebook') }}" class="popup__socials-item facebook">
					<span class="popup__socials-item--icon">
						<i class="fab fa-facebook-f"></i>
					</span>
					<span>Facebook</span>
				</a>
				<a href="{{ url('login/google') }}" class="popup__socials-item gmail">
					<span class="popup__socials-item--icon">
						<i class="far fa-envelope"></i>
					</span>
					<span>Gmail</span>
				</a>
			</div>

			<div class="popup__divider m-tb">
				<span>АБО</span>
			</div>

			<form method="POST" action="{{ route('register') }}" class="popup__form">
				@csrf
				<label for="register-name" class="popup__form-item">
					<input type="text" name="name" id="register-name" placeholder="Ім'я">
					<span class="popup__form-error"></span>
				</label>
				<label for="register-email" class="popup__form-item">
					<input type="email" name="email" id="register-email" placeholder="E-mail" inputmode="email">
					<span class="popup__form-error"></span>
				</label>
				<label for="register-password" class="popup__form-item">
					<input type="password" name="password" id="register-password" placeholder="Пароль">
					<span class="popup__form-error"></span>
				</label>
				<label for="register-password-confirmation" class="popup__form-item">
					<input type="password" name="password_confirmation" id="register-password-confirmation" placeholder="Підтвердження паролю">
				</label>
				<button class="auth">Зареєструватися</button>
			</form>

		</div>
		<div class="popup__footer">
			<div class="popup__footer-line">Вже зареєстровані?</div>
			<div class="popup__footer-line"><a class="show-popup" data-popup-id="popup-login">Увійти на сайт</a></div>
		</div>
	</div>
</div>