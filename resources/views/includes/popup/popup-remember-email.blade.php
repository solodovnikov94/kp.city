<div class="popup__wrapper" id="popup-remember-email">
	<div class="popup">
		<div class="popup__controls justify-between">
			<a class="popup__back show-popup" data-popup-id="popup-login"><i class="far fa-long-arrow-left"></i>Вхід</a>
			<a class="popup__close"><i class="far fa-times"></i></a>
		</div>
		<div class="popup__header">
			<div class="popup__title">Забули пароль?</div>
			<div class="popup__description">Введіть адресу Вашої електронної пошти і ми вишлемо Вам інстуркцію по відновленню паролю.</div>
		</div>
		<div class="popup__content">

			<form method="POST" action="{{ route('password.email') }}" class="popup__form">
				@csrf
				<label for="remember-email" class="popup__form-item">
					<input type="email" name="email" id="remember-email" placeholder="E-mail" inputmode="email">
					<span class="popup__form-error"></span>
				</label>
				<button class="auth">Відправити</button>
			</form>

		</div>
		<div class="popup__footer">
			<div class="popup__footer-line">Згадали пароль?</div>
			<div class="popup__footer-line"><a class="show-popup" data-popup-id="popup-login">Увійти на сайт</a></div>
		</div>
	</div>
</div>