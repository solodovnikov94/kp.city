<script type="application/ld+json">
	{
		"@context": "http://schema.org",
		"@type": "NewsArticle",
		"mainEntityOfPage": {
			"@type": "WebPage",
			"@id": "{{ url(Request::path()) }}"
		},
		"headline": "{{ $article->meta_title ?? $article->title }}",
		@isset($article->meta_description) "description": "{{ $article->meta_description }}",@endisset
		@if($article->getCroppedImage('og', 'jpg', 'og_image') || $article->getCroppedImage('max'))
			"image": {
				"@type": "ImageObject",
				"url": "{{ $article->getCroppedImage('og', 'jpg', 'og_image') ?? $article->getCroppedImage('max') }}",
				"width": {{ $article->getCroppedImage('og', 'jpg', 'og_image') ? config('crops.default_size.og.w') : config('crops.default_size.max.w') }},
				"height": {{ $article->getCroppedImage('og', 'jpg', 'og_image') ? config('crops.default_size.og.h') : config('crops.default_size.max.h') }}
			},
		@endif
		"datePublished": "{{ $article->published_at->toAtomString() }}",
		"dateModified": "{{ $article->updated_at->toAtomString() }}",
		"author":{
			"@type": "Person",
			"name": "{{ $article->author->name }} {{ $article->author->surname }}"
		},
		"publisher":{
			"@type": "Organization",
			"name": "KP.CITY",
			"email": "info@kp.city",
			"url": "{{ url('/') }}",
			"logo":{
				"@type": "ImageObject",
				"url": "https://kp.city/images/logotypes/publisher-logo.png",
				"width": 60,
				"height": 60
			}
		}
	}
</script>
