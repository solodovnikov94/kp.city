<script type="application/ld+json">
	{
	"@context": "http://schema.org",
	"@type": "WebSite",
	"url": "{{ env('APP_URL') }}",
	"potentialAction": {
			"@type": "SearchAction",
			"target": "{{ env('APP_URL') }}/search?q={search_term_string}",
			"query-input": "required name=search_term_string"
		}
	}
</script>

<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "Organization",
  "description": "KP.CITY — сайт міста Кам'янця-Подільського. Актуальні новини та цікаві статті, каталог закладів та установ міста, опитування, відгуки та багато іншого.",
	"name": "KP.CITY",
  "url": "https://kp.city",
  "email": "info@kp.city",
  "logo":{
		"@type": "ImageObject",
		"url": "https://kp.city/images/logotypes/publisher-logo.png",
		"width": 60,
		"height": 60
	},
	"sameAs": ["{{ config('app.kp_socials.instagram.link') }}", "{{ config('app.kp_socials.facebook.link') }}", "{{ config('app.kp_socials.twitter.link') }}", "{{ config('app.kp_socials.telegram.link') }}", "{{ config
	('app.kp_socials.vkontakte.link') }}"]
}
</script>