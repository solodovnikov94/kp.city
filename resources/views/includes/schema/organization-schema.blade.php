<script type="application/ld+json">
	{
		"description": "{{ $organization->excerpt }}",
		"name": "{{ $organization->name }}",
		@if($organization->getCroppedImage('og', 'jpg', 'og_image') || $organization->getCroppedImage('max'))
			"image": {
				"@type": "ImageObject",
				"url": "{{ $organization->getCroppedImage('og', 'jpg', 'og_image') ?? $organization->getCroppedImage('max') }}",
				"width": {{ $organization->getCroppedImage('og', 'jpg', 'og_image') ? config('crops.default_size.og.w') : config('crops.default_size.max.w') }},
				"height": {{ $organization->getCroppedImage('og', 'jpg', 'og_image') ? config('crops.default_size.og.h') : config('crops.default_size.max.h') }}
			},
		@endif
		@if($organization->getCroppedImage('logo', 'jpg', 'logotype'))
			"logo":{
				"@type": "ImageObject",
				"url": "{{ $organization->getCroppedImage('logo', 'jpg', 'logotype') }}",
				"width": 66,
				"height": 66
			},
		@endif
		@if(isset($organization->sites) && count($organization->sites) > 0)
			"url": "{{ (strpos($organization->sites[0]['url'], 'http://') === false) && (strpos($organization->sites[0]['url'], 'https://') === false)
				? 'http://'.$organization->sites[0]['url']
				: $organization->sites[0]['url']}}",
		@else
			"url": "{{ url(Request::path()) }}",
		@endif
		@if(isset($organization->phones) && count($organization->phones) > 0)
			"telephone": "{{ $organization->phones[0]['phone'] }}",
		@endif
		@if(isset($organization->emails) && count($organization->emails) > 0)
			"email": "{{ $organization->emails[0]['email'] }}",
		@endif
		@if(isset($organization->addresses) && count($organization->addresses) > 0)
			"address": {
				"@type": "PostalAddress",
				"streetAddress": "{{ $organization->addresses[0]['address'] }}",
				"addressLocality": "Кам'янець-Подільський",
				"addressRegion": "Хмельницька область",
				"postalCode": "32301",
				"addressCountry": "Україна"
			},
		@endif
		@if(isset($organization->social_networks) && count($organization->social_networks) > 0)
			"sameAs": [
				@php $socialNetworks = collect($organization->social_networks)->whereNotIn('type', ['skype']) @endphp
				@foreach($socialNetworks as $social)
					"{{ $social['url'] }}"
					@if(!$loop->last)
						,
					@endif
				@endforeach
			],
		@endif
		{{--@php $schedule = $organization->scheduleCollect(); @endphp--}}
		{{--@if(isset($schedule->first_day))--}}
			{{--"openingHours":[--}}
        	{{--@if($schedule[0]['work_from'] && $schedule[0]['work_to'])--}}
                {{--"{{ $days[$schedule->first_day] }}-{{ $days[$schedule->last_day] }}--}}
				{{--{{ $schedule[0]['work_from'] }}-{{ $schedule[0]['work_to'] }}"--}}
			{{--@endif--}}
			{{--],--}}
		{{--@else--}}
			{{--"openingHours":[--}}
			{{--@foreach($schedule as $day)--}}
				{{--@if($day['work_from'] && $day['work_to'])--}}
					{{--"{{ $days[$day['day']] }} {{ $day['work_from'] }}-{{ $day['work_to'] }}"--}}
				{{--@endif--}}
				{{--@if(!$loop->last)--}}
					{{--,--}}
				{{--@endif--}}
			{{--@endforeach--}}
			{{--],--}}
		{{--@endif--}}
		@if(count($ratingComments) > 0)
			"aggregateRating" : {
				"@type" : "AggregateRating",
				"bestRating" : 5,
				"reviewCount" : {{ count($ratingComments) }},
				"ratingValue" : {{ round($rating, 1) }}
			 },
		 @endif
		"@type": "Organization",
		"@context": "http://schema.org"
	}
</script>
