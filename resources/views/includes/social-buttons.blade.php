<div class="social-buttons__wrapper">
	<div class="social-buttons__title">Поділитись:</div>
	<ul class="social-buttons__list {{ $class ?? '' }}">
		<li>
			<a class="social-buttons__item facebook"
				 href="https://www.facebook.com/sharer.php?u={{ $url }}"
				 target="_blank"
				 rel="noopener"
				 aria-label="Поширити в Facebook"
				 onclick="ga('send', 'social', 'Facebook', 'share', '{{ url(Request::path()) }}')">
				<i class="fab fa-facebook-f"></i>
			</a>
		</li>
		<li>
			<a class="social-buttons__item vkontakte"
				 href="http://vk.com/share.php?url={{ $url }}"
				 target="_blank"
				 rel="noopener"
				 aria-label="Поширити в Vkontakte"
				 onclick="ga('send', 'social', 'Vkontakte', 'share', '{{ url(Request::path()) }}')">
				<i class="fab fa-vk"></i>
			</a>
		</li>
		<li>
			<a class="social-buttons__item twitter"
				 href="https://twitter.com/intent/tweet?url={{ $url }}"
				 target="_blank"
				 rel="noopener"
				 aria-label="Поширити в Twitter"
				 onclick="ga('send', 'social', 'Twitter', 'share', '{{ url(Request::path()) }}')">
				<i class="fab fa-twitter"></i>
			</a>
		</li>
		<li>
			<a class="social-buttons__item telegram"
				 href="https://t.me/share/url?url={{ $url }}"
				 target="_blank"
				 rel="noopener"
				 aria-label="Поширити в Telegram"
				 onclick="ga('send', 'social', 'Telegram', 'share', '{{ url(Request::path()) }}')">
				<i class="fab fa-telegram-plane"></i>
			</a>
		</li>
	</ul>
</div>