<div class="header__banner-wrapper">
	@include('includes.banners.banners-header')

	<header class="header">

		<div class="header__content">
			<div class="header__main">
				<div class="container">

					<div class="header__main-wrapper">

						<h1 class="header__main-logo">
							<a href="{{ url('/') }}">
								<img src="{{ asset('images/logotypes/logo.svg') }}" alt="KP.CITY">
							</a>
						</h1>

						<div class="header__main-content">

							<div class="header__info">
								@if(isset($weather))
									<div class="header__info-weather" title="Оновлено: {{ $weather->last_updated->format('H:i, d.m.y') }}">
										<svg aria-hidden="true" focusable="false" data-prefix="fal" data-icon="thermometer-half" class="svg-inline--fa fa-thermometer-half fa-w-8" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512"><path d="M176 384c0 26.51-21.49 48-48 48s-48-21.49-48-48c0-20.898 13.359-38.667 32-45.258V208c0-8.837 7.163-16 16-16s16 7.163 16 16v130.742c18.641 6.591 32 24.36 32 45.258zm48-84.653c19.912 22.564 32 52.195 32 84.653 0 70.696-57.302 128-128 128-.299 0-.61-.001-.909-.003C56.789 511.509-.357 453.636.002 383.333.166 351.135 12.225 321.756 32 299.347V96c0-53.019 42.981-96 96-96s96 42.981 96 96v203.347zM224 384c0-39.894-22.814-62.144-32-72.553V96c0-35.29-28.71-64-64-64S64 60.71 64 96v215.447c-9.467 10.728-31.797 32.582-31.999 72.049-.269 52.706 42.619 96.135 95.312 96.501L128 480c52.935 0 96-43.065 96-96z"></path></svg><span>+{{ $weather->temp_c }}&deg;C</span>
									</div>
								@endif

								@if(isset($currencies[0]) && isset($currencies[1]))
									<div class="header__info-currencies">
										<span class="header__info-currency" title="Оновлено: {{ $currencies[0]->created_at->format('H:i, d.m.y') }}">
											<span>$ {{ $currencies[0]->value }}</span>
											<span class="header__info-currency--{{ $currencies[0]->strengthened }}">@if($currencies[0]->strengthened == 'up') ↑ @elseif($currencies[0]->strengthened == 'down') ↓ @endif</span>
										</span>
										<span class="header__info-currency" title="Оновлено: {{ $currencies[0]->created_at->format('H:i, d.m.y') }}">
											<span>€ {{ $currencies[1]->value }}</span>
											<span class="header__info-currency--{{ $currencies[1]->strengthened }}">@if($currencies[1]->strengthened == 'up') ↑ @elseif($currencies[1]->strengthened == 'down') ↓ @endif</span>
										</span>
									</div>
								@endif
							</div>

							<div class="header__main-flex">
								<div class="header__main-menu">
									<a href="{{ route('news') }}">Новини</a>
									<a href="{{ route('articles') }}">Статті</a>
									<a href="{{ url('/polls') }}">Опитування</a>
									@hasrole('developer')
									<a href="{{ route('adverts.category') }}">Оголошення</a>
									@endrole
									<a href="{{ route('organizations.categories') }}">Заклади міста</a>
								</div>

								<div class="header__main-user">
									@guest
										<a class="header__main-user--login show-popup"
											 data-popup-id="popup-login">
											<span>Вхід</span>
											<svg aria-hidden="true" data-prefix="far" data-icon="sign-in" class="svg-inline--fa fa-sign-in fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
												<path d="M416 448h-84c-6.6 0-12-5.4-12-12v-24c0-6.6 5.4-12 12-12h84c26.5 0 48-21.5 48-48V160c0-26.5-21.5-48-48-48h-84c-6.6 0-12-5.4-12-12V76c0-6.6 5.4-12 12-12h84c53 0 96 43 96 96v192c0 53-43 96-96 96zM167.1 83.5l-19.6 19.6c-4.8 4.8-4.7 12.5.2 17.1L260.8 230H12c-6.6 0-12 5.4-12 12v28c0 6.6 5.4 12 12 12h248.8L147.7 391.7c-4.8 4.7-4.9 12.4-.2 17.1l19.6 19.6c4.7 4.7 12.3 4.7 17 0l164.4-164c4.7-4.7 4.7-12.3 0-17l-164.4-164c-4.7-4.6-12.3-4.6-17 .1z"></path>
											</svg>
										</a>
									@endguest
									@auth
										{{--<a href="{{ url('/profile') }}"--}}
										{{--class="header__main-user--profile">--}}
										{{--<svg aria-hidden="true" data-prefix="far" data-icon="user" class="svg-inline--fa fa-user fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">--}}
										{{--<path d="M313.6 304c-28.7 0-42.5 16-89.6 16-47.1 0-60.8-16-89.6-16C60.2 304 0 364.2 0 438.4V464c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48v-25.6c0-74.2-60.2-134.4-134.4-134.4zM400 464H48v-25.6c0-47.6 38.8-86.4 86.4-86.4 14.6 0 38.3 16 89.6 16 51.7 0 74.9-16 89.6-16 47.6 0 86.4 38.8 86.4 86.4V464zM224 288c79.5 0 144-64.5 144-144S303.5 0 224 0 80 64.5 80 144s64.5 144 144 144zm0-240c52.9 0 96 43.1 96 96s-43.1 96-96 96-96-43.1-96-96 43.1-96 96-96z"></path>--}}
										{{--</svg>--}}
										{{--</a>--}}
										<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
											{{ csrf_field() }}
										</form>
										<a class="header__main-user--logout"
											 onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
											<svg aria-hidden="true" data-prefix="far" data-icon="sign-out" class="svg-inline--fa fa-sign-out fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
												<path d="M96 64h84c6.6 0 12 5.4 12 12v24c0 6.6-5.4 12-12 12H96c-26.5 0-48 21.5-48 48v192c0 26.5 21.5 48 48 48h84c6.6 0 12 5.4 12 12v24c0 6.6-5.4 12-12 12H96c-53 0-96-43-96-96V160c0-53 43-96 96-96zm231.1 19.5l-19.6 19.6c-4.8 4.8-4.7 12.5.2 17.1L420.8 230H172c-6.6 0-12 5.4-12 12v28c0 6.6 5.4 12 12 12h248.8L307.7 391.7c-4.8 4.7-4.9 12.4-.2 17.1l19.6 19.6c4.7 4.7 12.3 4.7 17 0l164.4-164c4.7-4.7 4.7-12.3 0-17l-164.4-164c-4.7-4.6-12.3-4.6-17 .1z"></path>
											</svg>
										</a>
									@endauth
								</div>

								<a class="header__main-search toggle-header-search">
									<svg aria-hidden="true" data-prefix="far" data-icon="search" class="svg-inline--fa fa-search fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
										<path d="M508.5 468.9L387.1 347.5c-2.3-2.3-5.3-3.5-8.5-3.5h-13.2c31.5-36.5 50.6-84 50.6-136C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c52 0 99.5-19.1 136-50.6v13.2c0 3.2 1.3 6.2 3.5 8.5l121.4 121.4c4.7 4.7 12.3 4.7 17 0l22.6-22.6c4.7-4.7 4.7-12.3 0-17zM208 368c-88.4 0-160-71.6-160-160S119.6 48 208 48s160 71.6 160 160-71.6 160-160 160z"></path>
									</svg>
								</a>

								<form action="{{ route('search') }}" class="header__main-search-form">

									<input type="text"
												 id="header-form-search-input"
												 placeholder="Введіть ваш запит..."
												 inputmode="search"
												 name="q"
												 autocomplete="off"
												 minlength="3"
												 required
												 aria-label="Пошук на KP.CITY">

									<div class="header__main-search-form--buttons">
										<button type="submit" aria-label="Шукати">
											<svg aria-hidden="true" data-prefix="far" data-icon="search" class="svg-inline--fa fa-search fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
												<path d="M508.5 468.9L387.1 347.5c-2.3-2.3-5.3-3.5-8.5-3.5h-13.2c31.5-36.5 50.6-84 50.6-136C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c52 0 99.5-19.1 136-50.6v13.2c0 3.2 1.3 6.2 3.5 8.5l121.4 121.4c4.7 4.7 12.3 4.7 17 0l22.6-22.6c4.7-4.7 4.7-12.3 0-17zM208 368c-88.4 0-160-71.6-160-160S119.6 48 208 48s160 71.6 160 160-71.6 160-160 160z"></path>
											</svg>
										</button>
										<a class="toggle-header-search">
											<svg aria-hidden="true" data-prefix="far" data-icon="times" class="svg-inline--fa fa-times fa-w-10" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">
												<path d="M207.6 256l107.72-107.72c6.23-6.23 6.23-16.34 0-22.58l-25.03-25.03c-6.23-6.23-16.34-6.23-22.58 0L160 208.4 52.28 100.68c-6.23-6.23-16.34-6.23-22.58 0L4.68 125.7c-6.23 6.23-6.23 16.34 0 22.58L112.4 256 4.68 363.72c-6.23 6.23-6.23 16.34 0 22.58l25.03 25.03c6.23 6.23 16.34 6.23 22.58 0L160 303.6l107.72 107.72c6.23 6.23 16.34 6.23 22.58 0l25.03-25.03c6.23-6.23 6.23-16.34 0-22.58L207.6 256z"></path>
											</svg>
										</a>
									</div>

								</form>
							</div>

							<div class="header__main-hamburger" id="mobile-menu-hamburger"><span></span></div>

						</div>

					</div>

				</div>
			</div>

			<div class="header__second">
				<div class="container">
					<div class="header__second-wrapper">
						<div class="header__second-list">
							<a href="{{ url('/organizations/avto-ta-moto') }}"
								 class="header__second-list-item">
								<div class="header__second-list-item--icon" style="background-image: url('{{ asset('images/icons/header/01.png') }}')"></div>
								<span>Авто та мото</span>
							</a>
							<a href="{{ url('/organizations/vidpochinok-ta-rozvagi') }}"
								 class="header__second-list-item">
								<div class="header__second-list-item--icon" style="background-image: url('{{ asset('images/icons/header/02.png') }}')"></div>
								<span>Відпочинок та розваги</span>
							</a>
							<a href="{{ url('/organizations/zakladi-kharchuvannya') }}"
								 class="header__second-list-item">
								<div class="header__second-list-item--icon" style="background-image: url('{{ asset('images/icons/header/03.png') }}')"></div>
								<span>Заклади харчування</span>
							</a>
							<a href="{{ url('/organizations/magazini-ta-torgovi-centri') }}"
								 class="header__second-list-item visible-md">
								<div class="header__second-list-item--icon" style="background-image: url('{{ asset('images/icons/header/04.png') }}')"></div>
								<span>Магазини та торгові центри</span>
							</a>
							<a href="{{ url('/organizations/osvita') }}"
								 class="header__second-list-item">
								<div class="header__second-list-item--icon" style="background-image: url('{{ asset('images/icons/header/05.png') }}')"></div>
								<span>Освіта</span>
							</a>
							<a href="{{ url('/organizations/turizm-ta-podorozhuvannya') }}"
								 class="header__second-list-item visible-lg">
								<div class="header__second-list-item--icon" style="background-image: url('{{ asset('images/icons/header/06.png') }}')"></div>
								<span>Туризм та подорожування</span>
							</a>
						</div>
						<a href="{{ route('organizations.categories') }}" class="header__second-button">Всі категорії</a>
					</div>
				</div>
			</div>

			<div class="header__left" id="mobile-menu">
				<div class="header__left-content">
					<div class="header__left-content--main">
						<ul class="header__left-main-menu">
							<li><a href="{{ route('news') }}">Новини</a></li>
							<li><a href="{{ route('articles') }}">Статті</a></li>
							<li><a href="{{ url('/polls') }}">Опитування</a></li>
							@hasrole('developer')
								<li><a href="{{ route('adverts.category') }}">Оголошення</a></li>
							@endrole
							<li><a href="{{ route('organizations.categories') }}">Заклади міста</a></li>
						</ul>
						<hr>
						<ul class="header__left-second-menu">
							@can('Access to Nova')
								<li><a href="{{ url('adm') }}">Панель адміністратора</a></li>
							@endcan
							<li><a href="{{ url('about') }}">Про проект</a></li>
							<li><a href="{{ url('contacts') }}">Контакти</a></li>
							<li><a href="{{ url('advertising') }}">Реклама</a></li>
							<li><a href="{{ url('rules') }}">Правила</a></li>
							<li><a href="{{ url('privacy') }}">Конфіденційність</a></li>
						</ul>
					</div>
					<div class="header__left-content--footer">
						<div class="header__left-socials">
							<a href="{{ config('app.kp_socials.facebook.link') }}"
								 target="_blank"
								 rel="noopener"
								 aria-label="{{ config('app.kp_socials.facebook.alt_text') }}">
								<svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="facebook-f" class="svg-inline--fa fa-facebook-f fa-w-10" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512"><path fill="currentColor" d="M279.14 288l14.22-92.66h-88.91v-60.13c0-25.35 12.42-50.06 52.24-50.06h40.42V6.26S260.43 0 225.36 0c-73.22 0-121.08 44.38-121.08 124.72v70.62H22.89V288h81.39v224h100.17V288z"></path></svg>
							</a>
							<a href="{{ config('app.kp_socials.twitter.link') }}"
								 target="_blank"
								 rel="noopener"
								 aria-label="{{ config('app.kp_socials.twitter.alt_text') }}">
								<svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="twitter" class="svg-inline--fa fa-twitter fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M459.37 151.716c.325 4.548.325 9.097.325 13.645 0 138.72-105.583 298.558-298.558 298.558-59.452 0-114.68-17.219-161.137-47.106 8.447.974 16.568 1.299 25.34 1.299 49.055 0 94.213-16.568 130.274-44.832-46.132-.975-84.792-31.188-98.112-72.772 6.498.974 12.995 1.624 19.818 1.624 9.421 0 18.843-1.3 27.614-3.573-48.081-9.747-84.143-51.98-84.143-102.985v-1.299c13.969 7.797 30.214 12.67 47.431 13.319-28.264-18.843-46.781-51.005-46.781-87.391 0-19.492 5.197-37.36 14.294-52.954 51.655 63.675 129.3 105.258 216.365 109.807-1.624-7.797-2.599-15.918-2.599-24.04 0-57.828 46.782-104.934 104.934-104.934 30.213 0 57.502 12.67 76.67 33.137 23.715-4.548 46.456-13.32 66.599-25.34-7.798 24.366-24.366 44.833-46.132 57.827 21.117-2.273 41.584-8.122 60.426-16.243-14.292 20.791-32.161 39.308-52.628 54.253z"></path></svg>
							</a>
							<a href="{{ config('app.kp_socials.vkontakte.link') }}"
								 target="_blank"
								 rel="noopener"
								 aria-label="{{ config('app.kp_socials.vkontakte.alt_text') }}">
								<svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="vk" class="svg-inline--fa fa-vk fa-w-18" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M545 117.7c3.7-12.5 0-21.7-17.8-21.7h-58.9c-15 0-21.9 7.9-25.6 16.7 0 0-30 73.1-72.4 120.5-13.7 13.7-20 18.1-27.5 18.1-3.7 0-9.4-4.4-9.4-16.9V117.7c0-15-4.2-21.7-16.6-21.7h-92.6c-9.4 0-15 7-15 13.5 0 14.2 21.2 17.5 23.4 57.5v86.8c0 19-3.4 22.5-10.9 22.5-20 0-68.6-73.4-97.4-157.4-5.8-16.3-11.5-22.9-26.6-22.9H38.8c-16.8 0-20.2 7.9-20.2 16.7 0 15.6 20 93.1 93.1 195.5C160.4 378.1 229 416 291.4 416c37.5 0 42.1-8.4 42.1-22.9 0-66.8-3.4-73.1 15.4-73.1 8.7 0 23.7 4.4 58.7 38.1 40 40 46.6 57.9 69 57.9h58.9c16.8 0 25.3-8.4 20.4-25-11.2-34.9-86.9-106.7-90.3-111.5-8.7-11.2-6.2-16.2 0-26.2.1-.1 72-101.3 79.4-135.6z"></path></svg>
							<a href="{{ config('app.kp_socials.instagram.link') }}"
								 target="_blank"
								 rel="noopener"
								 aria-label="{{ config('app.kp_socials.instagram.alt_text') }}">
								<svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="instagram" class="svg-inline--fa fa-instagram fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M224.1 141c-63.6 0-114.9 51.3-114.9 114.9s51.3 114.9 114.9 114.9S339 319.5 339 255.9 287.7 141 224.1 141zm0 189.6c-41.1 0-74.7-33.5-74.7-74.7s33.5-74.7 74.7-74.7 74.7 33.5 74.7 74.7-33.6 74.7-74.7 74.7zm146.4-194.3c0 14.9-12 26.8-26.8 26.8-14.9 0-26.8-12-26.8-26.8s12-26.8 26.8-26.8 26.8 12 26.8 26.8zm76.1 27.2c-1.7-35.9-9.9-67.7-36.2-93.9-26.2-26.2-58-34.4-93.9-36.2-37-2.1-147.9-2.1-184.9 0-35.8 1.7-67.6 9.9-93.9 36.1s-34.4 58-36.2 93.9c-2.1 37-2.1 147.9 0 184.9 1.7 35.9 9.9 67.7 36.2 93.9s58 34.4 93.9 36.2c37 2.1 147.9 2.1 184.9 0 35.9-1.7 67.7-9.9 93.9-36.2 26.2-26.2 34.4-58 36.2-93.9 2.1-37 2.1-147.8 0-184.8zM398.8 388c-7.8 19.6-22.9 34.7-42.6 42.6-29.5 11.7-99.5 9-132.1 9s-102.7 2.6-132.1-9c-19.6-7.8-34.7-22.9-42.6-42.6-11.7-29.5-9-99.5-9-132.1s-2.6-102.7 9-132.1c7.8-19.6 22.9-34.7 42.6-42.6 29.5-11.7 99.5-9 132.1-9s102.7-2.6 132.1 9c19.6 7.8 34.7 22.9 42.6 42.6 11.7 29.5 9 99.5 9 132.1s2.7 102.7-9 132.1z"></path></svg>
							</a>
							<a href="{{ config('app.kp_socials.telegram.link') }}"
								 target="_blank"
								 rel="noopener"
								 aria-label="{{ config('app.kp_socials.telegram.alt_text') }}">
								<svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="telegram-plane" class="svg-inline--fa fa-telegram-plane fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M446.7 98.6l-67.6 318.8c-5.1 22.5-18.4 28.1-37.3 17.5l-103-75.9-49.7 47.8c-5.5 5.5-10.1 10.1-20.7 10.1l7.4-104.9 190.9-172.5c8.3-7.4-1.8-11.5-12.9-4.1L117.8 284 16.2 252.2c-22.1-6.9-22.5-22.1 4.6-32.7L418.2 66.4c18.4-6.9 34.5 4.1 28.5 32.2z"></path></svg>
							</a>
						</div>
						<div class="header__left-copy">&copy; <a href="https://origami.team" target="_blank" rel="noopener">Origami</a>, {{ date("Y") }}</div>
					</div>
				</div>
			</div>
		</div>

	</header>
</div>
