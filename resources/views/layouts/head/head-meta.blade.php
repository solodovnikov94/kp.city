<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="preconnect" href="//cdn.jsdelivr.net">
{{--<link rel="preconnect" href="//kpcity.imgix.net">--}}
<link rel="preconnect" href="//www.google-analytics.com">
<link rel="preconnect" href="//stats.g.doubleclick.net">
<link rel="preconnect" href="//www.google.com.ua">
<link rel="preconnect" href="//www.google.com">

<link rel="preload" href="{{ asset('fonts/PTSans/PTSans-Regular.woff') }}" as="font" type="font/woff" crossorigin>
<link rel="preload" href="{{ asset('fonts/PTSans/PTSans-Bold.woff') }}" as="font" type="font/woff" crossorigin>

@yield('priorities')

<link rel="search" title="KP.CITY" type="application/opensearchdescription+xml" href="https://kp.city/opensearch.xml">
<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('images/pwa/apple-touch-icon.png') }}">
<link rel="icon" type="image/png" sizes="32x32" href="{{ asset('images/pwa/favicon-32x32.png') }}">
<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/pwa/favicon-16x16.png') }}">
<link rel="manifest" href="{{ asset('manifest.json') }}">
<link rel="mask-icon" href="{{ asset('images/pwa/safari-pinned-tab.svg') }}" color="#5bbad5">

<meta name="msapplication-TileColor" content="#da532c">
<meta name="theme-color" content="#1E2039">
<meta name="author" content="https://origami.team">
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="og:site_name" content="KP.CITY — сайт твого міста">

@yield('meta')

<link rel="canonical" href="{{ url(Request::path()) }}"/>

@yield('schema')

<script data-ad-client="ca-pub-3898887918642538" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
