<!DOCTYPE html>
<html lang="uk-UA">
<head>
	@include('layouts.head.head-meta')

	<link rel="stylesheet" href="{{ mix('css/components/head.css') }}">

	@include('includes.google-analytics')
	@yield('counter')
</head>

<body>
<div class="wrapper">
	<div class="wrapper__content">
		@include('layouts.head.head-header')
		@yield('styles')
		<link rel="stylesheet" href="{{ asset('libs/fa5/fontawesome-optimized.min.css') }}">
		<main>
			<div class="container" id="main-content">
				<div class="main-content" id="content">
					<div id="main-floated-content-container">
						<div id="main-floated-content">
							@yield('content')
							<div class="floated-stub"></div>
						</div>
					</div>
				</div>
				<div class="right-col">
					@include('layouts.layouts-sidebar')
				</div>
			</div>
		</main>
	</div>
	<div class="wrapper__footer">
		@include('layouts.foot.foot-footer')
	</div>
</div>

@if(!auth()->user())
	@include('includes.popup.popup-login')
	@include('includes.popup.popup-register')
	@include('includes.popup.popup-remember-email')
@endif

@yield('popups')

<div class="float-alerts" id="float-alerts-container">
	@if (session()->has('success-auth'))
		@include('includes.float-alert', ['type' => 'success', 'message' => session('success-auth')])
	@endif

	@if (session()->has('success-logout'))
		@include('includes.float-alert', ['type' => 'info', 'message' => session('success-logout')])
	@endif
</div>
<div class="float-alert__container" id="clone-float-alert">
	<div class="float-alert float-alert--danger">
		<div class="float-alert__content">Відбулась помилка на сервері, спробуйте пізніше, або повідомте про це адміністрацію</div>
		<a class="float-alert__close"><i class="far fa-times"></i></a>
	</div>
</div>
@include('includes.service-worker')
@yield('scripts')
<script>
	$(document).ready(function () {
		//Initialize sticky sidebar
		//https://abouolia.github.io/sticky-sidebar/
		let sidebar = new StickySidebar('#sidebar', {
				innerWrapperSelector: '#floated-content',
				resizeSensor: true
			}
		);

		let sidebarContent = new StickySidebar('#main-floated-content-container', {
				innerWrapperSelector: '#main-floated-content',
				resizeSensor: true
			}
		);

		//Auto resize textarea
		jQuery.each(jQuery('textarea[data-autoresize]'), function () {
			let offset = this.offsetHeight - this.clientHeight;

			let resizeTextarea = function (el) {
				jQuery(el).css('height', 'auto').css('height', el.scrollHeight + offset);
			};
			jQuery(this).on('keyup input', function () {
				resizeTextarea(this);
				sidebar.updateSticky();
			}).removeAttr('data-autoresize');
		});

		$(".popup__close").on("click", function () {
			$(this).parent().parent().parent().fadeOut(150);
			$("body").removeClass('no-scroll');
		});

		$(".show-popup").on("click", function () {
			let popup = $(this).data("popupId");

			$(".popup__wrapper").fadeOut(300);

			$("#" + popup).css("display", "flex").hide().fadeIn(300);
			$("body").addClass('no-scroll');
		});

		$(".popup__wrapper").on("click", function () {
			if (!($(this).hasClass('blocked'))) {
				$(this).fadeOut(150);
				$("body").removeClass('no-scroll');
			}
		});

		$('.popup').on("click", function (e) {
			e.stopPropagation();
		});
	});
</script>
<script src="{{ mix('js/common.js') }}"></script>
</body>
</html>
