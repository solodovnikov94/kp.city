<div id="sidebar">
	<div id="floated-content">

		@auth
			<div class="short-profile-block {{ !auth()->user()->hasAnyRole(['developer', 'admin']) ? 'm-b' : '' }}">
				<div href="#" class="profile-avatar">
					<img class="b-lazy" data-src="{{ auth()->user()->getAvatar() }}" alt="{{ auth()->user()->name }}">
				</div>

				<div class="profile-information">
					<div class="profile-name">
						{{--<a href="#">--}}
						{{ auth()->user()->name }} {{ auth()->user()->surname }}
						{{--</a>--}}
					</div>
					<div class="profile-role">
						@if(count(auth()->user()->roles) > 0)
							@foreach(auth()->user()->roles as $role)
								{{ $role->display_name }}@if(!$loop->last),@endif
							@endforeach
						@else
							Користувач
						@endif
					</div>
				</div>

				<div class="profile-buttons">
					@can('Access to Nova')
						<a href="{{ url('/adm') }}"
							 target="_blank"
							 rel="noopener"
							 aria-label="Панель адміністратора">
{{--              <i class="fal fa-sliders-h"></i>--}}
              <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="sliders-h" class="svg-inline--fa fa-sliders-h fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M496 72H288V48c0-8.8-7.2-16-16-16h-32c-8.8 0-16 7.2-16 16v24H16C7.2 72 0 79.2 0 88v16c0 8.8 7.2 16 16 16h208v24c0 8.8 7.2 16 16 16h32c8.8 0 16-7.2 16-16v-24h208c8.8 0 16-7.2 16-16V88c0-8.8-7.2-16-16-16zm0 320H160v-24c0-8.8-7.2-16-16-16h-32c-8.8 0-16 7.2-16 16v24H16c-8.8 0-16 7.2-16 16v16c0 8.8 7.2 16 16 16h80v24c0 8.8 7.2 16 16 16h32c8.8 0 16-7.2 16-16v-24h336c8.8 0 16-7.2 16-16v-16c0-8.8-7.2-16-16-16zm0-160h-80v-24c0-8.8-7.2-16-16-16h-32c-8.8 0-16 7.2-16 16v24H16c-8.8 0-16 7.2-16 16v16c0 8.8 7.2 16 16 16h336v24c0 8.8 7.2 16 16 16h32c8.8 0 16-7.2 16-16v-24h80c8.8 0 16-7.2 16-16v-16c0-8.8-7.2-16-16-16z"></path></svg>
            </a>
					@endcan
					<form id="logout-form-sidebar"
								action="{{ route('logout') }}"
								method="POST"
								style="display: none;">
						{{ csrf_field() }}
					</form>
					<a href="{{ route('logout') }}"
						 onclick="event.preventDefault(); document.getElementById('logout-form-sidebar').submit();"
						 aria-label="Вилогуватись">
{{--						<i class="fal fa-sign-out"></i>--}}
            <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="sign-out" class="svg-inline--fa fa-sign-out fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M96 64h84c6.6 0 12 5.4 12 12v24c0 6.6-5.4 12-12 12H96c-26.5 0-48 21.5-48 48v192c0 26.5 21.5 48 48 48h84c6.6 0 12 5.4 12 12v24c0 6.6-5.4 12-12 12H96c-53 0-96-43-96-96V160c0-53 43-96 96-96zm231.1 19.5l-19.6 19.6c-4.8 4.8-4.7 12.5.2 17.1L420.8 230H172c-6.6 0-12 5.4-12 12v28c0 6.6 5.4 12 12 12h248.8L307.7 391.7c-4.8 4.7-4.9 12.4-.2 17.1l19.6 19.6c4.7 4.7 12.3 4.7 17 0l164.4-164c4.7-4.7 4.7-12.3 0-17l-164.4-164c-4.7-4.6-12.3-4.6-17 .1z"></path></svg>
					</a>
					{{--<a href=""><i class="fal fa-bell"></i><span class="profile-button-marker"></span></a>--}}
				</div>
			</div>
			@if(auth()->user()->hasAnyRole(['developer', 'admin']))
				<div class="sidebar-user-info">
					<div class="sidebar-user-info__item">@php $num = rand(0, 666) @endphp
						<div class="sidebar-user-info__item-count">{{ $num }}</div>
						<div class="sidebar-user-info__item-text">{{ num2word($num, ['закладка', 'закладки', 'закладок']) }}</div>
					</div>
					<div class="sidebar-user-info__item">@php $countComments = auth()->user()->getCountComments() @endphp
						<div class="sidebar-user-info__item-count">{{ $countComments }}</div>
						<div class="sidebar-user-info__item-text">{{ num2word($countComments, ['коментар', 'коментаря', 'коментарів']) }}</div>
					</div>
					<div class="sidebar-user-info__item">@php $num = rand(0, 666) @endphp
						<div class="sidebar-user-info__item-count">{{ $num }}</div>
						<div class="sidebar-user-info__item-text">{{ num2word($num, ['оголошення', 'оголошення', 'оголошень']) }}</div>
					</div>
				</div>

				<div class="sidebar-menu m-b">
				<a href="#" class="sidebar-menu__link">
					<span class="sidebar-menu__icon-container">
            <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="bell" class="svg-inline--fa fa-bell fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M439.39 362.29c-19.32-20.76-55.47-51.99-55.47-154.29 0-77.7-54.48-139.9-127.94-155.16V32c0-17.67-14.32-32-31.98-32s-31.98 14.33-31.98 32v20.84C118.56 68.1 64.08 130.3 64.08 208c0 102.3-36.15 133.53-55.47 154.29-6 6.45-8.66 14.16-8.61 21.71.11 16.4 12.98 32 32.1 32h383.8c19.12 0 32-15.6 32.1-32 .05-7.55-2.61-15.27-8.61-21.71zM67.53 368c21.22-27.97 44.42-74.33 44.53-159.42 0-.2-.06-.38-.06-.58 0-61.86 50.14-112 112-112s112 50.14 112 112c0 .2-.06.38-.06.58.11 85.1 23.31 131.46 44.53 159.42H67.53zM224 512c35.32 0 63.97-28.65 63.97-64H160.03c0 35.35 28.65 64 63.97 64z"></path></svg>
          </span>
					<span class="sidebar-menu__link-title">Мої сповіщення</span>@php $num = rand(0, 100) @endphp
					@if ($num > 0)
						<span class="sidebar-menu__link-label">{{ $num }} {{ num2word($num, ['нове', 'нових', 'нових']) }}</span>
					@endif
				</a>
				<a href="#" class="sidebar-menu__link">
					<span class="sidebar-menu__icon-container">
            <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="user" class="svg-inline--fa fa-user fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M313.6 304c-28.7 0-42.5 16-89.6 16-47.1 0-60.8-16-89.6-16C60.2 304 0 364.2 0 438.4V464c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48v-25.6c0-74.2-60.2-134.4-134.4-134.4zM400 464H48v-25.6c0-47.6 38.8-86.4 86.4-86.4 14.6 0 38.3 16 89.6 16 51.7 0 74.9-16 89.6-16 47.6 0 86.4 38.8 86.4 86.4V464zM224 288c79.5 0 144-64.5 144-144S303.5 0 224 0 80 64.5 80 144s64.5 144 144 144zm0-240c52.9 0 96 43.1 96 96s-43.1 96-96 96-96-43.1-96-96 43.1-96 96-96z"></path></svg>
          </span>
					<span class="sidebar-menu__link-title">Мій профіль</span>
				</a>
				<a href="#" class="sidebar-menu__link">
					<span class="sidebar-menu__icon-container">
            <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="comment" class="svg-inline--fa fa-comment fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M256 32C114.6 32 0 125.1 0 240c0 47.6 19.9 91.2 52.9 126.3C38 405.7 7 439.1 6.5 439.5c-6.6 7-8.4 17.2-4.6 26S14.4 480 24 480c61.5 0 110-25.7 139.1-46.3C192 442.8 223.2 448 256 448c141.4 0 256-93.1 256-208S397.4 32 256 32zm0 368c-26.7 0-53.1-4.1-78.4-12.1l-22.7-7.2-19.5 13.8c-14.3 10.1-33.9 21.4-57.5 29 7.3-12.1 14.4-25.7 19.9-40.2l10.6-28.1-20.6-21.8C69.7 314.1 48 282.2 48 240c0-88.2 93.3-160 208-160s208 71.8 208 160-93.3 160-208 160z"></path></svg>
          </span>
					<span class="sidebar-menu__link-title">Мої коментарі</span>
				</a>
				<a href="#" class="sidebar-menu__link">
					<span class="sidebar-menu__icon-container">
            <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="bullhorn" class="svg-inline--fa fa-bullhorn fa-w-18" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M544 184.88V32.01C544 23.26 537.02 0 512.01 0H512c-7.12 0-14.19 2.38-19.98 7.02l-85.03 68.03C364.28 109.19 310.66 128 256 128H64c-35.35 0-64 28.65-64 64v96c0 35.35 28.65 64 64 64l-.48 32c0 39.77 9.26 77.35 25.56 110.94 5.19 10.69 16.52 17.06 28.4 17.06h106.28c26.05 0 41.69-29.84 25.9-50.56-16.4-21.52-26.15-48.36-26.15-77.44 0-11.11 1.62-21.79 4.41-32H256c54.66 0 108.28 18.81 150.98 52.95l85.03 68.03a32.023 32.023 0 0 0 19.98 7.02c24.92 0 32-22.78 32-32V295.13c19.05-11.09 32-31.49 32-55.12.01-23.64-12.94-44.04-31.99-55.13zM127.73 464c-10.76-25.45-16.21-52.31-16.21-80 0-14.22 1.72-25.34 2.6-32h64.91c-2.09 10.7-3.52 21.41-3.52 32 0 28.22 6.58 55.4 19.21 80h-66.99zM240 304H64c-8.82 0-16-7.18-16-16v-96c0-8.82 7.18-16 16-16h176v128zm256 110.7l-59.04-47.24c-42.8-34.22-94.79-55.37-148.96-61.45V173.99c54.17-6.08 106.16-27.23 148.97-61.46L496 65.3v349.4z"></path></svg>
          </span>
					<span class="sidebar-menu__link-title">Мої оголошення</span>
				</a>
				<a href="#" class="sidebar-menu__link">
					<span class="sidebar-menu__icon-container">
            <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="heart" class="svg-inline--fa fa-heart fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M458.4 64.3C400.6 15.7 311.3 23 256 79.3 200.7 23 111.4 15.6 53.6 64.3-21.6 127.6-10.6 230.8 43 285.5l175.4 178.7c10 10.2 23.4 15.9 37.6 15.9 14.3 0 27.6-5.6 37.6-15.8L469 285.6c53.5-54.7 64.7-157.9-10.6-221.3zm-23.6 187.5L259.4 430.5c-2.4 2.4-4.4 2.4-6.8 0L77.2 251.8c-36.5-37.2-43.9-107.6 7.3-150.7 38.9-32.7 98.9-27.8 136.5 10.5l35 35.7 35-35.7c37.8-38.5 97.8-43.2 136.5-10.6 51.1 43.1 43.5 113.9 7.3 150.8z"></path></svg>
          </span>
					<span class="sidebar-menu__link-title">Мої закладки</span>
				</a>
			</div>
			@endif
		@endauth

		@guest
			<div class="floated-stub"></div>
		@endguest

		@include('includes.banners.premium1')

		@php $rand = rand(1, 3); @endphp
		<div class="banner__wrapper m-lr m-b">
			<div class="banner banner-h150 banner-rounded"
					 style="background-color: {{ $rand == 1 ? '#F03F43' : ($rand == 2 ? '#97CB43' : '#FF9514') }}">
				<a class="banner__link" href="{{ url('/contacts') }}"
					 rel="noopener">
					<img class="b-lazy" data-src="{{ asset('images/banners/kp.city-ad-'.$rand.'.png') }}"
							 alt="Реклама на порталі KP.CITY">
					<div class="banner__content">
						<div class="banner__title">Бажаєте розмістити своє оголошення?</div>
						<div class="banner__subtitle">Ми пропонуємо вигідні умови для Вашої реклами!</div>
					</div>
				</a>
				<div class="banner__button-container">
					<a href="{{ url('/contacts') }}"
						 rel="noopener">Замовити рекламу</a>
				</div>
			</div>
		</div>

		{{--<div class="banner__wrapper m-lr m-b banner-click-counter">--}}
		{{--<div class="banner banner-h150 banner-rounded" style="background-color: #ED9832">--}}
		{{--<a class="banner__link" href="{{ url('/contacts') }}">--}}
		{{--<img class="b-lazy" data-src="{{ asset('images/banners/kp.city-vacancy.png') }}" alt="Відкрита вакансія «Редактор сайту»">--}}
		{{--<div class="banner__content">--}}
		{{--<div class="banner__title">Відкрита вакансія «Редактор сайту»</div>--}}
		{{--<div class="banner__subtitle">Приєднуйтесь до нашої команди. Давайте йти до успіху разом</div>--}}
		{{--</div>--}}
		{{--</a>--}}
		{{--<div class="banner__button-container">--}}
		{{--<a href="{{ url('/contacts') }}">Детальніше <i class="fal fa-long-arrow-right"></i></a>--}}
		{{--</div>--}}
		{{--</div>--}}
		{{--</div>--}}

{{--		<div class="banner__wrapper m-lr m-b banner-click-counter" data-id="2">--}}
{{--			<div class="banner banner-h150 banner-rounded" style="background-color: #459461">--}}
{{--				<a class="banner__link" href="https://midasstone.com.ua" target="_blank" rel="noindex, nofollow, noopener">--}}
{{--					<img class="b-lazy"--}}
{{--							 data-srcset="{{ asset('images/banners/midasstone250x375.png') }}, {{ asset('images/banners/midasstone250x375-x2.png') }} 2x"--}}
{{--							 data-src="{{ asset('images/banners/midasstone250x375-x2.png') }}"--}}
{{--							 alt="MIDAS STONE - садово-ландшафтний центр">--}}
{{--					<div class="banner__content">--}}
{{--						<div class="banner__title uppercase lh15">Ландшафтний <span class="bigger">дизайн</span></div>--}}
{{--						<div class="banner__subtitle">...та інші послуги з благоустрою Вашої території</div>--}}
{{--					</div>--}}
{{--				</a>--}}
{{--				<div class="banner__button-container">--}}
{{--					<a href="https://midasstone.com.ua" target="_blank" rel="noindex, nofollow, noopener">Детальніше <svg aria-hidden="true" focusable="false" data-prefix="fal" data-icon="long-arrow-right" class="svg-inline--fa fa-long-arrow-right fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M311.03 131.515l-7.071 7.07c-4.686 4.686-4.686 12.284 0 16.971L387.887 239H12c-6.627 0-12 5.373-12 12v10c0 6.627 5.373 12 12 12h375.887l-83.928 83.444c-4.686 4.686-4.686 12.284 0 16.971l7.071 7.07c4.686 4.686 12.284 4.686 16.97 0l116.485-116c4.686-4.686 4.686-12.284 0-16.971L328 131.515c-4.686-4.687-12.284-4.687-16.97 0z"></path></svg></a>--}}
{{--				</div>--}}
{{--			</div>--}}
{{--		</div>--}}

		@include('includes.banners.banners-social')

		@if(isset($instagram_media->data) && count($instagram_media->data) > 0)
			<div class="sidebar-instagram m-b">
				<div class="sidebar-instagram__photos">
						@foreach ($instagram_media->data as $post)
							<a href="{{ $post->link }}" target="_blank" rel="nofollow,noopener">
								<img class="b-lazy"
										 data-src="{{ $post->images->thumbnail->url }}"
										 alt="Фото з Instagram аккаунта {{ config('app.kp_socials.instagram.title') }}">
							</a>
						@endforeach
				</div>
				<div class="sidebar-instagram__footer">
					<a href="{{ config('app.kp_socials.instagram.link') }}" target="_blank" rel="nofollow,noopener">
						<i class="fab fa-instagram"></i>Ми в Instagram: {{ config('app.kp_socials.instagram.title') }}
					</a>
				</div>
			</div>
		@endif

		<div class="sidebar-links">
			<a href="{{ url('/contacts') }}">Наші контакти</a>
			<a href="{{ url('/advertising') }}">Реклама на сайті</a>
		</div>

	</div>
</div>
