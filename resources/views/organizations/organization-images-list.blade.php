@foreach($gallery as $image)
    <figure class="gallery__item" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
        <a class="gallery__item-image"
           href="{{ $image->getCroppedImageOrError('max', 'jpg', 'image') }}"
           itemprop="contentUrl"
           data-size="{{ config('crops.galleries.main.types.max.w') }}x{{ config('crops.galleries.main.types.max.h') }}">

            <img src="{{ $image->getCroppedImageOrError('low', 'jpg', 'image') }}"
                 alt="{{ $image->image_title }}">

            <img class="b-lazy"
                 data-src="{{ $image->getCroppedImageOrError('card', 'jpg', 'image') }}"
                 itemprop="thumbnail"
                 alt="{{ $image->image_title }}">
        </a>
    </figure>
@endforeach