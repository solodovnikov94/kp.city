<div class="organization-single__rating m-full">
	<div class="organization-single__rating-left">
		@if(count($ratingComments))
			<div class="organization-single__rating-avatars">
				@foreach($ratingComments as $comment)
					<div class="organization-single__rating-avatar">
						<img class="b-lazy"
								 data-src="{{ $comment->user->getAvatar(!$comment->anonymous) }}"
								 alt="{{ $comment->anonymous ? 'Анонімний користувач' : ($comment->user->name . ' ' . $comment->user->surname) }}">
					</div>
				@endforeach
				@if(count($ratingComments) > 4)
					<div class="organization-single__rating-avatar">+{{ count($ratingComments) - 4 }}</div>
				@endif
			</div>
			<div class="organization-single__rating-votes">
				{{ count($ratingComments) }} {{ num2word(count($ratingComments), ['оцінка', 'оцінки', 'оцінок']) }}	закладу
			</div>
		@else
			<div class="organization-single__rating-votes">Ще немає оцінок</div>
		@endif
	</div>
	<div class="organization-single__rating-right">
		<span>Оцінка закладу:</span>
		<div class="rating" title="{{ number_format($rating, 1) }}">
			@php $rating = round($rating) @endphp
			@for($i=1; $i<=5; $i++)
				<div class="rating__item @if($i <= $rating) active @endif">
					<i class="fa fa-star"></i>
				</div>
			@endfor
		</div>
	</div>
</div>