@extends('layouts.layouts-main')

@section('meta')

	@include('includes.meta', [
		'title' => config('metatags.organization_categories.title'),
		'meta_description' => config('metatags.organization_categories.description'),
		'meta_keywords' => config('metatags.organization_categories.keywords'),
		'og_title' => config('metatags.organization_categories.title'),
		'og_description' => config('metatags.organization_categories.description'),
		'og_image' => asset('images/pages/homepage.jpg'),
		'active' => true
	])
@stop

@section('styles')
	<link rel="stylesheet" href="{{ mix('css/common.css') }}">
	<link rel="stylesheet" href="{{ mix('css/organizations.css') }}">
	<link rel="stylesheet" href="{{ asset('libs/lightslider/lightslider.css') }}">
@endsection

@section('content')

	<div class="breadcrumbs">
		@include('includes.breadcrumbs', ['current' => 'Заклади міста'])
	</div>

	{{--<div class="slider-section m-full">--}}
		{{--<div class="slider-section__header">--}}
			{{--<h3 class="slider-section__title">Головні спонсори</h3>--}}
			{{--<div class="slider-section__controls">--}}
				{{--<div class="slider-section__controls--prev" id="organizations-slider-prev">--}}
					{{--<i class="fal fa-angle-left"></i>--}}
				{{--</div>--}}
				{{--<div class="slider-section__controls--next" id="organizations-slider-next">--}}
					{{--<i class="fal fa-angle-right"></i>--}}
				{{--</div>--}}
			{{--</div>--}}
		{{--</div>--}}
		{{--<div class="slider-section__content" id="organizations-slider">--}}
			{{--@include('organizations.organizations-list')--}}
		{{--</div>--}}
	{{--</div>--}}

	{{--<hr>--}}

	@include('search.search-block', ['class' => 'm-full'])

	<hr>

	@include('includes.banners.top-banner')

	@if(isset($categories) && count($categories) > 0)
		<div class="oc-list p-t m--b">
			@foreach($categories->sortBy('title') as $category)
				<div class="oc {{ $category->children_count == 0 ? 'without-subcategories' : '' }}"
						 style="background-image: url({{ Storage::url($category->icon) }})">
					<a href="{{ url('/organizations/'.$category->slug) }}" class="oc__content">
						<div class="oc__title">{{ $category->title }}</div>
						<div class="oc__count">Закладів: {{ $category->getOrganizationsCount() }}</div>
					</a>
					@if($category->children_count > 0)
						<a class="oc__btn"><i class="fal fa-angle-down"></i></a>
						<ul class="oc__subcategories">
							@foreach($category->children as $child)
								<li>
									<a href="{{ url('/organizations/'.$category->slug.'/'.$child->slug ) }}">{{ $child->title }}</a>
								</li>
							@endforeach
						</ul>
					@endif
				</div>
			@endforeach
		</div>
	@endif

@stop

@section('scripts')
	<script src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@12.0.0/dist/lazyload.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/jquery@3.4.1/dist/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sticky-sidebar@3.3.1/dist/sticky-sidebar.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/resize-sensor@0.0.6/ResizeSensor.min.js"></script>

	<script src="{{ asset('js/common-slider.js') }}"></script>
	<script src="{{ asset('libs/lightslider/lightslider-blur-fix.min.js') }}"></script>
	<script>
		commonSlider('organizations-slider');
	</script>
@stop