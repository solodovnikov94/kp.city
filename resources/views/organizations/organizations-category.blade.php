@extends('layouts.layouts-main')

@section('meta')
	@include('includes.meta', [
		'title' => $category->meta_title,
		'meta_description' => $category->meta_description,
		'meta_keywords' => $category->meta_keywords,
		'og_title' => $category->og_title ?? $category->meta_title,
		'og_description' => $category->og_description ?? $category->meta_description,
		'og_image' => $category->getCroppedImage('og', 'jpg', 'og_image') ?? asset('images/pages/homepage.jpg'),
		'og_image_type' => 'og',
		'active' => true
	])
@stop

@section('styles')
	<link rel="stylesheet" href="{{ mix('css/common.css') }}">
	<link rel="stylesheet" href="{{ mix('css/organizations.css') }}">
	<link rel="stylesheet" href="{{ asset('libs/lightslider/lightslider.css') }}">
@endsection

@section('content')

	<div class="breadcrumbs">
		@include('includes.breadcrumbs', ['links' => [
			['link' => route('organizations.categories'), 'title' => 'Заклади міста'],
		], 'current' => $category->title])
	</div>

	{{--<div class="slider-section m-full">--}}
		{{--<div class="slider-section__header">--}}
			{{--<h3 class="slider-section__title">Спонсори категорії</h3>--}}
			{{--<div class="slider-section__controls">--}}
				{{--<div class="slider-section__controls--prev" id="organizations-slider-prev">--}}
					{{--<i class="fal fa-angle-left"></i>--}}
				{{--</div>--}}
				{{--<div class="slider-section__controls--next" id="organizations-slider-next">--}}
					{{--<i class="fal fa-angle-right"></i>--}}
				{{--</div>--}}
			{{--</div>--}}
		{{--</div>--}}
		{{--<div class="slider-section__content" id="organizations-slider">--}}
			{{--@include('organizations.organizations-list')--}}
		{{--</div>--}}
	{{--</div>--}}

	{{--<hr>--}}

	@include('search.search-block', ['class' => 'm-full'])

	<hr>

	@include('includes.banners.top-banner')

	@foreach($subcategories as $subcategory)
		@if(!$loop->first)
			<hr class="m-t">
		@endif

		@if($subcategory->organizations_count > 0)
			<div class="slider-section m-full">
				<div class="slider-section__header">
					<h3 class="slider-section__title">
						<a href="{{ route('organizations.subcategory', ['category_slug' => $category->slug, 'subcategory_slug' => $subcategory->slug ]) }}">{{ $subcategory->title }}</a>
					</h3>
					<div class="slider-section__controls">
						<div class="slider-section__controls--prev" id="{{ $subcategory->slug }}-slider-prev">
							<i class="fal fa-angle-left"></i>
						</div>
						<div class="slider-section__controls--next" id="{{ $subcategory->slug }}-slider-next">
							<i class="fal fa-angle-right"></i>
						</div>
					</div>
				</div>
				<div class="slider-section__content" id="{{ $subcategory->slug }}-slider">
					@include('organizations.organizations-list', [
					'organizations' => $subcategory->organizations->take(5),
					'category' => $category,
					'subcategory' => $subcategory
					])
				</div>
			</div>

			<a href="{{ route('organizations.subcategory', ['category_slug' => $category->slug, 'subcategory_slug' => $subcategory->slug ]) }}" class="load-more m-lr">Переглянути всі</a>
		@else
			<h3 class="section-title m-full">
				<a href="{{ route('organizations.subcategory', ['category_slug' => $category->slug, 'subcategory_slug' => $subcategory->slug ]) }}">{{ $subcategory->title }}</a>
			</h3>

			@include('includes.alert', [
				'type' => 'info',
				'class' => 'm-lr',
				'messages' => ['Упс... Закладів не знайдено, але найближчим часом ми це виправимо']
			])
		@endif

	@endforeach

@stop

@section('scripts')
	<script src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@12.0.0/dist/lazyload.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/jquery@3.4.1/dist/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sticky-sidebar@3.3.1/dist/sticky-sidebar.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/resize-sensor@0.0.6/ResizeSensor.min.js"></script>

	<script src="{{ asset('js/common-slider.js') }}"></script>
	<script src="{{ asset('libs/lightslider/lightslider-blur-fix.min.js') }}"></script>
	<script>
		commonSlider('organizations-slider');
		@foreach($subcategories as $subcategory) commonSlider('{{ $subcategory->slug }}-slider'); @endforeach
	</script>
@stop