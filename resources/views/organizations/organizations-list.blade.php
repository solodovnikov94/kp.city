@if(isset($organizations) && count($organizations))
	@foreach($organizations as $organization)
		@if(isset($socialBannerPosition) && $socialBannerPosition == $loop->iteration)
			@include('includes.banners.banners-social-card', ['type' => 'organization'])
		@endif
		<div class="organization">
			{{--<div class="organization__crown">--}}
				{{--<i class="far fa-crown"></i>--}}
			{{--</div>--}}
			<div class="organization__image">
				<img src="{{ $organization->getCroppedImageOrTransparent('low') }}"
						 class="b-lazy-default"
						 alt="{{ $organization->title }}">
				<img data-src="{{ $organization->getCroppedImageOrTransparent('card') }}"
						 class="b-lazy"
						 alt="{{ $organization->title }}">
			</div>
			<div class="organization__header">
				<div class="organization__labels">
					@if($organization->comments->isNotEmpty() && $organization->getOrganizationRating())
						<div class="lbl lbl-xl">{{ number_format($organization->getOrganizationRating(), 1) }}</div>
					@else
						<div class="lbl lbl-xl lbl-grey">Немає оцінок</div>
					@endif
				</div>
				{{--<div class="organization__buttons">--}}
					{{--<a class="organization__button"--}}
						 {{--href="#like"--}}
						 {{--aria-label="Додати до улюбленого"><i class="fal fa-heart"></i></a>--}}
				{{--</div>--}}
			</div>
			<a href="{{ isset($category) && isset($subcategory) && $category
			? url('/organizations/'.$category->slug.'/'.$subcategory->slug.'/'.$organization->id.'-'.$organization->slug)
			: $organization->getUrl() }}" class="organization__content">
				<div class="organization__title">
					{{ $organization->title }}
				</div>
			</a>
			@if(count($organization->addresses) && isset($organization->addresses[0]))
				<div class="organization__footer">
					<span class="organization__address">
            <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="map-marker-alt" class="svg-inline--fa fa-map-marker-alt fa-w-12" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><path fill="currentColor" d="M172.268 501.67C26.97 291.031 0 269.413 0 192 0 85.961 85.961 0 192 0s192 85.961 192 192c0 77.413-26.97 99.031-172.268 309.67-9.535 13.774-29.93 13.773-39.464 0zM192 272c44.183 0 80-35.817 80-80s-35.817-80-80-80-80 35.817-80 80 35.817 80 80 80z"></path></svg>
						<span>{{ $organization->addresses[0]['address'] }}</span>
					</span>
					@if($organization->isOpen() !== null)
						<div class="organization__status {{ $organization->isOpen() ? 'active' : '' }} "></div>
					@endif
				</div>
			@endif
		</div>
	@endforeach
@endif
