@extends('layouts.layouts-main')

@section('priorities')
	<link rel="preconnect" href="//www.google.com">
	<link rel="preconnect" href="//maps.google.com">

	@if($organization->video)
		<link rel="preconnect" href="https://www.youtube.com">
	@endif
@endsection

@section('schema')
	@include('includes.schema.organization-schema')
@endsection

@section('meta')
	@include('includes.meta', [
		'title' => $organization->meta_title,
		'meta_description' => $organization->meta_description,
		'meta_keywords' => $organization->meta_keywords,
		'og_title' => $organization->og_title ?? $organization->meta_title,
		'og_description' => $organization->og_description ?? $organization->meta_description,
		'og_image' => $organization->getCroppedImage('og', 'jpg', 'og_image') ?? $organization->getCroppedImage('max') ?? asset('images/pages/homepage.jpg'),
		'og_image_type' => $organization->getCroppedImage('og', 'jpg', 'og_image') ? 'og' : ($organization->getCroppedImage('max') ? 'max' : 'og'),
		'active' => $organization->active
	])
@stop

@section('styles')
	<link rel="stylesheet" href="{{ mix('css/common.css') }}">
	<link rel="stylesheet" href="{{ mix('css/articles.css') }}">
	<link rel="stylesheet" href="{{ mix('css/comments.css') }}">
	<link rel="stylesheet" href="{{ mix('css/organizations.css') }}">
	{{--<link rel="stylesheet" href="{{ asset('libs/jscrollpane/jquery.jscrollpane.css') }}">--}}
	<link rel="stylesheet" href="{{ asset('libs/lightslider/lightslider.css') }}">
	<link rel="stylesheet" href="{{ asset('libs/magnific-popup/magnific-popup.css') }}">
@endsection

@section('content')
	<div class="org">

		<div class="organization-single__image">
			<figure class="media-content__image">
				<div class="media-content__image-container">
					<img src="{{ $organization->getCroppedImageOrTransparent('low') }}" alt="{{ $organization->title }}">
					<picture>
						<source media="(min-width: 992px)"
										data-srcset="{{ $organization->getCroppedImageOrError('max', 'webp') }}"
										type="image/webp">
						<source media="(min-width: 992px)"
										data-srcset="{{ $organization->getCroppedImageOrError('max', 'jpg') }}">

						<source media="(min-width: 768px) and (max-width: 991px)"
										data-srcset="{{ $organization->getCroppedImageOrError('mobile', 'webp') }}"
										type="image/webp">
						<source media="(min-width: 768px) and (max-width: 991px)"
										data-srcset="{{ $organization->getCroppedImageOrError('mobile', 'jpg') }}">

						<source media="(min-width: 560px) and (max-width: 767px)"
										data-srcset="{{ $organization->getCroppedImageOrError('max', 'webp') }}"
										type="image/webp">
						<source media="(min-width: 560px) and (max-width: 767px)"
										data-srcset="{{ $organization->getCroppedImageOrError('max', 'jpg') }}">

						<source media="(min-width: 450px) and (max-width: 559px)"
										data-srcset="{{ $organization->getCroppedImageOrError('mobile', 'webp') }}"
										type="image/webp">
						<source media="(min-width: 450px) and (max-width: 559px)"
										data-srcset="{{ $organization->getCroppedImageOrError('mobile', 'jpg') }}">

						<img class="b-lazy"
								 data-src="{{ $organization->getCroppedImageOrError('card') }}"
								 alt="{{ $organization->title }}">
					</picture>
				</div>
			</figure>
			<div class="organization-single__image-content">
				<div class="organization-single__image-content--top">
					@if(isset($organization->social_networks) && count($organization->social_networks) > 0)
						<ul class="social-buttons__list">
							@foreach($organization->social_networks as $network)
								<li>
									<a class="social-buttons__item {{ $network['type'] }}"
										 href="{{ $network['type'] != 'skype' ? $network['url'] : 'skype:'.$network['url'].'?chat' }}"
										 @if($network['type'] != 'skype')
										 target="_blank"
										 rel="noopener"
										 @endif
										 aria-label="{{ $organization->title . ' в ' . config('app.social_keys.'.$network['type'].'.title') }}"><i class="fab {{ config('app.social_keys.'.$network['type'].'.icon') }}"></i>
									</a>
								</li>
							@endforeach
						</ul>
					@endif
				</div>
				<div class="organization-single__image-content--bottom">
					<div class="btn-container">
						{{--<a href="#" class="btn btn-xl btn-hover btn-red">Я власник</a>--}}
						<a class="btn btn-xl btn-hover btn-green show-popup" data-popup-id="popup-propose-changes">Запропонувати зміни</a>
					</div>
				</div>
			</div>
		</div>

		<div class="breadcrumbs">
			@include('includes.breadcrumbs', ['links' => [
					['link' => route('organizations.categories'), 'title' => 'Заклади міста'],
					['link' => $category->getUrl(), 'title' => $category->title],
					['link' => $subcategory->getUrl(), 'title' => $subcategory->title],
			], 'current' => $organization->title])
			@can('Access to Nova')
				<div class="breadcrumbs-controls tooltip-parent">
					<i class="fal fa-ellipsis-h"></i>
					<div class="tooltip tooltip-left tooltip-controls tooltip-blue">
						@role('developer')
							<span>Автор: {{ $organization->creator->name }}</span>
						@endrole
						<a href="{{ url('adm/resources/organizations/' . $organization->id . '/edit') }}" target="_blank" rel="noopener">Редагувати</a>
					</div>
				</div>
			@endcan
		</div>

		<div class="organization-brief p-full">
			<div class="organization-brief__background">
				<img class="b-lazy"
						 data-src="{{ Storage::disk('public')->url($organization->category->parent->icon) }}"
						 alt="{{ $organization->category->parent->title }}">
			</div>
			<div class="organization-brief__container">
				<div class="organization-brief__content">
					<div class="organization-brief__image">
						<img class="b-lazy"
								 data-src="{{ $organization->logotype ? Storage::disk('public')->url($organization->logotype) : asset('images/error.svg') }}"
								 alt="{{ $organization->title }}">
					</div>
					<div class="organization-brief__information">
						<div class="organization-brief__header">
							<h2 class="organization-brief__title bigger">{{ $organization->title }}</h2>
						</div>
						@if(isset($organization->addresses) && count($organization->addresses) > 0)
							<div class="organization-brief__address">
								<i class="fas fa-map-marker-alt"></i><span>{{ $organization->addresses[0]['address'] }}</span>
							</div>
						@endif
					</div>
				</div>
			</div>
		</div>

		<hr>

		<div class="organization-single__navigation">
			<div class="organization-single__navigation-wrapper">
				<div class="organization-single__navigation-list">
					<div class="organization-single__navigation-item">
						<a data-tab="0"
							 class="organization-single__navigation-link active">Загальна інформація</a>
					</div>
					@if(isset($filiations) && count($filiations))
						<div class="organization-single__navigation-item">
							<a data-tab="1"
								 class="organization-single__navigation-link">Філіали</a>
						</div>
					@endif
					@if((isset($gallery) && count($gallery)) || $organization->video)
						<div class="organization-single__navigation-item">
							<a data-tab="2"
								 class="organization-single__navigation-link">Фото та відео</a>
						</div>
					@endif
					@if(isset($news) && count($news))
						<div class="organization-single__navigation-item">
							<a data-tab="3"
								 class="organization-single__navigation-link">Новини</a>
						</div>
					@endif
				</div>
			</div>
		</div>

		<div class="org__tabs m-t">
			<div id="organization-tabs">

				{{--Основна інформація--}}
				<div class="org__tab" id="tab-info">
					<div class="org__excerpt m-lr">{{ $organization->excerpt }}</div>

					@include('includes.banners.horizontal-banner1')

					<hr class="m-full">

					<div class="organization-single__info m-lr m-b">
						<div class="organization-single__info-left">
							@if($addresses = not_null($organization->addresses))
								<div class="organization-single__info-item">
									<div class="info-dropdown single">
										<div class="info-dropdown__icon">
											<i class="fal fa-map-marker-alt"></i>
										</div>
										<div class="info-dropdown__content">
											<div class="info-dropdown__item info-dropdown__item--active">
												<div class="info-dropdown__item--title">
													<span>{{ first_element($addresses, 'address') }}</span>
												</div>
												<div class="info-dropdown__item--description">{{ first_element($addresses, 'note') ?? 'Адреса закладу' }}</div>
											</div>
										</div>
									</div>
								</div>
							@endif

							@if($phones = not_null($organization->phones))
								<div class="organization-single__info-item">
									<div class="info-dropdown">
										<div class="info-dropdown__icon">
											<i class="fal fa-phone"></i>
										</div>
										<div class="info-dropdown__content">
											<div class="info-dropdown__item @if(count($phones) > 1) info-dropdown__item--active @endif">
												<div class="info-dropdown__item--title">
													<span>{{ first_element($phones, 'phone') }}</span>
													@if(count($phones) > 1)
														<i class="fa fa-caret-down"></i>
													@endif
												</div>
												<div class="info-dropdown__item--description">{{ first_element($phones, 'note') ?? 'Телефон закладу' }}</div>
											</div>
											@if(count($phones) > 1)
												<div class="info-dropdown__list">
													@foreach(unset_first_element($phones) as $phone)
														<div class="info-dropdown__item">
															<div class="info-dropdown__item--title">{{ $phone ['phone'] }}</div>
															<div class="info-dropdown__item--description">{{ $phone['note'] }}</div>
														</div>
													@endforeach
												</div>
											@endif
										</div>
									</div>
								</div>
							@endif

							@if($emails = not_null($organization->emails))
								<div class="organization-single__info-item">
									<div class="info-dropdown single">
										<div class="info-dropdown__icon">
											<i class="fal fa-envelope"></i>
										</div>
										<div class="info-dropdown__content">
											<div class="info-dropdown__item info-dropdown__item--active">
												<div class="info-dropdown__item--title">
													<a href="mailto:{{ first_element($emails, 'email') }}">{{ first_element($emails, 'email') }}</a>
												</div>
												<div class="info-dropdown__item--description">{{ first_element($emails, 'note') ?? 'Email адреса закладу' }}</div>
											</div>
										</div>
									</div>
								</div>
							@endif

							@if($sites = not_null($organization->sites))
								<div class="organization-single__info-item">
									<div class="info-dropdown">
										<div class="info-dropdown__icon">
											<i class="fal fa-globe"></i>
										</div>
										<div class="info-dropdown__content">
											<div class="info-dropdown__item @if(count($sites) > 1) info-dropdown__item--active @endif">
												<div class="info-dropdown__item--title">
													<a href="{{ first_element($sites, 'url') }}" target="_blank" rel="nofollow noopener">
														{{ format_link(first_element($sites, 'url')) }}
													</a>
													@if(count($sites) > 1)
														<i class="fa fa-caret-down"></i>
													@endif
												</div>
												<div class="info-dropdown__item--description">{{ first_element($sites, 'note') ?? 'Сайт закладу' }}</div>
											</div>
											<div class="info-dropdown__list">
												@foreach(unset_first_element($sites) as $site)
													<div class="info-dropdown__item">
														<div class="info-dropdown__item--title">
															<a href="{{ $site['url'] }}" target="_blank" rel="nofollow">{{ format_link($site['url']) }}</a>
														</div>
														<div class="info-dropdown__item--description">{{ $site['note'] }}</div>
													</div>
												@endforeach
											</div>
										</div>
									</div>
								</div>
							@endif
						</div>
						@if(count($organization->schedule))
							<div class="organization-single__info-right">
								<div class="info-dropdown">
									<div class="info-dropdown__icon">
										<i class="fal fa-clock"></i>
									</div>
									<div class="info-dropdown__content">
										<div class="info-dropdown__item info-dropdown__item--active">
											<div class="info-dropdown__item--title">
											<span>{{ $days[$organization->numberDay] }}:
												@if($todayParam)
													{{ $todayParam['work_from'] }}-{{ $todayParam['work_to'] }}
												@else
													Вихідний
												@endif
											</span>
												<i class="fa fa-caret-down"></i>
											</div>
											@if($todayParam)
												<div class="info-dropdown__item--description">
													<strong>Перерва:</strong>
													<span>
													@if(not_null($organization->break_time))
															{{ $organization->break_time[0]['break_from'] }}-
															{{ $organization->break_time[0]['break_to'] }}
														@else
															без перерви
														@endif
												</span>
													<i class="fa fa-info-circle tooltip-parent">
														<span class="tooltip tooltip-left tooltip-blue tooltip-w150">Графік перерв закладу може відрізнятися від вказаного на сайті.</span>
													</i>
												</div>
											@endif
										</div>

										<div class="info-dropdown__list">
											@php $schedule = $organization->scheduleCollect(); @endphp
											@if(isset($schedule->first_day))
												{{-- згруповані дні --}}
												@if($schedule[0]['work_from'] && $schedule[0]['work_to'])
													<div class="info-dropdown__item">
														<div class="info-dropdown__item--title">
														<span>{{ $days[$schedule->first_day] }}-{{ $days[$schedule->last_day] }}:
															{{ $schedule[0]['work_from'] }}-{{ $schedule[0]['work_to'] }}</span>
														</div>
													</div>
												@endif
												@if(isset($schedule->weekends) && $schedule->weekends)
													<div class="info-dropdown__item">
														<div class="info-dropdown__item--title">
														<span>{{ $schedule->weekends }}:
															@if($schedule->count_day > 1)
																Вихідні
															@else
																Вихідний
															@endif
														</span>
														</div>
													</div>
												@endif
											@else
												@foreach($schedule as $day)
													@if($day['work_from'] && $day['work_to'])
														<div class="info-dropdown__item">
															<div class="info-dropdown__item--title">
																<span>{{ $days[$day['day']] }}: {{ $day['work_from'] }}-{{ $day['work_to'] }}</span>
															</div>
														</div>
													@endif
												@endforeach
												@if(isset($schedule->weekends) && $schedule->weekends)
													<div class="info-dropdown__item">
														<div class="info-dropdown__item--title">
															<span>{{ $schedule->weekends }}:
																@if($schedule->count_day > 1)
																	Вихідні
																@else
																	Вихідний
																@endif
															</span>
														</div>
													</div>
												@endif
											@endif
										</div>
										<div class="info-dropdown__footer">
											@if($isOpen === 'break')
												<div class="organization-single__info--schedule-marker closed">
													<span>Зачинено на перерву</span>
												</div>
											@elseif($isOpen)
												<div class="organization-single__info--schedule-marker open">
													<span>Зараз відчинено</span>
												</div>
											@else
												<div class="organization-single__info--schedule-marker closed">
													<span>Зараз зачинено</span>
												</div>
											@endif
										</div>
									</div>
								</div>
							</div>
						@endif
					</div>

					<div class="org__map">
						<iframe
								title="{{ $organization->title }} на Google Maps"
								class="map b-lazy"
								frameborder="0"
								data-src="https://maps.google.com/maps?hl=uk&q={{ $organization->google_map_id }}&output=embed"
								allowfullscreen>
						</iframe>
					</div>

					@if($organization->specifics && count($organization->specifics))
						<div class="org__advantages section-block m-t m-lr">
							<h3 class="section-title">Особливості закладу</h3>
							<ul class="org__advantages-list">
								@foreach($organization->specifics as $specific)
									<li>{{ $specific['specific'] }}</li>
								@endforeach
							</ul>
						</div>
					@endif
				</div>

				{{--Філіали--}}
				<div class="org__tab" id="tab-branches">
					@if(isset($filiations) && count($filiations))
						<div class="organizations-list">
							@include('organizations.organizations-list', [
								'organizations' => $filiations,
								'category' => false
							])
						</div>
					@endif
				</div>

				{{--Фото і відео--}}
				<div class="org__tab" id="tab-media">
					@if((isset($gallery) && count($gallery)) || $organization->video)
						@if($organization->video)
							<div class="m-lr m-b">
								<div class="youtube-video bordered">
									<iframe class="b-lazy"
													title="Відео від закладу {{ $organization->title }}"
													data-src="https://www.youtube.com/embed/{{ $organization->video }}"
													frameborder="0"
													allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
													allowfullscreen></iframe>
								</div>
							</div>
						@endif

						@if(isset($gallery) && count($gallery))
							<div class="m-lr m--b">
								<div class="gallery" itemscope itemtype="http://schema.org/ImageGallery">
									@include('organizations.organization-images-list', ['gallery' => $gallery])
								</div>
								@if(isset($galleryShowMore) && $galleryShowMore)
									<a class="load-more m-b show-more"
									   data-current-page="2"
									   data-url="{{ url(Request::path()) }}"
									   data-url-param="param=gallery">{{ $galleryOnNextPage }}</a>
								@endif
							</div>

						@endif
					@endif
				</div>

				{{--Новини--}}
				<div class="org__tab" id="tab-news">
					@if(isset($news) && count($news))
						<div class="articles-list">
							@include('articles.articles-list', ['articles' => $news])
						</div>
					@endif
				</div>
			</div>
		</div>

		<div class="organization-single__footer m-full">
			<div class="counters">
				<span><i class="fal fa-eye"></i>{{ $organization->views }}</span>
				<span><i class="fal fa-comment"></i>{{ $organization->comments_count }}</span>
			</div>

			@include('includes.social-buttons', ['url' => url(Request::path()), 'class' => 'share'])
		</div>

		<hr class="m-lr">

		<div class="rating-block">
			@include('organizations.organization-rating', [
				'ratingComments' => $ratingComments,
				'rating' => $rating
			])
		</div>

		<hr>

		@include('comments.comments-list', [
				'comments' => $organization->comments_list,
				'totalComments' => $organization->comments_count,
				'maxLikes' => $organization->max_likes,
				'commentRating' => true
		])

	</div>
@stop

@section('popups')
	@include('includes.popup.popup-propose-changes')
@endsection

@section('counter')
	@include('includes.counter', [
		'model' => 'Organization',
		'column' => 'views',
		'id' => $organization->id
	])
@endsection

@section('scripts')
	<script src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@12.0.0/dist/lazyload.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/jquery@3.4.1/dist/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sticky-sidebar@3.3.1/dist/sticky-sidebar.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/resize-sensor@0.0.6/ResizeSensor.min.js"></script>
	<script src="{{ asset('libs/lightslider/lightslider-blur-fix.min.js') }}"></script>
	<script src="https://cdn.jsdelivr.net/npm/magnific-popup@1.1.0/dist/jquery.magnific-popup.min.js"></script>
	<script src="https://www.google.com/recaptcha/api.js"></script>
	{{--<script src="{{ asset('libs/jscrollpane/jquery.jscrollpane.min.js') }}"></script>--}}
	<script>
		// $(function () {
		// 	$('.scroll-pane').jScrollPane({
		// 		autoReinitialise: true
		// 	});
		// });

		var organization_tabs = $("#organization-tabs").lightSlider({
			item: 1,
			slideMargin: 0,
			adaptiveHeight: true,
			pager: 0,
			easing: 'ease',
			speed: 300,
			loop: false,
			enableDrag: 0,
			enableTouch: 0
		});

		$(".organization-single__navigation-link").on("click", function () {
			var slideNum = $(this).data("tab");

			$(".organization-single__navigation-link").removeClass('active');
			$(this).addClass('active');

			organization_tabs.goToSlide(slideNum);
		});

		$('.gallery').magnificPopup({
			delegate: 'a',
			type: 'image',
			gallery: {
				enabled: true
			},
			image: {
				titleSrc: 'title'
				// this tells the script which attribute has your caption
			}
		});
	</script>
@stop