@extends('layouts.layouts-main')

@section('meta')
	@include('includes.meta', [
		'title' => $subcategory->meta_title,
		'meta_description' => $subcategory->meta_description,
		'meta_keywords' => $subcategory->meta_keywords,
		'og_title' => $subcategory->og_title ?? $subcategory->meta_title,
		'og_description' => $subcategory->og_description ?? $subcategory->meta_description,
		'og_image' => $subcategory->getCroppedImage('og', 'jpg', 'og_image') ?? asset('images/pages/homepage.jpg'),
		'og_image_type' => 'og',
		'active' => $subcategory->active
	])
@stop

@section('styles')
	<link rel="stylesheet" href="{{ mix('css/common.css') }}">
	<link rel="stylesheet" href="{{ mix('css/organizations.css') }}">
	<link rel="stylesheet" href="{{ asset('libs/lightslider/lightslider.css') }}">
@endsection

@section('content')

	<div class="breadcrumbs">
		@include('includes.breadcrumbs', ['links' => [
			['link' => route('organizations.categories'), 'title' => 'Заклади міста'],
			['link' => $category->getUrl(), 'title' => $category->title],
		], 'current' => $subcategory->title])
	</div>

	{{--<div class="slider-section m-full">--}}
	{{--<div class="slider-section__header">--}}
	{{--<h3 class="slider-section__title">Спонсори категорії</h3>--}}
	{{--<div class="slider-section__controls">--}}
	{{--<div class="slider-section__controls--prev" id="organizations-slider-prev">--}}
	{{--<i class="fal fa-angle-left"></i>--}}
	{{--</div>--}}
	{{--<div class="slider-section__controls--next" id="organizations-slider-next">--}}
	{{--<i class="fal fa-angle-right"></i>--}}
	{{--</div>--}}
	{{--</div>--}}
	{{--</div>--}}
	{{--<div class="slider-section__content" id="organizations-slider">--}}
	{{--@include('organizations.organizations-list')--}}
	{{--</div>--}}
	{{--</div>--}}

	{{--<hr>--}}

	@include('search.search-block', ['class' => 'm-full'])

	<hr>

	@include('includes.banners.top-banner')

	<h3 class="section-title m-full">{{ $subcategory->title }}</h3>
	@if($organizations->count() > 0)
		<div class="organizations-list m--b">
			@include('organizations.organizations-list', [
					'organizations' => $organizations,
					'category' => $category,
					'subcategory' => $subcategory
					])
		</div>
		@if(isset($showMore) && $showMore)
			<a class="load-more m-t m-lr show-more"
				 data-current-page="2"
				 data-url="{{ url(Request::path()) }}">{{ $onNextPage }}</a>
		@endif
	@else

		@include('includes.alert', [
			'type' => 'info',
			'class' => 'm-lr m-b',
			'messages' => ['Упс... Закладів не знайдено, але найближчим часом ми це виправимо']
		])
	@endif

@stop

@section('scripts')
	<script src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@12.0.0/dist/lazyload.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/jquery@3.4.1/dist/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sticky-sidebar@3.3.1/dist/sticky-sidebar.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/resize-sensor@0.0.6/ResizeSensor.min.js"></script>

	<script src="{{ asset('js/common-slider.js') }}"></script>
	<script src="{{ asset('libs/lightslider/lightslider-blur-fix.min.js') }}"></script>
	<script>
		commonSlider('organizations-slider');
	</script>
@stop