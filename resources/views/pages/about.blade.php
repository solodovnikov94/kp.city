@extends('layouts.layouts-without-sidebar')

@section('meta')
	@include('includes.meta', [
		'title' => config('metatags.about.title'),
		'meta_description' => config('metatags.about.description'),
		'meta_keywords' => config('metatags.about.keywords'),
		'og_title' => config('metatags.about.title'),
		'og_description' => config('metatags.about.description'),
		'og_image' => asset('images/pages/static.jpg'),
		'active' => true
	])
@stop

@section('styles')
	<link rel="stylesheet" href="{{ mix('css/common.css') }}">
	<link rel="stylesheet" href="{{ mix('css/static.css') }}">
@endsection

@section('scripts')
	<script src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@12.0.0/dist/lazyload.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/jquery@3.4.1/dist/jquery.min.js"></script>
@stop

@section('content')
	<div class="static-page">
		<figure class="static-page__figure">
			<img src="{{ asset('images/pages/static.svg') }}" alt="Портал Кам'янця-Подільського — KP.CITY" class="b-lazy-default">
			<picture>
				<source media="(min-width: 1200px)" data-srcset="{{ asset('images/pages/static1170.jpg') }}">
				<source media="(min-width: 992px)" data-srcset="{{ asset('images/pages/static970.jpg') }}">
				<img class="b-lazy"
						 data-src="{{ asset('images/pages/static750.jpg') }}"
						 alt="Портал Кам'янця-Подільського — KP.CITY">
			</picture>
		</figure>
		<div class="breadcrumbs">
			@include('includes.breadcrumbs', ['current' => 'Про проект'])
		</div>
		<div class="static-page__content p-full">
			<h1 class="static-page__title">Про проект</h1>
			<div class="static-page__content-container">
				<div class="static-page__left">
					<div class="static-page__description">
						<p>Ми - невелика команда з Кам’янця-Подільського, яка поставила перед собою ціль - зробити кращий інтернет-ресурс міста.</p>
					</div>
				</div>
				<div class="static-page__right">
					<div class="static-page__main-content media-content">
						<p>KP.CITY щодня повідомляє про останні події в місті та Україні, підбирає цікаві матеріали по актуальних темах, а також дає можливість шукати та знаходити компанії, місця і заклади нашого міста. Кожен користувач сайту має можливість ділитися своїми думками та залишати відгуки до закладів, тим самим допомагаючи один одному в виборі кращих закладів міста.</p>
						<br>
						<p>Наша команда щодня додає нову, та оновлює вже наявну інформацію на сайті. Ми працюємо над покращенням вже наявного функціоналу, і, звичайно, над розширенням проекту - найближчим часом планується відкриття нових розділів і актуальних спецтем. Залишайтесь з нами!</p>
					</div>

					<hr class="m-tb">
					<div class="links-list">
						<a href="{{ url('/contacts') }}">Наші контакти</a>
						<a href="{{ url('/advertising') }}">Реклама на сайті</a>
						<a href="{{ url('/rules') }}">Правила</a>
						<a href="{{ url('/privacy') }}">Конфіденційність</a>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop