@extends('layouts.layouts-without-sidebar')

@section('meta')
	@include('includes.meta', [
		'title' => config('metatags.advertising.title'),
		'meta_description' => config('metatags.advertising.description'),
		'meta_keywords' => config('metatags.advertising.keywords'),
		'og_title' => config('metatags.advertising.title'),
		'og_description' => config('metatags.advertising.description'),
		'og_image' => asset('images/pages/static.jpg'),
		'active' => true
	])
@stop

@section('styles')
	<link rel="stylesheet" href="{{ mix('css/common.css') }}">
	<link rel="stylesheet" href="{{ mix('css/static.css') }}">
@endsection

@section('scripts')
	<script src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@12.0.0/dist/lazyload.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/jquery@3.4.1/dist/jquery.min.js"></script>
@stop

@section('content')
	<div class="static-page">
		<figure class="static-page__figure">
			<img src="{{ asset('images/pages/static.svg') }}" alt="Портал Кам'янця-Подільського — KP.CITY" class="b-lazy-default">
			<picture>
				<source media="(min-width: 1200px)" data-srcset="{{ asset('images/pages/static1170.jpg') }}">
				<source media="(min-width: 992px)" data-srcset="{{ asset('images/pages/static970.jpg') }}">
				<img class="b-lazy"
						 data-src="{{ asset('images/pages/static750.jpg') }}"
						 alt="Портал Кам'янця-Подільського — KP.CITY">
			</picture>
		</figure>
		<div class="breadcrumbs">
			@include('includes.breadcrumbs', ['current' => 'Реклама на сайті'])
		</div>
		<div class="static-page__content p-full">
			<h1 class="static-page__title">Реклама на сайті</h1>
			<div class="static-page__content-container">
				<div class="static-page__left">
					<div class="static-page__description">
						<p>У нас широка аудиторія, яка потрібна Вашому бізнесу!</p>
					</div>
				</div>
				<div class="static-page__right">
					<div class="static-page__main-content media-content">
						<p>При створенні проекту KP.CITY ми зробили ставку на якісну розробку сайту, в основі якої лежить сучасний дизайн, продуманий функціонал і останні технології зі світу ІТ. На сайті представлено лише якісний і актуальний контент, цікавий для користувачів. В цілому, саме завдяки цим факторам нам вдалось зібрати широку аудиторію в Кам’янці-Подільському.</p>
						<br>
						<p>Основну частину нашої аудиторії складають кам’янчани, віком від 19 до 45 років. Їх дохід - середній і вище середнього. Відвідувачі переважно цікавляться туризмом, відпочинком, спортом, авто і мото, їжею і напоями, комп'ютерами і електронікою.</p>
						<br>
						{{--<p>#Email</p>--}}
						{{--<br>--}}
						{{--<p>#Phone</p>--}}
						{{--<br>--}}
						<p>Детальна інформація по рекламі на KP.CITY представлена в Media Kit.</p>
					</div>

					<hr class="m-tb">
					<div class="links-list">
						<a href="{{ url('/about') }}">Про проект</a>
						<a href="{{ url('/contacts') }}">Наші контакти</a>
						<a href="{{ url('/rules') }}">Правила</a>
						<a href="{{ url('/privacy') }}">Конфіденційність</a>
					</div>
				</div>
			</div>
		</div>

		<!-- Load Facebook SDK for JavaScript -->
		<div id="fb-root"></div>
		<script>
			window.fbAsyncInit = function() {
				FB.init({
					xfbml            : true,
					version          : 'v3.2'
				});
			};

			(function(d, s, id) {
				var js, fjs = d.getElementsByTagName(s)[0];
				if (d.getElementById(id)) return;
				js = d.createElement(s); js.id = id;
				js.src = 'https://connect.facebook.net/ru_RU/sdk/xfbml.customerchat.js';
				fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));</script>

		<!-- Your customer chat code -->
		<div class="fb-customerchat"
				 attribution=setup_tool
				 page_id="2209729585918706"
				 theme_color="#6699cc"
				 logged_in_greeting="Доброго часу доби! Напишіть нам, якщо у вас виникли питання."
				 logged_out_greeting="Доброго часу доби! Напишіть нам, якщо у вас виникли питання.">
		</div>

	</div>
@stop