@extends('layouts.layouts-without-sidebar')

@section('meta')
	@include('includes.meta', [
		'title' => config('metatags.contacts.title'),
		'meta_description' => config('metatags.contacts.description'),
		'meta_keywords' => config('metatags.contacts.keywords'),
		'og_title' => config('metatags.contacts.title'),
		'og_description' => config('metatags.contacts.description'),
		'og_image' => asset('images/pages/static.jpg'),
		'active' => true
	])
@stop

@section('priorities')
	<link rel="preconnect" href="//www.google.com">
	<link rel="preconnect" href="//www.gstatic.com">
@stop

@section('styles')
	<link rel="stylesheet" href="{{ mix('css/common.css') }}">
	<link rel="stylesheet" href="{{ mix('css/static.css') }}">
@endsection

@section('scripts')
	<script src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@12.0.0/dist/lazyload.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/jquery@3.4.1/dist/jquery.min.js"></script>
	<script src="https://www.google.com/recaptcha/api.js"></script>
@stop

@section('content')
	<div class="static-page">
		<figure class="static-page__figure">
			<img src="{{ asset('images/pages/static.svg') }}" alt="Портал Кам'янця-Подільського — KP.CITY" class="b-lazy-default">
			<picture>
				<source media="(min-width: 1200px)" data-srcset="{{ asset('images/pages/static1170.jpg') }}">
				<source media="(min-width: 992px)" data-srcset="{{ asset('images/pages/static970.jpg') }}">
				<img class="b-lazy"
						 data-src="{{ asset('images/pages/static750.jpg') }}"
						 alt="Портал Кам'янця-Подільського — KP.CITY">
			</picture>
		</figure>
		<div class="breadcrumbs">
			@include('includes.breadcrumbs', ['current' => 'Наші контакти'])
		</div>
		<div class="static-page__content p-full">
			<h1 class="static-page__title">Наші контакти</h1>
			<div class="static-page__content-container">
				<div class="static-page__left">
					<div class="static-page__description">
						<p>Виникли запитання? Звертайтесь за допомогою!</p>
					</div>
				</div>
				<div class="static-page__right">
					<div class="static-page__main-content media-content">
						<p>Якщо у Вас виникають будь-які запитання по роботі інтернет-ресурсу KP.CITY - будь ласка, зверніться до нас використовуючи контактні дані.</p>
						<br>
						{{--<p>#Адреса офісу</p>--}}
						{{--<br>--}}
						{{--<p>#Номер телефону</p>--}}
						{{--<br>--}}
						{{--<p>#Емейл</p>--}}
						{{--<br>--}}
						{{--<p>#Карта</p>--}}
						{{--<br>--}}
						<p>Ви можете зв'язатися з нами або задати нам питання, скориставшись формою зворотнього зв'язку.</p>
						<br>
						<form class="popup__form" method="post" action="{{ url('contacts') }}">
							@csrf
							<div class="popup__form-group">
								<label for="contacts-name" class="popup__form-item">
									<input type="text" name="name" id="contacts-name" placeholder="Ваше ім'я" aria-label="Ваше ім'я">
									<span class="popup__form-error"></span>
								</label>
								<label for="contacts-email" class="popup__form-item">
									<input type="email" name="email" id="contacts-email" placeholder="Ваш e-mail" aria-label="Ваш e-mail" inputmode="email">
									<span class="popup__form-error"></span>
								</label>
							</div>
							<label for="contacts-text" class="popup__form-item">
								<textarea name="text"
													id="contacts-text"
													aria-label="Ваше повідомлення"
													placeholder="Чим ми можемо Вам допомогти?"
													></textarea>
								<span class="popup__form-error"></span>
							</label>
							<label class="popup__form-item">
								<span class="g-recaptcha" data-sitekey="6LfqoIQUAAAAAK2WDkXbwN4so4la7h6sS-_Iooqr"></span>
								<input type="hidden" id="contacts-g-recaptcha-response" aria-label="Captcha">
								<span class="popup__form-error"></span>
							</label>
							<button class="width-auto send-contacts">Відправити</button>
						</form>
					</div>

					<hr class="m-tb">
					<div class="links-list">
						<a href="{{ url('/about') }}">Про проект</a>
						<a href="{{ url('/advertising') }}">Реклама на сайті</a>
						<a href="{{ url('/rules') }}">Правила</a>
						<a href="{{ url('/privacy') }}">Конфіденційність</a>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop