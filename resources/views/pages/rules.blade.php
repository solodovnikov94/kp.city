@extends('layouts.layouts-without-sidebar')

@section('meta')
	@include('includes.meta', [
		'title' => config('metatags.rules.title'),
		'meta_description' => config('metatags.rules.description'),
		'meta_keywords' => config('metatags.rules.keywords'),
		'og_title' => config('metatags.rules.title'),
		'og_description' => config('metatags.rules.description'),
		'og_image' => asset('images/pages/static.jpg'),
		'active' => true
	])
@stop

@section('styles')
	<link rel="stylesheet" href="{{ mix('css/common.css') }}">
	<link rel="stylesheet" href="{{ mix('css/static.css') }}">
@endsection

@section('scripts')
	<script src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@12.0.0/dist/lazyload.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/jquery@3.4.1/dist/jquery.min.js"></script>
@stop

@section('content')
	<div class="static-page">
		<figure class="static-page__figure">
			<img src="{{ asset('images/pages/static.svg') }}" alt="Портал Кам'янця-Подільського — KP.CITY"
					 class="b-lazy-default">
			<picture>
				<source media="(min-width: 1200px)" data-srcset="{{ asset('images/pages/static1170.jpg') }}">
				<source media="(min-width: 992px)" data-srcset="{{ asset('images/pages/static970.jpg') }}">
				<img class="b-lazy"
						 data-src="{{ asset('images/pages/static750.jpg') }}"
						 alt="Портал Кам'янця-Подільського — KP.CITY">
			</picture>
		</figure>
		<div class="breadcrumbs">
			@include('includes.breadcrumbs', ['current' => 'Правила користування сайтом'])
		</div>
		<div class="static-page__content p-full">
			<h1 class="static-page__title">Правила користування сайтом</h1>
			<div class="static-page__content-container">
				<div class="static-page__left">
					<div class="static-page__description">На цій сторінці описані умови, які регулюють використання матеріалів проекту «KP.CITY» (далі — «Сайт», «Компанія», «Ми»). Отримуючи доступ або використовуючи будь-яку частину сайту —	Ви погоджуєтеся з	умовами цієї угоди. Також, рекомендуємо ознайомитися з нашою <a href="{{ url("/privacy") }}">політикою конфіденційності</a>.
					</div>
				</div>
				<div class="static-page__right">
					<div class="static-page__main-content media-content">

						<h3>1. Загальні умови</h3>
						<p>1.1. Якщо Ви вирішили користуватися сайтом KP.CITY та додатками сайту, включаючи  RSS, API, програмним забезпеченням та іншими файлами, Ви даєте згоду на дотримання всіх умов, викладених у цих Правилах користування сайтом (далі – «Правила»), у тому числі, шляхом реєстрації на сайті свого облікового запису (аккаунту). Ці Правила є юридично обов'язковою угодою між користувачем і компанією KP.CITY, предметом якої є надання користувачеві послуг з використання сайту і його сервісів.</p>
						<br>
						<p>1.2. Ми можемо у будь-який час змінювати умови, викладені в цих Правилах, і такі зміни набирають чинності негайно після їх оприлюднення на сайті KP.CITY.</p>
						<br>
						<p>1.3. Ви зобов’язані ознайомлюватися з цими  Правилами перед кожним користуванням сайтом KP.CITY. Якщо Ви продовжуєте користуватися сайтом після оприлюднення змінених Правил, це означає Вашу згоду на дотримання Правил з усіма його змінами.</p>
						<br>
						<p>1.4. Якщо будь-які умови цих Правил або зміни до них неприйнятні для Вас, Ви можете не розпочинати користування сайтом KP.CITY або припинити дію Вашого облікового запису у порядку, визначеному розділом 8 Правил.</p>
						<br><br>

						<h3>2. Контент Сайту</h3>
						<p>2.1. Матеріали, опубліковані на сайті KP.CITY, призначені для Вашого особистого некомерційного використання. Всі матеріали, опубліковані на сайті KP.CITY захищені авторським правом і знаходяться у володінні або під контролем компанії KP.CITY або сторони, яка є постачальником контенту. Ви повинні дотримуватися всіх додаткових повідомлень про авторські права, інформації або обмежень, що містяться на будь-якій сторінці.</p>
						<br>
						<p>2.2. Зміст сайту KP.CITY захищений авторським правом відповідно до законодавства України та міжнародного авторського права. Ви не маєте права змінювати, публікувати, передавати, брати участь у передачі або продажу, відтворювати (за винятком випадків, передбачених в пункті 2.4 цих Правил), створювати нові твори, поширювати, виконувати, або яким-небудь чином використовувати зміст сайту KP.CITY (включаючи програмне забезпечення) в цілому або частково.</p>
						<br>
						<p>2.3. Ви можете завантажити або скопіювати вміст сайту KP.CITY, інші компоненти та елементи, що відображаються на сайті, тільки для особистого використання, за умови дотримання всіх авторських прав та інших повідомлень, що містяться в ньому.</p>
						<br>
						<p>2.4. Інтернет-виданням дозволяється безкоштовно використовувати інформацію, розміщену на сайті KP.CITY, за умови відкритого гіперпосилання та згадки першоджерела не нижче першого абзацу. Для ЗМІ дозволяється використовувати інформацію, розміщену на сайті, за умови посилання на першоджерело. Під використанням інформації мається на увазі будь-яке відтворення, републікація, поширення, переробка, переклад, включення його частин до інших творів та інші способи, передбачені Законом України "Про авторське право та суміжні права".</p>
						<br>
						<p>2.5. Використання матеріалів, що розміщуються на сайті KP.CITY, шляхом їх повного або часткового відтворення новин дозволяється тільки з письмового дозволу. Компанія KP.CITY надає письмовий дозвіл на використання своїх матеріалів на свій розсуд на підставі його звернення, направленого в електронній формі за адресою info[собачка]kp.city. Письмовий дозвіл може поширюватися на окремий матеріал (матеріали) або на всі поточні і майбутні матеріали на ресурсах порталу KP.CITY, і є дійсним протягом установленого в ньому строку.</p>
						<br><br>

						<h3>3. Контент користувачів</h3>
						<p>3.1. Ви погоджуєтесь користуватися сайтом KP.CITY виключно с законною метою та таким чином, щоб не порушувати права інших користувачів, не обмежувати та не перешкоджати доступу інших до сайту та користуванню ним.</p>
						<br>
						<p>3.2. Не дозволяється завантажувати, розповсюджувати та публікувати контент будь-якого наклепницького, образливого, непристойного, порнографічного чи іншого незаконного матеріалу; рекламні прояви, комерційні повідомлення, а також повідомлення, які не мають інформаційного навантаження і не стосуються тематики ресурсу.</p>
						<br>
						<p>3.3. Будьте ввічливі. Ви погоджуєтесь, що не будете погрожувати або ображати інших користувачів, використовуючи наклепницькі прийоми, або свідомо публікувати повідомлення з повторювальним змістом (спам). Ви погоджуєтесь не використовувати мову, яка зловживає або розрізняє на підставі раси, релігії, національності, роду, сексуальних переваг, віку, регіону, інвалідності  і т.п. Ворожнеча будь-якого роду є підставою для негайного призупинення доступу до всього сайту або його частини.</p>
						<br>
						<p>3.4. Не розголошуйте в своїх повідомленнях ніякої особистої інформації про себе або про будь-кого іншого (наприклад: телефонний номер, домашню адресу, адресу електронної пошти).</p>
						<br>
						<p>3.5. Ви визнаєте, що будь-які матеріали поширені Вами (наприклад контент, створюваний Вами, в тому числі, але не обмежуючись: коментарі, форум, повідомлення, рецензії, текст, відео, аудіо та фотографій, а також комп'ютерний код і додатки) може бути видалений або змінений нами. Ви відмовляєтесь від будь-яких прав, які Ви можете мати в разі наявності такого матеріалу на сайті KP.CITY, а зміна матеріалів може бути здійснена навіть у разі якщо Ви не згодні. Матеріали також можуть бути включені в наші RSS-канали, API, і доступні для перевидання через інші формати.</p>
						<br>
						<p>3.6. Ви надаєте компанії KP.CITY, безстрокову, невиключну, по всьому світу, роялті-фрі, ліцензію на розміщені/надіслані/поширені Вами матеріали, включаючи передачу компанії KP.CITY або будь-якій третій стороні необмежене право на використання, копіювання, передання, публікацію, поширення, створення похідних робіт, змінення і адаптування індексу, кеша, тегів, кодування (включаючи без обмеження права на адаптацію до потокового завантаження, передачі, мобільного, цифрового сканування або інших технології) в будь-якій формі або засобах масової інформації, відомих зараз або в подальшому розроблених, в тому числі,  будь-які публікації, розміщені Вами на сайті KP.CITY або на веб-сайті, який належить компанії KP.CITY.</p>
						<br>
						<p>3.7. Ви несете повну відповідальність за зміст Ваших публікацій. Компанія KP.CITY не може контролювати кожен матеріал і не несе відповідальності за зміст цих повідомлень, ми  залишаємо за собою право видаляти, переміщати або редагувати публікації, які компанія KP.CITY, за власним розсудом, вважає образливими, наклепницькими, непристойними або неприйнятними, які порушують авторські права і товарні знаки.</p>
						<br>
						<p>3.8. Ви даєте згоду на відображення і публікацію на сайті KP.CITY розробленого Вами  матеріалу, а також в рекламних цілях за межами сайту.</p>
						<br><br>

						<h3>4. Заяви та гарантії</h3>
						<p>4.1. Ви запевнюєте, гарантуєте та обіцяєте, що матеріали будь-якого роду, представлені через Ваш аккаунт не будуть порушені, плагіатом або порушувати права третіх осіб, включаючи авторське право, товарні знаки, недоторканність приватного життя або інших особистих або майнових прав, або містити наклепи чи інший незаконний матеріал, а також, Ви досягнули щонайменше вісімнадцяти років.</p>
						<br>
						<p>4.2. Компанія KP.CITY не гарантує і не підтверджує точності або надійності будь-яких порад, думок, заяв чи іншої  завантаженої або розповсюдження інформації через аккаунти будь-яких користувачів, постачальників інформації або будь-яких інших фізичних чи юридичних осіб. Ви визнаєте та усвідомлюєте, що покладання на такі думки, поради, заяви, меморандуми або іншу інформацію здійснюється Вами на власний страх і ризик. Матеріали сайту KP.CITY поширюються «як є», без будь-яких гарантій, включаючи гарантії  на право власності або придатності для конкретних цілей. Цим Ви визнаєте, що користуєтеся сайтом KP.CITY на свій страх і ризик.</p>
						<br><br>

						<h3>5. Реєстрація та безпека</h3>
						<p>5.1. Як частина процесу реєстрації або створення облікового запису, Ви будете створювати облікові дані для входу, вибравши пароль та надання адреси електронної пошти. Ви також повинні надати нам певні відомості про себе, які повинні бути точними і оновленими.</p>
						<br>
						<p>5.2. Кожна реєстрація дійсна для одного користувача. Ви не можете поділитися своїми обліковими даними реєстрації для входу, щоб ніхто інший не мав доступу до Вашого аккаунта. Ми можемо скасувати або призупинити Ваш доступ до сайту KP.CITY, якщо Ви поділитеся своїми обліковими даними для входу. Ви несете відповідальність за збереження конфіденційності пароля, який Ви не повинні відкривати будь-якому представнику або агенту компанії KP.CITY. Ви не можете вибрати або використовувати облікові дані іншої людини з тією метою, щоб видати себе за цю людину, використовувати дані облікового запису без права дозволу власника, або використовувати облікові дані, на свій розсуд, якщо вони є чи вважаються образливими. Недотримання вищезазначених положень вважається порушенням Правил, які можуть призвести до негайного припинення існування Вашого аккаунта.</p>
						<br>
						<p>5.3. Ви зобов’язані повідомити на info[собачка]kp.city про будь-які відомі або передбачувані випадки несанкціонованого використання Вашого аккаунта, або про будь-яке відоме або передбачуване порушення безпеки, у тому числі втрату, крадіжку або несанкціоноване розкриття Вашого пароля.</p>
						<br>
						<p>5.4. Ви несете відповідальність за будь-яке використання чи діяльність на Вашому обліковому запису на сайті KP.CITY, в тому числі за використання аккаунту третіми особами з Вашого дозволу. Будь-яка шахрайська, образлива або інша протизаконна діяльність може бути підставою для припинення дій Вашого облікового запису, на власний розсуд, і компанія KP.CITY може направити відомості до відповідних правоохоронних органів.</p>
						<br><br>

						<h3>6. Зв'язок між Сайтом і користувачами</h3>
						<p>6.1. Якщо Ви заповнюєте реєстраційну форму з метою отримання певної інформації, ми, як власники і правонаступники, з  дозволу певних сторонніх постачальників, надаємо Вам інформацію про матеріали, опубліковані на сайті KP.CITY.</p>
						<br>
						<p>6.2. Ви надаєте, а компанія KP.CITY залишає за собою право відправляти на Вашу електронну пошту інформацію про зміни або доповнення послуг на сайті.</p>
						<br>
						<p>6.3. Компанія KP.CITY залишає за собою право розкривати інформацію про використання та демографію за умови, що це не буде розкривати Вашу особистість. Рекламодавці на сайті можуть збирати і поширювати особисту інформацію про Вас, тільки якщо Ви підтверджуєте свою згоду відповідно до Політики конфіденційності та захисту персональних даних компанії KP.CITY.</p>
						<br>
						<p>6.4. Компанія KP.CITY може зв'язатися з Вами по електронній пошті про Вашу участь в опитуванні користувачів, задаючи питання з метою покращення поточних або перспективних матеріалів. Ця інформація буде використана для покращення роботи сайту KP.CITY і для кращого розуміння наших користувачів. Будь-яка інформація, яку ми отримуємо в таких опитуваннях, не буде передана третім особам, за винятком узагальнених знеособлених даних.</p>
						<br><br>

						<h3>7. Ліцензії та програмне забезпечення</h3>
						<p>7.1. Ви не маєте ніяких прав на пропрієтарне програмне забезпечення та супровідну документацію, або на будь-які поліпшення або зміни до них («програмне забезпечення»), якщо у Вас  не має доступу до сайту KP.CITY. Ви не можете субліцензувати, поступатися або передавати будь-які ліцензії, надані rомпанією KP.CITY, і будь-яка спроба такого субліцензування, привласнення або передачі вважається недійсною та протиправною. Ви не можете копіювати, поширювати, змінювати, переробляти, або створювати похідні роботи від програмного забезпечення.</p>
						<br><br>

						<h3>8. Припинення доступу до сайту</h3>
						<p>8.1. Ви можете припинити дію аккаунта в будь-який момент, звернувшись на на info[собачка]kp.city. Ви отримаєте автоматичне підтвердження по електронній пошті, що заяву на видалення аккаунту прийнято, і Ваш доступ буде припинений протягом 24 годин.</p>
						<br>
						<p>8.2. Компанія KP.CITY, може, на свій власний розсуд, видалити або призупинити доступ до всього cайту або його частини з будь-якої причини, включаючи, порушення або невиконання цих Правил.</p>
						<br><br>

						<h3>9. Різне</h3>
						<p>9.1. Ці Правила застосовуються відповідно до законодавства України. Будь-які спори щодо реалізації цих Правил будуть вирішуватися відповідним судом, в порядку, визначеному чинними законодавством України.</p>
						<br>
						<p>9.2. Незважаючи на будь-які положення з вищевикладеного, нічого в цих Правилах не може використовуватися для тлумачення положень, зазначених в Політиці конфіденційності.</p>
						<br>
						<p>9.3. Листування слід здійснювати на info[собачка]kp.city.</p>
						<br>
						<p>9.4. Ви погоджуєтесь негайно повідомляти про будь-які порушення авторських прав щодо матеріалів cайту KP.CITY. Про виникнення претензій з Вашого боку щодо порушення авторських прав стосовно матеріалів, які містяться на Сайті, повідомляйте на info[собачка]kp.city.</p>
						<br>
						<p>9.5. Матеріали можуть містити посилання, пов'язані з іншими інтернет-сайтами, ресурсами та рекламодавцями. Так як ми не несемо відповідальності за доступність цих зовнішніх ресурсів або їх зміст, Ви повинні направляти будь-яке занепокоєння щодо будь-якого зовнішнього посилання адміністратору такого веб-сайту.</p>
						<br><br>

						<h3>10. Реклама та партнерські матеріали</h3>
						<p>10.1 Матеріали з плашками «реклама», «новини компаній», «офіційно», «партнерський матеріал» публікуються на комерційних або партнерських засадах. Докладніше <a href="{{ url('/advertising') }}">про рекламу</a>.</p>
					</div>

					<hr class="m-tb">
					<div class="links-list">
						<a href="{{ url('/about') }}">Про проект</a>
						<a href="{{ url('/contacts') }}">Наші контакти</a>
						<a href="{{ url('/advertising') }}">Реклама на сайті</a>
						<a href="{{ url('/privacy') }}">Конфіденційність</a>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop