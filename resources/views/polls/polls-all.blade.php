@extends('layouts.layouts-main')

@section('meta')

	@include('includes.meta', [
		'title' => config('metatags.polls.title'),
		'meta_description' => config('metatags.polls.description'),
		'meta_keywords' => config('metatags.polls.keywords'),
		'og_title' => config('metatags.polls.title'),
		'og_description' => config('metatags.polls.description'),
		'og_image' => asset('images/pages/polls.png'),
		'active' => true
	])
@stop

@section('styles')
	<link rel="stylesheet" href="{{ mix('css/common.css') }}">
	<link rel="stylesheet" href="{{ mix('css/polls.css') }}">
	<link rel="stylesheet" href="{{ mix('css/articles.css') }}">
	<link rel="stylesheet" href="{{ mix('css/organizations.css') }}">
	<link rel="stylesheet" href="{{ asset('libs/lightslider/lightslider.css') }}">
@endsection

@section('content')
	<div class="breadcrumbs">
		@include('includes.breadcrumbs', ['current' => 'Опитування'])
	</div>

	@if(isset($polls) && count($polls))

		@include('includes.banners.top-banner')
		<div class="m-t m-lr">
			<div class="polls-list">
				@include('polls.polls-list', ['topPoll' => true])
			</div>
			@if(isset($showMore) && $showMore)
				<a class="load-more show-more m-b"
					 data-current-page="2"
					 data-url="{{ url(Request::path()) }}">{{ $onNextPage }}</a>
			@endif

		</div>
	@else
		@include('includes.alert', [
				'type' => 'info',
				'class' => 'm-lr m-b m-t',
				'messages' => ['Поки що немає актуальних опитувань']
			])
	@endif

	<hr>

	@if(isset($popularPolls) && count($popularPolls) >= 2)
		<div class="slider-section m-lr m-t">
			<div class="slider-section__header">
				<h3 class="slider-section__title">Популярні опитування</h3>
				<div class="slider-section__controls">
					<div class="slider-section__controls--prev" id="slider-top-polls-prev">
						<i class="fal fa-angle-left"></i>
					</div>
					<div class="slider-section__controls--next" id="slider-top-polls-next">
						<i class="fal fa-angle-right"></i>
					</div>
				</div>
			</div>
			<div class="slider-section__content polls-slider" id="slider-top-polls">
				@foreach($popularPolls as $popularPoll)
					<a href="{{ $popularPoll->getUrl() }}" class="poll-card">
						<div class="poll-card__title">{{ $popularPoll->title }}</div>
						<div class="buttons-container">
							<span>Вже проголосувало: {{ $popularPoll->answer_scores_count }}</span>
						</div>
					</a>
				@endforeach
			</div>
		</div>

		<hr class="m-t">
	@endif

	@if(isset($news) && count($news) > 0)
		<div class="slider-section m-full">
			<div class="slider-section__header">
				<h3 class="slider-section__title">Свіжі новини</h3>
				<div class="slider-section__controls">
					<div class="slider-section__controls--prev" id="slider-news-prev">
						<i class="fal fa-angle-left"></i>
					</div>
					<div class="slider-section__controls--next" id="slider-news-next">
						<i class="fal fa-angle-right"></i>
					</div>
				</div>
			</div>
			<div class="slider-section__content" id="slider-news">
				@include('articles.articles-list', ['articles' => $news])
			</div>
		</div>
	@endif

	<hr>

	@if(isset($organizations) && count($organizations) > 0)
		<div class="slider-section m-lr m-t">
			<div class="slider-section__header">
				<h3 class="slider-section__title">Популярні заклади міста</h3>
				<div class="slider-section__controls">
					<div class="slider-section__controls--prev" id="slider-organizations-prev">
						<i class="fal fa-angle-left"></i>
					</div>
					<div class="slider-section__controls--next" id="slider-organizations-next">
						<i class="fal fa-angle-right"></i>
					</div>
				</div>
			</div>
			<div class="slider-section__content" id="slider-organizations">
				@include('organizations.organizations-list', ['organizations' => $organizations])
			</div>
		</div>
	@endif

@endsection

@section('scripts')
	<script src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@12.0.0/dist/lazyload.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/jquery@3.4.1/dist/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sticky-sidebar@3.3.1/dist/sticky-sidebar.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/resize-sensor@0.0.6/ResizeSensor.min.js"></script>

	<script src="{{ asset('js/common-slider.js') }}"></script>
	<script src="{{ asset('libs/lightslider/lightslider-blur-fix.min.js') }}"></script>
	<script>
		let slider = $("#slider-top-polls").lightSlider({
			item: 2,
			slideMargin: 25,
			easing: "ease",
			speed: 300,
			pager: !1,
			controls: !1,
			pauseOnHover: !0,
			pause: 5e3,
			enableTouch: 0,
			responsive: [
				{breakpoint: 1200, settings: {item: 2, slideMargin: 15}},
				{breakpoint: 992, settings: {item: 1, slideMargin: 15}},
				{breakpoint: 768, settings: {item: 2, slideMargin: 15}},
				{breakpoint: 640, settings: {item: 1, slideMargin: 15}},
				{breakpoint: 480, settings: {item: 1, slideMargin: 15}}
			],
			onSliderLoad: function (el) {
				let maxHeight = 0,
					container = $(el),
					children = container.children();

				children.each(function () {
					let childHeight = $(this).height();
					if (childHeight > maxHeight) {
						maxHeight = childHeight;
					}
				});
				container.height(maxHeight);
			}
		});

		$("#slider-top-polls-prev").on("click", function () {
			slider.goToPrevSlide();
		});

		$("#slider-top-polls-next").on("click", function () {
			slider.goToNextSlide();
		});
		commonSlider('slider-news');
		commonSlider('slider-organizations');
	</script>
@stop