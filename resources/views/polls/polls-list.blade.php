@foreach($polls as $poll)
	@if(isset($topPoll) && $loop->iteration <= 2)
		<a href="{{ $poll->getUrl() }}" class="poll-card big {{ $poll->isActiveToVote() ? '' : 'disabled' }}">
			<div class="poll-card__title">{{ $poll->title }}</div>
			<div class="buttons-container">
				@if($poll->isActiveToVote())
					<button class="btn btn-xl btn-hover btn-red">Проголосувати</button>
				@else
					<button class="btn btn-xl btn-grey">Результати</button>
				@endif
				<span>Вже проголосувало: {{ $poll->totalVotes() }}</span>
			</div>
		</a>
	@else
		<a href="{{ $poll->getUrl() }}" class="poll-card {{ $poll->isActiveToVote() ? '' : 'disabled' }}">
			<div class="poll-card__title">{{ $poll->title }}</div>
			<div class="buttons-container">
				<span>Вже проголосувало: {{ $poll->answer_scores_count }}</span>
			</div>
		</a>
	@endif
@endforeach
