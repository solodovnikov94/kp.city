@extends('layouts.layouts-main')

@section('meta')
	@include('includes.meta', [
		'title' => $poll->meta_title ?? $poll->title,
		'meta_description' => $poll->meta_description,
		'meta_keywords' => $poll->meta_keywords,
		'og_title' => $poll->og_title ?? $poll->meta_title,
		'og_description' => $poll->og_description ?? $poll->meta_description,
		'og_image' => $poll->getCroppedImage('og', 'jpg', 'og_image') ?? asset('images/pages/polls.png'),
		'og_image_type' => 'og',
		'active' => $poll->isPublished()
	])
@stop

@section('styles')
	<link rel="stylesheet" href="{{ mix('css/common.css') }}">
	<link rel="stylesheet" href="{{ mix('css/polls.css') }}">
	<link rel="stylesheet" href="{{ mix('css/articles.css') }}">
	<link rel="stylesheet" href="{{ mix('css/comments.css') }}">
	<link rel="stylesheet" href="{{ asset('libs/lightslider/lightslider.css') }}">
@endsection

@section('content')
	<div class="breadcrumbs">
		@include('includes.breadcrumbs', ['links' => [
				['link' => '/polls', 'title' => 'Опитування'],
		], 'current' => $poll->title])
		@can('Access to Nova')
			<div class="breadcrumbs-controls tooltip-parent">
				<i class="fal fa-ellipsis-h"></i>
				<div class="tooltip tooltip-left tooltip-controls tooltip-blue">
					<a href="{{ url('adm/resources/polls/' . $poll->id . '/edit') }}" target="_blank"
						 rel="noopener">Редагувати</a>
				</div>
			</div>
		@endcan
	</div>

	<div class="poll-single media-content m-lr">
		@include('includes.poll.poll-list')

		<div class="poll-single__content m-t">
			{!! $poll->description !!}
		</div>

	</div>

	<div class="article-single__footer m-full">
		<div class="counters">
			<span><i class="fal fa-eye"></i>{{ $poll->views }}</span>
			<span><i class="fal fa-comment"></i>{{ $poll->comments_count }}</span>
		</div>

		@include('includes.social-buttons', ['url' => url(Request::path()), 'class' => 'share'])
	</div>

	@if(isset($otherPolls) && count($otherPolls) > 1)
		<hr>

		<div class="slider-section m-lr m-t">
			<div class="slider-section__header">
				<h3 class="slider-section__title">Приймайте участь в інших опитуваннях</h3>
				<div class="slider-section__controls">
					<div class="slider-section__controls--prev" id="slider-top-polls-prev">
						<i class="fal fa-angle-left"></i>
					</div>
					<div class="slider-section__controls--next" id="slider-top-polls-next">
						<i class="fal fa-angle-right"></i>
					</div>
				</div>
			</div>
			<div class="slider-section__content polls-slider" id="slider-top-polls">
				@foreach($otherPolls as $otherPoll)
					<a href="{{ $otherPoll->getUrl() }}" class="poll-card">
						<div class="poll-card__title">{{ $otherPoll->title }}</div>
						<div class="buttons-container">
							<span>Вже проголосувало: {{ $otherPoll->totalVotes() }}</span>
						</div>
					</a>
				@endforeach
			</div>
		</div>
	@endif

	<hr class="m-t">

		@include('comments.comments-list', [
		'comments' => $poll->comments_list,
		'totalComments' => $poll->comments_count,
		'maxLikes' => $poll->max_likes
	])

@endsection

@section('scripts')
	<script src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@12.0.0/dist/lazyload.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/jquery@3.4.1/dist/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sticky-sidebar@3.3.1/dist/sticky-sidebar.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/resize-sensor@0.0.6/ResizeSensor.min.js"></script>

	<script src="{{ asset('js/common-slider.js') }}"></script>
	<script src="{{ asset('libs/lightslider/lightslider-blur-fix.min.js') }}"></script>
	<script>
		let slider = $("#slider-top-polls").lightSlider({
			item: 2,
			slideMargin: 25,
			easing: "ease",
			speed: 300,
			pager: !1,
			controls: !1,
			pauseOnHover: !0,
			pause: 5e3,
			enableTouch: 0,
			responsive: [
				{breakpoint: 1200, settings: {item: 2, slideMargin: 15}},
				{breakpoint: 992, settings: {item: 1, slideMargin: 15}},
				{breakpoint: 768, settings: {item: 2, slideMargin: 15}},
				{breakpoint: 640, settings: {item: 1, slideMargin: 15}},
				{breakpoint: 480, settings: {item: 1, slideMargin: 15}}
			],
			onSliderLoad: function (el) {
				let maxHeight = 0,
					container = $(el),
					children = container.children();

				children.each(function () {
					let childHeight = $(this).height();
					if (childHeight > maxHeight) {
						maxHeight = childHeight;
					}
				});
				container.height(maxHeight);
			}
		});

		$("#slider-top-polls-prev").on("click", function () {
			slider.goToPrevSlide();
		});

		$("#slider-top-polls-next").on("click", function () {
			slider.goToNextSlide();
		});
	</script>
@stop

@section('counter')
	@include('includes.counter', [
		'model' => $model,
		'column' => 'views',
		'id' => $poll->id
	])
@endsection
