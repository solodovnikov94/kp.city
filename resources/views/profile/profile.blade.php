@extends('layouts.layouts-main')

{{--@section('meta')--}}
{{--@include('includes.meta', [--}}
{{--'title' => config('metatags.tags.title'),--}}
{{--'meta_description' => config('metatags.tags.description'),--}}
{{--'meta_keywords' => config('metatags.tags.keywords'),--}}
{{--'og_title' => config('metatags.tags.title'),--}}
{{--'og_description' => config('metatags.tags.description'),--}}
{{--'og_image' => asset('images/pages/tags.jpg'),--}}
{{--'active' => true--}}
{{--])--}}
{{--@stop--}}

@section('styles')
	<link rel="stylesheet" href="{{ mix('css/common.css') }}">
	<link rel="stylesheet" href="{{ mix('css/organizations.css') }}">
	{{--<link rel="stylesheet" href="{{ mix('css/articles.css') }}">--}}
@endsection

@section('content')

	<div class="breadcrumbs">
		@include('includes.breadcrumbs', ['current' => 'Профіль'])
	</div>

	<div class="alert alert-danger m-t m-lr">Профіль користувача на даний момент в розробці... Слідкуйте за оновленнями
		;)
	</div>

	<div id="app">
		{{--<div class="row">--}}
		{{--<div class="col-md-8 col-md-offset-2">--}}
		{{--<div class="panel panel-default">--}}
		{{--<div class="panel-heading">organization changes</div>--}}
		{{--<div class="panel-body table-responsive">--}}
		{{--<router-view name="organizationIndex"></router-view>--}}
		{{--<router-view></router-view>--}}
		{{--</div>--}}
		{{--</div>--}}
		{{--</div>--}}
		{{--</div>--}}

			<organization-list></organization-list>
	</div>

	{{--<div class="static-page__content-container">--}}
	{{--<div class="static-page__left">--}}
	{{--<form method="POST" action="{{ url('vue-ajax') }}" class="popup__form">--}}
	{{--@csrf--}}
	{{--<label for="login-email" class="popup__form-item">--}}
	{{--<input type="text" name="vue-input" id="login-email" placeholder="E-mail">--}}
	{{--<span class="popup__form-error"></span>--}}
	{{--</label>--}}
	{{--<button>Відправити</button>--}}
	{{--</form>--}}
	{{--</div>--}}
	{{--</div>--}}
@stop

@section('scripts')
	<script src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@12.0.0/dist/lazyload.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/jquery@3.4.1/dist/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sticky-sidebar@3.3.1/dist/sticky-sidebar.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/resize-sensor@0.0.6/ResizeSensor.min.js"></script>
@stop