<form action="{{ route('search') }}" class="search-section {{ $class ?? '' }}">
	<label for="search-section">
		<input type="text"
					 id="search-section"
					 name="{{ $name ?? 'q' }}"
					 placeholder="{{ $placeholder ?? 'Що будемо шукати?' }}"
					 autocomplete="{{ isset($autocomplete) && $autocomplete === true ? 'on' : 'off' }}"
					 minlength="3"
					 required
					 aria-label="Що будемо шукати?">
	</label>
	<button type="submit" class="search-section__button">{{ $button_text ?? 'Знайти' }}</button>
</form>