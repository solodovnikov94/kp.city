@extends('layouts.layouts-main')

@section('meta')
	@include('includes.meta', [
		'title' => config('metatags.search.title'),
		'meta_description' => config('metatags.search.description'),
		'meta_keywords' => config('metatags.search.keywords'),
		'og_title' => config('metatags.search.title'),
		'og_description' => config('metatags.search.description'),
		'og_image' => asset('images/pages/search.jpg'),
		'active' => false
	])
@stop

@section('styles')
	<link rel="stylesheet" href="{{ mix('css/common.css') }}">
	<link rel="stylesheet" href="{{ mix('css/organizations.css') }}">
	<link rel="stylesheet" href="{{ mix('css/articles.css') }}">
@endsection

@section('content')

	<div class="breadcrumbs">
		@include('includes.breadcrumbs', ['current' => 'Пошук'])
	</div>

	@include('search.search-block', ['class' => 'm-full'])

	@if(isset($result) && count($result) > 1)
		<hr>

		<h3 class="section-title m-t m-lr">Результати пошуку за запитом "{{ $searchParam }}"</h3>
		<div class="section-description m-lr m-b">Знайдено матеріалів: {{ $result['total_number'] }}</div>

		@isset($result['organizations'])
			<hr class="m-t">

			<h3 class="section-title m-t m-lr">Заклади міста <span class="section-title__second-text">{{ $result['organizations_total'] }}</span></h3>
			<div class="organizations-list m-t m--b">
				@include('organizations.organizations-list', ['organizations' => $result['organizations']])
			</div>
			@if($result['organizations_last_page'] > 1)
				<a class="load-more show-more m-t m-lr"
					 data-current-page="2"
					 data-url="{{ url(Request::path()) }}"
					 data-url-param="q={{ $searchParam }}&param=organizations">{{ $result['organizations_on_next_page'] }}</a>
			@endif
		@endisset

		@isset($result['news'])
			<hr class="m-t">

			<h3 class="section-title m-t m-lr">Новини <span class="section-title__second-text">{{ $result['news_total'] }}</span></h3>
			<div class="articles-list m-t m--b">
				@include('articles.articles-list', ['articles' => $result['news']])
			</div>
			@if($result['news_last_page'] > 1)
				<a class="load-more show-more m-t m-lr"
					 data-current-page="2"
					 data-url="{{ url(Request::path()) }}"
					 data-url-param="q={{ $searchParam }}&param=news">{{ $result['news_on_next_page'] }}</a>
			@endif
		@endisset

		@isset($result['article'])
			<hr class="m-t">

			<h3 class="section-title m-t m-lr">Статті <span class="section-title__second-text">{{ $result['article_total'] }}</span></h3>
			<div class="articles-list m-t m--b">
				@include('articles.articles-list', ['articles' => $result['article']])
			</div>
			@if($result['article_last_page'] > 1)
				<a class="load-more m-lr m-t"
					 data-current-page="2"
					 data-url="{{ url(Request::path()) }}"
					 data-url-param="q={{ $searchParam }}&param=article">{{ $result['article_on_next_page'] }}</a>
			@endif
		@endisset

	@else
		@if($errors->has('q'))
			@include('includes.alert', [
				'type' => 'danger',
				'class' => 'm-lr',
				'messages' => [$errors->first('q')]
			])
		@else
			@include('includes.alert', [
				'type' => 'info',
				'class' => 'm-lr',
				'messages' => ['Нажаль, за запитом "' . $searchParam . '" не знайдено матеріалів']
			])
		@endif
	@endif
@stop

@section('scripts')
	<script src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@12.0.0/dist/lazyload.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/jquery@3.4.1/dist/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sticky-sidebar@3.3.1/dist/sticky-sidebar.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/resize-sensor@0.0.6/ResizeSensor.min.js"></script>
@stop