<div class="article-slider m-tb m--lr">
	<div id="article-slider" class="blazy-container">
		@foreach($data->photos->sortBy('order_') as $photo)
			<div class="article-slider-item">
				<div class="slider-img">
					<img src="{{ $photo->getCroppedImageOrTransparent('low') }}"
							 class="b-lazy-default"
							 alt="{{ $photo->image_title }}">
					<picture>
						<source media="(min-width: 992px)"
										data-srcset="{{ $photo->getCroppedImageOrError('max', 'webp') }}"
										type="image/webp">

						<source media="(min-width: 992px)"
										data-srcset="{{ $photo->getCroppedImageOrError('max', 'jpg') }}">

						<source media="(min-width: 768px) and (max-width: 991px)"
										data-srcset="{{ $photo->getCroppedImageOrError('mobile', 'webp') }}"
										type="image/webp">

						<source media="(min-width: 768px) and (max-width: 991px)"
										data-srcset="{{ $photo->getCroppedImageOrError('mobile', 'jpg') }}">

						<source media="(min-width: 560px) and (max-width: 767px)"
										data-srcset="{{ $photo->getCroppedImageOrError('max', 'webp') }}"
										type="image/webp">

						<source media="(min-width: 560px) and (max-width: 767px)"
										data-srcset="{{ $photo->getCroppedImageOrError('max', 'jpg') }}">

						<source data-srcset="{{ $photo->getCroppedImageOrError('mobile', 'webp') }}"
										type="image/webp">

						<img class="b-lazy"
								 data-src="{{ $photo->getCroppedImageOrError('mobile') }}"
								 alt="{{ $photo->image_title }}">
					</picture>
				</div>
			</div>
		@endforeach
	</div>
</div>
