@isset($data)
	<figure class="media-content__image m-tb m--lr">
		<div class="media-content__image-container">
			<img src="{{ $data->getCroppedImageOrTransparent('low', 'jpg', 'url') }}"
					 alt="{{ $data->title }}">
			<picture>
				<source media="(min-width: 992px)"
								data-srcset="{{ $data->getCroppedImageOrError('max', 'webp', 'url') }}"
								type="image/webp">
				<source media="(min-width: 992px)"
								data-srcset="{{ $data->getCroppedImageOrError('max', 'jpg', 'url') }}">

				<source media="(min-width: 768px) and (max-width: 991px)"
								data-srcset="{{ $data->getCroppedImageOrError('mobile', 'webp', 'url') }}"
								type="image/webp">
				<source media="(min-width: 768px) and (max-width: 991px)"
								data-srcset="{{ $data->getCroppedImageOrError('mobile', 'jpg', 'url') }}">

				<source media="(min-width: 560px) and (max-width: 767px)"
								data-srcset="{{ $data->getCroppedImageOrError('max', 'webp', 'url') }}"
								type="image/webp">
				<source media="(min-width: 560px) and (max-width: 767px)"
								data-srcset="{{ $data->getCroppedImageOrError('max', 'jpg', 'url') }}">

				<source data-srcset="{{ $data->getCroppedImageOrError('mobile', 'webp', 'url') }}"
								type="image/webp">

				<img class="b-lazy"
						 data-src="{{ $data->getCroppedImageOrError('mobile', 'jpg', 'url') }}"
						 alt="{{ $data->title }}">
			</picture>
		</div>
		@isset($data->title)
			<figcaption>{{ $data->title }}</figcaption>
		@endisset
	</figure>
@endif