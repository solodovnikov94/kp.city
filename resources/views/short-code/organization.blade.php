@if(!is_null($data))
	<div class="organization-brief bordered m-tb">
		<a href="{{ $data->getUrl() }}" class="organization-brief__link p-full">
			<div class="organization-brief__background">
				@if($data->category->parent)
				<img class="b-lazy"
						 data-src="{{ asset('/storage/' . $data->category->parent->icon) }}"
						 alt="{{ $data->category->parent->title }}">
				@endif
			</div>
			<div class="organization-brief__container">
				<div class="organization-brief__content">
					<div class="organization-brief__image">
						<img class="b-lazy"
								 data-src="{{ $data->getCroppedImageOrError('brief', 'jpg', 'image') }}"
								 alt="{{ $data->title }}">
					</div>
					<div class="organization-brief__information">
						<div class="organization-brief__header">
							<h2 class="organization-brief__title">{{ $data->title }}</h2>
						</div>
						<div class="organization-brief__address">
							@isset($data->addresses[0]['address'])
								<i class="fas fa-map-marker-alt"></i><span>{{ $data->addresses[0]['address'] }}</span>
							@endif
						</div>
					</div>
				</div>
				<span class="organization-brief__button btn btn-xl btn-red">Переглянути заклад</span>
			</div>
		</a>
	</div>
@endif