<div class="youtube-video bordered m-tb">
	<iframe class="b-lazy"
					data-src="https://www.youtube.com/embed/{{ $data }}?rel=0&amp;showinfo=0&amp;"
					frameborder="0"
					allow="autoplay; encrypted-media"
					allowfullscreen></iframe>
</div>