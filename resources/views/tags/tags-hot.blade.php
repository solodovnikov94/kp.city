@extends('layouts.layouts-main')

@section('meta')
	@include('includes.meta', [
		'title' => config('metatags.tags.title'),
		'meta_description' => config('metatags.tags.description'),
		'meta_keywords' => config('metatags.tags.keywords'),
		'og_title' => config('metatags.tags.title'),
		'og_description' => config('metatags.tags.description'),
		'og_image' => asset('images/pages/tags.jpg'),
		'active' => true
	])
@stop

@section('styles')
	<link rel="stylesheet" href="{{ mix('css/common.css') }}">
	<link rel="stylesheet" href="{{ mix('css/organizations.css') }}">
	<link rel="stylesheet" href="{{ mix('css/articles.css') }}">
@endsection

@section('content')

	<div class="breadcrumbs">
		@include('includes.breadcrumbs', ['current' => 'Теги'])
	</div>

	<h3 class="section-title m-t m-lr">Теги</h3>
	<div class="section-description m-lr m-b">Матеріали, об'єднані за певною тематикою</div>
	<div class="m-lr">
		<div class="tags-list">
			@include('tags.tags-list', ['tags' => $tags])
		</div>

		@if(isset($showMore) && $showMore)
			<a class="load-more m-lr show-more"
			   data-current-page="2"
			   data-url="{{ url(Request::path()) }}">{{ $onNextPage }}</a>
		@endif
	</div>
@stop

@section('scripts')
	<script src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@12.0.0/dist/lazyload.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/jquery@3.4.1/dist/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sticky-sidebar@3.3.1/dist/sticky-sidebar.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/resize-sensor@0.0.6/ResizeSensor.min.js"></script>
@stop