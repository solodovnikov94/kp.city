@foreach($tags as $tag)
	<a href="{{ $tag->getUrl() }}" class="tag">
		<span class="tag__image">
			<img data-src="{{ $tag->getCroppedImageOrTransparent('card', 'jpg', 'og_image') }}"
					 class="b-lazy"
					 alt="{{ $tag->title }}">
		</span>
		<span class="tag__title">#{{ $tag->title }}</span>
	</a>
@endforeach