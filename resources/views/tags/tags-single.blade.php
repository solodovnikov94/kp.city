@extends('layouts.layouts-main')

@section('meta')
	@include('includes.meta', [
		'title' => $tag->meta_title,
		'meta_description' => $tag->meta_description,
		'meta_keywords' => $tag->meta_keywords,
		'og_title' => $tag->og_title ?? $tag->title,
		'og_description' => $tag->og_description ?? $tag->meta_description,
		'og_image' => $tag->getCroppedImage('og', 'jpg', 'og_image') ?? asset('images/pages/tags.jpg'),
		'og_image_type' => 'og',
		'active' => true
	])
@stop

@section('styles')
	<link rel="stylesheet" href="{{ mix('css/common.css') }}">
	<link rel="stylesheet" href="{{ mix('css/organizations.css') }}">
	<link rel="stylesheet" href="{{ mix('css/articles.css') }}">
@endsection

@section('content')

	<div class="breadcrumbs">
		@include('includes.breadcrumbs', ['links' => [
			['link' => route('tags'), 'title' => 'Теги'],
		], 'current' => $tag->title])
	</div>

	<h3 class="section-title m-t m-lr">Матеріали з тегом "{{ $tag->title }}"</h3>
	<div class="section-description m-lr m-b">Матеріалів по темі: {{ $tag->total }}</div>

	@if($organizations->isNotEmpty())
		<hr>

		<h3 class="section-title m-full">Заклади міста <span class="section-title__second-text">{{ $organizations->total() }}</span></h3>
		<div class="organizations-list m--b">
			@include('organizations.organizations-list', ['organizations' => $organizations])
		</div>
		@if($showMoreOrganizations)
			<a class="load-more show-more m-t m-lr"
			   data-current-page="2"
			   data-url="{{ url(Request::path()) }}"
			   data-url-param="param=organizations">{{ $onNextPageOrganizations }}</a>
		@endif
	@endif

	@if($news->isNotEmpty())
		<hr class="m-t">

		<h3 class="section-title m-t m-lr">Новини <span class="section-title__second-text">{{ $news->total() }}</span></h3>
		<div class="articles-list m-t m--b">
			@include('articles.articles-list', ['articles' => $news])
		</div>
		@if($showMoreNews)
			<a class="load-more show-more m-t m-lr"
			   data-current-page="2"
			   data-url="{{ url(Request::path()) }}"
			   data-url-param="param=news">{{ $onNextPageNews }}</a>
		@endif
	@endif

	@if($articles->isNotEmpty())
		<hr class="m-t">

		<h3 class="section-title m-t m-lr">Статті <span class="section-title__second-text">{{ $articles->total() }}</span></h3>
		<div class="articles-list m-t m--b">
			@include('articles.articles-list', ['articles' => $articles])
		</div>
		@if($showMoreArticles)
			<a class="load-more show-more m-t m-lr"
			   data-current-page="2"
			   data-url="{{ url(Request::path()) }}"
			   data-url-param="param=articles">{{ $onNextPageArticles }}</a>
		@endif
	@endif

@stop

@section('scripts')
	<script src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@12.0.0/dist/lazyload.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/jquery@3.4.1/dist/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sticky-sidebar@3.3.1/dist/sticky-sidebar.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/resize-sensor@0.0.6/ResizeSensor.min.js"></script>
@stop