@extends('layouts.layouts-main')

@section('meta')
	@include('includes.meta', [
		'title' => config('metatags.homepage.title'),
		'meta_description' => config('metatags.homepage.description'),
		'meta_keywords' => config('metatags.homepage.keywords'),
		'og_title' => config('metatags.homepage.title'),
		'og_description' => config('metatags.homepage.description'),
		'og_image' => asset('images/pages/homepage.jpg'),
		'active' => true
	])
@stop

@section('schema')
	@include('includes.schema.homepage-schema')
@stop

@section('styles')
	<link rel="stylesheet" href="{{ mix('css/common.css') }}">
	<link rel="stylesheet" href="{{ mix('css/articles.css') }}">
	<link rel="stylesheet" href="{{ mix('css/polls.css') }}">
	<link rel="stylesheet" href="{{ mix('css/organizations.css') }}">
@endsection

@section('content')

	<h3 class="section-title m-t m-lr">Цікаві статті</h3>
	<div class="articles-list m-t">
		<div class="article">
			<div class="loaded-loader"></div>
			<div class="article__image">
				<img src="https://sandbox-uploads.imgix.net/u/1558867462-5d100b5b0abfa18ebb9e6567be8a3889?w=64&fit=crop&h=36&blur=20"
						 class="b-lazy-default"
						 alt="#">
				<img class="b-lazy"
						 data-src="https://sandbox-uploads.imgix.net/u/1558867462-5d100b5b0abfa18ebb9e6567be8a3889?w=420&fit=crop&h=260"
						 alt="#">
			</div>

			<div class="article__buttons">
				<a href="#" class="btn btn-sm btn-bordered btn-news-ecology active">Екологія і медицина</a>
			</div>

			<a href="#"
				 class="article__content">
				<div class="article__footer">
					<div class="article__title">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus, voluptate!</div>
					<div class="article__information">
						<time class="article__datetime">Вчора о 13:44</time>
						<div class="article__views-counter">
							<i class="fal fa-eye"></i>
							<span>145</span>
						</div>
					</div>
				</div>
			</a>
		</div>
		<div class="article">
			<div class="loaded-loader"></div>
			<div class="article__image">
				<img src="https://sandbox-uploads.imgix.net/u/1558885098-831d147eefb1042b12108c7219b4c3ec?w=64&fit=crop&h=36&blur=20"
						 class="b-lazy-default"
						 alt="#">
				<img class="b-lazy"
						 data-src="https://sandbox-uploads.imgix.net/u/1558885098-831d147eefb1042b12108c7219b4c3ec?w=420&fit=crop&h=260"
						 alt="#">
			</div>

			<div class="article__buttons">
				<a href="#" class="btn btn-sm btn-bordered btn-news-ecology active">Екологія і медицина</a>
			</div>

			<a href="#"
				 class="article__content">
				<div class="article__footer">
					<div class="article__title">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus, voluptate!</div>
					<div class="article__information">
						<time class="article__datetime">Вчора о 13:44</time>
						<div class="article__views-counter">
							<i class="fal fa-eye"></i>
							<span>145</span>
						</div>
					</div>
				</div>
			</a>
		</div>
		<div class="article">
			<div class="loaded-loader"></div>
			<div class="article__image">
				<img src="https://sandbox-uploads.imgix.net/u/1558885269-417228853da86ddea6be3a89427c132f?w=64&fit=crop&h=36&blur=20"
						 class="b-lazy-default"
						 alt="#">
				<img class="b-lazy"
						 data-src="https://sandbox-uploads.imgix.net/u/1558885269-417228853da86ddea6be3a89427c132f?w=420&fit=crop&h=260"
						 alt="#">
			</div>

			<div class="article__buttons">
				<a href="#" class="btn btn-sm btn-bordered btn-news-ecology active">Екологія і медицина</a>
			</div>

			<a href="#"
				 class="article__content">
				<div class="article__footer">
					<div class="article__title">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus, voluptate!</div>
					<div class="article__information">
						<time class="article__datetime">Вчора о 13:44</time>
						<div class="article__views-counter">
							<i class="fal fa-eye"></i>
							<span>145</span>
						</div>
					</div>
				</div>
			</a>
		</div>
	</div>

	{{--@php $url = 'https://sandbox-uploads.imgix.net/u/1558867462-5d100b5b0abfa18ebb9e6567be8a3889' @endphp--}}
	{{--@php $url = 'https://sandbox-uploads.imgix.net/u/1558885098-831d147eefb1042b12108c7219b4c3ec' @endphp--}}
{{--	@php $url = 'https://sandbox-uploads.imgix.net/u/1558885269-417228853da86ddea6be3a89427c132f' @endphp--}}
	@php $url = 'https://sandbox-uploads.imgix.net/u/1558897152-0ce0763114bba6f41458de589b487731' @endphp

	<figure class="media-content__image">
		<div class="media-content__image-container">
			<img src="{{ $url }}?w=64&fit=crop&h=36&blur=25" alt="#">
			<picture>
				<source media="(min-width: 1200px)" data-srcset="{{ $url }}?w=870&fit=crop&h=490&auto=compress,format 1x, {{ $url }}?w=870&fit=crop&h=490&auto=compress,format&q=25&dpr=2 2x">
				<source media="(min-width: 992px) and (max-width: 1199px)" data-srcset="{{ $url }}?w=710&fit=crop&h=400&auto=compress,format 1x, {{ $url }}?w=710&fit=crop&h=400&auto=compress,format&q=25&dpr=2 2x">
				<source media="(min-width: 768px) and (max-width: 991px)" data-srcset="{{ $url }}?w=530&fit=crop&h=300&auto=compress,format 1x, {{ $url }}?w=530&fit=crop&h=300&auto=compress,format&q=25&dpr=2 2x">
				<source media="(min-width: 480px) and (max-width: 767px)" data-srcset="{{ $url }}?w=710&fit=crop&h=400&auto=compress,format 1x, {{ $url }}?w=710&fit=crop&h=400&auto=compress,format&q=25&dpr=2 2x">
				<img class="b-lazy" data-src="{{ $url }}?w=450&fit=crop&h=253&auto=compress,format 1x, {{ $url }}?w=450&fit=crop&h=253&auto=compress,format&q=25&dpr=2 2x" alt="#">
			</picture>
		</div>
	</figure>

	<hr class="m-t">

@stop

@section('scripts')
	<script src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@12.0.0/dist/lazyload.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/jquery@3.4.1/dist/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sticky-sidebar@3.3.1/dist/sticky-sidebar.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/resize-sensor@0.0.6/ResizeSensor.min.js"></script>
@stop