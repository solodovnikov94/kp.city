<tr>
	<td>
		<table class="footer" align="center" width="570" cellpadding="0" cellspacing="0">
			<tr>
				<td align="center">
					<hr>

					<table class="w-100p">
						<tr>
							<td class="footer-menu">
								<a href="{{ url('/about') }}">Про проект</a>
								<a href="{{ url('/contacts') }}">Контакти</a>
								<a href="{{ url('/advertising') }}">Реклама на сайті</a>
								<a href="{{ url('/rules') }}">Правила сайту</a>
								<a href="{{ url('/privacy') }}">Конфіденційність</a>
							</td>
						</tr>
					</table>

					<table class="w-100p">
						<tr>
							<td class="footer-copy">
								&copy; KP.CITY - портал Кам'янця-Подільського!
							</td>
						</tr>
					</table>

					{{--<div class="footer-copy-socials">--}}
						{{--<span class="footer-copy">&copy; KP.CITY - портал Кам'янця-Подільського!</span>--}}
						{{--<div class="footer-socials">--}}
							{{--<a href="{{ config('app.kp_socials.facebook.link') }}">--}}
								{{--<img src="{{ asset('images/emails/fb-dark.png') }}"--}}
										 {{--alt="{{ config('app.kp_socials.facebook.alt_text') }}">--}}
							{{--</a>--}}
							{{--<a href="{{ config('app.kp_socials.twitter.link') }}">--}}
								{{--<img src="{{ asset('images/emails/tw-dark.png') }}"--}}
										 {{--alt="{{ config('app.kp_socials.twitter.alt_text') }}">--}}
							{{--</a>--}}
							{{--<a href="{{ config('app.kp_socials.vkontakte.link') }}">--}}
								{{--<img src="{{ asset('images/emails/vk-dark.png') }}"--}}
										 {{--alt="{{ config('app.kp_socials.vkontakte.alt_text') }}">--}}
							{{--</a>--}}
							{{--<a href="{{ config('app.kp_socials.instagram.link') }}">--}}
								{{--<img src="{{ asset('images/emails/ins-dark.png') }}"--}}
										 {{--alt="{{ config('app.kp_socials.instagram.alt_text') }}">--}}
							{{--</a>--}}
							{{--<a href="{{ config('app.kp_socials.telegram.link') }}">--}}
								{{--<img src="{{ asset('images/emails/tg-dark.png') }}"--}}
										 {{--alt="{{ config('app.kp_socials.telegram.alt_text') }}">--}}
							{{--</a>--}}
						{{--</div>--}}
					{{--</div>--}}
				</td>
			</tr>
		</table>
	</td>
</tr>
