<tr>
	<td class="body" width="100%" cellpadding="0" cellspacing="0">
		<table class="header-wrapper" align="center" width="570" cellpadding="0" cellspacing="0">
			<tr>
				<td>
					<table class="header">
						<tr>
							<td>
								<a href="{{ url('/') }}">
									<img src="{{ asset('images/emails/logo.png') }}" alt="KP.CITY">
								</a>
							</td>
							<td class="header-socials">
								<a href="{{ config('app.kp_socials.facebook.link') }}">
									<img src="{{ asset('images/emails/fb.png') }}"
											 alt="{{ config('app.kp_socials.facebook.alt_text') }}">
								</a>
								<a href="{{ config('app.kp_socials.twitter.link') }}">
									<img src="{{ asset('images/emails/tw.png') }}"
											 alt="{{ config('app.kp_socials.twitter.alt_text') }}">
								</a>
								<a href="{{ config('app.kp_socials.vkontakte.link') }}">
									<img src="{{ asset('images/emails/vk.png') }}"
											 alt="{{ config('app.kp_socials.vkontakte.alt_text') }}">
								</a>
								<a href="{{ config('app.kp_socials.instagram.link') }}">
									<img src="{{ asset('images/emails/ins.png') }}"
											 alt="{{ config('app.kp_socials.instagram.alt_text') }}">
								</a>
								<a href="{{ config('app.kp_socials.telegram.link') }}">
									<img src="{{ asset('images/emails/tg.png') }}"
											 alt="{{ config('app.kp_socials.telegram.alt_text') }}">
								</a>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</td>
</tr>