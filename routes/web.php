<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/login/{driver}', 'Auth\LoginController@redirectToProvider')->name('login.social');
Route::get('/login/{driver}/callback', 'Auth\LoginController@handleProviderCallback')->name('login.social.callback');
Route::get('/account-confirm', 'Auth\LoginController@accountConfirm')->name('account.confirm');
Route::post('/account-confirm', 'Auth\LoginController@accountConfirmSuccess')->name('account.confirm.success');
Route::post('/account-do-not-confirm', 'Auth\LoginController@accountConfirmDanger')->name('account.confirm.danger');
Route::get('/verify-email/{email}/{verify_token}', 'Auth\RegisterController@verifyEmail')->name('verify.email');

Route::get('/', 'PagesController@index')->name('home');

Route::get('/about', 'PagesController@about');
Route::get('/contacts', 'PagesController@contacts');
Route::get('/advertising', 'PagesController@advertising');
Route::get('/rules', 'PagesController@rules');
Route::get('/privacy', 'PagesController@privacy');
Route::get('/search', 'SearchController@index')->name('search');

Route::get('/news/{category_slug?}', 'ArticlesController@showNewsCategoryPage')
	->where(['category_slug' => '[a-z-]+'])
	->name('news');
Route::get('/news/{category_slug}/{id}-{article_slug}', 'ArticlesController@showNewsPage')
	->where(['category_slug' => '[a-z-]+', 'id' => '[0-9]+'])
	->name('news.single');

Route::get('/articles/{category_slug?}', 'ArticlesController@showArticleCategoryPage')
	->where(['category_slug' => '[a-z-]+'])
	->name('articles');
Route::get('/articles/{category_slug}/{id}-{article_slug}', 'ArticlesController@showArticlePage')
	->where(['category_slug' => '[a-z-]+', 'id' => '[0-9]+'])
	->name('articles.single');

Route::get('/organizations', 'OrganizationsController@showOrganizationCategoriesPage')->name('organizations.categories');
Route::get('/organizations/{category_slug}', 'OrganizationsController@showOrganizationCategoryPage')
	->where(['category_slug' => '[a-z-]+'])
	->name('organizations.category');
Route::get('/organizations/{category_slug}/{subcategory_slug}', 'OrganizationsController@showOrganizationSubcategoryPage')
	->where(['category_slug' => '[a-z-]+', 'subcategory_slug' => '[a-z-]+'])
	->name('organizations.subcategory');
Route::get('/organizations/{category_slug}/{subcategory_slug}/{organization_id}-{organization_slug}', 'OrganizationsController@showOrganizationPage')
	->where(['category_slug' => '[a-z-]+', 'subcategory_slug' => '[a-z-]+', 'id' => '[0-9]+'])
	->name('organizations.single');
Route::post('/organization/like', 'OrganizationsController@likeOrganization')->name('organization.like');
Route::post('/organization-changes', 'OrganizationsController@addChanges');

Route::get('/tags', 'TagsController@tags')->name('tags');
Route::get('/tags/{tag_slug}', 'TagsController@tag')
	->where(['tag_slug' => '[0-9a-z-]+'])
	->name('tag.single');

Route::post('/poll/{slug}', 'PollsController@pollVote')->name('poll');
Route::get('/polls', 'PollsController@showPollsPage');
Route::get('/polls/{id}-{poll_slug}', 'PollsController@showPollPage')
	->where(['id' => '[0-9]+']);

Route::group(['prefix' => 'comment'], function () {
	Route::post('/add', 'CommentsController@addComment')->name('comment.add');
	Route::post('/remove', 'CommentsController@removeComment')->name('comment.remove');
	Route::post('/like', 'CommentsController@likeComment')->name('comment.like');
});

Route::get('/cdn/{project}-{key}.css', 'CdnController@returnStyle')
  ->where(['project' => '[a-z]+', 'key' => '[0-9a-z]+']);

Route::group(['prefix' => 'banners'], function () {
	Route::post('/close-banner', 'BannersController@closeBanner');
	Route::post('/click-counter', 'BannersController@clickCounter');
});

Route::group(['prefix' => 'adverts'], function () {
	Route::get('/', function () {
		return view('adverts.adverts-categories');
	})->name('adverts.category');
	Route::get('/adverts/{category_slug}/{id}-{article_slug}', 'AdvertsController@showNewsPage')
		->where(['category_slug' => '[a-z-]+', 'id' => '[0-9]+'])
		->name('advert.single');
});

Route::post('contacts', 'PagesController@saveContacts');

Route::group(['prefix' => 'profile'], function () {
	Route::get('/', 'ProfileController@profile')->name('profile');
//	Route::get('/notifications', 'ProfileController@notifications')->name('profile.notifications');
//	Route::get('/profile-edit', 'ProfileController@profileEdit')->name('profile.edit');
//	Route::get('/profile-update', 'ProfileController@profileUpdate')->name('profile.update');
//	Route::get('/change-password-edit', 'ProfileController@changePasswordEdit')->name('profile.change-password-edit');
//	Route::get('/change-password-update', 'ProfileController@changePasswordUpdate')->name('profile.change-password-update');
//	Route::get('/comments', 'ProfileController@comments')->name('profile.comments');
//	Route::get('/adverts', 'ProfileController@adverts')->name('profile.adverts');
//	Route::get('/create-advert', 'ProfileController@createAdvert')->name('profile.create-advert');
//	Route::get('/edit-advert', 'ProfileController@editAdvert')->name('profile.edit-advert');
//	Route::get('/update-advert', 'ProfileController@updateAdvert')->name('profile.update-advert');
//	Route::get('/bookmarks', 'ProfileController@bookmarks')->name('profile.bookmarks');
//	Route::get('/hz-button', 'ProfileController@addNewRecords')->name('profile.');
	Route::get('/organization-changes-list', 'ProfileController@organizationChangesIndex');
	Route::get('/organization-edit/{id}', 'ProfileController@organizationChangesShow');
	Route::post('/organization-save', 'ProfileController@organizationChangesStore');
	Route::patch('/organization-save/{id}', 'ProfileController@organizationChangesUpdate');
	Route::delete('/organization-delete/{id}', 'ProfileController@organizationChangesDestroy');
	Route::get('/organization-list', 'ProfileController@organizationList');
});

//Route::get('test-facebook', 'ApiController@sendLinkToFacebook');

Route::get('sitemap-create', function(){
	Artisan::call('sitemap:create');
	return '{success:created}';
});

//Route::get('twitter-post', function (){
//	Artisan::call('twitter:post');
//});

Route::get('cache-clear', function(){
	Artisan::call('cache:clear');
});

//TODO хай поки буде :)
//Route::get('process-images', function () {
//
//	$organizations = \App\Organization::where('id', '>', 59)->get();
//
//	foreach ($organizations as $organization) {
//		$image = Image::make($organization->getCroppedImage('max'));
//		$image->fit(66, 66);
////		return $image->response('jpg', 95);
//
//		$image->encode('jpg', 95);
//
//		Storage::disk('public')
//			->put('organizations/'.$organization->image.'_brief.jpg', (string)$image);
//	}
//});

//Route::get('/test-imgix', function () {
//	return view('test-imgix');
//});

Route::get('/adverts/single', function () {
	return view('adverts.adverts-single');
});

Route::get('/adverts/form', function () {
	return view('adverts.adverts-form');
});
