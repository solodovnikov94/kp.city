<?php

namespace Tests\Unit;

use App\Article;
use App\ArticleCategory;
use App\News;
use App\NewsCategory;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ArticleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->get('/articles');
        $response->assertStatus(200);

        $newsCategories = NewsCategory::active()->get();
        foreach ($newsCategories as $category) {
            $response = $this->get($category->getUrl());
            $response->assertStatus(200);
        }

        $news = News::get();
        foreach ($news as $item) {
            $response = $this->get($item->getUrl());
            $response->assertStatus(200);
        }

        $response = $this->get('/news');
        $response->assertStatus(200);

        $articleCategories = ArticleCategory::active()->get();
        foreach ($articleCategories as $category) {
            $response = $this->get($category->getUrl());
            $response->assertStatus(200);
        }

        $articles = Article::get();
        foreach ($articles as $item) {
            $response = $this->get($item->getUrl());
            $response->assertStatus(200);
        }

    }
}
