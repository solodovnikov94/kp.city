<?php

namespace Tests\Unit;

use App\OrganizationCategory;
use App\Organization;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class OrganizationTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->get('/organizations');
        $response->assertStatus(200);

        $parentCategories = OrganizationCategory::where([
            ['active', '=', 1],
            ['parent_id', '=', 0]
        ])->get();

        foreach ($parentCategories as $parentCategory) {
            $response = $this->get($parentCategory->getUrl());
            $response->assertStatus(200);
        }

        $subcategoryOrganizations = OrganizationCategory::where([
            ['active', '=', 1],
            ['parent_id', '!=', 0]
        ])->get();

        foreach ($subcategoryOrganizations as $subcategoryOrganization) {
            $response = $this->get($subcategoryOrganization->getUrl());
            $response->assertStatus(200);
        }

        $organizations = Organization::get();

        foreach ($organizations as $organization) {
            $response = $this->get($organization->getUrl());
            $response->assertStatus(200);
        }

    }
}
