<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class StaticPageTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->get('/advertising');
        $response->assertStatus(200);

        $response = $this->get('/about');
        $response->assertStatus(200);

        $response = $this->get('/');
        $response->assertStatus(200);

        $response = $this->get('/contacts');
        $response->assertStatus(200);

        $response = $this->get('/rules');
        $response->assertStatus(200);

        $response = $this->get('/documentation');
        $response->assertStatus(200);

        $response = $this->get('/privacy');
        $response->assertStatus(200);

        $response = $this->get('/search');
        $response->assertStatus(200);
    }
}
