<?php

namespace Tests\Unit;

use App\Tag;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TagTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->get('/tags');
        $response->assertStatus(200);

        $tags = Tag::active()->get();
        foreach ($tags as $tag) {
            $response = $this->get($tag->getUrl());
            $response->assertStatus(200);
        }

    }
}
