const SWPrecacheWebpackPlugin = require('sw-precache-webpack-plugin');
const mix = require('laravel-mix');

//Progressive Web Application
mix.webpackConfig({
	plugins: [
		new SWPrecacheWebpackPlugin({
			cacheId: 'pwa',
			filename: 'service-worker.js',
			staticFileGlobs: [
				'public/css/**/*.css',
				'public/fonts/**/*.{woff,woff2}',
				'public/images/icons/header/*.{png}',
				'public/images/logotypes/logo.svg',
				'public/images/pwa/favicon.ico',
				'public/images/error.svg',
				'public/images/transparent-pixel.png',
				'public/js/*.{js}',
				'public/libs/fa5/fontawesome-optimized.min.css',
				// 'public/**/*.{css,eot,svg,ttf,woff,woff2,js,html}'
			],
			minify: true,
			stripPrefix: 'public/',
			handleFetch: true,
			dynamicUrlToDependencies: {
				// '/about': ['resources/views/pages/about.blade.php'],
				// '/rules': ['resources/views/pages/rules.blade.php']
			},
			staticFileGlobsIgnorePatterns: [/\.map$/, /mix-manifest\.json$/, /manifest\.json$/, /service-worker\.js$/],
			navigateFallback: '/',
			runtimeCaching: [
				{
					urlPattern: /^https:\/\/cdn.jsdelivr.net\//,
					handler: 'cacheFirst'
				},
				{
					urlPattern: /^https:\/\/www\.google-analytics\.com\/analytics\.js/,
					handler: 'cacheFirst'
				},
				{
					urlPattern: /^https:\/\/www\.gstatic\.com\//,
					handler: 'cacheFirst'
				},
				{
					urlPattern: /^https:\/\/www\.gravatar\.com\//,
					handler: 'cacheFirst',
					options: {
						cache: {
							maxEntries: 25,
							name: 'gravatars-cache'
						}
					}
				},
				{
					handler: "cacheFirst",
					urlPattern: /[.](png|jpg|jpeg|webp)/,
					options: {
						cache: {
							maxEntries: 250,
							name: 'images-cache'
						}
					}
				},
				{
					urlPattern: /\/$/,
					handler: 'networkFirst'
				},
				{
					urlPattern: /\/news/,
					handler: 'networkFirst'
				},
				{
					urlPattern: /\/articles/,
					handler: 'networkFirst'
				},
				{
					urlPattern: /\/organizations/,
					handler: 'networkFirst'
				},
				{
					urlPattern: /\/polls/,
					handler: 'networkFirst'
				},
				{
					urlPattern: /\/about/,
					handler: 'networkFirst'
				},
				{
					urlPattern: /\/rules/,
					handler: 'networkFirst'
				},
				{
					urlPattern: /\/documentation/,
					handler: 'networkFirst'
				},
				{
					urlPattern: /\/privacy/,
					handler: 'networkFirst'
				},
				{
					urlPattern: /\/advertising/,
					handler: 'networkFirst'
				},
				{
					urlPattern: /\/contacts/,
					handler: 'networkFirst'
				}
			]
			// importScripts: ['./js/push_message.js']
		})
	]
});

mix
	.js('resources/assets/js/common.js', 'public/js')
	.sass('resources/assets/sass/articles.scss', 'public/css')
	.sass('resources/assets/sass/polls.scss', 'public/css')
	.sass('resources/assets/sass/comments.scss', 'public/css')
	.sass('resources/assets/sass/common.scss', 'public/css')
	.sass('resources/assets/sass/head.scss', 'public/css/components')
	.sass('resources/assets/sass/organizations.scss', 'public/css')
	.sass('resources/assets/sass/adverts.scss', 'public/css/adwerts.css')
	.sass('resources/assets/sass/static.scss', 'public/css')
	.browserSync('kp.city.loc')
	.disableNotifications()
	.options({
		processCssUrls: false
	});

if (mix.inProduction()) {
	mix
	// .version()
		.options({
			autoprefixer: {
				options: {
					browsers: [
						'last 5 versions'
					]
				}
			}
		})
		.sourceMaps();
}